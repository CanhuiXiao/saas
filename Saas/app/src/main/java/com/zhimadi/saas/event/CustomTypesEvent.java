package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class CustomTypesEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<CustomType> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<CustomType> getList() {
            return list;
        }

        public void setList(List<CustomType> list) {
            this.list = list;
        }
    }

    public class CustomType
    {
        private String type_id;
        private String name;

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
