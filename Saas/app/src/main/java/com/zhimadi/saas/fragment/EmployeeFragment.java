package com.zhimadi.saas.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.EmployeeEditActivity;
import com.zhimadi.saas.adapter.EmployeeAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.EmployeesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/8.
 */

public class EmployeeFragment extends BaseFragment {

    private View returnView;
    private List<EmployeesEvent.Employee> employees;
    private SwipeMenuListView lvEmployeeHome;
    private EmployeeAdapter employeeAdapter;

    private int mStart = 0;
    private int mLimit = 10;

    private void inte(){
        employees = new ArrayList<EmployeesEvent.Employee>();
        lvEmployeeHome = (SwipeMenuListView) returnView.findViewById(R.id.lv_employee_home);
        employeeAdapter = new EmployeeAdapter(activity, R.layout.item_lv_employee_home, employees);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_employee, null);
        inte();
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //打电话
                SwipeMenuItem callItem = new SwipeMenuItem(
                        activity);
                callItem.setBackground(new ColorDrawable(Color.rgb(0x6C, 0xE0,
                        0xC3)));
                callItem.setWidth(90);
                callItem.setIcon(R.drawable.shou_ji);
                menu.addMenuItem(callItem);

                //发短信
                SwipeMenuItem smsItem = new SwipeMenuItem(
                        activity);
                smsItem.setBackground(new ColorDrawable(Color.rgb(0x8D, 0xEA,
                        0xD3)));
                smsItem.setWidth(90);
                smsItem.setIcon(R.drawable.duan_xin);
                menu.addMenuItem(smsItem);

                //删除
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("解除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };
        lvEmployeeHome.setMenuCreator(creator);
        lvEmployeeHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent =new Intent();
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("EMPLOYEE_ID", employees.get(position).getUser_id());
                intent.setClass(activity, EmployeeEditActivity.class);
                startActivity(intent);
            }
        });

        lvEmployeeHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            GetEmployees();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        lvEmployeeHome.setAdapter(employeeAdapter);
        GetEmployees();
        return  returnView;
    }

    public void refresh(){
        mStart = 0;
        GetEmployees();
    }


    private void GetEmployees(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mUserAddress, map);
        AsyncUtil.asyncGet(activity, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                EmployeesEvent employeesEvent = gson.fromJson(response, EmployeesEvent.class);
                if(mStart == 0){
                    employees.clear();
                }
                mStart = mStart + employeesEvent.getData().getList().size();
                employees.addAll(employeesEvent.getData().getList());
                employeeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }



}