package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class EditTextWithUnitItem extends LinearLayout {

	private View returnView;

	private TextView tvItemName;
	private EditText etItemValue;
	private TextView tvItemUnit;

	private View viewDivider;
	private View viewDividerFirst;
	private View viewDividerLast;

	public EditTextWithUnitItem(Context context, AttributeSet attrs, String itemName, String hint, String unit) {
		super(context, attrs);
		returnView = LayoutInflater.from(context).inflate(R.layout.widget_edittext_with_unit, this);
		tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
		etItemValue = (EditText) returnView.findViewById(R.id.et_item_value);
		tvItemUnit = (TextView) returnView.findViewById(R.id.tv_item_unit);
		viewDivider = returnView.findViewById(R.id.view_divider);
		viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
		viewDividerLast = returnView.findViewById(R.id.view_divider_last);



		tvItemName.setText(itemName);
		etItemValue.setHint(hint);
		tvItemUnit.setText(unit);
	}

	public EditTextWithUnitItem isTop(boolean isTop){
		if(isTop){
			viewDividerFirst.setVisibility(View.VISIBLE);
		}else {
			viewDividerFirst.setVisibility(View.GONE);
		}
		return this;
	}

	public EditTextWithUnitItem isBottom(boolean isBottom){
		if(isBottom){
			viewDividerLast.setVisibility(View.VISIBLE);
			viewDivider.setVisibility(View.GONE);
		}else {
			viewDividerLast.setVisibility(View.GONE);
			viewDivider.setVisibility(View.VISIBLE);
		}
		return this;
	}


	public void SetItemValue(String value){
		etItemValue.setText(value);
	}
	public String GetItemValue(){
		return etItemValue.getText().toString();
	}


	public EditTextWithUnitItem SetItemName(String name){
		tvItemName.setText(name);
		return this;
	}

	public EditTextWithUnitItem SetHint(String hint){
		etItemValue.setHint(hint);
		return this;
	}

	public EditTextWithUnitItem SetUnit(String unit){
		tvItemUnit.setText(unit);
		return this;
	}


	public void setEditEnable(boolean enable){
		etItemValue.setEnabled(enable);
		if(enable){
			returnView.setBackgroundResource(R.color.colorWhite);
		}else {
			returnView.setBackgroundResource(R.color.colorCross);
			etItemValue.setText("0");
		}

	}

}
