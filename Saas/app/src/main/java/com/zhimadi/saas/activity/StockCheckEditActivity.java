package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.ProductData;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.BillBuy;
import com.zhimadi.saas.widget.Cat;
import com.zhimadi.saas.widget.CatDetail;
import com.zhimadi.saas.widget.CatItem;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.TextViewItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockCheckEditActivity extends BaseActivity {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    
    private static final int BUY = 0;
    private static final int NOTE = 3;

    //是否可编辑
    private boolean isEditEnable;

    //传入的数据
    private int intentMode;
    private String stockCheckId;
    private int mState;

    private LinearLayout llStockCheckEditWindow;

    private TextViewItem textViewItemOderNummber;
    private TextViewItem textViewItemMaker;


    private SelelctableTextViewItem selelctableTextViewItemStore;

    //购物车
    private Cat cat;
    //明细表
    private CatDetail catDetail;
    //消费提示栏
    private BillBuy billBuy;

    private EditTextITem editTextITemNote;

    //底部操作栏
    private LinearLayout llStockCheckEditTab;
    private TextView tvStockCheckEditFirst;
    private TextView tvStockCheckEditSecond;
    private TextView tvStockCheckSum;

    //隐藏post数据
    private String storeId;
    private String supplierId;

    //缓存商品列表
    private List<CatItem> catItems;

    //存商品的总重量，总件数
    Map<String, Float> mapNumber;
    Map<String, Float> mapWeight;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", Constant.DEFAULT_MODE);
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llStockCheckEditWindow = (LinearLayout) findViewById(R.id.ll_stock_check_edit_window);

        textViewItemOderNummber = new TextViewItem(mContext, null, "单号");
        selelctableTextViewItemStore = new SelelctableTextViewItem(mContext, null, "仓库", "请选择仓库");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        tvStockCheckEditFirst = (TextView) findViewById(R.id.tv_stock_check_edit_first);
        tvStockCheckEditSecond = (TextView) findViewById(R.id.tv_stock_check_edit_second);
        tvStockCheckSum = (TextView) findViewById(R.id.tv_stock_check_edit_sum);
        llStockCheckEditTab = (LinearLayout) findViewById(R.id.ll_stock_check_edit_tab);

        //第一步判断可不可以编辑
        if (intentMode == Constant.EDIT_MODE) {
            /***
             * 编辑状态下只有草稿能编辑，还要特殊对待，蛋疼
             */
            if (mState == Constant.STATE_NOTE) {
                setEditEnable(true);
                isEditEnable = true;
            } else {
                setEditEnable(false);
                isEditEnable = false;
            }
        } else {
            setEditEnable(true);
            isEditEnable = true;
        }

        //第二步选择加载的内容
        if (intentMode == Constant.EDIT_MODE) {
            mState = Integer.valueOf(getIntent().getStringExtra("STATE"));
            stockCheckId = getIntent().getStringExtra("CHECK_ID");
            if (mState == Constant.STATE_NOTE) {
                cat = new Cat(mContext, null);
                billBuy = new BillBuy(mContext, null, true);
                catItems = new ArrayList<CatItem>();
                mapNumber = new HashMap<String, Float>();
                mapWeight = new HashMap<String, Float>();
            } else {
                catDetail = new CatDetail(mContext, null);
                billBuy = new BillBuy(mContext, null, false);
            }
            textViewItemMaker = new TextViewItem(mContext, null, "制单人");
        } else {
            cat = new Cat(mContext, null);
            billBuy = new BillBuy(mContext, null, true);
            catItems = new ArrayList<CatItem>();
            mapNumber = new HashMap<String, Float>();
            mapWeight = new HashMap<String, Float>();
            textViewItemMaker = new TextViewItem(mContext, null, "制单人", UserInfo.getmUserName());
        }

        //第三步根据订单状态修改底部操作栏
        if (intentMode == Constant.EDIT_MODE) {
            switch (mState) {
                case Constant.DEFAULT_MODE:
                    llStockCheckEditTab.setVisibility(View.VISIBLE);
                    tvStockCheckEditFirst.setBackgroundResource(R.color.colorOrange);
                    tvStockCheckEditFirst.setText("");
                    tvStockCheckEditSecond.setBackgroundResource(R.color.colorBlue);
                    tvStockCheckEditSecond.setText("打印");
                    tvStockCheckEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvStockCheckEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_NOTE:
                    tvStockCheckEditFirst.setBackgroundResource(R.color.colorWhite);
                    tvStockCheckEditFirst.setText("存为草稿");
                    tvStockCheckEditSecond.setBackgroundResource(R.color.colorSecondBody);
                    tvStockCheckEditSecond.setText("盘点");
                    tvStockCheckEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvStockCheckEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_CANCEL:
                    break;
                default:
                    finish();
                    break;
            }
        } else {
            llStockCheckEditTab.setVisibility(View.VISIBLE);
            tvStockCheckEditFirst.setBackgroundResource(R.color.colorAquamarine);
            tvStockCheckEditFirst.setText("存为草稿");
            tvStockCheckEditFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stockCheckPost();
                }
            });
            tvStockCheckEditSecond.setBackgroundResource(R.color.colorSaffronYellow);
            tvStockCheckEditSecond.setText("盘点");
            tvStockCheckEditSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    stockCheckPost();
                }
            });
        }


    }

    //设置不可按，把输入提示语去掉，箭头弄掉
    private void setEditEnable(boolean enable) {
        selelctableTextViewItemStore.setEnabled(enable);
        editTextITemNote.setEditEnable(enable);
        if (!enable) {
            editTextITemNote.SetHint("");
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_check_edit);
        inte();
        titleBarCommonBuilder.setTitle("盘点单").setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });



        selelctableTextViewItemStore
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, StoreSelectActivity.class);
                        startActivityForResult(intent, Constant.REQUEST_CODE_FOR_STORE);
                    }
                });


        textViewItemOderNummber.isTop(true);
        llStockCheckEditWindow.addView(textViewItemOderNummber);
        textViewItemMaker.isBottom(true);
        llStockCheckEditWindow.addView(textViewItemMaker);



        selelctableTextViewItemStore.isBottom(true).isBottom(true);
        llStockCheckEditWindow.addView(selelctableTextViewItemStore);

        //通过是否为空，判断加载购物车还是明细
        if (cat != null) {
            llStockCheckEditWindow.addView(cat);
            //添加监听
            cat.setOnClickListenerAdd(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, ProductSelectActivity.class);
                    List<String> productIds = new ArrayList<String>();
                    if (catItems.size() > 0) {
                        for (int i = 0; i < catItems.size(); i++) {
                            productIds.add(catItems.get(i).getProductId());
                        }
                        Bundle mBundle = new Bundle();
                        intent.putExtra("PRODUCT_IDS", (Serializable) productIds);
                        intent.putExtras(mBundle);
                    }
                    startActivityForResult(intent, Constant.REQUEST_CODE_FOR_PRODUCT);
                }
            });
            //编辑监听
            cat.setOnClickListenerEdit(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            billBuy.isTop(true);
            llStockCheckEditWindow.addView(billBuy);
        } else if (catDetail != null) {
            llStockCheckEditWindow.addView(catDetail);
            billBuy.isBottom(true);
            llStockCheckEditWindow.addView(billBuy);
        }




        editTextITemNote.isTop(true).isBottom(true);
        llStockCheckEditWindow.addView(editTextITemNote);

        //最后才加载
        if (intentMode == Constant.EDIT_MODE) {
            getStockCheckDetail();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_FOR_STORE:
                if (resultCode == SupplierSelectActivity.RESULT_CODE_SUCCESS) {
                    storeId = data.getStringExtra("STORE_ID");
                    selelctableTextViewItemStore.SetItemValue(data.getStringExtra("STORE_NAME"));
                }
                break;

            case Constant.REQUEST_CODE_FOR_PRODUCT:
                if (resultCode == ProductSelectActivity.RESULT_SUCCESS) {
                    //传过来的玩意
                    List<ProductData> productDatas = (List<ProductData>) data.getExtras().getSerializable("PRODUCT_ARRAY");
                    List<String> productIds = (List<String>) data.getExtras().getSerializable("PRODUCTIDS_ARRY");

                    Toast.makeText(StockCheckEditActivity.this, "返回" + productIds.size() + "个id", Toast.LENGTH_SHORT).show();
                    Toast.makeText(StockCheckEditActivity.this, "返回" + productDatas.size() + "对象", Toast.LENGTH_SHORT).show();
                    //缓存要删掉的东西
                    List<ProductData> productDatasTemp = new ArrayList<ProductData>();
                    List<CatItem> catItemsTemp = new ArrayList<CatItem>();
                    List<String> productIdsTemp = new ArrayList<String>();

                    //第一步：返回的数据预处理，把购物车出现过的剔除掉
                    for (int i = 0; i < productDatas.size(); i++) {
                        for (int k = 0; k < catItems.size(); k++) {
                            if (productDatas.get(i).getProductId().equals(catItems.get(k).getProductId())) {
                                productDatasTemp.add(productDatas.get(i));
                            }
                        }
                    }


                    //第二步：从购物车中删掉返回没有的数据,不过如果先前已经选中的因为没点击或者没加载并不会返回，这里需要分辨没有返回的到底有没有选中，好绕 = =
                    //所以要一个id数组来分辨
                    boolean isExist;
                    for (int i = 0; i < catItems.size(); i++) {
                        isExist = false;
                        for (int k = 0; k < productIds.size(); k++) {
                            if (productIds.get(k).equals(catItems.get(i).getProductId())) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            productIdsTemp.add(catItems.get(i).getProductId());
                            catItemsTemp.add(catItems.get(i));
                        }
                    }


                    productDatas.removeAll(productDatasTemp);

                    catItems.removeAll(catItemsTemp);
                    cat.removeAllViews(productIdsTemp, billBuy);


                    //这里留下来的是不重复的
                    for (int i = 0; i < productDatas.size(); i++) {
                        final ProductData productData = productDatas.get(i);
                        CatItem catItem = new CatItem(mContext, null, productData.getProductId(), productData.getName(), productData.getFormat(), productData.getIsFit(), productData.getFitWeight(), new CatItem.TextChangeListener() {
                            @Override
                            public void textChange(float number, float weight, float total) {
                                mapNumber.put(productData.getProductId(), number);
                                Float sum = new Float(0);
                                for (Float temp : mapNumber.values()) {
                                    sum += temp;
                                }
                                billBuy.setNumber(sum);

                                mapWeight.put(productData.getProductId(), weight);
                                sum = 0f;
                                for (Float temp : mapWeight.values()) {
                                    sum += temp;
                                }
                                billBuy.setWeight(sum);

                                tvStockCheckSum.setText("￥" + sum);
                            }
                        });
                        //之前的去边最后一个改边
                        if (i == productDatas.size() - 1) {
                            catItem.isBottom(true);
                        } else {
                            catItem.isBottom(false);
                        }
                        cat.addView(catItem);
                        //缓存比较
                        catItems.add(catItem);
                    }
                }

                break;
            default:
                break;
        }
    }

    private void getStockCheckDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("check_id", stockCheckId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mStockCheckDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
//                StockCheckDetailEvent stockCheckDetailEvent = gson.fromJson(response, StockCheckDetailEvent.class);
//                textViewItem2OderNummber.SetItemValue(stockCheckDetailEvent.getData().getOrder_no());
//                textViewItem2Maker.SetItemValue(stockCheckDetailEvent.getData().getCreate_user_name());
//                selelctableTextViewItem2Supplier.SetItemValue(stockCheckDetailEvent.getData().getSupplier_name());
//                selelctableTextViewItem2Store.SetItemValue(stockCheckDetailEvent.getData().getWarehouse_name());
//
//                bill.setNumber(stockCheckDetailEvent.getData().getTotal_package());
//                bill.setPay(stockCheckDetailEvent.getData().getTotal_amount());
//                bill.setWeight(stockCheckDetailEvent.getData().getTotal_weight());
//
//                editTextITem2Pay.SetItemValue(stockCheckDetailEvent.getData().getPaid_amount());
//                textViewItem2BillOwe.SetItemValue(stockCheckDetailEvent.getData().getTotal_owed());
//                textViewItem2AllOwe.SetItemValue(stockCheckDetailEvent.getData().getOwed_amount());
//                editTextITem2Note.SetItemValue(stockCheckDetailEvent.getData().getNote());
//
//                if (cat != null) {
//                    for (int i = 0; i < stockCheckDetailEvent.getData().getProducts().size(); i++) {
//                        final StockCheckDetailEvent.Product product = stockCheckDetailEvent.getData().getProducts().get(i);
//                        CatItem catItem = new CatItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), new CatItem.TextChangeListener() {
//
//                            @Override
//                            public void textChange(float number, float weight, float total) {
//
//                                mapNumber.put(product.getProduct_id(), total);
//                                Float sum = new Float(0);
//                                for (Float temp : mapNumber.values()) {
//                                    sum += temp;
//                                }
//                                bill.setNumber(sum + "");
//
//                                mapWeight.put(product.getProduct_id(), total);
//                                sum = 0f;
//                                for (Float temp : mapWeight.values()) {
//                                    sum += temp;
//                                }
//                                bill.setWeight(sum + "");
//                                mapTotal.put(product.getProduct_id(), total);
//                                sum = 0f;
//                                for (Float temp : mapTotal.values()) {
//                                    sum += temp;
//                                }
//                                bill.setPay(sum + "");
//
//                            }
//                        });
//                        if (i == stockCheckDetailEvent.getData().getProducts().size() - 1) {
//                            catItem.isBottom(true);
//                        } else {
//                            catItem.isBottom(false);
//                        }
//
//                        cat.addView(catItem);
//                    }
//                } else if (catDetail != null) {
//                    for (int i = 0; i < stockCheckDetailEvent.getData().getProducts().size(); i++) {
//                        StockCheckDetailEvent.Product product = stockCheckDetailEvent.getData().getProducts().get(i);
//                        CatDetailItem catDetailItem = new CatDetailItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), product.getQuantity(), product.getPrice_unit(), product.getWeight());
//                        if (i == stockCheckDetailEvent.getData().getProducts().size() - 1) {
//                            catDetailItem.isBottom(true);
//                        }
//                        catDetail.addView(catDetailItem);
//                    }
//                }
//                supplierId = stockCheckDetailEvent.getData().getSupplier_id();
//                storeId = stockCheckDetailEvent.getData().getWarehouse_id();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void stockCheckPost() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == Constant.EDIT_MODE) {
            jsonObject.addProperty("stockCheck_id", stockCheckId);
        }
        jsonObject.addProperty("warehouse_id", storeId);
        jsonObject.addProperty("supplier_id", supplierId);
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
        //只能填 0 或 3 或不传. 0 或 不传表示发布，如果当前用户有审核权限，直接审核通过。传 3 表示草稿。
        jsonObject.addProperty("state", mState + "");
        //组装product
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < catItems.size(); i++) {
            CatItem catItem = catItems.get(i);
            JsonObject product = new JsonObject();
            product.addProperty("weight", catItem.getWeight());
            product.addProperty("product_id", catItem.getProductId());
            product.addProperty("package", catItem.getNumber());
            product.addProperty("price", catItem.getUnitPrice());
            //1：公斤   2：件
            product.addProperty("price_unit", catItem.getUnit() + "");
            jsonArray.add(product);
        }
        jsonObject.add("products", jsonArray);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditStockCheckAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                finish();
            }

            @Override
            public void onFail() {

            }
        });
    }

}
