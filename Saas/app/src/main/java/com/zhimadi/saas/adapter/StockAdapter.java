package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.StocksSellEvent;
import com.zhimadi.saas.util.DisplayUtil;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */

//没挂图片
public class StockAdapter extends ArrayAdapter<StocksSellEvent.Stock> {

    private Context mContext;
    private int resourceId;

    private String unit;

    private TextView tvStockHomeName;
    private TextView tvStockHomeFormat;
    private TextView tvStockHomePrice;
    private TextView tvStockHomeSku;
    private TextView tvStockHomeWeight;
    private TextView tvStockHomeQuantity;
    private ImageView ivStockHomePic;

    public StockAdapter(Context context, int resource, List<StocksSellEvent.Stock> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StocksSellEvent.Stock stock = getItem(position);
        convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvStockHomeName = (TextView) convertView.findViewById(R.id.tv_stock_home_name);
        tvStockHomeFormat = (TextView) convertView.findViewById(R.id.tv_stock_home_format);

        tvStockHomePrice = (TextView) convertView.findViewById(R.id.tv_stock_home_price);

        tvStockHomeSku = (TextView) convertView.findViewById(R.id.tv_stock_home_sku);
        tvStockHomeQuantity = (TextView) convertView.findViewById(R.id.tv_stock_home_quantity);
        tvStockHomeWeight = (TextView) convertView.findViewById(R.id.tv_stock_home_weight);

        //1定装，0非定装
        if (stock.getIs_fixed().equals("0")) {
            tvStockHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shan, 0, 0, 0);
            unit = "/公斤";
        } else if (stock.getIs_fixed().equals("1")) {
            tvStockHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ding, 0, 0, 0);
            unit = "/件";
        }

        ivStockHomePic = (ImageView) convertView.findViewById(R.id.iv_stock_home_pic);
        ivStockHomePic.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String picURL = stock.getPic_urls();
        String picURLs[] = picURL.split(";");
        Picasso.with(mContext).load(picURLs[0]).resize(DisplayUtil.dip2px(mContext, 43.5f), DisplayUtil.dip2px(mContext, 43.5f)).into(ivStockHomePic);
        tvStockHomeName.setText(stock.getName());
        tvStockHomeFormat.setText(stock.getProps_name());
        tvStockHomePrice.setText("￥" + stock.getPrice()+ unit);
        tvStockHomeSku.setText("货号: " + stock.getSku());
        tvStockHomeQuantity.setText("数量: " + stock.getQuantity() + " 件");
        tvStockHomeWeight.setText("重量: " + stock.getWeight() + " 公斤");
        return convertView;
    }

}
