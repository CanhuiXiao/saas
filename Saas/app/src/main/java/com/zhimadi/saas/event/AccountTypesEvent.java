//package com.zhimadi.saas.event;
//
//import java.util.List;
//
///**
// * Created by carry on 2016/7/14.
// */
//public class AccountTypesEvent {
//
//
//    private int code;
//    private String msg;
//    private Data data;
//
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public class Data {
//        private String count;
//        private List<AccountType> list;
//
//
//        public String getCount() {
//            return count;
//        }
//
//        public void setCount(String count) {
//            this.count = count;
//        }
//
//        public List<AccountType> getList() {
//            return list;
//        }
//
//        public void setList(List<AccountType> list) {
//            this.list = list;
//        }
//    }
//
//    public class AccountType {
//
//        private String account_type_id;
//        private String name;
//
//        public String getAccount_type_id() {
//            return account_type_id;
//        }
//
//        public void setAccount_type_id(String account_type_id) {
//            this.account_type_id = account_type_id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//    }
//
//}
