package com.zhimadi.saas.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.TakenEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.view.PopVersionSelector;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.Timer;
import java.util.TimerTask;

public class LoginRegisterActivity extends BaseActivity implements View.OnClickListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    //四个小图标
    private TextView tvLoginRegusterPhone;
    private TextView tvLoginRegusterCode;
    private TextView tvLoginRegusterData;
    private TextView tvLoginRegusterComplete;

    private TextView tvLoginRegusterGetCode;
    private TextView tvLoginRegusterGetCodeAgain;
    private TextView tvLoginRegusterNext;
    private TextView tvLoginReguster;

    private LinearLayout llLoginRegusterPhone;
    private LinearLayout llLoginRegusterMessage;
    private LinearLayout llLoginRegusterData;
    private LinearLayout llLoginRegusterComplete;

    private EditText etLoginRegisterAccount;
    private EditText etLoginRegisterCode;
    private EditText etLoginRegisterCompany;
    private EditText etLoginRegisterName;
    private EditText etLoginRegisterPassword;
    private EditText etLoginRegisterPasswordComfirm;
    private TextView tvLoginRegisterVersion;
    private TextView tvLoginRegisterLand;

    private Handler handlerLoginReistered;

    private static final int PHONE_STATE = 1;
    private static final int CODE_STATE = 2;
    private static final int DATA_STATE = 3;
    private static final int COMPLETE_STATE = 4;

    private int waitTime = 10;
    //缓存手机号
    private String phoneNumber = "";
    //缓存taken
    private String taken = "";
    //缓存状态
    private int state = PHONE_STATE;

    private boolean isRun = false;

    private PopupWindow popupWindow;
    //该死的弹窗
    private PopVersionSelector popVersionSelector;

    Thread thread;

    private void inte() {

        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        tvLoginRegusterPhone = (TextView) findViewById(R.id.tv_login_register_phone);
        tvLoginRegusterCode = (TextView) findViewById(R.id.tv_login_register_code);
        tvLoginRegusterData = (TextView) findViewById(R.id.tv_login_register_data);
        tvLoginRegusterComplete = (TextView) findViewById(R.id.tv_login_register_complete);

        tvLoginRegusterGetCode = (TextView) findViewById(R.id.tv_login_register_get_code);
        tvLoginRegusterGetCodeAgain = (TextView) findViewById(R.id.tv_login_register_get_code_again);
        tvLoginRegusterNext = (TextView) findViewById(R.id.tv_login_register_next);
        tvLoginReguster = (TextView) findViewById(R.id.tv_login_register);

        llLoginRegusterPhone = (LinearLayout) findViewById(R.id.ll_login_register_phone);
        llLoginRegusterMessage = (LinearLayout) findViewById(R.id.ll_login_register_message);
        llLoginRegusterData = (LinearLayout) findViewById(R.id.ll_login_register_data);
        llLoginRegusterComplete = (LinearLayout) findViewById(R.id.ll_login_register_complete);

        etLoginRegisterAccount = (EditText) findViewById(R.id.et_login_register_account);
        etLoginRegisterCode = (EditText) findViewById(R.id.et_login_register_code);
        etLoginRegisterCompany = (EditText) findViewById(R.id.et_login_register_company);
        etLoginRegisterName = (EditText) findViewById(R.id.et_login_register_name);
        etLoginRegisterPassword = (EditText) findViewById(R.id.et_login_register_password);
        etLoginRegisterPasswordComfirm = (EditText) findViewById(R.id.et_login_register_password_comfirm);

        tvLoginRegisterVersion = (TextView) findViewById(R.id.tv_login_register_version);
        tvLoginRegisterLand = (TextView) findViewById(R.id.tv_login_register_land);

        popVersionSelector = new PopVersionSelector(mContext, null);

        handlerLoginReistered = new Handler() {
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case 1:
                        if (waitTime > 0) {
                            if (waitTime == 1) {
                                isRun = false;
                                waitTime--;
                                tvLoginRegusterGetCodeAgain.setText("获取验证码");
                                tvLoginRegusterGetCodeAgain.setEnabled(true);
                            } else {
                                waitTime--;
                                tvLoginRegusterGetCodeAgain.setText("等待" + waitTime + "秒");
                            }
                        }
                        break;
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);
        inte();
        titleBarCommonBuilder.setLeftText("返回").setLeftImageRes(R.drawable.fan_hui02).setTitle("")
                .setLeftTextColor(R.color.colorMainBody).setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (state) {
                    case PHONE_STATE:
                        finish();
                        break;
                    case CODE_STATE:
                        lighten(PHONE_STATE);
                        state = PHONE_STATE;
                        break;
                    case DATA_STATE:
                        lighten(CODE_STATE);
                        state = CODE_STATE;
                        break;
                    case COMPLETE_STATE:
                        finish();
                        break;
                    default:
                        finish();
                        break;
                }
            }
        });
        popVersionSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
                if (popVersionSelector.getVersion() == 1) {
                    tvLoginRegisterVersion.setText("水果版");
                } else if (popVersionSelector.getVersion() == 2) {
                    tvLoginRegisterVersion.setText("蔬菜版");
                } else {
                    tvLoginRegisterVersion.setText("");
                }

            }
        });

        tvLoginRegusterGetCode.setOnClickListener(this);
        tvLoginRegusterGetCodeAgain.setOnClickListener(this);
        tvLoginRegusterNext.setOnClickListener(this);
        tvLoginReguster.setOnClickListener(this);
        tvLoginRegisterVersion.setOnClickListener(this);
        tvLoginRegisterLand.setOnClickListener(this);
    }


    @Override
    public void onBackPressed() {
        switch (state) {
            case PHONE_STATE:
                finish();
                break;
            case CODE_STATE:
                isRun = false;
                lighten(PHONE_STATE);
                break;
            case DATA_STATE:
                lighten(CODE_STATE);
                break;
            case COMPLETE_STATE:
                finish();
                break;
            default:
                finish();
                break;
        }

    }

    //获取验证码
    private void getCode(String phoneNumber) {
        tvLoginRegusterGetCodeAgain.setEnabled(false);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phone", phoneNumber);
//        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mCodeAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
//            @Override
//            public void onSuccess(String response) {
        state = CODE_STATE;
        lighten(state);
        waitTime = 10;
        isRun = true;
//        Thread thread = new Thread(new MessageRunnable());
        thread = new Thread(new MessageRunnable());
        thread.start();

        LoginRegisterActivity.this.phoneNumber = etLoginRegisterAccount.getText().toString();
//            }

//            @Override
//            public void onFail() {
//            }
//        });

    }

    //检验验证码
    private void checkCode(String phoneNumber, String code) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("phone", phoneNumber);
        jsonObject.addProperty("code", code);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mCheckAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                TakenEvent takenEvent = gson.fromJson(response, TakenEvent.class);
                taken = takenEvent.getData().getToken();
                state = DATA_STATE;
                lighten(state);
            }

            @Override
            public void onFail() {

            }
        });

    }

    //注册
    private void register(String companyName, String userName, String password, String version) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("token", taken);
        jsonObject.addProperty("company_name", companyName);
        jsonObject.addProperty("user_name", userName);
        jsonObject.addProperty("password", password);
        jsonObject.addProperty("version", version);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mRegisterAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                state = COMPLETE_STATE;
                lighten(state);
            }

            @Override
            public void onFail() {

            }
        });

    }

    public class MessageRunnable implements Runnable {
        @Override
        public void run() {
            while (isRun) {
                try {
                    Thread.sleep(1000);
                    Message message = new Message();
                    message.what = 1;
                    handlerLoginReistered.sendMessage(message);
                } catch (Exception e) {
                }
            }
        }
    }


    private void lighten(int state) {

        switch (state) {
            case PHONE_STATE:
                tvLoginRegusterPhone.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterCode.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterData.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterComplete.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterCode.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.duan_xin_ren_zhen, 0, 0);
                tvLoginRegusterData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tian_xie_zhi_liao, 0, 0);
                tvLoginRegusterComplete.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.zhen_que, 0, 0);
                llLoginRegusterPhone.setVisibility(View.VISIBLE);
                llLoginRegusterMessage.setVisibility(View.GONE);
                llLoginRegusterData.setVisibility(View.GONE);
                llLoginRegusterComplete.setVisibility(View.GONE);


                break;
            case CODE_STATE:
                tvLoginRegusterPhone.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterCode.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterData.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterComplete.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterCode.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.duan_xin_ren_zhen02, 0, 0);
                tvLoginRegusterData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tian_xie_zhi_liao, 0, 0);
                tvLoginRegusterComplete.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.zhen_que, 0, 0);
                llLoginRegusterPhone.setVisibility(View.GONE);
                llLoginRegusterMessage.setVisibility(View.VISIBLE);
                llLoginRegusterData.setVisibility(View.GONE);
                llLoginRegusterComplete.setVisibility(View.GONE);

                break;
            case DATA_STATE:
                tvLoginRegusterPhone.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterCode.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterData.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterComplete.setTextColor(Color.rgb(0xcd, 0xcd, 0xcd));
                tvLoginRegusterCode.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.duan_xin_ren_zhen02, 0, 0);
                tvLoginRegusterData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tian_xie_zhi_liao02, 0, 0);
                tvLoginRegusterComplete.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.zhen_que, 0, 0);
                llLoginRegusterPhone.setVisibility(View.GONE);
                llLoginRegusterMessage.setVisibility(View.GONE);
                llLoginRegusterData.setVisibility(View.VISIBLE);
                llLoginRegusterComplete.setVisibility(View.GONE);

                break;
            case COMPLETE_STATE:
                tvLoginRegusterPhone.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterCode.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterData.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterComplete.setTextColor(Color.rgb(0xa3, 0xe6, 0xdc));
                tvLoginRegusterCode.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.duan_xin_ren_zhen02, 0, 0);
                tvLoginRegusterData.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.tian_xie_zhi_liao02, 0, 0);
                tvLoginRegusterComplete.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.zhen_que02, 0, 0);
                llLoginRegusterPhone.setVisibility(View.GONE);
                llLoginRegusterMessage.setVisibility(View.GONE);
                llLoginRegusterData.setVisibility(View.GONE);
                llLoginRegusterComplete.setVisibility(View.VISIBLE);
                break;
        }
    }


//    private void backgroundAlpha(float bgAlpha) {
//        WindowManager.LayoutParams lp = getWindow().getAttributes();
//        lp.alpha = bgAlpha;
//        getWindow().setAttributes(lp);
//    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_login_register_get_code:
                if (etLoginRegisterAccount.getText().toString().equals("")) {
                    Toast.makeText(mContext, "请填写手机号！", Toast.LENGTH_SHORT).show();
                } else {
                    getCode(etLoginRegisterAccount.getText().toString());
                }
                break;

            case R.id.tv_login_register_get_code_again:
                getCode(phoneNumber);
                break;

            case R.id.tv_login_register_next:
                if (etLoginRegisterCode.getText().toString().equals("")) {
                    Toast.makeText(mContext, "请填写验证码！", Toast.LENGTH_SHORT).show();
                } else {
                    checkCode(phoneNumber, etLoginRegisterCode.getText().toString());
                }
                break;

            case R.id.tv_login_register_version:
                popupWindow = new PopupWindow();
                popupWindow.setContentView(popVersionSelector);
                popupWindow.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
                popupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                popupWindow.showAtLocation(findViewById(R.id.ll_login_register_window), Gravity.CENTER, 0, 0);
                break;

            case R.id.tv_login_register:
                if (etLoginRegisterPassword.getText().toString().equals(etLoginRegisterPasswordComfirm.getText().toString())) {
                    if (popVersionSelector.getVersion() == 1 || popVersionSelector.getVersion() == 2) {
                        register(etLoginRegisterCompany.getText().toString(), etLoginRegisterName.getText().toString(), etLoginRegisterPassword.getText().toString(), popVersionSelector.getVersion() + "");
                    } else {
                        Toast.makeText(mContext, "请选择运行版本", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mContext, "密码不一致！", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_login_register_land:
                finish();
                break;
            default:
        }
    }
}
