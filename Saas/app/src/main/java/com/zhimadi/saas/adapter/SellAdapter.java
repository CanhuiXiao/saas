package com.zhimadi.saas.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.BuysEvent;
import com.zhimadi.saas.event.SellsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class SellAdapter extends ArrayAdapter<SellsEvent.Sell> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals("0")) {
            holder.tvSellHomestate.setText("已出售");
            holder.tvSellHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("3")) {
            holder.tvSellHomestate.setText("草稿");
            holder.tvSellHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("2")) {
            holder.tvSellHomestate.setText("撤销");
            holder.tvSellHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public SellAdapter(Context context, int resource, List<SellsEvent.Sell> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SellsEvent.Sell sell = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvSellHomeCustom.setText(sell.getCustom_name());
        showStateText(holder, sell.getState());
        holder.tvSellHomeOderNumber.setText(sell.getOrder_no());
        holder.tvSellHomeToGet.setText("￥" + sell.getTotal_amount());
        holder.tvSellHomeCreateDate.setText(sell.getCreate_time());
        holder.tvSellHomeStore.setText(sell.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {
        TextView tvSellHomeCustom;
        TextView tvSellHomestate;
        TextView tvSellHomeOderNumber;
        TextView tvSellHomeToGet;
        TextView tvSellHomeCreateDate;
        TextView tvSellHomeStore;

        public ViewHolder(View view) {
            tvSellHomeCustom = (TextView) view.findViewById(R.id.tv_sell_home_custom);
            tvSellHomestate = (TextView) view.findViewById(R.id.tv_sell_home_state);
            tvSellHomeOderNumber = (TextView) view.findViewById(R.id.tv_sell_home_oder_number);
            tvSellHomeToGet = (TextView) view.findViewById(R.id.tv_sell_home_to_get);
            tvSellHomeCreateDate = (TextView) view.findViewById(R.id.tv_sell_home_create_date);
            tvSellHomeStore = (TextView) view.findViewById(R.id.tv_sell_home_store);
            view.setTag(this);
        }
    }


}
