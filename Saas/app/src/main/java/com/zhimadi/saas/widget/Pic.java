package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import com.zhimadi.saas.R;

public class Pic extends LinearLayout {

    private View returnView;
    private Context mcContext;


    private void inite() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_pic, this);
    }

    public Pic(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcContext = context;
        inite();
    }

}
