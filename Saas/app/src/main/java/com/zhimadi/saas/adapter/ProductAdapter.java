package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.ProductsEvent;
import com.zhimadi.saas.util.DisplayUtil;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */

//没挂图片
public class ProductAdapter extends ArrayAdapter<ProductsEvent.Product> {

    private Context mContext;
    private int resourceId;

    private String unit;

    private TextView tvProductHomeName;
    private TextView tvProductHomeFormat;
    private TextView tvProductHomePrice;
    private TextView tvProductHomeSku;
    private TextView tvProductHomeWeight;
    private TextView tvProductHomeQuantity;
    private ImageView ivProductHomePic;
    private TextView tvProductHomeState;

    public ProductAdapter(Context context, int resource, List<ProductsEvent.Product> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ProductsEvent.Product product = getItem(position);
        convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvProductHomeName = (TextView) convertView.findViewById(R.id.tv_product_home_name);
        tvProductHomeFormat = (TextView) convertView.findViewById(R.id.tv_product_home_format);

        tvProductHomePrice = (TextView) convertView.findViewById(R.id.tv_product_home_price);

        tvProductHomeSku = (TextView) convertView.findViewById(R.id.tv_product_home_sku);
        tvProductHomeQuantity = (TextView) convertView.findViewById(R.id.tv_product_home_quantity);
        tvProductHomeWeight = (TextView) convertView.findViewById(R.id.tv_product_home_weight);

        tvProductHomeState = (TextView) convertView.findViewById(R.id.tv_product_home_state);
        //1定装，0非定装
        if (product.getIs_fixed().equals("0")) {
            tvProductHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shan, 0, 0, 0);
            unit = "/公斤";
        } else if (product.getIs_fixed().equals("1")) {
            tvProductHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ding, 0, 0, 0);
            unit = "/件";
        }

        ivProductHomePic = (ImageView) convertView.findViewById(R.id.iv_product_home_pic);
        ivProductHomePic.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String picURL = product.getPic_urls();
        String picURLs[] = picURL.split(";");
        Picasso.with(mContext).load(picURLs[0]).resize(DisplayUtil.dip2px(mContext, 43.5f), DisplayUtil.dip2px(mContext, 43.5f)).into(ivProductHomePic);
        tvProductHomeName.setText(product.getType_name());
        tvProductHomeFormat.setText(product.getProps_name());
        tvProductHomePrice.setText("￥" + product.getSelling_price() + unit);
        tvProductHomeSku.setText("货号: " + product.getSku());
        tvProductHomeQuantity.setText("数量: " + product.getQuantity() + " 件");
        tvProductHomeWeight.setText("重量: " + product.getWeight() + " 公斤");
        if (product.getIs_sell().equals("0")) {
            tvProductHomeState.setText("已下架");
            tvProductHomeState.setBackgroundResource(R.drawable.shape_bt_product_state_2);
        } else if (product.getIs_sell().equals("1")) {
            tvProductHomeState.setText("上架中");
            tvProductHomeState.setBackgroundResource(R.drawable.shape_bt_product_state_1);
        }
        return convertView;
    }

}
