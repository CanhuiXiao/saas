package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class SelelctableOnlyTextViewItem extends LinearLayout {

	private View returnView;
	private TextView tvItemName;

	private View viewDivider;
	private View viewDividerFirst;
	private View viewDividerLast;

	public SelelctableOnlyTextViewItem(Context context, AttributeSet attrs, String itemName) {
		super(context, attrs);
		returnView = LayoutInflater.from(context).inflate(R.layout.widget_selectable_textview_only, this);
		tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
		viewDivider = returnView.findViewById(R.id.view_divider);
		viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
		viewDividerLast = returnView.findViewById(R.id.view_divider_last);



		tvItemName.setText(itemName);
	}

	public SelelctableOnlyTextViewItem isTop(boolean isTop){
		if(isTop){
			viewDividerFirst.setVisibility(View.VISIBLE);
		}else {
			viewDividerFirst.setVisibility(View.GONE);
		}
		return this;
	}

	public SelelctableOnlyTextViewItem isBottom(boolean isBottom){
		if(isBottom){
			viewDividerLast.setVisibility(View.VISIBLE);
			viewDivider.setVisibility(View.GONE);
		}else {
			viewDividerLast.setVisibility(View.GONE);
			viewDivider.setVisibility(View.VISIBLE);
		}
		return this;
	}


	public SelelctableOnlyTextViewItem SetItemName(String name){
		tvItemName.setText(name);
		return this;
	}



}
