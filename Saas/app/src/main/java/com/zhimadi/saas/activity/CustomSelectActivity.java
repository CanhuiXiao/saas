package com.zhimadi.saas.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.CustomSelectAdapter;
import com.zhimadi.saas.event.CustomsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomSelectActivity extends BaseActivity {

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_Fail = 0;

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private ListView lvCustomSelect;
    private List<CustomsEvent.Custom> customs;
    private CustomSelectAdapter customSelectAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        customs = new ArrayList<CustomsEvent.Custom>();
        lvCustomSelect = (ListView) findViewById(R.id.lv_custom_select);
        customSelectAdapter = new CustomSelectAdapter(mContext, R.layout.item_lv_custom_select, customs, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_select);
        inte();
        titleBarCommonBuilder.setTitle("选择客户")
                .setLeftImageRes(R.drawable.fan_hui02)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, CustomEditActivity.class);
                        intent.putExtra("INTENT_MODE", CustomEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvCustomSelect.setAdapter(customSelectAdapter);
        lvCustomSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, CustomEditActivity.class);
                intent.putExtra("INTENT_MODE", CustomEditActivity.EDIT_MODE);
                startActivity(intent);
            }
        });
        //加上死锁，没错就是配合动画
        lvCustomSelect.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            GetCustom(mStart, mLimit);
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        GetCustom(mStart, mLimit);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetCustom(mStart, mLimit);
    }



    private void GetCustom(final int start, int limit) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", start + "");
        map.put("limit", limit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mCustomAddress, map);
        final ProgressDialog progressDialog = new ProgressDialog(mContext);
        progressDialog.show();
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CustomsEvent customsEvent = gson.fromJson(response, CustomsEvent.class);
                if (start == 0) {
                    customs.clear();
                }
                customs.addAll(customsEvent.getData().getList());
                mStart = mStart + customsEvent.getData().getList().size();
                customSelectAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }

            @Override
            public void onFail() {

            }
        });
    }

    //选中后返回数据
    public void select(String customId, String customName){
        Intent intent = new Intent();
        intent.putExtra("STORE_ID", customId);
        intent.putExtra("STORE_NAME", customName);
        setResult(RESULT_CODE_SUCCESS, intent);
        finish();
    }





}