package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class SellAgentAgentsEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<SellAgentAgent> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<SellAgentAgent> getList() {
            return list;
        }

        public void setList(List<SellAgentAgent> list) {
            this.list = list;
        }
    }

    public class SellAgentAgent {

        private String agent_sell_id;
        private String create_time;
        private String agent_id;
        private String warehouse_id;
        private String total_amount;
        private String state;
        private String order_no;
        private String warehouse_name;
        private String agent_name;

        public String getAgent_sell_id() {
            return agent_sell_id;
        }

        public void setAgent_sell_id(String agent_sell_id) {
            this.agent_sell_id = agent_sell_id;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getWarehouse_id() {
            return warehouse_id;
        }

        public void setWarehouse_id(String warehouse_id) {
            this.warehouse_id = warehouse_id;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getWarehouse_name() {
            return warehouse_name;
        }

        public void setWarehouse_name(String warehouse_name) {
            this.warehouse_name = warehouse_name;
        }

        public String getAgent_name() {
            return agent_name;
        }

        public void setAgent_name(String agent_name) {
            this.agent_name = agent_name;
        }
    }

}
