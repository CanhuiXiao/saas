package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.PaymentTypeAdapter;
import com.zhimadi.saas.event.PaymentTypesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentTypeHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private SwipeMenuListView lvPaymentTypeHome;
    private List<PaymentTypesEvent.PaymentType> paymentTypes;
    private PaymentTypeAdapter paymenTypeAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        paymentTypes = new ArrayList<PaymentTypesEvent.PaymentType>();
        lvPaymentTypeHome = (SwipeMenuListView) findViewById(R.id.lv_payment_type_home);
        paymenTypeAdapter = new PaymentTypeAdapter(mContext, R.layout.item_lv_payment_type_home, paymentTypes);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_type_home);
        inte();
        titleBarCommonBuilder.setTitle("账目类型管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightText("添加分类")
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, PaymentTypeEditActivity.class);
                        intent.putExtra("INTENT_MODE", PaymentTypeEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });




        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);


            }

        };
        lvPaymentTypeHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (index == 0) {
                    PaymentTypeDelete(paymentTypes.get(position).getPayment_type_id());
                }
                return true;
            }
        });
        lvPaymentTypeHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, PaymentTypeEditActivity.class);
                intent.putExtra("INTENT_MODE", PaymentTypeEditActivity.EDIT_MODE);
                intent.putExtra("PAYMENT_TYPE_ID", paymentTypes.get(i).getPayment_type_id());
                activity.startActivity(intent);
            }
        });


        lvPaymentTypeHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetPaymentType();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvPaymentTypeHome.setMenuCreator(creator);
        lvPaymentTypeHome.setAdapter(paymenTypeAdapter);
        GetPaymentType();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetPaymentType();
    }


    private void GetPaymentType() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mPaymentTypeAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                PaymentTypesEvent paymentTypesEvent = gson.fromJson(response, PaymentTypesEvent.class);
                if (mStart == 0) {
                    paymentTypes.clear();
                }
                mStart = mStart + paymentTypesEvent.getData().getList().size();
                paymentTypes.addAll(paymentTypesEvent.getData().getList());
                paymenTypeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });

    }

    private void PaymentTypeDelete(String paymentTypeId) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("payment_type_id", paymentTypeId);

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeletePaymentTypeAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                mStart = 0;
                GetPaymentType();
            }

            @Override
            public void onFail() {

            }
        });

    }

}
