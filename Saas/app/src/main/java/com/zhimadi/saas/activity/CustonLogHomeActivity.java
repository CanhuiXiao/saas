package com.zhimadi.saas.activity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.CustomAdapter;
import com.zhimadi.saas.event.CustomsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.LogHeadItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustonLogHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;


    private TitleBarCommonBuilder titleBarCommonBuilder;

    private List<CustomsEvent.Custom> customs;
    private SwipeMenuListView lvCustomLogHome;
    private CustomAdapter customAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        customs = new ArrayList<CustomsEvent.Custom>();
        lvCustomLogHome = (SwipeMenuListView) findViewById(R.id.lv_custom_log_home);
        customAdapter = new CustomAdapter(mContext, R.layout.item_lv_custom_home, customs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_log_home);
        inte();
        titleBarCommonBuilder.setTitle("客户流水对账")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //付款
                SwipeMenuItem payItem = new SwipeMenuItem(
                        activity);
                payItem.setBackground(new ColorDrawable(Color.rgb(0x48,
                        0xCF, 0xAD)));
                payItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                payItem.setTitle("付款");
                payItem.setTitleSize(12);
                payItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(payItem);

            }
        };


        lvCustomLogHome.setMenuCreator(creator);
        lvCustomLogHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                return true;
            }
        });

        lvCustomLogHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, CustomEditActivity.class);
//                intent.putExtra("INTENT_MODE", CustomEditActivity.EDIT_MODE);
//                intent.putExtra("CUSTOM_ID", customs.get(i).getCustom_id());
//                startActivity(intent);
            }
        });

        lvCustomLogHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            GetCustom();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvCustomLogHome.addHeaderView(new LogHeadItem(mContext, null));
        lvCustomLogHome.setAdapter(customAdapter);
        GetCustom();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
    }

    private void GetCustom() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mCustomAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                String i = response;
                Gson gson = new Gson();
                CustomsEvent customsEvent = gson.fromJson(response, CustomsEvent.class);
                if (mStart == 0) {
                    customs.clear();
                }
                mStart = customsEvent.getData().getList().size();
                customs.addAll(customsEvent.getData().getList());
                customAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });


    }


}
