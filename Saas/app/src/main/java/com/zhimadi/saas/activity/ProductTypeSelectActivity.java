package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.ProductTypesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.FlowLayout;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

public class ProductTypeSelectActivity extends BaseActivity {

    public static final int resultCodeSuccess = 1;
    public static final int resultCodetFail = 0;

    //接数据
    private String categoryId;
    private String categoryName;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private FlowLayout flProductTypeSelect;

    private void inte() {
        categoryId = getIntent().getStringExtra("CATEGORY_ID");
        categoryName = getIntent().getStringExtra("CATEGORY_NAME");
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        flProductTypeSelect = (FlowLayout) findViewById(R.id.fl_product_type_select);
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_type_select);
        inte();
        titleBarCommonBuilder.setTitle(categoryName)
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        flProductTypeSelect.SetMarginVertical(DisplayUtil.dip2px(mContext, 9.6f));
        flProductTypeSelect.SetMarginHorizontal(DisplayUtil.dip2px(mContext, 13.44f));
        GetProductType(categoryId);
    }

    private List<TextView> textViews = new ArrayList<TextView>();

    private void GetProductType(String categoryId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("category_id", categoryId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mProductTypeAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ProductTypesEvent productTypesEvent = gson.fromJson(response, ProductTypesEvent.class);
                List<ProductTypesEvent.ProductType> productTypes = new ArrayList<ProductTypesEvent.ProductType>();
                productTypes.addAll(productTypesEvent.getData().getList());
                for (int i = 0; i < productTypes.size(); i++) {
                    final ProductTypesEvent.ProductType productType = productTypesEvent.getData().getList().get(i);
                    TextView textView = new TextView(mContext);
                    textView.setText(productType.getName());
                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(-2, DisplayUtil.dip2px(mContext, 26.4f));
                    textView.setLayoutParams(params);
                    textView.setTextColor(Color.rgb(0x36, 0x36, 0x36));
                    textView.setGravity(Gravity.CENTER);
                    textView.setBackgroundResource(R.drawable.sel_press_product_type);
                    textViews.add(textView);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent  = new Intent();
                            intent.putExtra("TYPE_ID", productType.getType_id());
                            intent.putExtra("TYPE_NAME", productType.getName());
                            setResult(resultCodeSuccess, intent);
                            finish();
                        }
                    });
                    flProductTypeSelect.addView(textView);
                }

                Toast.makeText(mContext, textViews.size()+"", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail() {

            }
        });
    }


}
