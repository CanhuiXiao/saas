package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.OwnersEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class OwnerAdapter extends ArrayAdapter<OwnersEvent.Owner> {

    private Context mContext;
    private int resourceId;

    public OwnerAdapter(Context context, int resource, List<OwnersEvent.Owner> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OwnersEvent.Owner owner = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvOwnerHomeName.setText(owner.getName());
        return convertView;
    }

    class ViewHolder {
        TextView tvOwnerHomeName;
        public ViewHolder(View view) {
            tvOwnerHomeName = (TextView) view.findViewById(R.id.tv_owner_home_name);
            view.setTag(this);
        }
    }




}
