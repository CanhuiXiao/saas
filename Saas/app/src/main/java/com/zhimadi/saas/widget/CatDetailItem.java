package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class CatDetailItem extends LinearLayout {

    private View returnView;
    private Context mcContext;

    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;

    private String productId;
    private String unit;

    private TextView tvCatName;
    private TextView tvCatFormat;

    private TextView tvCatDetailNumber;
    private TextView tvCatDetailUnitPrice;
    private TextView tvCatDetailWeight;

    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_cat_detail_item, this);
        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
        tvCatName = (TextView) returnView.findViewById(R.id.tv_cat_name);
        tvCatFormat = (TextView) returnView.findViewById(R.id.tv_cat_format);
        tvCatDetailNumber = (TextView) returnView.findViewById(R.id.tv_cat_detail_number);
        tvCatDetailUnitPrice = (TextView) returnView.findViewById(R.id.tv_cat_detail_unit_price);
        tvCatDetailWeight = (TextView) returnView.findViewById(R.id.tv_cat_detail_weight);
    }

    public CatDetailItem(Context context, AttributeSet attrs, String productId, String name, String format, String isFit, String number, String unitPrice, String weight) {
        super(context, attrs);
        mcContext = context;
        this.productId = productId;
        inte();
        tvCatName.setText(name);
        tvCatFormat.setText(format);
        if (isFit.equals("1")) {
            tvCatName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ding, 0, 0, 0);
            unit = "/件";
        } else if (isFit.equals("0")) {
            tvCatName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shan, 0, 0, 0);
            unit = "/公斤";
        }
        tvCatDetailNumber.setText("数量:" + number + "件");
        tvCatDetailUnitPrice.setText("单价:￥" + unitPrice + unit);
        tvCatDetailWeight.setText("重量:" + weight);
    }


    public CatDetailItem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public CatDetailItem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public String getProductId() {
        return productId;
    }

}
