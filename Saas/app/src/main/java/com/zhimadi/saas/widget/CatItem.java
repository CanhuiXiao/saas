package com.zhimadi.saas.widget;

import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;

public class CatItem extends LinearLayout {

    private View returnView;
    private Context mcContext;

    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;

    private String productId;

    private TextView tvCatName;
    private TextView tvCatFormat;
    private TextView tvCatItemUnitPrice;

    private EditText etCatNumber;
    private EditText etCatWeight;
    private EditText etCatUnitPrice;

    private int unit;

    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_cat_item, this);
        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
        tvCatName = (TextView) returnView.findViewById(R.id.tv_cat_name);
        tvCatFormat = (TextView) returnView.findViewById(R.id.tv_cat_format);
        tvCatItemUnitPrice = (TextView) returnView.findViewById(R.id.tv_cat_item_unit_price);

        etCatNumber = (EditText) returnView.findViewById(R.id.et_cat_number);
        etCatWeight = (EditText) returnView.findViewById(R.id.et_cat_weight);
        etCatUnitPrice = (EditText) returnView.findViewById(R.id.et_cat_unit_price);
    }

    public CatItem(Context context, AttributeSet attrs, String productId, String name, String format, final String isFit, final float fitWeight, final TextChangeListener textChangeListener) {
        super(context, attrs);
        mcContext = context;
        this.productId = productId;
        inte();
        tvCatName.setText(name);
        tvCatFormat.setText(format);
        //根据定装状态  改图标，改编辑权限，改背景色，改字体颜色
        //定装
        if (isFit.equals("1")) {
            tvCatName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ding, 0, 0, 0);
            tvCatItemUnitPrice.setText("单价(元/件)");
            etCatWeight.setEnabled(false);
            etCatWeight.setBackgroundResource(R.color.colorNull);
            etCatWeight.setTextColor(Color.parseColor("#363636"));
            unit = 2;
            //散装
        } else if (isFit.equals("0")) {
            tvCatName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shan, 0, 0, 0);
            tvCatItemUnitPrice.setText("单价(元/公斤)");
            etCatWeight.setEnabled(true);
            etCatWeight.setBackgroundResource(R.drawable.shape_et_product_cat);
            etCatWeight.setTextColor(Color.parseColor("#ffffff"));
            unit = 1;
        }

        etCatNumber.setText("0");
        etCatWeight.setText("0");
        etCatUnitPrice.setText("0");

        etCatNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(isFit.equals(Constant.STATE_FIT+"")){
                    etCatWeight.setText(getValue(etCatNumber)*fitWeight + "");
                }
                if(textChangeListener != null){
                    textChangeListener.textChange(getValue(etCatNumber), getValue(etCatWeight), count(isFit.equals(Constant.STATE_FIT)));
                }
            }
        });

        etCatWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(textChangeListener != null){
                    textChangeListener.textChange(getValue(etCatNumber), getValue(etCatWeight), count(isFit.equals("1")));
                }
            }
        });

        etCatUnitPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(textChangeListener != null){
                    textChangeListener.textChange(getValue(etCatNumber), getValue(etCatWeight), count(isFit.equals(Constant.STATE_FIT+"")));
                }
            }
        });

    }

    public String getWeight() {
        return etCatWeight.getText().toString();
    }

    public String getNumber() {
        return etCatNumber.getText().toString();
    }

    public String getUnitPrice() {
        return etCatUnitPrice.getText().toString();
    }

    public int getUnit() {
        return unit;
    }

    public CatItem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public CatItem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public String getProductId() {
        return productId;
    }

    private float count(boolean isFit) {
        //初始值为0
        float temp = 0;
        //定装
        if(isFit){
            temp = getValue(etCatUnitPrice) * getValue(etCatNumber);
        }else {
            temp = getValue(etCatUnitPrice) * getValue(etCatWeight);
        }
       return temp;
    }

    private float getValue(EditText editText) {
        //初始值为0
        float temp = 0;
        if (editText.getText().toString().equals("")) {
            temp = 0;
        } else {
            try {
                temp = Float.valueOf(editText.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return temp;
    }

    //回调接口(监听器)
    public interface TextChangeListener {
        public void textChange(float number, float weight, float total);
    }
}
