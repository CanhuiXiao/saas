package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.BuyAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.BuysEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BuyHomeActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener{

//    public static final int BUY_ALL = -1;
//    public static final int BUY_WAIT = 1;
//    public static final int BUY_FINISH = 0;
//    public static final int BUY_NOTE = 3;
//    public static final int BUY_CANCRL = 2;


    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private RadioGroupWithLayout rgBuyHome;
    //滑块集合
    private List<View> sliders;

    private List<BuysEvent.Buy> buys;
    private ListView lvBuyHome;
    private BuyAdapter buyAdapter;

    private void inte(){
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        rgBuyHome = (RadioGroupWithLayout) findViewById(R.id.rg_buy_home);
        sliders = new ArrayList<View>();
        View viewBuyHomeAll = findViewById(R.id.view_buy_home_all);
        sliders.add(viewBuyHomeAll);
        View viewBuyHomeWait = findViewById(R.id.view_buy_home_wait_check);
        sliders.add(viewBuyHomeWait);
        View viewBuyHomeFinish = findViewById(R.id.view_buy_home_finish);
        sliders.add(viewBuyHomeFinish);
        View viewBuyHomeNote = findViewById(R.id.view_buy_home_note);
        sliders.add(viewBuyHomeNote);
        View viewBuyHomeCancel = findViewById(R.id.view_buy_home_cancel);
        sliders.add(viewBuyHomeCancel);

        lvBuyHome = (ListView) findViewById(R.id.lv_buy_home);
        buys = new ArrayList<BuysEvent.Buy>();
        buyAdapter = new BuyAdapter(mContext, R.layout.item_lv_buy_home, buys);
    }

    //显示滑块的方法
    private void ShowSlider(int RadioButtonId){
        for (int i = 0; i < sliders.size(); i++) {
            if(RadioButtonId == sliders.get(i).getId()){
                sliders.get(i).setVisibility(View.VISIBLE);
            }else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_home);
        inte();
        titleBarCommonBuilder.setTitle("采购历史")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setRightImageRes(R.drawable.tian_jia02)
                .setRightImageResSecond(R.drawable.shou_suo);
        titleBarCommonBuilder.setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarCommonBuilder.setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, BuyEditActivity.class);
                intent.putExtra("INTENT_MODE", BuyEditActivity.ADD_MODE);
                startActivity(intent);
            }
        });


        lvBuyHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, BuyEditActivity.class);
                intent.putExtra("INTENT_MODE", BuyEditActivity.EDIT_MODE);
                intent.putExtra("BUY_ID", buys.get(position).getBuy_id());
                intent.putExtra("STATE", buys.get(position).getState());
                startActivity(intent);
            }
        });

        lvBuyHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetBuy();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvBuyHome.setAdapter(buyAdapter);
        GetBuy();
        rgBuyHome.setOnCheckedChangeListener(this);
    }

    private void GetBuy(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        if(mState != Constant.STATE_ALL){
            map.put("state", mState+"");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mBuyAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                BuysEvent buysEvent = gson.fromJson(response, BuysEvent.class);
                if(mStart == 0){
                    buys.clear();
                }
                mStart = mStart + buysEvent.getData().getList().size();
                buys.addAll(buysEvent.getData().getList());
                buyAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    private int mState = Constant.STATE_ALL;

    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId){
            case R.id.rb_buy_home_all:
                ShowSlider(R.id.view_buy_home_all);
                mStart = 0;
                mState = Constant.STATE_ALL;
                GetBuy();
                break;
            case R.id.rb_buy_home_wait_check:
                ShowSlider(R.id.view_buy_home_wait_check);
                mStart = 0;
                mState = Constant.STATE_WAIT_CHECK;
                GetBuy();
                break;
            case R.id.rb_buy_home_finish:
                ShowSlider(R.id.view_buy_home_finish);
                mStart = 0;
                mState = Constant.STATE_DEFAULT;
                GetBuy();
                break;
            case R.id.rb_buy_home_note:
                ShowSlider(R.id.view_buy_home_note);
                mStart = 0;
                mState = Constant.STATE_NOTE;
                GetBuy();
                break;
            case R.id.rb_buy_home_cancel:
                ShowSlider(R.id.view_buy_home_cancel);
                mStart = 0;
                mState = Constant.STATE_CANCEL;
                GetBuy();
                break;
            default:
                break;
        }
    }
}
