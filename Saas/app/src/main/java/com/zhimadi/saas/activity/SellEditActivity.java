package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.SellDetailEvent;
import com.zhimadi.saas.event.ProductData;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.BillBuy;
import com.zhimadi.saas.widget.Cat;
import com.zhimadi.saas.widget.CatDetail;
import com.zhimadi.saas.widget.CatDetailItem;
import com.zhimadi.saas.widget.CatItem;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.TextViewItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SellEditActivity extends BaseActivity {

    private TitleBarCommonBuilder titleBarCommonBuilder;

//    //是否可编辑
//    private boolean isEditEnable;

    //传入的数据
    private int intentMode;
    private String sellId;
    private int mState;

    private LinearLayout llSellEditWindow;

    private TextViewItem textViewItemOderNummber;
    private TextViewItem textViewItemMaker;

    private SelelctableTextViewItem selelctableTextViewItemCustom;
    private SelelctableTextViewItem selelctableTextViewItemStore;

    //购物车
    private Cat cat;
    //明细表
    private CatDetail catDetail;
    //消费提示栏
    private BillBuy billBuy;

    private EditTextITem editTextITemPay;
    private TextViewItem textViewItemBillOwe;
    private TextViewItem textViewItemAllOwe;

    private EditTextITem editTextITemNote;

    //底部操作栏
    private LinearLayout llSellEditTab;
    private TextView tvSellEditFirst;
    private TextView tvSellEditSecond;
    private TextView tvSellSum;

    //隐藏post数据
    private String storeId;
    private String customId;

    //缓存商品列表
    private List<CatItem> catItems;

    //存商品的总金额，总重量，总件数
    Map<String, Float> mapNumber;
    Map<String, Float> mapWeight;
    Map<String, Float> mapTotal;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", Constant.DEFAULT_MODE);
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llSellEditWindow = (LinearLayout) findViewById(R.id.ll_sell_edit_window);
        textViewItemOderNummber = new TextViewItem(mContext, null, "单号");

        selelctableTextViewItemCustom = new SelelctableTextViewItem(mContext, null, "客户", "请选择客户");
        selelctableTextViewItemStore = new SelelctableTextViewItem(mContext, null, "仓库", "请选择仓库");
        editTextITemPay = new EditTextITem(mContext, null, "实收金额", "请输入实收金额");
        textViewItemBillOwe = new TextViewItem(mContext, null, "本单欠款", "");
        textViewItemAllOwe = new TextViewItem(mContext, null, "客户尚欠款", "");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        tvSellEditFirst = (TextView) findViewById(R.id.tv_sell_edit_first);
        tvSellEditSecond = (TextView) findViewById(R.id.tv_sell_edit_second);
        tvSellSum = (TextView) findViewById(R.id.tv_sell_sum);
        llSellEditTab = (LinearLayout) findViewById(R.id.ll_sell_edit_tab);

        //第一步判断可不可以编辑
        if (intentMode == Constant.EDIT_MODE) {
            /***
             * 编辑状态下只有草稿能编辑，还要特殊对待
             */
            if (mState == Constant.STATE_NOTE) {
                setEditEnable(true);
//                isEditEnable = true;
            } else {
                setEditEnable(false);
//                isEditEnable = false;
            }
        } else {
            setEditEnable(true);
//            isEditEnable = true;
        }

        //第二步选择加载的内容
        //如果是草稿或者新建就写自己名字，其他就写传进来的
        //根据可否编辑改变账单样式
        //不可编辑加载明细，可编辑加载购物车
        if (intentMode == Constant.EDIT_MODE) {
            mState = Integer.valueOf(getIntent().getStringExtra("STATE"));
            sellId = getIntent().getStringExtra("SELL_ID");

            if (mState == Constant.STATE_NOTE) {
                cat = new Cat(mContext, null);
                billBuy = new BillBuy(mContext, null, true);
                textViewItemMaker = new TextViewItem(mContext, null, "制单人");
                catItems = new ArrayList<CatItem>();
                mapNumber = new HashMap<String, Float>();
                mapWeight = new HashMap<String, Float>();
                mapTotal = new HashMap<String, Float>();
            } else {
                //销售明细
                catDetail = new CatDetail(mContext, null);
                billBuy = new BillBuy(mContext, null, false);
                textViewItemMaker = new TextViewItem(mContext, null, "制单人");
            }
        } else {
            cat = new Cat(mContext, null);
            billBuy = new BillBuy(mContext, null, true);
            catItems = new ArrayList<CatItem>();
            mapNumber = new HashMap<String, Float>();
            mapWeight = new HashMap<String, Float>();
            mapTotal = new HashMap<String, Float>();
            textViewItemMaker = new TextViewItem(mContext, null, "制单人", UserInfo.getmUserName());
        }



        //第三步根据订单状态修改底部操作栏
        if (intentMode == Constant.EDIT_MODE) {
            switch (mState) {
                case Constant.STATE_DEFAULT:
                    llSellEditTab.setVisibility(View.VISIBLE);
                    tvSellEditFirst.setBackgroundResource(R.color.colorOrange);
                    tvSellEditFirst.setText("撤销");
                    tvSellEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvSellEditSecond.setBackgroundResource(R.color.colorSecondBody);
                    tvSellEditSecond.setText("待审核");
                    tvSellEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_FINISH:
                    llSellEditTab.setVisibility(View.VISIBLE);
                    tvSellEditFirst.setBackgroundResource(R.color.colorOrange);
                    tvSellEditFirst.setText("撤销");
                    tvSellEditSecond.setBackgroundResource(R.color.colorBlue);
                    tvSellEditSecond.setText("打印");
                    tvSellEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvSellEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_NOTE:
                    tvSellEditFirst.setBackgroundResource(R.color.colorWhite);
                    tvSellEditFirst.setText("保存");
                    tvSellEditSecond.setBackgroundResource(R.color.colorSecondBody);
                    tvSellEditSecond.setText("采购");
                    tvSellEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvSellEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_CANCEL:
                    break;
                default:
                    finish();
                    break;
            }
        } else {
            llSellEditTab.setVisibility(View.VISIBLE);
            tvSellEditFirst.setBackgroundResource(R.color.colorAquamarine);
            tvSellEditFirst.setText("草稿");
            tvSellEditFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sellPost();
                }
            });
            tvSellEditSecond.setBackgroundResource(R.color.colorSaffronYellow);
            tvSellEditSecond.setText("采购");
            tvSellEditSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sellPost();
                }
            });
        }


    }

    //设置不可按，把输入提示语去掉，箭头弄掉
    private void setEditEnable(boolean enable) {
        selelctableTextViewItemCustom.setEnabled(enable);
        selelctableTextViewItemStore.setEnabled(enable);
        editTextITemPay.setEditEnable(enable);
        editTextITemNote.setEditEnable(enable);
        if (!enable) {
            editTextITemNote.SetHint("");
            editTextITemNote.SetHint("");
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_edit);
        inte();
        titleBarCommonBuilder.setTitle("销售单").setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        selelctableTextViewItemCustom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, CustomSelectActivity.class);
                        startActivityForResult(intent, Constant.REQUEST_CODE_FOR_SUPPLIER);
                    }
                });

        selelctableTextViewItemStore
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, StoreSelectActivity.class);
                        startActivityForResult(intent, Constant.REQUEST_CODE_FOR_STORE);
                    }
                });

        editTextITemPay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editTextITemPay.GetItemValue().equals("")) {
                    textViewItemBillOwe.SetItemValue(billBuy.getPay() + "");
                } else {
                    textViewItemBillOwe.SetItemValue(billBuy.getPay() - Float.valueOf(editTextITemPay.GetItemValue()) + "");
                }

            }
        });
        textViewItemOderNummber.isTop(true);
        llSellEditWindow.addView(textViewItemOderNummber);
        textViewItemMaker.isBottom(true);
        llSellEditWindow.addView(textViewItemMaker);

        selelctableTextViewItemCustom.isTop(true);
        llSellEditWindow.addView(selelctableTextViewItemCustom);
        selelctableTextViewItemStore.isBottom(true);
        llSellEditWindow.addView(selelctableTextViewItemStore);

        //通过是否为空，判断加载购物车还是明细
        if (cat != null) {
            llSellEditWindow.addView(cat);
            //添加监听
            cat.setOnClickListenerAdd(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, ProductSelectActivity.class);
                    List<String> productIds = new ArrayList<String>();
                    if (catItems.size() > 0) {
                        for (int i = 0; i < catItems.size(); i++) {
                            productIds.add(catItems.get(i).getProductId());
                        }
                        Bundle mBundle = new Bundle();
                        intent.putExtra("PRODUCT_IDS", (Serializable) productIds);
                        intent.putExtras(mBundle);
                    }
                    startActivityForResult(intent, Constant.REQUEST_CODE_FOR_PRODUCT);
                }
            });
            //编辑监听
            cat.setOnClickListenerEdit(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            billBuy.isTop(true);
            llSellEditWindow.addView(billBuy);
        } else if (catDetail != null) {
            llSellEditWindow.addView(catDetail);
            billBuy.isBottom(true);
            llSellEditWindow.addView(billBuy);
            editTextITemPay.isTop(true);
        }


        llSellEditWindow.addView(editTextITemPay);
        llSellEditWindow.addView(textViewItemBillOwe);
        textViewItemAllOwe.isBottom(true);
        llSellEditWindow.addView(textViewItemAllOwe);

        editTextITemNote.isTop(true).isBottom(true);
        llSellEditWindow.addView(editTextITemNote);

        //最后才加载
        if (intentMode == Constant.EDIT_MODE) {
            getSellDetail();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_FOR_SUPPLIER:
                if (resultCode == CustomSelectActivity.RESULT_CODE_SUCCESS) {
                    customId = data.getStringExtra("SUPPLIER_ID");
                    selelctableTextViewItemCustom.SetItemValue(data.getStringExtra("SUPPLIER_NAME"));
                }
                break;
            case Constant.REQUEST_CODE_FOR_STORE:
                if (resultCode == CustomSelectActivity.RESULT_CODE_SUCCESS) {
                    storeId = data.getStringExtra("STORE_ID");
                    selelctableTextViewItemStore.SetItemValue(data.getStringExtra("STORE_NAME"));
                }
                break;

            case Constant.REQUEST_CODE_FOR_PRODUCT:
                if (resultCode == ProductSelectActivity.RESULT_SUCCESS) {
                    //传过来的玩意
                    List<ProductData> productDatas = (List<ProductData>) data.getExtras().getSerializable("PRODUCT_ARRAY");
                    List<String> productIds = (List<String>) data.getExtras().getSerializable("PRODUCTIDS_ARRY");

                    Toast.makeText(SellEditActivity.this, "返回" + productIds.size() + "个id", Toast.LENGTH_SHORT).show();
                    Toast.makeText(SellEditActivity.this, "返回" + productDatas.size() + "对象", Toast.LENGTH_SHORT).show();
                    //缓存要删掉的东西
                    List<ProductData> productDatasTemp = new ArrayList<ProductData>();
                    List<CatItem> catItemsTemp = new ArrayList<CatItem>();
                    List<String> productIdsTemp = new ArrayList<String>();

                    //第一步：返回的数据预处理，把购物车出现过的剔除掉
                    for (int i = 0; i < productDatas.size(); i++) {
                        for (int k = 0; k < catItems.size(); k++) {
                            if (productDatas.get(i).getProductId().equals(catItems.get(k).getProductId())) {
                                productDatasTemp.add(productDatas.get(i));
                            }
                        }
                    }


                    //第二步：从购物车中删掉返回没有的数据,不过如果先前已经选中的因为没点击或者没加载并不会返回，这里需要分辨没有返回的到底有没有选中，好绕 = =
                    //所以要一个id数组来分辨
                    boolean isExist;
                    for (int i = 0; i < catItems.size(); i++) {
                        isExist = false;
                        for (int k = 0; k < productIds.size(); k++) {
                            if (productIds.get(k).equals(catItems.get(i).getProductId())) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            productIdsTemp.add(catItems.get(i).getProductId());
                            catItemsTemp.add(catItems.get(i));
                        }
                    }


                    productDatas.removeAll(productDatasTemp);

                    catItems.removeAll(catItemsTemp);
                    cat.removeAllViews(productIdsTemp, billBuy);


                    //这里留下来的是不重复的
                    for (int i = 0; i < productDatas.size(); i++) {
                        final ProductData productData = productDatas.get(i);
                        CatItem catItem = new CatItem(mContext, null, productData.getProductId(), productData.getName(), productData.getFormat(), productData.getIsFit(), productData.getFitWeight(), new CatItem.TextChangeListener() {
                            @Override
                            public void textChange(float number, float weight, float total) {
                                mapNumber.put(productData.getProductId(), number);
                                Float sum = new Float(0);
                                for (Float temp : mapNumber.values()) {
                                    sum += temp;
                                }
                                billBuy.setNumber(sum);

                                mapWeight.put(productData.getProductId(), weight);
                                sum = 0f;
                                for (Float temp : mapWeight.values()) {
                                    sum += temp;
                                }
                                billBuy.setWeight(sum);


                                mapTotal.put(productData.getProductId(), total);
                                sum = 0f;
                                for (Float temp : mapTotal.values()) {
                                    sum += temp;
                                }
                                billBuy.setPay(sum);
                                tvSellSum.setText("￥" + sum);
                                if (editTextITemPay.GetItemValue().equals("")) {
                                    textViewItemBillOwe.SetItemValue(sum + "");
                                } else {
                                    textViewItemBillOwe.SetItemValue(sum - Integer.valueOf(editTextITemPay.GetItemValue().toString()) + "");
                                }
                            }
                        });
                        //之前的去边最后一个改边
                        if (i == productDatas.size() - 1) {
                            catItem.isBottom(true);
                        } else {
                            catItem.isBottom(false);
                        }
                        cat.addView(catItem);
                        //缓存比较
                        catItems.add(catItem);
                    }
                }

                break;
            default:
                break;
        }
    }

    private void getSellDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("sell_id", sellId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mSellDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SellDetailEvent sellDetailEvent = gson.fromJson(response, SellDetailEvent.class);
                textViewItemOderNummber.SetItemValue(sellDetailEvent.getData().getOrder_no());
                textViewItemMaker.SetItemValue(sellDetailEvent.getData().getCreate_user_name());
//                selelctableTextViewItem2Custom.SetItemValue(sellDetailEvent.getData().getCustom_name());
                selelctableTextViewItemStore.SetItemValue(sellDetailEvent.getData().getWarehouse_name());

                billBuy.setNumber(sellDetailEvent.getData().getTotal_package());
                billBuy.setPay(sellDetailEvent.getData().getTotal_amount());
                billBuy.setWeight(sellDetailEvent.getData().getTotal_weight());

//                editTextITem2Pay.SetItemValue(sellDetailEvent.getData().getPaid_amount());
                textViewItemBillOwe.SetItemValue(sellDetailEvent.getData().getTotal_owed());
                textViewItemAllOwe.SetItemValue(sellDetailEvent.getData().getOwed_amount());
                editTextITemNote.SetItemValue(sellDetailEvent.getData().getNote());

                if (cat != null) {
                    for (int i = 0; i < sellDetailEvent.getData().getProducts().size(); i++) {
                        final SellDetailEvent.Product product = sellDetailEvent.getData().getProducts().get(i);
                        CatItem catItem = new CatItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), Float.valueOf(product.getWeight()), new CatItem.TextChangeListener() {

                            @Override
                            public void textChange(float number, float weight, float total) {

                                mapNumber.put(product.getProduct_id(), total);
                                Float sum = new Float(0);
                                for (Float temp : mapNumber.values()) {
                                    sum += temp;
                                }
                                billBuy.setNumber(sum + "");

                                mapWeight.put(product.getProduct_id(), total);
                                sum = 0f;
                                for (Float temp : mapWeight.values()) {
                                    sum += temp;
                                }
                                billBuy.setWeight(sum + "");
                                mapTotal.put(product.getProduct_id(), total);
                                sum = 0f;
                                for (Float temp : mapTotal.values()) {
                                    sum += temp;
                                }
                                billBuy.setPay(sum + "");

                            }
                        });
                        if (i == sellDetailEvent.getData().getProducts().size() - 1) {
                            catItem.isBottom(true);
                        } else {
                            catItem.isBottom(false);
                        }

                        cat.addView(catItem);
                    }
                } else if (catDetail != null) {
                    for (int i = 0; i < sellDetailEvent.getData().getProducts().size(); i++) {
                        SellDetailEvent.Product product = sellDetailEvent.getData().getProducts().get(i);
                        CatDetailItem catDetailItem = new CatDetailItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), product.getQuantity(), product.getPrice_unit(), product.getWeight());
                        if (i == sellDetailEvent.getData().getProducts().size() - 1) {
                            catDetailItem.isBottom(true);
                        }
                        catDetail.addView(catDetailItem);
                    }
                }
//                customId = sellDetailEvent.getData().getCustom_id();
                storeId = sellDetailEvent.getData().getWarehouse_id();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void sellPost() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == Constant.EDIT_MODE) {
            jsonObject.addProperty("sell_id", sellId);
        }
        jsonObject.addProperty("warehouse_id", storeId);
        jsonObject.addProperty("custom_id", customId);
        jsonObject.addProperty("paid_amount", editTextITemPay.GetItemValue());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
        jsonObject.addProperty("state", mState + "");
        //组装product
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < catItems.size(); i++) {
            CatItem catItem = catItems.get(i);
            JsonObject product = new JsonObject();
            product.addProperty("weight", catItem.getWeight());
            product.addProperty("product_id", catItem.getProductId());
            product.addProperty("package", catItem.getNumber());
            product.addProperty("price", catItem.getUnitPrice());
            //1：公斤   2：件
            product.addProperty("price_unit", catItem.getUnit() + "");
            jsonArray.add(product);
        }
        jsonObject.add("products", jsonArray);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditSellAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                finish();
            }

            @Override
            public void onFail() {

            }
        });
    }

}
