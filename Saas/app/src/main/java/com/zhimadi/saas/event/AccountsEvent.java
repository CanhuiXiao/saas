//package com.zhimadi.saas.event;
//
//import java.io.Serializable;
//import java.util.List;
//
///**
// * Created by carry on 2016/7/14.
// */
//public class AccountsEvent {
//
//
//    private int code;
//    private String msg;
//    private Data data;
//
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public class Data {
//        private String count;
//        private List<Account> list;
//
//
//        public String getCount() {
//            return count;
//        }
//
//        public void setCount(String count) {
//            this.count = count;
//        }
//
//        public List<Account> getList() {
//            return list;
//        }
//
//        public void setList(List<Account> list) {
//            this.list = list;
//        }
//    }
//
//    public class Account {
//
//        private String account_id;
//        private String name;
//        private String shop_id;
//        private String shop_name;
//        private String balance;
//
//
//        public String getAccount_id() {
//            return account_id;
//        }
//
//        public void setAccount_id(String account_id) {
//            this.account_id = account_id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getShop_id() {
//            return shop_id;
//        }
//
//        public void setShop_id(String shop_id) {
//            this.shop_id = shop_id;
//        }
//
//        public String getShop_name() {
//            return shop_name;
//        }
//
//        public void setShop_name(String shop_name) {
//            this.shop_name = shop_name;
//        }
//
//        public String getBalance() {
//            return balance;
//        }
//
//        public void setBalance(String balance) {
//            this.balance = balance;
//        }
//    }
//
//}
