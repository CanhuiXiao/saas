package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by carry on 2016/7/11.
 */
public class AppDeskTop extends ViewGroup {

    private Context mContext;

    //动态设置列数
    private int colCount = 1;
    private int rowCount;

    //左边距
    private int leftPadding = 0;
    //右边距
    private int rightPadding = 0;
    //上边距
    private int topPadding = 0;
    //下边距
    private int bottomPadding = 0;

    //行间距
    private int rowSpace = 0;
    //列间距
    private int colSpace = 0;

    //屏幕大小
    private int screenWidth;
    private int screenHeight;



    private void inte(Context context){
        mContext = context;
    }

    public AppDeskTop(Context context) {
        super(context);
        inte(context);
    }

    public AppDeskTop(Context context, AttributeSet attrs) {
        super(context, attrs);
        inte(context);
    }

    public AppDeskTop(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inte(context);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        final int width = MeasureSpec.getSize(widthMeasureSpec);
        MeasureSpec.getMode(widthMeasureSpec);

        final int height = MeasureSpec.getSize(heightMeasureSpec);
        MeasureSpec.getMode(heightMeasureSpec);

        screenWidth = width;
        screenHeight = height;

        int usedWidth = width - leftPadding - rightPadding - (colCount - 1) * colSpace;

        int usedheight = ((height - topPadding - bottomPadding - (rowCount - 1) * rowSpace));

        int childWidth = usedWidth / colCount;
        int childHeight = usedheight / rowCount;
        final int count = getChildCount();

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);
            int childWidthSpec = getChildMeasureSpec(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY), 20, childWidth);
            int childHeightSpec = getChildMeasureSpec(MeasureSpec.makeMeasureSpec(childHeight,MeasureSpec.EXACTLY), 20, childHeight);
            child.measure(childWidthSpec, childHeightSpec);
        }


    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {




    }





}
