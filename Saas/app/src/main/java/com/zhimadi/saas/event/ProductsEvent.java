package com.zhimadi.saas.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class ProductsEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Product> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Product> getList() {
            return list;
        }

        public void setList(List<Product> list) {
            this.list = list;
        }
    }

    public class Product {
        private String product_id;
        private String category_id;
        private String type_id;
        private String cat_id;
        private String warehouse_id;
        private String name;
        private String description;
        private String selling_price;
        private String purchase_price;
        private String sku;
        private String is_fixed;
        private String fixed_weight;
        private String manufacturer_part_number;
        private String img_url;
        private String mini_num;
        private String quantity;
        private String is_stock_warning;
        private String warn_number_low;
        private String warn_number_top;
        private String status;
        private String is_sell;
        private String display_order;

        private String pic_urls;
        private String category_name;
        private String type_name;


        private String cat_name;

        private String props;
        private String props_name;

        private String weight;

        @SerializedName("package")
        private String package_;


        //私用，非服务器传，默认非选
        private boolean isSelect = false;


        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getCat_id() {
            return cat_id;
        }

        public void setCat_id(String cat_id) {
            this.cat_id = cat_id;
        }

        public String getWarehouse_id() {
            return warehouse_id;
        }

        public void setWarehouse_id(String warehouse_id) {
            this.warehouse_id = warehouse_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSelling_price() {
            return selling_price;
        }

        public void setSelling_price(String selling_price) {
            this.selling_price = selling_price;
        }

        public String getPurchase_price() {
            return purchase_price;
        }

        public void setPurchase_price(String purchase_price) {
            this.purchase_price = purchase_price;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getIs_fixed() {
            return is_fixed;
        }

        public void setIs_fixed(String is_fixed) {
            this.is_fixed = is_fixed;
        }

        public String getFixed_weight() {
            return fixed_weight;
        }

        public void setFixed_weight(String fixed_weight) {
            this.fixed_weight = fixed_weight;
        }

        public String getManufacturer_part_number() {
            return manufacturer_part_number;
        }

        public void setManufacturer_part_number(String manufacturer_part_number) {
            this.manufacturer_part_number = manufacturer_part_number;
        }

        public String getImg_url() {
            return img_url;
        }

        public void setImg_url(String img_url) {
            this.img_url = img_url;
        }

        public String getMini_num() {
            return mini_num;
        }

        public void setMini_num(String mini_num) {
            this.mini_num = mini_num;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getIs_stock_warning() {
            return is_stock_warning;
        }

        public void setIs_stock_warning(String is_stock_warning) {
            this.is_stock_warning = is_stock_warning;
        }

        public String getWarn_number_low() {
            return warn_number_low;
        }

        public void setWarn_number_low(String warn_number_low) {
            this.warn_number_low = warn_number_low;
        }

        public String getWarn_number_top() {
            return warn_number_top;
        }

        public void setWarn_number_top(String warn_number_top) {
            this.warn_number_top = warn_number_top;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIs_sell() {
            return is_sell;
        }

        public void setIs_sell(String is_sell) {
            this.is_sell = is_sell;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }

        public String getProps() {
            return props;
        }

        public void setProps(String props) {
            this.props = props;
        }

        public String getPic_urls() {
            return pic_urls;
        }

        public void setPic_urls(String pic_urls) {
            this.pic_urls = pic_urls;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }

        public String getCat_name() {
            return cat_name;
        }

        public void setCat_name(String cat_name) {
            this.cat_name = cat_name;
        }

        public String getProps_name() {
            return props_name;
        }

        public void setProps_name(String props_name) {
            this.props_name = props_name;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPackage_() {
            return package_;
        }

        public void setPackage_(String package_) {
            this.package_ = package_;
        }

        public boolean isSelect() {
            return isSelect;
        }

        public void setSelect(boolean select) {
            isSelect = select;
        }
    }

}
