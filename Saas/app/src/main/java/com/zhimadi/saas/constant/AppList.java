package com.zhimadi.saas.constant;

import android.app.Activity;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.AgentHomeActivity;
import com.zhimadi.saas.activity.BuyEditActivity;
import com.zhimadi.saas.activity.BuyHomeActivity;
import com.zhimadi.saas.activity.BuyReturnHomeActivity;
import com.zhimadi.saas.activity.CustomHomeActivity;
import com.zhimadi.saas.activity.CustomTypeHomeActivity;
import com.zhimadi.saas.activity.CustonLogHomeActivity;
import com.zhimadi.saas.activity.DefinedCategoryHomeActivity;
import com.zhimadi.saas.activity.ProductHomeActivity;
import com.zhimadi.saas.activity.SellAgentHomeActivity;
import com.zhimadi.saas.activity.SellEditActivity;
import com.zhimadi.saas.activity.SellHomeActivity;
import com.zhimadi.saas.activity.StockAllotHomeActivity;
import com.zhimadi.saas.activity.StockCheckHomeActivity;
import com.zhimadi.saas.activity.StockHomeActivity;
import com.zhimadi.saas.activity.StockLossHomeActivity;
import com.zhimadi.saas.activity.StoreHomeActivity;
import com.zhimadi.saas.activity.SupplierHomeActivity;

/**
 * Created by carry on 2016/8/26.
 */
public class AppList {

public enum App {

    /* 采购管理 */
    PurchaseOrderCell("采购单", BuyEditActivity.class, R.drawable.cai_gou_dan),
    PurchaseReturnOrderCell("采购退货单", BuyReturnHomeActivity.class, R.drawable.cai_gou_dan),
    PurchaseSupplierCell("供应商管理", SupplierHomeActivity.class, R.drawable.cai_gou_dan),
    PurchaseOrderHistoryCell("采购历史", BuyHomeActivity.class, R.drawable.cai_gou_dan),
    PurchaseReturnHistoryCell("采购退货历史", BuyReturnHomeActivity.class, R.drawable.cai_gou_dan),
    SaleOrderCell("销售单", SellEditActivity.class, R.drawable.cai_gou_dan),
    SaleReturnOrderCell("销售退货单", null, R.drawable.cai_gou_dan),
    CustomerManagementCell("客户管理", CustomHomeActivity.class, R.drawable.cai_gou_dan),
    CustomerSortCell("客户分类", CustomTypeHomeActivity.class, R.drawable.cai_gou_dan),
    SaleHistoryCell("销售历史", SellHomeActivity.class, R.drawable.cai_gou_dan),
    SaleReturnHistoryCell("销售退货历史", null, R.drawable.cai_gou_dan),
    WarehouseManagement("仓库管理", StoreHomeActivity.class, R.drawable.cai_gou_dan),
    WaterOfWarehouse("库存流水", StockAllotHomeActivity.class, R.drawable.cai_gou_dan),
    StockLoss("报损单", null, R.drawable.cai_gou_dan),
    WarehouseInventory("仓库盘点", null, R.drawable.cai_gou_dan),
    WarehouseRequisition("仓库调拨单", null, R.drawable.cai_gou_dan),
    HistoryOfWarehouses("仓库调拨历史", null, R.drawable.cai_gou_dan),
    InventoryHistory("盘点历史", StockCheckHomeActivity.class, R.drawable.cai_gou_dan),
    StockLossHistory("报损历史", StockLossHomeActivity.class, R.drawable.cai_gou_dan),
    SettlementAccount("结算账户", null, R.drawable.cai_gou_dan),
    OwnerWaterCheckForFriendsStuff("代卖货主流水对账", null, R.drawable.cai_gou_dan),
    FriendsStuffWaterReconciliation("代卖商流水对账", null, R.drawable.cai_gou_dan),
    WaterSuppliers("供应商流水对账", null, R.drawable.cai_gou_dan),
    ClientReconciliationWater("客户流水对账", CustonLogHomeActivity.class, R.drawable.cai_gou_dan),
    InventoryQuery("库存查询", StockHomeActivity.class, R.drawable.cai_gou_dan),
    AccountReconciliationWater("账户流水对账", null, R.drawable.cai_gou_dan),
    AccountType("账目类型", null, R.drawable.cai_gou_dan),
    CommodityManagement("商品管理", ProductHomeActivity.class, R.drawable.cai_gou_dan),
    CommodityClassification("商品分类管理", DefinedCategoryHomeActivity.class, R.drawable.cai_gou_dan),
    InventoryWarning("商品库存预警", null, R.drawable.cai_gou_dan),
    FriendsStuffAlone("代卖单", null, R.drawable.cai_gou_dan),
    FriendsStuffHistory("代卖历史", SellAgentHomeActivity.class, R.drawable.cai_gou_dan),
    FriendsStuffBusinessManagement("代卖商管理", AgentHomeActivity.class, R.drawable.cai_gou_dan),
//    OwnerManagement("货主管理", OwnerHomeActivity.class, R.drawable.cai_gou_dan),
    /* 报表*/
    PurchasingReport("采购报表", null, R.drawable.cai_gou_dan),
    SalesReports("销售报表", null, R.drawable.cai_gou_dan),
    CustomerSales("客户销量", null, R.drawable.cai_gou_dan),
    NetThan("进销比", null, R.drawable.cai_gou_dan),
    BusinessSummary("经营汇总", null, R.drawable.cai_gou_dan),
    CommoditySales("商品销量", null, R.drawable.cai_gou_dan),
    FriendsStuffSummary("代卖汇总", null, R.drawable.cai_gou_dan);
//    Rank("业绩排名", null, R.drawable.cai_gou_dan);

    private String title;
    private Class<? extends Activity> activity;
    private int drawableResource;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<? extends Activity> getActivity() {
        return activity;
    }

    public void setActivity(Class<? extends Activity> activity) {
        this.activity = activity;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    App(String title, Class<? extends Activity> activity, int drawableResource) {
        this.title = title;
        this.activity = activity;
        this.drawableResource = drawableResource;
    }

    //手写过滤器惨惨惨...
    public static App getApp(int value) {    //手写的从int到enum的转换函数
        switch (value) {
            case 1:
                return App.PurchaseOrderCell;
            case 2:
                return App.PurchaseReturnOrderCell;
            case 3:
                return App.PurchaseSupplierCell;
            case 4:
                return App.PurchaseOrderHistoryCell;
            case 5:
                return App.PurchaseReturnHistoryCell;

        /* 销售管理 */
            case 6:
                return App.SaleOrderCell;
            case 7:
                return App.SaleReturnOrderCell;
            case 8:
                return App.CustomerManagementCell;
            case 9:
                return App.CustomerSortCell;
            case 10:
                return App.SaleHistoryCell;
            case 11:
                return App.SaleReturnHistoryCell;

        /* 仓库管理 */
            case 12:
                return App.WarehouseManagement;
            case 16:
                return App.WaterOfWarehouse;
            case 19:
                return App.StockLossHistory;
            case 13:
                return App.WarehouseInventory;
            case 15:
                return App.WarehouseRequisition;
            case 18:
                return App.HistoryOfWarehouses;
            case 17:
                return App.InventoryHistory;
            case 14:
                return App.StockLoss;

        /* 财务管理 */
            case 26:
                return App.OwnerWaterCheckForFriendsStuff;
            case 27:
                return App.FriendsStuffWaterReconciliation;
            case 22:
                return App.WaterSuppliers;
            case 21:
                return App.SettlementAccount;
            case 23:
                return App.ClientReconciliationWater;
            case 25:
                return App.InventoryQuery;
            case 24:
                return App.AccountReconciliationWater;
            case 20:
                return App.AccountType;

        /* 运营*/
            case 28:
                return App.CommodityManagement;
            case 29:
                return App.CommodityClassification;
            case 30:
                return App.InventoryWarning;
            case 32:
                return App.FriendsStuffAlone;
            case 31:
                return App.FriendsStuffHistory;
            case 33:
                return App.FriendsStuffBusinessManagement;
//            case 0:
//                return App.OwnerManagement;

        /* 报表*/
            case 34:
                return App.PurchasingReport;
            case 35:
                return App.SalesReports;
            case 36:
                return App.CustomerSales;
            case 37:
                return App.NetThan;
            case 38:
                return App.BusinessSummary;
            case 39:
                return App.CommoditySales;
            case 40:
                return App.FriendsStuffSummary;
//            case 42:
//                return App.Rank;
            default:
                return null;
        }
    }
}

}
