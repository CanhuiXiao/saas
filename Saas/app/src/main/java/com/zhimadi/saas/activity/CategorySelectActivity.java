package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CategoriesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.GridLayoutBySameChild;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.List;

public class CategorySelectActivity extends BaseActivity {



    public static final int resultCodeSuccess = 1;
    public static final int resultCodetFail = 0;

    //请求码
    public static final int requestCodeForProductType = 1;

    private String categoryId;
    private String categoryName;
    private String typeId;
    private String typeName;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private GridLayoutBySameChild glCategoryHome;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        glCategoryHome = (GridLayoutBySameChild) findViewById(R.id.gl_category_home);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_home);
        inte();
        titleBarCommonBuilder.setTitle("水果类")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        glCategoryHome.setColumnCount(4);
        glCategoryHome.setMarginVertical(DisplayUtil.dip2px(mContext, 5.76f));
        glCategoryHome.setMarginHorizontal(DisplayUtil.dip2px(mContext, 4.8f));
        GetCategory();
    }



    private void GetCategory() {
        AsyncUtil.asyncGet(mContext, OkHttpUtil.mCategoryAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CategoriesEvent categoriesEvent = gson.fromJson(response, CategoriesEvent.class);
                final List<CategoriesEvent.Category> categories = categoriesEvent.getData().getList();
                for (int i = 0; i < categories.size(); i++) {
                    TextView textView = new TextView(mContext);
                    textView.setText(categories.get(i).getName());
                    RadioGroupWithLayout.LayoutParams layoutParams = new RadioGroupWithLayout.LayoutParams(DisplayUtil.dip2px(mContext, 76.8f), DisplayUtil.dip2px(mContext, 26.4f));
                    textView.setLayoutParams(layoutParams);
                    textView.setGravity(Gravity.CENTER);
                    textView.setTextSize(12);
                    textView.setTextColor(Color.rgb(0x36, 0x36, 0x36));
                    final int d = i;
                    textView.setBackgroundResource(R.drawable.sel_press_category_home);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent();
                            intent.setClass(mContext, ProductTypeSelectActivity.class);
                            intent.putExtra("CATEGORY_ID", categories.get(d).getCategory_id());
                            intent.putExtra("CATEGORY_NAME", categories.get(d).getName());
                            startActivityForResult(intent, requestCodeForProductType);
                            categoryId = categories.get(d).getCategory_id();
                            categoryName = categories.get(d).getName();
                        }
                    });
                    glCategoryHome.addView(textView);
                }

            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case requestCodeForProductType:
                if(resultCode == ProductTypeSelectActivity.resultCodeSuccess){
                    typeId = data.getStringExtra("TYPE_ID");
                    typeName = data.getStringExtra("TYPE_NAME");
                    Intent intent = new Intent();
                    intent.putExtra("TYPE_ID", typeId);
                    intent.putExtra("TYPE_NAME", typeName);
                    intent.putExtra("CATEGORY_ID", categoryId);
                    intent.putExtra("CATEGORY_NAME", categoryName);
                    setResult(resultCodeSuccess, intent);
                    finish();
                }

                break;
            default:
                break;
        }
    }
}
