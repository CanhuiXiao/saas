package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AreaEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class AreaAdapter extends ArrayAdapter<AreaEvent.Area> {

    private Context mContext;
    private int resourceId;

    public AreaAdapter(Context context, int resource, List<AreaEvent.Area> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AreaEvent.Area area = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvAreaName.setText(area.getN());
        return convertView;
    }

    class ViewHolder {
        private TextView tvAreaName;
        private ViewHolder(View view) {
            tvAreaName = (TextView) view.findViewById(R.id.tv_area_name);
            view.setTag(this);
        }
    }


}
