package com.zhimadi.saas.event;

/**
 * Created by carry on 2016/7/21.
 */
public class CategoryDetailEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String category_id;
        private String name;
        private String description;
        private String parent_category_id;
        private String is_published;
        private String is_deleted;
        private String display_order;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getParent_category_id() {
            return parent_category_id;
        }

        public void setParent_category_id(String parent_category_id) {
            this.parent_category_id = parent_category_id;
        }

        public String getIs_published() {
            return is_published;
        }

        public void setIs_published(String is_published) {
            this.is_published = is_published;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }
    }

}
