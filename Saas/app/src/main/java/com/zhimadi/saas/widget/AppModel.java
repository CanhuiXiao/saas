package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AppsEvent;

public class AppModel extends LinearLayout {

    private View returnView;
    private Context mContext;
    private LinearLayout llAppHomeModel;
    private TextView tvAppHomeModelTitle;
    private int cateId;

    public AppModel(Context context, AttributeSet attrs, AppsEvent.Model model) {
        super(context, attrs);
        mContext = context;
        returnView = LayoutInflater.from(mContext).inflate(R.layout.widget_app_model, this);
        llAppHomeModel = (LinearLayout) findViewById(R.id.ll_app_home_model);
        tvAppHomeModelTitle = (TextView) findViewById(R.id.tv_app_home_model_title);
        tvAppHomeModelTitle.setText(model.getCate().getName());
        addItems(model);
    }

    public void addItems(AppsEvent.Model model) {
        for (int i = 0; i < model.getList().size(); i++) {
            llAppHomeModel.addView(new AppModelItem(mContext, null, model.getList().get(i)));
        }
    }

}
