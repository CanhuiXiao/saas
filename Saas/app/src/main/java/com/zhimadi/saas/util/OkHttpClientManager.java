package com.zhimadi.saas.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;

import com.google.gson.Gson;

import com.google.gson.internal.$Gson$Types;

import okhttp3.Call;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.FormBody;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Callback;

public class OkHttpClientManager {
	private static OkHttpClientManager mInstance;
	private OkHttpClient mOkHttpClient;
	private Handler mDelivery;
	private Gson mGson;
	public static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");
	public static final MediaType URLENCODED = MediaType.parse("application/x-www-form-urlencoded");
	
	private static final String TAG = "OkHttpClientManager";

	private OkHttpClientManager() {
		mOkHttpClient = new OkHttpClient();
		// cookie enabled
//		mOkHttpClient.setCookieHandler(new CookieManager(null, CookiePolicy.ACCEPT_ORIGINAL_SERVER));
		mDelivery = new Handler(Looper.getMainLooper());
		mGson = new Gson();
	}


	public static OkHttpClientManager getInstance() {
		if (mInstance == null) {
			synchronized (OkHttpClientManager.class) {
				if (mInstance == null) {
					mInstance = new OkHttpClientManager();
				}
			}
		}
		return mInstance;
	}


	private Response _getAsyn(String url) throws IOException {
		final Request request = new Request.Builder().url(url).build();
		Call call = mOkHttpClient.newCall(request);
		Response execute = call.execute();
		return execute;
	}


	private String _getAsString(String url) throws IOException {
		Response execute = _getAsyn(url);
		return execute.body().string();
	}


	private void _getAsyn(Request request, final ResultCallback callback) {

		deliveryResult(callback, request);
	}


	private Response _post(Request.Builder requestBuilder, Param... params)
			throws IOException {
		Request request = buildPostRequest(requestBuilder, params);
		Response response = mOkHttpClient.newCall(request).execute();
		return response;
	}

	private String _postAsString(Request.Builder requestBuilder,
			Param... params) throws IOException {
		Response response = _post(requestBuilder, params);
		return response.body().string();
	}


	private void _postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, Param... params) {
		Request request = buildPostRequest(requestBuilder, params);
		deliveryResult(callback, request);
	}
	

	private void _postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback) {
		Request request = requestBuilder
				.post(null)
				.build();
		deliveryResult(callback, request);
	}


	private void _postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, Map<String, String> params) {
		Param[] paramsArr = map2Params(params);
		Request request = buildPostRequest(requestBuilder, paramsArr);
		deliveryResult(callback, request);
	}

	

	private void _postJsonAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, String jsonStr) {
		Request request = buildJsonPostRequest(requestBuilder, jsonStr);
		deliveryResult(callback, request);
	}
	

	private void _postUrlencodedAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, String urlencoded) {
		Request request = buildUrlencodedPostRequest(requestBuilder, urlencoded);
		deliveryResult(callback, request);
	}
	

	private Response _post(String url, File[] files, String[] fileKeys,
			Param... params) throws IOException {
		Request request = buildMultipartFormRequest(url, files, fileKeys,
				params);
		return mOkHttpClient.newCall(request).execute();
	}

	private Response _post(String url, File file, String fileKey)
			throws IOException {
		Request request = buildMultipartFormRequest(url, new File[] { file },
				new String[] { fileKey }, null);
		return mOkHttpClient.newCall(request).execute();
	}

	private Response _post(String url, File file, String fileKey,
			Param... params) throws IOException {
		Request request = buildMultipartFormRequest(url, new File[] { file },
				new String[] { fileKey }, params);
		return mOkHttpClient.newCall(request).execute();
	}


	private void _postAsyn(String url, ResultCallback callback, File[] files,
			String[] fileKeys, Param... params) throws IOException {
		Request request = buildMultipartFormRequest(url, files, fileKeys,
				params);
		deliveryResult(callback, request);
	}


	private void _postAsyn(String url, ResultCallback callback, File file,
			String fileKey) throws IOException {
		Request request = buildMultipartFormRequest(url, new File[] { file },
				new String[] { fileKey }, null);
		deliveryResult(callback, request);
	}


	private void _postAsyn(String url, ResultCallback callback, File file,
			String fileKey, Param... params) throws IOException {
		Request request = buildMultipartFormRequest(url, new File[] { file },
				new String[] { fileKey }, params);
		deliveryResult(callback, request);
	}



	private void _downloadAsyn(final String url, final String destFileDir,
			final ResultCallback callback) {
		final Request request = new Request.Builder().url(url).build();
		final Call call = mOkHttpClient.newCall(request);
		call.enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				sendFailedStringCallback(request, e, callback);
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				InputStream is = null;
				byte[] buf = new byte[2048];
				int len = 0;
				FileOutputStream fos = null;
				try {
					is = response.body().byteStream();
					File file = new File(destFileDir, getFileName(url));
					fos = new FileOutputStream(file);
					while ((len = is.read(buf)) != -1) {
						fos.write(buf, 0, len);
					}
					fos.flush();
					sendSuccessResultCallback(file.getAbsolutePath(), callback);
				} catch (IOException e) {
					sendFailedStringCallback(response.request(), e, callback);
				} finally {
					try {
						if (is != null)
							is.close();
					} catch (IOException e) {
					}
					try {
						if (fos != null)
							fos.close();
					} catch (IOException e) {
					}
				}
			}
		});









	}

	private String getFileName(String path) {
		int separatorIndex = path.lastIndexOf("/");
		return (separatorIndex < 0) ? path : path.substring(separatorIndex + 1,
				path.length());
	}


	private void _displayImage(final ImageView view, final String url,
			final int errorResId) {
		final Request request = new Request.Builder().url(url).build();
		Call call = mOkHttpClient.newCall(request);

		call.enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				setErrorResId(view, errorResId);
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				InputStream is = null;
				try {
					is = response.body().byteStream();
					// ImageUtils.ImageSize actualImageSize =
					// ImageUtils.getImageSize(is);
					// ImageUtils.ImageSize imageViewSize =
					// ImageUtils.getImageViewSize(view);
					// int inSampleSize =
					// ImageUtils.calculateInSampleSize(actualImageSize,
					// imageViewSize);
					try {
						is.reset();
					} catch (IOException e) {
						response = _getAsyn(url);
						is = response.body().byteStream();
					}

					BitmapFactory.Options ops = new BitmapFactory.Options();
					ops.inJustDecodeBounds = false;
					// ops.inSampleSize = inSampleSize;
					final Bitmap bm = BitmapFactory.decodeStream(is, null, ops);
					mDelivery.post(new Runnable() {
						@Override
						public void run() {
							view.setImageBitmap(bm);
						}
					});
				} catch (Exception e) {
					setErrorResId(view, errorResId);

				} finally {
					if (is != null)
						try {
							is.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
				}
			}
		});



	}

	private void setErrorResId(final ImageView view, final int errorResId) {
		mDelivery.post(new Runnable() {
			@Override
			public void run() {
				view.setImageResource(errorResId);
			}
		});
	}


	public static Response getAsyn(String url) throws IOException {
		return getInstance()._getAsyn(url);
	}

	public static String getAsString(String url) throws IOException {
		return getInstance()._getAsString(url);
	}

	public static void getAsyn(Request request, ResultCallback callback) {
		getInstance()._getAsyn(request, callback);
	}

	public static Response post(Request.Builder requestBuilder, Param... params)
			throws IOException {
		return getInstance()._post(requestBuilder, params);
	}

	public static String postAsString(Request.Builder requestBuilder,
			Param... params) throws IOException {
		return getInstance()._postAsString(requestBuilder, params);
	}
	
	

	public static void postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, Param... params) {
		getInstance()._postAsyn(requestBuilder, callback, params);
	}

	public static void postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, Map<String, String> params) {
		getInstance()._postAsyn(requestBuilder, callback, params);
	}
	
	public static void postAsyn(Request.Builder requestBuilder,
			final ResultCallback callback ) {
		getInstance()._postAsyn(requestBuilder, callback);
	}
	
	public static void postJsonAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, String jsonStr) {
		getInstance()._postJsonAsyn(requestBuilder, callback, jsonStr);
	}
	
	public static void postUrlencodedAsyn(Request.Builder requestBuilder,
			final ResultCallback callback, String urlencoded) {
		getInstance()._postUrlencodedAsyn(requestBuilder, callback, urlencoded);
	}
	
	public static Response post(String url, File[] files, String[] fileKeys,
			Param... params) throws IOException {
		return getInstance()._post(url, files, fileKeys, params);
	}

	public static Response post(String url, File file, String fileKey)
			throws IOException {
		return getInstance()._post(url, file, fileKey);
	}

	public static Response post(String url, File file, String fileKey,
			Param... params) throws IOException {
		return getInstance()._post(url, file, fileKey, params);
	}

	public static void postAsyn(String url, ResultCallback callback,
			File[] files, String[] fileKeys, Param... params)
			throws IOException {
		getInstance()._postAsyn(url, callback, files, fileKeys, params);
	}

	public static void postAsyn(String url, ResultCallback callback, File file,
			String fileKey) throws IOException {
		getInstance()._postAsyn(url, callback, file, fileKey);
	}

	public static void postAsyn(String url, ResultCallback callback, File file,
			String fileKey, Param... params) throws IOException {
		getInstance()._postAsyn(url, callback, file, fileKey, params);
	}

	public static void displayImage(final ImageView view, String url,
			int errorResId) throws IOException {
		getInstance()._displayImage(view, url, errorResId);
	}

	public static void displayImage(final ImageView view, String url) {
		getInstance()._displayImage(view, url, -1);
	}

	public static void downloadAsyn(String url, String destDir,
			ResultCallback callback) {
		getInstance()._downloadAsyn(url, destDir, callback);
	}


	private Request buildMultipartFormRequest(String url, File[] files,
			String[] fileKeys, Param[] params) {
		params = validateParam(params);


		MultipartBody.Builder builder = new MultipartBody.Builder()
				.setType(MultipartBody.FORM);

		for (Param param : params) {
			builder.addPart(
					Headers.of("Content-Disposition", "form-data; name=\""
							+ param.key + "\""),
					RequestBody.create(null, param.value));
		}
		if (files != null) {
			RequestBody fileBody = null;
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				String fileName = file.getName();
				fileBody = RequestBody.create(
						MediaType.parse(guessMimeType(fileName)), file);
				// TODO �����ļ�������contentType
				builder.addPart(
						Headers.of("Content-Disposition", "form-data; name=\""
								+ fileKeys[i] + "\"; filename=\"" + fileName
								+ "\""), fileBody);
			}
		}

		RequestBody requestBody = builder.build();
		return new Request.Builder().url(url).post(requestBody).build();
	}

	private String guessMimeType(String path) {
		FileNameMap fileNameMap = URLConnection.getFileNameMap();
		String contentTypeFor = fileNameMap.getContentTypeFor(path);
		if (contentTypeFor == null) {
			contentTypeFor = "application/octet-stream";
		}
		return contentTypeFor;
	}

	private Param[] validateParam(Param[] params) {
		if (params == null)
			return new Param[0];
		else
			return params;
	}

	private Param[] map2Params(Map<String, String> params) {
		if (params == null)
			return new Param[0];
		int size = params.size();
		Param[] res = new Param[size];
		Set<Map.Entry<String, String>> entries = params.entrySet();
		int i = 0;
		for (Map.Entry<String, String> entry : entries) {
			res[i++] = new Param(entry.getKey(), entry.getValue());
		}
		return res;
	}

	private static final String SESSION_KEY = "Set-Cookie";
	private static final String mSessionKey = "JSESSIONID";

	private Map<String, String> mSessions = new HashMap<String, String>();

	private void deliveryResult(final ResultCallback callback, Request request) {
		mOkHttpClient.newCall(request).enqueue(new Callback() {
			@Override
			public void onFailure(Call call, IOException e) {
				sendFailedStringCallback(call.request(), e, callback);
			}

			@Override
			public void onResponse(Call call, Response response) throws IOException {
				try {
					final String string = response.body().string();
					if (callback.mType == String.class) {
						sendSuccessResultCallback(string, callback);
					} else {
						Object o = mGson.fromJson(string, callback.mType);
						sendSuccessResultCallback(o, callback);
					}

				} catch (IOException e) {
					sendFailedStringCallback(response.request(), e, callback);
				} catch (com.google.gson.JsonParseException e)
				{
					sendFailedStringCallback(response.request(), e, callback);
				}


			}
		});

	}

	private void sendFailedStringCallback(final Request request,
			final Exception e, final ResultCallback callback) {
		mDelivery.post(new Runnable() {
			@Override
			public void run() {
				if (callback != null)
					callback.onError(request, e);
			}
		});
	}

	private void sendSuccessResultCallback(final Object object,
			final ResultCallback callback) {
		mDelivery.post(new Runnable() {
			@Override
			public void run() {
				if (callback != null) {
					callback.onResponse(object);
				}
			}
		});
	}

	private Request buildPostRequest(Request.Builder requestBuilder,
			Param[] params) {
		if (params == null) {
			params = new Param[0];
		}
		FormBody.Builder builder = new FormBody.Builder();
		for (Param param : params) {
			builder.add(param.key, param.value);
		}
		RequestBody requestBody = builder.build();
		return requestBuilder.post(requestBody).build();
	}

	private Request buildJsonPostRequest(Request.Builder requestBuilder,
			String jsonStr) {
		RequestBody requestBody = RequestBody.create(JSON, jsonStr);
		requestBuilder = requestBuilder.post(requestBody);
		Request request = requestBuilder.build();
		return request;
	}
	
	private Request buildUrlencodedPostRequest(Request.Builder requestBuilder,
			String urlencoded) {
		RequestBody requestBody = RequestBody.create(URLENCODED, urlencoded);
		return requestBuilder.post(requestBody).build();
	}



	public static abstract class ResultCallback<T> {
		Type mType;

		public ResultCallback() {
			mType = getSuperclassTypeParameter(getClass());
		}

		static Type getSuperclassTypeParameter(Class<?> subclass) {
			Type superclass = subclass.getGenericSuperclass();
			if (superclass instanceof Class) {
				throw new RuntimeException("Missing type parameter.");
			}
			ParameterizedType parameterized = (ParameterizedType) superclass;
			return $Gson$Types.canonicalize(parameterized
					.getActualTypeArguments()[0]);
		}

		public abstract void onError(Request request, Exception e);

		public abstract void onResponse(T response);
	}

	public static class Param {
		public Param() {
		}

		public Param(String key, String value) {
			this.key = key;
			this.value = value;
		}

		String key;
		String value;
	}

}
