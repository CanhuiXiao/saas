package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.SellAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.SellsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SellHomeActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener{

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private RadioGroupWithLayout rgSellHome;
    //滑块集合
    private List<View> sliders;

    private List<SellsEvent.Sell> sells;
    private ListView lvSellHome;
    private SellAdapter sellAdapter;

    private void inte(){
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        rgSellHome = (RadioGroupWithLayout) findViewById(R.id.rg_sell_home);
        sliders = new ArrayList<View>();
        View viewSellHomeAll = findViewById(R.id.view_sell_home_all);
        sliders.add(viewSellHomeAll);
        View viewSellHomeFinish = findViewById(R.id.view_sell_home_finish);
        sliders.add(viewSellHomeFinish);
        View viewSellHomeNote = findViewById(R.id.view_sell_home_note);
        sliders.add(viewSellHomeNote);
        View viewSellHomeCancel = findViewById(R.id.view_sell_home_cancel);
        sliders.add(viewSellHomeCancel);
        lvSellHome = (ListView) findViewById(R.id.lv_sell_home);
        sells = new ArrayList<SellsEvent.Sell>();
        sellAdapter = new SellAdapter(mContext, R.layout.item_lv_sell_home, sells);
    }

    //显示滑块的方法
    private void ShowSlider(int RadioButtonId){
        for (int i = 0; i < sliders.size(); i++) {
            if(RadioButtonId == sliders.get(i).getId()){
                sliders.get(i).setVisibility(View.VISIBLE);
            }else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_home);
        inte();
        titleBarCommonBuilder.setTitle("销售历史")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setRightImageRes(R.drawable.tian_jia02)
                .setRightImageResSecond(R.drawable.shou_suo);
        titleBarCommonBuilder.setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarCommonBuilder.setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, SellEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                startActivity(intent);
            }
        });


        lvSellHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, SellEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("SELL_ID", sells.get(position).getSell_id());
                intent.putExtra("STATE", sells.get(position).getState());
                startActivity(intent);
            }
        });

        lvSellHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetSell();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvSellHome.setAdapter(sellAdapter);
        GetSell();
        rgSellHome.setOnCheckedChangeListener(this);
    }

    private void GetSell(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        if(state != Constant.STATE_ALL){
            map.put("state", state + "");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mSellAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SellsEvent sellsEvent = gson.fromJson(response, SellsEvent.class);
                if(mStart == 0){
                    sells.clear();
                }
                mStart = mStart + sellsEvent.getData().getList().size();
                sells.addAll(sellsEvent.getData().getList());
                sellAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    private int state = Constant.STATE_ALL;

    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId){
            case R.id.rb_sell_home_all:
                ShowSlider(R.id.view_sell_home_all);
                mStart = 0;
                state = Constant.STATE_ALL;
                GetSell();
                break;
            case R.id.rb_sell_home_finish:
                ShowSlider(R.id.view_sell_home_finish);
                mStart = 0;
                state = Constant.STATE_DEFAULT;
                GetSell();
                break;
            case R.id.rb_sell_home_note:
                ShowSlider(R.id.view_sell_home_note);
                mStart = 0;
                state = Constant.STATE_NOTE;
                GetSell();
                break;
            case R.id.rb_sell_home_cancel:
                ShowSlider(R.id.view_sell_home_cancel);
                mStart = 0;
                state = Constant.STATE_CANCEL;
                GetSell();
                break;
            default:
                break;
        }
    }
}
