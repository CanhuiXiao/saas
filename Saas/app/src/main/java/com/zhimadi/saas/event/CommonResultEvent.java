package com.zhimadi.saas.event;

/**
 * Created by carry on 2016/7/13.
 */

public class CommonResultEvent {

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
