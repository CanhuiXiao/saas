package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CustomTypesEvent;
import java.util.List;
/**
 * Created by carry on 2016/7/13.
 */
public class CustomTypeAdapter extends ArrayAdapter<CustomTypesEvent.CustomType> {


    private Context mContext;
    private int resourceId;


    public CustomTypeAdapter(Context context, int resource, List<CustomTypesEvent.CustomType> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomTypesEvent.CustomType customType = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvCustomTypeName.setText(customType.getName());
        return convertView;
    }



    class ViewHolder {
        private TextView tvCustomTypeName;
        public ViewHolder(View view) {
            tvCustomTypeName = (TextView) view.findViewById(R.id.tv_custom_type_home_name);
            view.setTag(this);
        }
    }
}
