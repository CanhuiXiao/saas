package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StoreAdapter;
import com.zhimadi.saas.adapter.StoreSelectAdapter;
import com.zhimadi.saas.event.StoresEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreSelectActivity extends BaseActivity {

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_Fail = 0;

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private ListView lvStoreHome;
    private List<StoresEvent.Store> stores;
    private StoreSelectAdapter storeSelectAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        stores = new ArrayList<StoresEvent.Store>();
        lvStoreHome = (ListView) findViewById(R.id.lv_store_home);
        storeSelectAdapter = new StoreSelectAdapter(mContext, R.layout.item_lv_store_select, stores, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_home);
        inte();
        titleBarCommonBuilder.setTitle("选择仓库")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, StoreEditActivity.class);
                        intent.putExtra("INTENT_MODE", StoreEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvStoreHome.setAdapter(storeSelectAdapter);
        lvStoreHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, StoreEditActivity.class);
                intent.putExtra("INTENT_MODE", StoreEditActivity.EDIT_MODE);
                startActivity(intent);
            }
        });
        GetStore(mStart, mLimit);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetStore(mStart, mLimit);
    }



    private void GetStore(final int start, int limit) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", start + "");
        map.put("limit", limit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mStoreAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                StoresEvent storesEvent = gson.fromJson(response, StoresEvent.class);
                if (start == 0) {
                    stores.clear();
                }
                stores.addAll(storesEvent.getData().getList());
                storeSelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    //选中后返回数据
    public void select(String storeId, String storeName){
        Intent intent = new Intent();
        intent.putExtra("STORE_ID", storeId);
        intent.putExtra("STORE_NAME", storeName);
        setResult(RESULT_CODE_SUCCESS, intent);
        finish();
    }





}