package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.EmployeesEvent;
import com.zhimadi.saas.event.ShopsEvent;
import com.zhimadi.saas.event.SuppliersEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class EmployeeAdapter extends ArrayAdapter<EmployeesEvent.Employee>{


    private Context mContext;
    private int resourceId;

    public EmployeeAdapter(Context context, int resource, List<EmployeesEvent.Employee> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        EmployeesEvent.Employee employee = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvEmployeeHomeName.setText(employee.getName());
        holder.tvEmployeeHomePhone.setText(employee.getAccount());
        holder.tvEmployeeHomeShop.setText("所属门店 : " + employee.getShop_name());
        holder.tvEmployeeHomeLandTime.setText("最后登录时间 : " + employee.getLast_login_time());
        holder.tvEmployeeHomeCreateTime.setText("创建时间 : " + employee.getAtime());
        return convertView;
    }

    class ViewHolder {
        private TextView tvEmployeeHomeName;
        private TextView tvEmployeeHomePhone;
        private TextView tvEmployeeHomeShop;
        private TextView tvEmployeeHomeLandTime;
        private TextView tvEmployeeHomeCreateTime;

        public ViewHolder(View view) {
            tvEmployeeHomeName = (TextView) view.findViewById(R.id.tv_employee_home_name);
            tvEmployeeHomePhone = (TextView) view.findViewById(R.id.tv_employee_home_phone);
            tvEmployeeHomeShop = (TextView) view.findViewById(R.id.tv_employee_home_shop);
            tvEmployeeHomeLandTime = (TextView) view.findViewById(R.id.tv_employee_home_land_time);
            tvEmployeeHomeCreateTime = (TextView) view.findViewById(R.id.tv_employee_home_create_time);
            view.setTag(this);
        }
    }
}
