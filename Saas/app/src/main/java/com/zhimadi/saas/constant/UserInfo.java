package com.zhimadi.saas.constant;

/**
 * Created by carry on 2016/7/13.
 */
public class UserInfo {


    private static String mIMEI;

    private static String mAuth;

    private static String mRoleId;

    private static String mUserName;

    private static String mCompanyId;

    private static String mCompanyName;

    private static String mPhone;

    private static String mCompanyPic;

    public static void clear(){
        mIMEI = "";
        mAuth = "";
        mRoleId = "";
        mUserName = "";
        mCompanyId = "";
        mCompanyName = "";
        mPhone = "";
        mCompanyPic = "";
    }

    public static String getmIMEI() {
        return mIMEI;
    }

    public static void setmIMEI(String mIMEI) {
        UserInfo.mIMEI = mIMEI;
    }

    public static String getmAuth() {
        return mAuth;
    }

    public static void setmAuth(String mAuth) {
        UserInfo.mAuth = mAuth;
    }

    public static String getmRoleId() {
        return mRoleId;
    }

    public static void setmRoleId(String mRoleId) {
        UserInfo.mRoleId = mRoleId;
    }

    public static String getmUserName() {
        return mUserName;
    }

    public static void setmUserName(String mUserName) {
        UserInfo.mUserName = mUserName;
    }

    public static String getmCompanyId() {
        return mCompanyId;
    }

    public static void setmCompanyId(String mCompanyId) {
        UserInfo.mCompanyId = mCompanyId;
    }

    public static String getmCompanyName() {
        return mCompanyName;
    }

    public static void setmCompanyName(String mCompanyName) {
        UserInfo.mCompanyName = mCompanyName;
    }

    public static String getmPhone() {
        return mPhone;
    }

    public static void setmPhone(String mPhone) {
        UserInfo.mPhone = mPhone;
    }

    public static String getmCompanyPic() {
        return mCompanyPic;
    }

    public static void setmCompanyPic(String mCompanyPic) {
        UserInfo.mCompanyPic = mCompanyPic;
    }
}
