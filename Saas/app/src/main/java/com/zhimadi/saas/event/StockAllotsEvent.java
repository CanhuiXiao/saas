package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class StockAllotsEvent {

    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<StockAllot> list;

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<StockAllot> getList() {
            return list;
        }

        public void setList(List<StockAllot> list) {
            this.list = list;
        }
    }


    public class StockAllot {
        private String allot_id;
        private String out_warehouse_id;
        private String in_warehouse_id;
        private String order_no;
        private String create_user_id;
        private String state;
        private String create_time;
        private String create_user_name;
        private String in_warehouse_name;
        private String out_warehouse_name;

        public String getAllot_id() {
            return allot_id;
        }

        public void setAllot_id(String allot_id) {
            this.allot_id = allot_id;
        }

        public String getOut_warehouse_id() {
            return out_warehouse_id;
        }

        public void setOut_warehouse_id(String out_warehouse_id) {
            this.out_warehouse_id = out_warehouse_id;
        }

        public String getIn_warehouse_id() {
            return in_warehouse_id;
        }

        public void setIn_warehouse_id(String in_warehouse_id) {
            this.in_warehouse_id = in_warehouse_id;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getCreate_user_id() {
            return create_user_id;
        }

        public void setCreate_user_id(String create_user_id) {
            this.create_user_id = create_user_id;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getCreate_user_name() {
            return create_user_name;
        }

        public void setCreate_user_name(String create_user_name) {
            this.create_user_name = create_user_name;
        }

        public String getIn_warehouse_name() {
            return in_warehouse_name;
        }

        public void setIn_warehouse_name(String in_warehouse_name) {
            this.in_warehouse_name = in_warehouse_name;
        }

        public String getOut_warehouse_name() {
            return out_warehouse_name;
        }

        public void setOut_warehouse_name(String out_warehouse_name) {
            this.out_warehouse_name = out_warehouse_name;
        }
    }

}
