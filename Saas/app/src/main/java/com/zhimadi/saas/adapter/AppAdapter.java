//package com.zhimadi.saas.adapter;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.event.AppEvent;
//import java.util.List;
//
///**
// * Created by carry on 2016/7/13.
// */
//public class AppAdapter extends ArrayAdapter<AppEvent>{
//
//    private Activity mActivity;
//    private Context mContext;
//    private View returnView;
//    private int resourceId;
//    private TextView tvHomeAppTitle;
//    private ImageView ivHomeAppIcon;
//
//
//    public AppAdapter(Context context, int resource, List<AppEvent> objects) {
//        super(context, resource, objects);
//        mContext = context;
//        resourceId = resource;
//    }
//
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
//        AppEvent appEvent = getItem(position);
//        tvHomeAppTitle = (TextView) returnView.findViewById(R.id.tv_home_app_title);
//        tvHomeAppTitle.setText(appEvent.getName());
//        ivHomeAppIcon = (ImageView) returnView.findViewById(R.id.iv_home_app_icon);
//        if(appEvent.getIcon() != 0){
//            ivHomeAppIcon.setImageResource(appEvent.getIcon());
//        }
//
//        return returnView;
//    }
//
//
//
//}
