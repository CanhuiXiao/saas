package com.zhimadi.saas.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.AreaAdapter;
import com.zhimadi.saas.event.AreaEvent;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */

public class AreaSelectActivity extends BaseActivity {

    public static final int requestCodeForArea = 1;

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_Fail = 0;

    private TitleBarCommonBuilder titleBarCommonBuilder;

    //第一次的标志
    private boolean isFirst;

    private ListView lvAreaSelect;
    private AreaAdapter areaAdapter;

    private String areaCode;
    private String areaName;
    private List<AreaEvent.Area> areas;

    private void inte() {
        isFirst = getIntent().getBooleanExtra("IS_FIRST", true);
        areas = (List<AreaEvent.Area>) getIntent().getSerializableExtra("AREAS");
        areaName = getIntent().getStringExtra("AREA_NAME");
        if(areaName == null){
            areaName = "";
        }
        areaCode = getIntent().getStringExtra("AREA_CODE");
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        lvAreaSelect = (ListView) findViewById(R.id.lv_area_select);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_select);
        inte();
        titleBarCommonBuilder.setTitle("选择地区").setLeftImageRes(R.drawable.fan_hui).setLeftText("返回").setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //如果不是第一个页面且数据源为空则返回
        if(!isFirst){
            if(areas==null || areas.size() ==0){
                Intent intent = new Intent();
                intent.putExtra("AREA_CODE", areaCode);
                intent.putExtra("AREA_NAME", areaName);
                setResult(RESULT_CODE_SUCCESS, intent);
                finish();
        }
        }

        lvAreaSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, AreaSelectActivity.class);
                intent.putExtra("IS_FIRST", false);
                intent.putExtra("AREAS", (Serializable) areas.get(position).getC());
                intent.putExtra("AREA_NAME", areas.get(position).getN());
                intent.putExtra("AREA_CODE", areas.get(position).getK()+"");
                startActivityForResult(intent, requestCodeForArea);
            }
        });

        //第一个页面
        if(isFirst){
            areas = new ArrayList<AreaEvent.Area>();
            Gson gson = new Gson();
            AreaEvent areaEvent = gson.fromJson(GetArea(), AreaEvent.class);
            areas.addAll(areaEvent.getAreas());
            areaAdapter = new AreaAdapter(mContext, R.layout.item_lv_area, areas);
            lvAreaSelect.setAdapter(areaAdapter);
        }else {
            if(areas!=null) {
                areaAdapter = new AreaAdapter(mContext, R.layout.item_lv_area, areas);
                lvAreaSelect.setAdapter(areaAdapter);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case requestCodeForArea:
                if(resultCode == RESULT_CODE_SUCCESS){
                    areaName = areaName + data.getStringExtra("AREA_NAME");
                    areaCode =data.getStringExtra("AREA_CODE");
                    Intent intent = new Intent();
                    intent.putExtra("AREA_NAME", areaName);
                    intent.putExtra("AREA_CODE", areaCode);
                    setResult(RESULT_CODE_SUCCESS, intent);
                    finish();
                }
                break;
            default:
                break;
        }
    }

    private String GetArea(){
        StringBuilder stringBuilder = new StringBuilder("");
        try {
            AssetManager assetManager = mContext.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(assetManager.open("area.json")));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

}