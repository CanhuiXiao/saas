package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.BuyDetailEvent;
import com.zhimadi.saas.event.ProductData;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.BillBuy;
import com.zhimadi.saas.widget.Cat;
import com.zhimadi.saas.widget.CatDetail;
import com.zhimadi.saas.widget.CatDetailItem;
import com.zhimadi.saas.widget.CatItem;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.TextViewItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SellAgentAgentEditActivity extends BaseActivity {

    private TitleBarCommonBuilder titleBarCommonBuilder;


    //请求码
    public static final int requestCodeForSupplier = 1;
    public static final int requestCodeForStore = 2;
    public static final int requestCodeForProductSelect = 3;

    private static final int BUY = 0;
    private static final int NOTE = 3;

    //是否可编辑
    private boolean isEditEnable;

    //传入的数据
    private int intentMode;
    private String buyId;
    private int mState;

    private LinearLayout llBuyEditWindow;

    private TextViewItem textViewItemOderNummber;
    private TextViewItem textViewItemMaker;

    private SelelctableTextViewItem selelctableTextViewItemSupplier;
    private SelelctableTextViewItem selelctableTextViewItemStore;

    //购物车
    private Cat cat;
    //明细表
    private CatDetail catDetail;
    //消费提示栏
    private BillBuy billBuy;



    private EditTextITem editTextITemNote;

    //底部操作栏
    private LinearLayout llBuyEditTab;
    private TextView tvBuyEditFirst;
    private TextView tvBuyEditSecond;
    private TextView tvBuySum;

    //隐藏post数据
    private String storeId;
    private String supplierId;

    //缓存商品列表
    private List<CatItem> catItems;

    //存商品的总金额，总重量，总件数
    Map<String, Float> mapNumber;
    Map<String, Float> mapWeight;
    Map<String, Float> mapTotal;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llBuyEditWindow = (LinearLayout) findViewById(R.id.ll_buy_edit_window);

        textViewItemOderNummber = new TextViewItem(mContext, null, "单号");
        selelctableTextViewItemSupplier = new SelelctableTextViewItem(mContext, null, "代卖商", "请选择代卖商");
        selelctableTextViewItemStore = new SelelctableTextViewItem(mContext, null, "仓库", "请选择仓库");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");


        tvBuyEditFirst = (TextView) findViewById(R.id.tv_buy_edit_first);
        tvBuyEditSecond = (TextView) findViewById(R.id.tv_buy_edit_second);
        tvBuySum = (TextView) findViewById(R.id.tv_buy_sum);
        llBuyEditTab = (LinearLayout) findViewById(R.id.ll_buy_edit_tab);

        //第一步判断可不可以编辑
        if (intentMode == Constant.EDIT_MODE) {
            /***
             * 编辑状态下只有草稿能编辑，还要特殊对待，蛋疼
             */
            if (mState == Constant.STATE_NOTE) {
                setEditEnable(true);
                isEditEnable = true;
            } else {
                setEditEnable(false);
                isEditEnable = false;
            }
        } else {
            setEditEnable(true);
            isEditEnable = true;
        }

        //第二步选择加载的内容
        if (intentMode == Constant.EDIT_MODE) {
            mState = Integer.valueOf(getIntent().getStringExtra("STATE"));
            buyId = getIntent().getStringExtra("BUY_ID");
            if (mState == Constant.STATE_NOTE) {
                cat = new Cat(mContext, null);
                billBuy = new BillBuy(mContext, null, true);
                catItems = new ArrayList<CatItem>();
                mapNumber = new HashMap<String, Float>();
                mapWeight = new HashMap<String, Float>();
                mapTotal = new HashMap<String, Float>();
            } else {
                catDetail = new CatDetail(mContext, null);
                billBuy = new BillBuy(mContext, null, false);
            }
            textViewItemMaker = new TextViewItem(mContext, null, "制单人");
        } else {
            cat = new Cat(mContext, null);
            billBuy = new BillBuy(mContext, null, true);
            catItems = new ArrayList<CatItem>();
            mapNumber = new HashMap<String, Float>();
            mapWeight = new HashMap<String, Float>();
            mapTotal = new HashMap<String, Float>();
            textViewItemMaker = new TextViewItem(mContext, null, "制单人", UserInfo.getmUserName());
        }

        //第三步根据订单状态修改底部操作栏
        if (intentMode == Constant.EDIT_MODE) {
            switch (mState) {
                case Constant.STATE_WAIT_CHECK:
                    llBuyEditTab.setVisibility(View.VISIBLE);
                    tvBuyEditFirst.setBackgroundResource(R.color.colorOrange);
                    tvBuyEditFirst.setText("撤销");
                    tvBuyEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvBuyEditSecond.setBackgroundResource(R.color.colorSecondBody);
                    tvBuyEditSecond.setText("待审核");
                    tvBuyEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_DEFAULT:
                    llBuyEditTab.setVisibility(View.VISIBLE);
                    tvBuyEditFirst.setBackgroundResource(R.color.colorOrange);
                    tvBuyEditFirst.setText("撤销");
                    tvBuyEditSecond.setBackgroundResource(R.color.colorBlue);
                    tvBuyEditSecond.setText("打印");
                    tvBuyEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvBuyEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_NOTE:
                    tvBuyEditFirst.setBackgroundResource(R.color.colorWhite);
                    tvBuyEditFirst.setText("保存");
                    tvBuyEditSecond.setBackgroundResource(R.color.colorSecondBody);
                    tvBuyEditSecond.setText("采购");
                    tvBuyEditFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    tvBuyEditSecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                    break;
                case Constant.STATE_CANCEL:
                    break;
                default:
                    finish();
                    break;
            }
        } else {
            llBuyEditTab.setVisibility(View.VISIBLE);
            tvBuyEditFirst.setBackgroundResource(R.color.colorAquamarine);
            tvBuyEditFirst.setText("草稿");
            tvBuyEditFirst.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buyPost(NOTE);
                }
            });
            tvBuyEditSecond.setBackgroundResource(R.color.colorSaffronYellow);
            tvBuyEditSecond.setText("采购");
            tvBuyEditSecond.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    buyPost(BUY);
                }
            });
        }


    }

    //设置不可按，把输入提示语去掉，箭头弄掉
    private void setEditEnable(boolean enable) {
        selelctableTextViewItemSupplier.setEnabled(enable);
        selelctableTextViewItemStore.setEnabled(enable);
        editTextITemNote.setEditEnable(enable);
        if (!enable) {
            editTextITemNote.SetHint("");
            editTextITemNote.SetHint("");
        }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_edit);
        inte();
        titleBarCommonBuilder.setTitle("代卖单").setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        selelctableTextViewItemSupplier
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, AgentHomeActivity.class);
                        startActivityForResult(intent, requestCodeForSupplier);
                    }
                });

        selelctableTextViewItemStore
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, StoreSelectActivity.class);
                        startActivityForResult(intent, requestCodeForStore);
                    }
                });


        textViewItemOderNummber.isTop(true);
        llBuyEditWindow.addView(textViewItemOderNummber);
        textViewItemMaker.isBottom(true);
        llBuyEditWindow.addView(textViewItemMaker);

        selelctableTextViewItemSupplier.isTop(true);
        llBuyEditWindow.addView(selelctableTextViewItemSupplier);
        selelctableTextViewItemStore.isBottom(true);
        llBuyEditWindow.addView(selelctableTextViewItemStore);

        //通过是否为空，判断加载购物车还是明细
        if (cat != null) {
            llBuyEditWindow.addView(cat);
            //添加监听
            cat.setOnClickListenerAdd(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(mContext, ProductSelectActivity.class);
                    List<String> productIds = new ArrayList<String>();
                    if (catItems.size() > 0) {
                        for (int i = 0; i < catItems.size(); i++) {
                            productIds.add(catItems.get(i).getProductId());
                        }
                        Bundle mBundle = new Bundle();
                        intent.putExtra("PRODUCT_IDS", (Serializable) productIds);
                        intent.putExtras(mBundle);
                    }
                    startActivityForResult(intent, requestCodeForProductSelect);
                }
            });
            //编辑监听
            cat.setOnClickListenerEdit(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            billBuy.isTop(true).isBottom(true);
            llBuyEditWindow.addView(billBuy);
        } else if (catDetail != null) {
            llBuyEditWindow.addView(catDetail);
            billBuy.isBottom(true);
            llBuyEditWindow.addView(billBuy);
        }



        editTextITemNote.isTop(true).isBottom(true);
        llBuyEditWindow.addView(editTextITemNote);

        //最后才加载
        if (intentMode == Constant.EDIT_MODE) {
            getBuyDetail();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case requestCodeForSupplier:
                if (resultCode == SupplierSelectActivity.RESULT_CODE_SUCCESS) {
                    supplierId = data.getStringExtra("SUPPLIER_ID");
                    selelctableTextViewItemSupplier.SetItemValue(data.getStringExtra("SUPPLIER_NAME"));
                }
                break;
            case requestCodeForStore:
                if (resultCode == SupplierSelectActivity.RESULT_CODE_SUCCESS) {
                    storeId = data.getStringExtra("STORE_ID");
                    selelctableTextViewItemStore.SetItemValue(data.getStringExtra("STORE_NAME"));
                }
                break;

            case requestCodeForProductSelect:
                if (resultCode == ProductSelectActivity.RESULT_SUCCESS) {
                    //传过来的玩意
                    List<ProductData> productDatas = (List<ProductData>) data.getExtras().getSerializable("PRODUCT_ARRAY");
                    List<String> productIds = (List<String>) data.getExtras().getSerializable("PRODUCTIDS_ARRY");

                    //缓存要删掉的东西
                    List<ProductData> productDatasTemp = new ArrayList<ProductData>();
                    List<CatItem> catItemsTemp = new ArrayList<CatItem>();
                    List<String> productIdsTemp = new ArrayList<String>();

                    //第一步：返回的数据预处理，把购物车出现过的剔除掉
                    for (int i = 0; i < productDatas.size(); i++) {
                        for (int k = 0; k < catItems.size(); k++) {
                            if (productDatas.get(i).getProductId().equals(catItems.get(k).getProductId())) {
                                productDatasTemp.add(productDatas.get(i));
                            }
                        }
                    }


                    //第二步：从购物车中删掉返回没有的数据,不过如果先前已经选中的因为没点击或者没加载并不会返回，这里需要分辨没有返回的到底有没有选中，好绕 = =
                    //所以要一个id数组来分辨
                    boolean isExist;
                    for (int i = 0; i < catItems.size(); i++) {
                        isExist = false;
                        for (int k = 0; k < productIds.size(); k++) {
                            if (productIds.get(k).equals(catItems.get(i).getProductId())) {
                                isExist = true;
                            }
                        }
                        if (!isExist) {
                            productIdsTemp.add(catItems.get(i).getProductId());
                            catItemsTemp.add(catItems.get(i));
                        }
                    }


                    productDatas.removeAll(productDatasTemp);

                    catItems.removeAll(catItemsTemp);
                    cat.removeAllViews(productIdsTemp, billBuy);


                    //这里留下来的是不重复的
                    for (int i = 0; i < productDatas.size(); i++) {
                        final ProductData productData = productDatas.get(i);
                        CatItem catItem = new CatItem(mContext, null, productData.getProductId(), productData.getName(), productData.getFormat(), productData.getIsFit(), productData.getFitWeight(), new CatItem.TextChangeListener() {
                            @Override
                            public void textChange(float number, float weight, float total) {
                                mapNumber.put(productData.getProductId(), number);
                                Float sum = new Float(0);
                                for (Float temp : mapNumber.values()) {
                                    sum += temp;
                                }
                                billBuy.setNumber(sum);

                                mapWeight.put(productData.getProductId(), weight);
                                sum = 0f;
                                for (Float temp : mapWeight.values()) {
                                    sum += temp;
                                }
                                billBuy.setWeight(sum);


                                mapTotal.put(productData.getProductId(), total);
                                sum = 0f;
                                for (Float temp : mapTotal.values()) {
                                    sum += temp;
                                }
                                billBuy.setPay(sum);
                                tvBuySum.setText("￥" + sum);

                            }
                        });
                        //之前的去边最后一个改边
                        if (i == productDatas.size() - 1) {
                            catItem.isBottom(true);
                        } else {
                            catItem.isBottom(false);
                        }
                        cat.addView(catItem);
                        //缓存比较
                        catItems.add(catItem);
                    }
                }

                break;
            default:
                break;
        }
    }

    private void getBuyDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("buy_id", buyId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mBuyDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                BuyDetailEvent buyDetailEvent = gson.fromJson(response, BuyDetailEvent.class);
                textViewItemOderNummber.SetItemValue(buyDetailEvent.getData().getOrder_no());
                textViewItemMaker.SetItemValue(buyDetailEvent.getData().getCreate_user_name());
                selelctableTextViewItemSupplier.SetItemValue(buyDetailEvent.getData().getSupplier_name());
                selelctableTextViewItemStore.SetItemValue(buyDetailEvent.getData().getWarehouse_name());

                billBuy.setNumber(buyDetailEvent.getData().getTotal_package());
                billBuy.setPay(buyDetailEvent.getData().getTotal_amount());
                billBuy.setWeight(buyDetailEvent.getData().getTotal_weight());

                editTextITemNote.SetItemValue(buyDetailEvent.getData().getNote());

                if (cat != null) {
                    for (int i = 0; i < buyDetailEvent.getData().getProducts().size(); i++) {
                        final BuyDetailEvent.Product product = buyDetailEvent.getData().getProducts().get(i);
                        CatItem catItem = new CatItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), Float.valueOf(product.getFixed_weight()), new CatItem.TextChangeListener() {

                            @Override
                            public void textChange(float number, float weight, float total) {

                                mapNumber.put(product.getProduct_id(), total);
                                Float sum = new Float(0);
                                for (Float temp : mapNumber.values()) {
                                    sum += temp;
                                }
                                billBuy.setNumber(sum + "");

                                mapWeight.put(product.getProduct_id(), total);
                                sum = 0f;
                                for (Float temp : mapWeight.values()) {
                                    sum += temp;
                                }
                                billBuy.setWeight(sum + "");
                                mapTotal.put(product.getProduct_id(), total);
                                sum = 0f;
                                for (Float temp : mapTotal.values()) {
                                    sum += temp;
                                }
                                billBuy.setPay(sum + "");

                            }
                        });
                        if (i == buyDetailEvent.getData().getProducts().size() - 1) {
                            catItem.isBottom(true);
                        } else {
                            catItem.isBottom(false);
                        }

                        cat.addView(catItem);
                    }
                } else if (catDetail != null) {
                    for (int i = 0; i < buyDetailEvent.getData().getProducts().size(); i++) {
                        BuyDetailEvent.Product product = buyDetailEvent.getData().getProducts().get(i);
                        CatDetailItem catDetailItem = new CatDetailItem(mContext, null, product.getProduct_id(), product.getName(), product.getProps_name(), product.getIs_fixed(), product.getQuantity(), product.getPrice_unit(), product.getWeight());
                        if (i == buyDetailEvent.getData().getProducts().size() - 1) {
                            catDetailItem.isBottom(true);
                        }
                        catDetail.addView(catDetailItem);
                    }
                }
                supplierId = buyDetailEvent.getData().getSupplier_id();
                storeId = buyDetailEvent.getData().getWarehouse_id();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void buyPost(int state) {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == Constant.EDIT_MODE) {
            jsonObject.addProperty("buy_id", buyId);
        }
        jsonObject.addProperty("warehouse_id", storeId);
        jsonObject.addProperty("supplier_id", supplierId);
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
        //只能填 0 或 3 或不传. 0 或 不传表示发布，如果当前用户有审核权限，直接审核通过。传 3 表示草稿。
        jsonObject.addProperty("state", state + "");
        //组装product
        JsonArray jsonArray = new JsonArray();
        for (int i = 0; i < catItems.size(); i++) {
            CatItem catItem = catItems.get(i);
            JsonObject product = new JsonObject();
            product.addProperty("weight", catItem.getWeight());
            product.addProperty("product_id", catItem.getProductId());
            product.addProperty("package", catItem.getNumber());
            product.addProperty("price", catItem.getUnitPrice());
            //1：公斤   2：件
            product.addProperty("price_unit", catItem.getUnit() + "");
            jsonArray.add(product);
        }
        jsonObject.add("products", jsonArray);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditBuyAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                finish();
            }

            @Override
            public void onFail() {

            }
        });
    }

}
