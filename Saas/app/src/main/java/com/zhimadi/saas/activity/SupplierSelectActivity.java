package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StoreSelectAdapter;
import com.zhimadi.saas.adapter.SupplierSelectAdapter;
import com.zhimadi.saas.event.StoresEvent;
import com.zhimadi.saas.event.SuppliersEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierSelectActivity extends BaseActivity {

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_FAIL = 0;

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private ListView lvSupplierHome;
    private List<SuppliersEvent.Supplier> suppliers;
    private SupplierSelectAdapter supplierSelectAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        suppliers = new ArrayList<SuppliersEvent.Supplier>();
        lvSupplierHome = (ListView) findViewById(R.id.lv_supplier_select);
        supplierSelectAdapter = new SupplierSelectAdapter(mContext, R.layout.item_lv_supplier_select, suppliers, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_select);
        inte();
        titleBarCommonBuilder.setTitle("选择供应商")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, SupplierEditActivity.class);
                        intent.putExtra("INTENT_MODE", SupplierEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvSupplierHome.setAdapter(supplierSelectAdapter);
        lvSupplierHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(mContext, SupplierEditActivity.class);
                intent.putExtra("INTENT_MODE", SupplierEditActivity.EDIT_MODE);
                startActivity(intent);
            }
        });
        GetSupplier(mStart, mLimit);

    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetSupplier(mStart, mLimit);
    }



    private void GetSupplier(final int start, int limit) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", start + "");
        map.put("limit", limit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mSupplierAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SuppliersEvent storesEvent = gson.fromJson(response, SuppliersEvent.class);
                if (start == 0) {
                    suppliers.clear();
                }
                suppliers.addAll(storesEvent.getData().getList());
                supplierSelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    //选中后返回数据
    public void select(String storeId, String storeName, String oweAll){
        Intent intent = new Intent();
        intent.putExtra("SUPPLIER_ID", storeId);
        intent.putExtra("SUPPLIER_NAME", storeName);
        intent.putExtra("SUPPLIER_OWE_All", oweAll);
        setResult(RESULT_CODE_SUCCESS, intent);
        finish();
    }


}