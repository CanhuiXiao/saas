package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.EmployeesEvent;
import com.zhimadi.saas.event.ShopsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class ShopAdapter extends ArrayAdapter<ShopsEvent.Shop>{

    private Context mContext;
    private int resourceId;

    public ShopAdapter(Context context, int resource, List<ShopsEvent.Shop> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ShopsEvent.Shop shop = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvShopHomeName.setText(shop.getName());
        return convertView;
    }

    class ViewHolder {
        private TextView tvShopHomeName;
        public ViewHolder(View view) {
            tvShopHomeName = (TextView) view.findViewById(R.id.tv_shop_home_name);
            view.setTag(this);
        }
    }

}
