package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.ShopSelectAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.ShopsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.List;

public class ShopSelectActivity extends BaseActivity {

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_Fail = 0;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private ListView lvShopHome;
    private ShopSelectAdapter shopSelectAdapter;
    private List<ShopsEvent.Shop> shops;


    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        shops = new ArrayList<ShopsEvent.Shop>();
        lvShopHome = (ListView) findViewById(R.id.lv_shop_select);
        shopSelectAdapter = new ShopSelectAdapter(mContext, R.layout.item_lv_shop_select, shops, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_select);
        inte();
        titleBarCommonBuilder.setTitle("选择门店")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, ShopEditActivity.class);
                        intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvShopHome.setAdapter(shopSelectAdapter);
        GetShop();
    }


    private int mStart = 0;
    private int mLimit = 10;

    public void GetShop() {
        AsyncUtil.asyncGet(mContext, OkHttpUtil.mShopAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ShopsEvent shopsEvent = gson.fromJson(response, ShopsEvent.class);
                shops.clear();
                shops.addAll(shopsEvent.getData().getList());
                shopSelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    //注入门店id和门店名字
    public void select(String shopId, String shopName) {
        Intent intent = new Intent();
        intent.putExtra("SHOP_ID", shopId);
        intent.putExtra("SHOP_NAME", shopName);
        setResult(RESULT_CODE_SUCCESS, intent);
        finish();
    }


}
