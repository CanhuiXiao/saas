package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class ProductTypesEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<ProductType> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<ProductType> getList() {
            return list;
        }

        public void setList(List<ProductType> list) {
            this.list = list;
        }
    }

    public class ProductType
    {
        private String type_id;
        private String category_id;
        private String name;
        private String chandi;
        private String unit;
        private String weight_unit;
        private String is_published;
        private String is_deleted;
        private String display_order;

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getChandi() {
            return chandi;
        }

        public void setChandi(String chandi) {
            this.chandi = chandi;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public String getWeight_unit() {
            return weight_unit;
        }

        public void setWeight_unit(String weight_unit) {
            this.weight_unit = weight_unit;
        }

        public String getIs_published() {
            return is_published;
        }

        public void setIs_published(String is_published) {
            this.is_published = is_published;
        }

        public String getIs_deleted() {
            return is_deleted;
        }

        public void setIs_deleted(String is_deleted) {
            this.is_deleted = is_deleted;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }
    }

}
