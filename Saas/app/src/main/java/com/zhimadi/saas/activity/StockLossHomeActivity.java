package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StockLossAdapter;
import com.zhimadi.saas.event.StockLossesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockLossHomeActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener{

    public static final int STOCK_LOSS_ALL = -1;
    public static final int STOCK_LOSS_WAIT = 1;
    public static final int STOCK_LOSS_FINISH = 0;
    public static final int STOCK_LOSS_NOTE = 3;
    public static final int STOCK_LOSS_CANCRL = 2;

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private RadioGroupWithLayout rgStockLossHome;
    //滑块集合
    private List<View> sliders;

    private List<StockLossesEvent.StockLoss> stockLosss;
    private ListView lvStockLossHome;
    private StockLossAdapter stockLossAdapter;

    private void inte(){
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        rgStockLossHome = (RadioGroupWithLayout) findViewById(R.id.rg_stock_loss_home);
        sliders = new ArrayList<View>();
        View viewStockLossHomeAll = findViewById(R.id.view_stock_loss_home_all);
        sliders.add(viewStockLossHomeAll);
        View viewStockLossHomeWait = findViewById(R.id.view_stock_loss_home_wait);
        sliders.add(viewStockLossHomeWait);
        View viewStockLossHomeFinish = findViewById(R.id.view_stock_loss_home_finish);
        sliders.add(viewStockLossHomeFinish);
        View viewStockLossHomeNote = findViewById(R.id.view_stock_loss_home_note);
        sliders.add(viewStockLossHomeNote);
        View viewStockLossHomeCancel = findViewById(R.id.view_stock_loss_home_cancel);
        sliders.add(viewStockLossHomeCancel);

        lvStockLossHome = (ListView) findViewById(R.id.lv_stock_loss_home);
        stockLosss = new ArrayList<StockLossesEvent.StockLoss>();
        stockLossAdapter = new StockLossAdapter(mContext, R.layout.item_lv_stock_loss_home, stockLosss);
    }

    //显示滑块的方法
    private void ShowSlider(int RadioButtonId){
        for (int i = 0; i < sliders.size(); i++) {
            if(RadioButtonId == sliders.get(i).getId()){
                sliders.get(i).setVisibility(View.VISIBLE);
            }else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_loss_home);
        inte();
        titleBarCommonBuilder.setTitle("报损历史")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setRightImageRes(R.drawable.tian_jia02)
                .setRightImageResSecond(R.drawable.shou_suo);
        titleBarCommonBuilder.setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarCommonBuilder.setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
//                intent.setClass(mContext, StockLossEditActivity.class);
//                intent.putExtra("INTENT_MODE", StockLossEditActivity.ADD_MODE);
//                startActivity(intent);
            }
        });


        lvStockLossHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
//                intent.setClass(mContext, StockLossEditActivity.class);
//                intent.putExtra("INTENT_MODE", StockLossEditActivity.EDIT_MODE);
//                intent.putExtra("StockLoss_ID", stockLosss.get(position).getStockLoss_id());
//                intent.putExtra("STATE", stockLosss.get(position).getState());
//                startActivity(intent);
            }
        });

        lvStockLossHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetStockLoss(state);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvStockLossHome.setAdapter(stockLossAdapter);
        GetStockLoss(state);
        rgStockLossHome.setOnCheckedChangeListener(this);
    }

    private void GetStockLoss(int state){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        if(state != STOCK_LOSS_ALL){
            map.put("state", state+"");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mStockLossAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                StockLossesEvent stockLosssEvent = gson.fromJson(response, StockLossesEvent.class);
                if(mStart == 0){
                    stockLosss.clear();
                }
                mStart = mStart + stockLosssEvent.getData().getList().size();
                stockLosss.addAll(stockLosssEvent.getData().getList());
                stockLossAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }





    private int state = STOCK_LOSS_ALL;

    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId){
            case R.id.rb_stock_loss_home_all:
                ShowSlider(R.id.view_stock_loss_home_all);
                mStart = 0;
                state = STOCK_LOSS_ALL;
                GetStockLoss(state);
                break;
            case R.id.rb_stock_loss_home_wait:
                ShowSlider(R.id.view_stock_loss_home_wait);
                mStart = 0;
                state = STOCK_LOSS_WAIT;
                GetStockLoss(state);
                break;
            case R.id.rb_stock_loss_home_finish:
                ShowSlider(R.id.view_stock_loss_home_finish);
                mStart = 0;
                state = STOCK_LOSS_FINISH;
                GetStockLoss(state);
                break;
            case R.id.rb_stock_loss_home_note:
                ShowSlider(R.id.view_stock_loss_home_note);
                mStart = 0;
                state = STOCK_LOSS_NOTE;
                GetStockLoss(state);
                break;
            case R.id.rb_stock_loss_home_cancel:
                ShowSlider(R.id.view_stock_loss_home_cancel);
                mStart = 0;
                state = STOCK_LOSS_CANCRL;
                GetStockLoss(state);
                break;
            default:
                break;
        }
    }
}
