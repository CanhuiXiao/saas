package com.zhimadi.saas.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

public class FlowLayout extends ViewGroup {

    private Context mContext;

    private int width;
    private int height;

    //行数
    private int lineNumber;

    private int marginHorizontal = 0;
    private int marginVertical = 0;

    private int cCount;

    private int sizeWidth;
    private int sizeHeight;

    public FlowLayout(Context context) {
        super(context);
        mContext = context;
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }
    
    public void SetMarginHorizontal(int marginHorizontal) {
        this.marginHorizontal = marginHorizontal;
    }

    public void SetMarginVertical(int marginVertical) {
        this.marginVertical = marginVertical;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int modeWidth = MeasureSpec.getMode(widthMeasureSpec);
        int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
        sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
        sizeHeight = MeasureSpec.getSize(heightMeasureSpec);

        int cCount = getChildCount();
        int temp = 0;
        int lineWidth = 0;
        int lineHeight = 0;
        width = 0;
        height = 0;
        lineNumber = 0;

        for (int i = 0; i < cCount; i++) {
            View child = getChildAt(i);
            measureChild(child, widthMeasureSpec, heightMeasureSpec);
            int childWidth = child.getMeasuredWidth();
            int childHeight = child.getMeasuredHeight();

            if (i == 0) {
                temp = childWidth;
            } else {
                temp = childWidth + marginHorizontal;
            }

            //不换行
            if ((lineWidth + temp) <= sizeWidth) {
                lineWidth = lineWidth + temp;
                width = Math.max(width, lineWidth);
                lineHeight = Math.max(lineHeight, childHeight);
            } else {//换行
                width = Math.max(width, childWidth);
                lineWidth = childWidth;
                lineNumber++;
                height += (lineHeight + marginVertical);
                lineHeight = childHeight;
            }

            //最后一个,且存在子view
            if (i == cCount - 1) {
                lineNumber++;
                if (lineNumber == 1) {
                    height += lineHeight;
                } else {
                    height += (lineHeight + marginVertical);
                }
            }


        }

        setMeasuredDimension((modeWidth == MeasureSpec.EXACTLY) ? sizeWidth
                : width, (modeHeight == MeasureSpec.EXACTLY) ? sizeHeight
                : height);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        lineNumber = 0;

        width = 0;
        height = 0;

        int lineWidth = 0;
        int lineHeight = 0;

        int left = 0;
        int top = 0;

        int temp = 0;

        int cCount = getChildCount();

        for (int i = 0; i < cCount; i++) {
            View child = getChildAt(i);
            int childWidth = child.getMeasuredWidth();
            int childHeight = child.getMeasuredHeight();


            if (i == 0) {
                temp = childWidth;
            } else {
                temp = childWidth + marginHorizontal;
            }
            //换行
            if (lineWidth + temp > sizeWidth) {
                lineNumber++;
                lineHeight = Math.max(lineHeight, childHeight);
                left = 0;
                top += (childHeight + marginVertical);
                lineWidth = temp;
            } else {
                lineWidth = lineWidth + temp + marginHorizontal;
            }

            //判断换行
            int lc = left;
            int tc = top;
            int rc = lc + childWidth;
            int bc = tc + childHeight;
            child.layout(lc, tc, rc, bc);
            left += childWidth + marginHorizontal;

        }

    }
}