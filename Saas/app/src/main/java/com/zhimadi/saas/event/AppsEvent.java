package com.zhimadi.saas.event;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class AppsEvent {


    private int code;
    private String msg;
    private List<Model> data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Model> getData() {
        return data;
    }

    public void setData(List<Model> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Model {
        private List<App> list;
        private Cate cate;

        public List<App> getList() {
            return list;
        }

        public void setList(List<App> apps) {
            this.list = apps;
        }

        public Cate getCate() {
            return cate;
        }

        public void setCate(Cate cate) {
            this.cate = cate;
        }

        public class Cate {
            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public class App {
            private String app_id;
            private String name;
            private String description;
            private String key;
            private int auth;
            private int is_add;

            public String getApp_id() {
                return app_id;
            }

            public void setApp_id(String app_id) {
                this.app_id = app_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            public int getAuth() {
                return auth;
            }

            public void setAuth(int auth) {
                this.auth = auth;
            }

            public int getIs_add() {
                return is_add;
            }

            public void setIs_add(int is_add) {
                this.is_add = is_add;
            }
        }

    }

}
