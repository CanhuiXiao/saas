package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CategoriesEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class CategoryAdapter extends ArrayAdapter<CategoriesEvent.Category>{

    private Context mContext;
    private View returnView;
    private int resourceId;
    private TextView tvCategoryHomeName;

    public CategoryAdapter(Context context, int resource, List<CategoriesEvent.Category> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoriesEvent.Category category = getItem(position);
        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvCategoryHomeName = (TextView) returnView.findViewById(R.id.tv_category_home_name);
        tvCategoryHomeName.setText(category.getName());
        return returnView;
    }
}
