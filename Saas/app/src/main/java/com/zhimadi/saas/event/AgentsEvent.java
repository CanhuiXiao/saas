package com.zhimadi.saas.event;

import java.io.Serializable;
import java.util.List;

/**
 * Created by carry on 2016/7/14.
 */
public class AgentsEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Agent> list;

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Agent> getList() {
            return list;
        }

        public void setList(List<Agent> list) {
            this.list = list;
        }
    }

    public class Agent {
        private String agent_id;
        private String name;
        private String charge_man;
        private String state;

        public String getAgent_id() {
            return agent_id;
        }

        public void setAgent_id(String agent_id) {
            this.agent_id = agent_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCharge_man() {
            return charge_man;
        }

        public void setCharge_man(String charge_man) {
            this.charge_man = charge_man;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }


}
