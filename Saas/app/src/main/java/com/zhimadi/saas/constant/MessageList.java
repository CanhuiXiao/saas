package com.zhimadi.saas.constant;

import android.app.Activity;

import com.zhimadi.saas.R;

/**
 * Created by carry on 2016/8/26.
 */
public class MessageList {

    public enum Message {

        ZhiMaDi("1", "芝麻地团队", "描述内容", "0", R.drawable.zhi_ma_di_tuan_dui, null),
        SomethingToDo("2", "待办事项", "描述内容", "0", R.drawable.dai_ban_shi_xian, null),
        System("3", "系统信息", "描述内容", "0", R.drawable.xi_tong_xiao_xi, null),
        Notice("4", "公告", "描述内容", "0", R.drawable.gong_gao, null),
        Notify("5", "通知", "描述内容", "0", R.drawable.tong_zhi, null);

        private String type;
        private String title;
        private String describe;
        private String count;
        private int drawableResource;
        private Class<? extends Activity> activity;


        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescribe() {
            return describe;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public int getDrawableResource() {
            return drawableResource;
        }

        public void setDrawableResource(int drawableResource) {
            this.drawableResource = drawableResource;
        }

        public Class<? extends Activity> getActivity() {
            return activity;
        }

        public void setActivity(Class<? extends Activity> activity) {
            this.activity = activity;
        }

        Message(String type, String title, String describe, String count, int drawableResource, Class<? extends Activity> activity) {
            this.type = type;
            this.title = title;
            this.describe = describe;
            this.count = count;
            this.drawableResource = drawableResource;
            this.activity = activity;
        }

        //手写过滤器惨惨惨...
        public static Message getMessage(int value) {    //手写的从int到enum的转换函数
            switch (value) {
                case 1:
                    return Message.ZhiMaDi;
                case 2:
                    return Message.SomethingToDo;
                case 3:
                    return Message.System;
                case 4:
                    return Message.Notice;
                case 5:
                    return Message.Notify;
                default:
                    return null;
            }
        }
    }
}
