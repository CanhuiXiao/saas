package com.zhimadi.saas.event;

import java.io.Serializable;
import java.util.List;

/**
 * Created by carry on 2016/8/22.
 */
public class AreaEvent {


    private List<Area> areas;


    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public class Area implements Serializable{
        private int k;
        private String n;
        private List<Area> c;

        public int getK() {
            return k;
        }

        public void setK(int k) {
            this.k = k;
        }

        public String getN() {
            return n;
        }

        public void setN(String n) {
            this.n = n;
        }

        public List<Area> getC() {
            return c;
        }

        public void setC(List<Area> c) {
            this.c = c;
        }
    }


}
