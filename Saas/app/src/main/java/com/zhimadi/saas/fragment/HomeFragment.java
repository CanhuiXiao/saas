package com.zhimadi.saas.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

/**
 * Created by carry on 2016/7/8.
 */
public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private View returnView;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private GridView gvHomeApp;
//    private AppAdapter appAdapter;


    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(returnView);
        gvHomeApp = (GridView) returnView.findViewById(R.id.gv_home_app);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_home, null);
        inte();
        titleBarCommonBuilder.setTitle(UserInfo.getmCompanyName())
                .setLeftImageRes(R.drawable.zhan_kai)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });

        return returnView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case 1:
                break;
            default:
                break;
        }
    }
}