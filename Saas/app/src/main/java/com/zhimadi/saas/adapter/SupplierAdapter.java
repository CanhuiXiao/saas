package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.SuppliersEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class SupplierAdapter extends ArrayAdapter<SuppliersEvent.Supplier> {

    private Context mContext;
    private int resourceId;


    public SupplierAdapter(Context context, int resource, List<SuppliersEvent.Supplier> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SuppliersEvent.Supplier supplier = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvSupplierHomeName.setText(supplier.getName());
        holder.tvSupplierHomeManeger.setText("负责人 : " + supplier.getCharge_man());
        holder.tvSupplierHomeDept.setText("￥" + supplier.getAmount_topay());
        return convertView;
    }

    class ViewHolder {
        TextView tvSupplierHomeName;
        TextView tvSupplierHomeManeger;
        TextView tvSupplierHomeDept;

        public ViewHolder(View view) {
            tvSupplierHomeName = (TextView) view.findViewById(R.id.tv_supplier_home_name);
            tvSupplierHomeManeger = (TextView) view.findViewById(R.id.tv_supplier_home_manager);
            tvSupplierHomeDept = (TextView) view.findViewById(R.id.tv_supplier_home_dept);
            view.setTag(this);
        }
    }




}
