package com.zhimadi.saas.widget;

import android.content.Context;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class EditTextITem extends LinearLayout {

	private View returnView;
	private TextView tvItemName;
	private TextView tvItemNotNull;
	private EditText etItemValue;
	private View viewDivider;
	private View viewDividerFirst;
	private View viewDividerLast;

	public EditTextITem(Context context, AttributeSet attrs, String itemName, String hint) {
		super(context, attrs);
		returnView = LayoutInflater.from(context).inflate(R.layout.widget_edittext, this);
		tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
		etItemValue = (EditText) returnView.findViewById(R.id.et_item_value);
		tvItemNotNull = (TextView) returnView.findViewById(R.id.tv_item_not_null);
		viewDivider = returnView.findViewById(R.id.view_divider);
		viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
		viewDividerLast = returnView.findViewById(R.id.view_divider_last);
		tvItemName.setText(itemName);
		etItemValue.setHint(hint);
	}

	public EditTextITem isTop(boolean isTop){
		if(isTop){
			viewDividerFirst.setVisibility(View.VISIBLE);
		}else {
			viewDividerFirst.setVisibility(View.GONE);
		}
		return this;
	}

	public EditTextITem isBottom(boolean isBottom){
		if(isBottom){
			viewDividerLast.setVisibility(View.VISIBLE);
			viewDivider.setVisibility(View.GONE);
		}else {
			viewDividerLast.setVisibility(View.GONE);
			viewDivider.setVisibility(View.VISIBLE);
		}
		return this;
	}


	public EditTextITem isNotNull(boolean isNotNull){
		if(isNotNull){
			tvItemNotNull.setVisibility(View.VISIBLE);
		}else {
			tvItemNotNull.setVisibility(View.GONE);
		}
		return this;
	}


	public void SetItemValue(String value){
		etItemValue.setText(value);
	}
	public String GetItemValue(){
		return etItemValue.getText().toString();
	}


	public EditTextITem SetItemName(String name){
		tvItemName.setText(name);
		return this;
	}

	public EditTextITem SetHint(String hint){
		etItemValue.setHint(hint);
		return this;
	}

	public void setEditEnable(boolean enable){
		etItemValue.setEnabled(enable);
	}

	public void addTextChangedListener(TextWatcher textWatcher){
		etItemValue.addTextChangedListener(textWatcher);
	}

}
