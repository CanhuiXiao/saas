package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class RoleItem extends LinearLayout {

    private View returnView;
    private TextView tvRoleName;
    private TextView tvRoleNote;
    private CheckBox cbRoleSelect;


    public RoleItem(Context context, AttributeSet attrs, String name, String note) {
        super(context, attrs);
        returnView = LayoutInflater.from(context).inflate(R.layout.widget_role_item, this);
        tvRoleName = (TextView) returnView.findViewById(R.id.tv_role_name);
        tvRoleNote = (TextView) returnView.findViewById(R.id.tv_role_note);
        cbRoleSelect = (CheckBox) returnView.findViewById(R.id.cb_role_select);
        tvRoleName.setText(name);
        tvRoleNote.setText(note);
    }


    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener){
        cbRoleSelect.setOnCheckedChangeListener(onCheckedChangeListener);
    }

    public void setChecked(boolean isCheck){
        cbRoleSelect.setChecked(isCheck);
    }

}
