package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CategoriesEvent;
import com.zhimadi.saas.event.DefinedCategoriesEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class DefinedCategoryAdapter extends ArrayAdapter<DefinedCategoriesEvent.DefinedCategory>{

    private Context mContext;
    private int resourceId;

    public DefinedCategoryAdapter(Context context, int resource, List<DefinedCategoriesEvent.DefinedCategory> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DefinedCategoriesEvent.DefinedCategory definedCategory = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvDefinedCategoryHomeName.setText(definedCategory.getName());
        return convertView;
    }


    class ViewHolder {
        private TextView tvDefinedCategoryHomeName;

        public ViewHolder(View view) {
            tvDefinedCategoryHomeName = (TextView) view.findViewById(R.id.tv_defined_category_home_name);
            view.setTag(this);
        }
    }
}
