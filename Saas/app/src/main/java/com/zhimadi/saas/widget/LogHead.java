package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.zhimadi.saas.R;

public class LogHead extends LinearLayout {

    private View returnView;
    private Context mcContext;
    LinearLayout llLogHead;

    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_log_head, this);
        llLogHead = (LinearLayout) returnView.findViewById(R.id.ll_log_head);
    }

    public LogHead(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcContext = context;
        inte();
    }

    public void addItem(LogHeadItem logHeadItem) {
        LayoutParams params = new LayoutParams(0, -2);
        params.weight = 1;
        llLogHead.addView(logHeadItem, params);
    }

}
