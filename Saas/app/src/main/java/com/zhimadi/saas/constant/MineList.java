package com.zhimadi.saas.constant;

import android.app.Activity;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.UserManagerActivity;

/**
 * Created by carry on 2016/8/26.
 */
public class MineList {

    public enum Mine {

        ZhiMaMarket("芝麻集市", R.drawable.zhi_ma_ji_shi, null),
        ShopAndEmployee("门店与员工", R.drawable.men_dian_yu_yuan_gong, UserManagerActivity.class),
        MyServer("我的服务", R.drawable.wo_de_fu_wu, null),
        BuyServer("购买续费", R.drawable.gou_mai_xu_fei, null),
        Setting("设置", R.drawable.she_zhi, null);


        private String title;
        private int drawableResource;
        private Class<? extends Activity> activity;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getDrawableResource() {
            return drawableResource;
        }

        public void setDrawableResource(int drawableResource) {
            this.drawableResource = drawableResource;
        }

        public Class<? extends Activity> getActivity() {
            return activity;
        }

        public void setActivity(Class<? extends Activity> activity) {
            this.activity = activity;
        }

        Mine(String title, int drawableResource, Class<? extends Activity> activity) {
            this.title = title;
            this.drawableResource = drawableResource;
            this.activity = activity;
        }

        //手写过滤器惨惨惨...
        public static Mine getMine(int value) {    //手写的从int到enum的转换函数
            switch (value) {
                case 1:
                    return Mine.ZhiMaMarket;
                case 2:
                    return Mine.ShopAndEmployee;
                case 3:
                    return Mine.MyServer;
                case 4:
                    return Mine.BuyServer;
                case 5:
                    return Mine.Setting;
                default:
                    return null;
            }
        }
    }
}
