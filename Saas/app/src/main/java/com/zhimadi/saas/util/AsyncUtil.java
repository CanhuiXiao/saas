package com.zhimadi.saas.util;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.CommonResultEvent;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.Map;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by carry on 2016/7/27.
 */
public class AsyncUtil {

    private  AsyncHttpClient asyncHttpClient;
    private static AsyncUtil mInstance;
    private AsyncUtil() {
        asyncHttpClient = new AsyncHttpClient();
    }

    //单例模式
    public static AsyncUtil getInstance() {
        if (mInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (mInstance == null) {
                    mInstance = new AsyncUtil();
                }
            }
        }
        return mInstance;
    }

    public static void asyncGet(Context context, String URLStr, final SuccessListener successListener) {
        getInstance()._asyncGet(context, URLStr, successListener);
    }

    public static void asyncJsonPost(final Context mContext, String URLStr, String jsonStr, final SuccessListener successListener) {
        getInstance()._asyncJsonPost(mContext, URLStr, jsonStr, successListener);
    }

    public static void Land(final Context mContext, String URLStr, String jsonStr, final SuccessListener successListener) {
        getInstance()._Land(mContext, URLStr, jsonStr, successListener);
    }





//    ----------------------------------------------------------------------------------------------------------------

    private  void _asyncGet(final Context mContext, String URLStr, final SuccessListener successListener){
        asyncHttpClient = AsyncUtil.RequestAssembly(asyncHttpClient);
        asyncHttpClient.get(URLStr, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Gson commonGson = new Gson();
                CommonResultEvent commonResultEvent = commonGson.fromJson(response.toString(), CommonResultEvent.class);
                if(commonResultEvent.getCode() != 0){
                    Toast.makeText(mContext, commonResultEvent.getMsg(), Toast.LENGTH_SHORT).show();
                }else if(commonResultEvent.getCode() == 0){
                    if(successListener != null){
                        successListener.onSuccess(response.toString());
                    }
                }
            }
        });
    }

    private void _asyncJsonPost(final Context mContext, String URLStr, String jsonStr, final SuccessListener successListener) {
        try {
            asyncHttpClient = AsyncUtil.RequestAssembly(asyncHttpClient);
            final StringEntity stringEntity = new StringEntity(jsonStr, "utf-8");
            asyncHttpClient.post(mContext, URLStr, stringEntity, "application/json", new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Gson commonGson = new Gson();
                    CommonResultEvent commonResultEvent = commonGson.fromJson(response.toString(), CommonResultEvent.class);
                    if(commonResultEvent.getCode() != 0){
                        Toast.makeText(mContext, commonResultEvent.getMsg(), Toast.LENGTH_SHORT).show();
                    }else if(commonResultEvent.getCode() == 0){
                        if(successListener != null){
                            successListener.onSuccess(response.toString());
                        }
                    }
                }


                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                }


                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void _Land(final Context mContext, String URLStr, String jsonStr, final SuccessListener successListener) {
        try {
            final StringEntity stringEntity = new StringEntity(jsonStr);
            asyncHttpClient.post(mContext, OkHttpUtil.mLandAddress, stringEntity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);
                    Gson commonGson = new Gson();
                    CommonResultEvent commonResultEvent = commonGson.fromJson(response.toString(), CommonResultEvent.class);
                    if(commonResultEvent.getCode() != 0){
                        Toast.makeText(mContext, commonResultEvent.getMsg(), Toast.LENGTH_SHORT).show();
                    }else {
                        if(successListener != null){
                            successListener.onSuccess(response.toString());
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //回调接口(监听器)
    public interface SuccessListener {
        public void onSuccess(String response);
        public void onFail();
    }





    /** url关键词搜索组装 */
    public static String UrlKeyAssembly(String urlStr, Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder(urlStr);
        boolean isFirst = true;
        for (String key : params.keySet()) {
            if(isFirst){
                stringBuilder.append("?" + key + "=" + params.get(key));
                isFirst = false;
            }else {
                stringBuilder.append("&" + key + "=" + params.get(key));
            }
        }
        urlStr = stringBuilder.toString();
        return urlStr;
    }

    public static AsyncHttpClient RequestAssembly(AsyncHttpClient asyncHttpClient) {
        asyncHttpClient.addHeader("Auth", UserInfo.getmAuth());
        asyncHttpClient.addHeader("App-id", "2");
        asyncHttpClient.addHeader("Version", "1.0");
        asyncHttpClient.addHeader("Accept", "application/json");
        asyncHttpClient.addHeader("Client-id", UserInfo.getmIMEI());
        asyncHttpClient.addHeader("utf-8", "Accept-Charset");
        return asyncHttpClient;
    }


}
