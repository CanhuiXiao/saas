package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class SwitchWithTipItem extends LinearLayout {

    private View returnView;

    private TextView tvItemName;
    private TextView tvItemValue;

    private Switch mSwitch;

    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;

    public SwitchWithTipItem(Context context, AttributeSet attrs, String itemName, String itemValue) {
        super(context, attrs);
        returnView = LayoutInflater.from(context).inflate(R.layout.widget_switch_with_tip, this);
        mSwitch = (Switch) returnView.findViewById(R.id.sw_item);

        tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
        tvItemValue = (TextView) returnView.findViewById(R.id.tv_item_value);
        tvItemName.setText(itemName);
        tvItemValue.setText(itemValue);

        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
    }

    public SwitchWithTipItem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public void setOnCheckListener(CompoundButton.OnCheckedChangeListener onCheckListener) {
        mSwitch.setOnCheckedChangeListener(onCheckListener);
    }

    public SwitchWithTipItem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }


    public void SetName(String name) {
        tvItemName.setText(name);
    }

    public void SetValue(String value) {
        tvItemValue.setText(value);
    }

    public String IsCheck() {
        if(mSwitch.isChecked()){
            return "1";
        }else {
            return "0";
        }
    }

    public String GetItemValue() {
        return tvItemValue.getText().toString();
    }

    public void SetCheck(String checked) {
        if(checked.equals("1")){
            mSwitch.setChecked(true);
        }else if(checked.equals("0")){
            mSwitch.setChecked(false);
        }

    }

}
