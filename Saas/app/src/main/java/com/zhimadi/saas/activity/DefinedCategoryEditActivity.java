package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.DefinedCategoryDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class DefinedCategoryEditActivity extends BaseActivity implements View.OnClickListener {


    private TitleBarCommonBuilder titleBarCommonBuilder;

    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    private int intentMode;

    /**
     * 传数据源
     */
    private String definedCategoryId;

    private EditTextITem etDefinedCategoryName;
    private EditTextITem etDefinedCategoryNote;
    private Button btDefinedCategorySave;

    private LinearLayout llDefinedCategiryEditwindow;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            definedCategoryId = getIntent().getStringExtra("DEFINED_CATEGORY_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llDefinedCategiryEditwindow = (LinearLayout) findViewById(R.id.ll_defined_categiry_edit_window);
        etDefinedCategoryName = new EditTextITem(mContext, null, "名称", "请输入描述名称");
        etDefinedCategoryNote = new EditTextITem(mContext, null, "描述", "请输入描述信息");
        btDefinedCategorySave = (Button) findViewById(R.id.bt_defined_categiry_save);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defined_category_edit);
        inte();
        etDefinedCategoryName.isTop(true).isNotNull(true);
        llDefinedCategiryEditwindow.addView(etDefinedCategoryName);
        etDefinedCategoryNote.isBottom(true);
        llDefinedCategiryEditwindow.addView(etDefinedCategoryNote);

        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑店铺商品分类");
            GetDefinedCategoryDetail(definedCategoryId);
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增店铺商品分类");
        } else {
            finish();
        }

        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        btDefinedCategorySave.setOnClickListener(this);

    }


    private void GetDefinedCategoryDetail(String definedCategoryId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("cat_id", definedCategoryId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mSellerCatsDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                DefinedCategoryDetailEvent definedCategoryDetailEvent = gson.fromJson(response, DefinedCategoryDetailEvent.class);
                etDefinedCategoryName.SetItemValue(definedCategoryDetailEvent.getData().getName());
                etDefinedCategoryNote.SetItemValue(definedCategoryDetailEvent.getData().getDescription());
            }

            @Override
            public void onFail() {

            }
        });
    }

    private void DefinedCategoryEdit() {
        JsonObject jsonObject = new JsonObject();
        if(intentMode == EDIT_MODE){
            jsonObject.addProperty("cat_id", definedCategoryId);
        }
        jsonObject.addProperty("name", etDefinedCategoryName.GetItemValue());
        jsonObject.addProperty("description", etDefinedCategoryNote.GetItemValue());
        jsonObject.addProperty("display_order", "100");
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditSellerCatsAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if(commonResultEvent.getCode() == 0){
                    if(intentMode == ADD_MODE){
                        Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    }else if(intentMode == EDIT_MODE){
                        Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFail() {

            }
        });


    }









    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_defined_categiry_save:
                    DefinedCategoryEdit();
                break;

            default:
                break;
        }
    }


}
