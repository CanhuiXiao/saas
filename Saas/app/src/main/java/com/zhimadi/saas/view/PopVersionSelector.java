package com.zhimadi.saas.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class PopVersionSelector extends LinearLayout {

	private View returnView;
	private Context mContext;
	private RadioButton rbFruit;
	private RadioButton rbVegetable;
	private TextView tvComfirm;
	private int version = 0;

	public int getVersion() {
		return version;
	}

	public PopVersionSelector(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		returnView = LayoutInflater.from(mContext).inflate(R.layout.view_pop_version_selector, this);
		rbFruit = (RadioButton) returnView.findViewById(R.id.rb_fruit);
		rbVegetable = (RadioButton) returnView.findViewById(R.id.rb_vegetable);
		tvComfirm = (TextView) returnView.findViewById(R.id.tv_comfirm);

		rbFruit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					rbVegetable.setChecked(false);
					version = 1;
				}
			}
		});

		rbVegetable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					rbFruit.setChecked(false);
					version = 2;
				}
			}
		});



	}

	public void setOnClickListener(OnClickListener onClickListener){
		tvComfirm.setOnClickListener(onClickListener);
	}

}
