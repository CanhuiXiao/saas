package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StockCheckAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.StockChecksEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockCheckHomeActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener{

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private RadioGroupWithLayout rgStockCheckHome;
    //滑块集合
    private List<View> sliders;

    private List<StockChecksEvent.StockCheck> stockChecks;
    private ListView lvStockCheckHome;
    private StockCheckAdapter stockCheckAdapter;

    private void inte(){
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        rgStockCheckHome = (RadioGroupWithLayout) findViewById(R.id.rg_stock_check_home);
        sliders = new ArrayList<View>();
        View viewStockCheckHomeAll = findViewById(R.id.view_stock_check_home_all);
        sliders.add(viewStockCheckHomeAll);
        View viewStockCheckHomeFinish = findViewById(R.id.view_stock_check_home_finish);
        sliders.add(viewStockCheckHomeFinish);
        View viewStockCheckHomeNote = findViewById(R.id.view_stock_check_home_note);
        sliders.add(viewStockCheckHomeNote);
        View viewStockCheckHomeCancel = findViewById(R.id.view_stock_check_home_cancel);
        sliders.add(viewStockCheckHomeCancel);
        lvStockCheckHome = (ListView) findViewById(R.id.lv_stock_check_home);
        stockChecks = new ArrayList<StockChecksEvent.StockCheck>();
        stockCheckAdapter = new StockCheckAdapter(mContext, R.layout.item_lv_stock_check_home, stockChecks);
    }

    //显示滑块的方法
    private void ShowSlider(int RadioButtonId){
        for (int i = 0; i < sliders.size(); i++) {
            if(RadioButtonId == sliders.get(i).getId()){
                sliders.get(i).setVisibility(View.VISIBLE);
            }else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_check_home);
        inte();
        titleBarCommonBuilder.setTitle("盘点历史")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setRightImageRes(R.drawable.tian_jia02)
                .setRightImageResSecond(R.drawable.shou_suo);
        titleBarCommonBuilder.setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarCommonBuilder.setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, StockCheckEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                startActivity(intent);
            }
        });


        lvStockCheckHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, StockCheckEditActivity.class);
//                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
//                intent.putExtra("STOCK_CHECK_ID", stockChecks.get(position).getStockCheck_id());
//                intent.putExtra("STATE", stockChecks.get(position).getState());
//                startActivity(intent);
            }
        });

        lvStockCheckHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetStockCheck();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvStockCheckHome.setAdapter(stockCheckAdapter);
        GetStockCheck();
        rgStockCheckHome.setOnCheckedChangeListener(this);
    }

    private void GetStockCheck(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        if(state != Constant.STATE_ALL){
            map.put("state", state + "");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mStockCheckAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                StockChecksEvent stockChecksEvent = gson.fromJson(response, StockChecksEvent.class);
                if(mStart == 0){
                    stockChecks.clear();
                }
                mStart = mStart + stockChecksEvent.getData().getList().size();
                stockChecks.addAll(stockChecksEvent.getData().getList());
                stockCheckAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    private int state = Constant.STATE_ALL;

    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId){
            case R.id.rb_stock_check_home_all:
                ShowSlider(R.id.view_stock_check_home_all);
                mStart = 0;
                state = Constant.STATE_ALL;
                GetStockCheck();
                break;
            case R.id.rb_stock_check_home_finish:
                ShowSlider(R.id.view_stock_check_home_finish);
                mStart = 0;
                state = Constant.STATE_DEFAULT;
                GetStockCheck();
                break;
            case R.id.rb_stock_check_home_note:
                ShowSlider(R.id.view_stock_check_home_note);
                mStart = 0;
                state = Constant.STATE_NOTE;
                GetStockCheck();
                break;
            case R.id.rb_stock_check_home_cancel:
                ShowSlider(R.id.view_stock_check_home_cancel);
                mStart = 0;
                state = Constant.STATE_CANCEL;
                GetStockCheck();
                break;
            default:
                break;
        }
    }
}
