package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.CustomTypeAdapter;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.CustomTypesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.List;

public class CustomTypeSelectActivity extends BaseActivity {


    private TitleBarCommonBuilder titleBarCommonBuilder;
    private SwipeMenuListView lvCustomTypeHome;
    private List<CustomTypesEvent.CustomType> customTypes;
    private CustomTypeAdapter customTypeAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        customTypes = new ArrayList<CustomTypesEvent.CustomType>();
        lvCustomTypeHome = (SwipeMenuListView) findViewById(R.id.lv_custom_type_home);
        customTypeAdapter = new CustomTypeAdapter(mContext, R.layout.item_lv_custom_type_home, customTypes);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_type_home);
        inte();
        titleBarCommonBuilder.setTitle("客户分类管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder
                .setRightText("添加分类")
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, CustomTypeEditActivity.class);
                        intent.putExtra("INTENT_MODE", CustomTypeEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };
        lvCustomTypeHome.setMenuCreator(creator);
        lvCustomTypeHome.setAdapter(customTypeAdapter);
        lvCustomTypeHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        CustomTypeDelete(customTypes.get(position).getType_id());
                        break;
                    default:
                        break;
                }

                return true;
            }
        });
        lvCustomTypeHome.setMenuCreator(creator);
        lvCustomTypeHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent();
                intent.setClass(activity, CustomTypeEditActivity.class);
                intent.putExtra("INTENT_MODE", CustomTypeEditActivity.EDIT_MODE);
                intent.putExtra("CUSTOM_TYPE_ID", customTypes.get(position).getType_id());
                activity.startActivity(intent);
            }
        });
        GetCustom();


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        GetCustom();
    }

    private void GetCustom() {

        AsyncUtil.asyncGet(mContext, OkHttpUtil.mCustomTypeAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CustomTypesEvent customTypesEvent = gson.fromJson(response, CustomTypesEvent.class);
                customTypes.clear();
                customTypes.addAll(customTypesEvent.getData().getList());
                customTypeAdapter = new CustomTypeAdapter(mContext, R.layout.item_lv_custom_type_home, customTypes);
                lvCustomTypeHome.setAdapter(customTypeAdapter);
            }

            @Override
            public void onFail() {

            }
        });


    }


    private void CustomTypeDelete(String typeId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type_id", typeId);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteCustomTypeAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if (commonResultEvent.getCode() == 0) {
                    GetCustom();
                }
            }

            @Override
            public void onFail() {

            }
        });
    }


}
