package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.StoreDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpClientManager;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Request;

public class StoreEditActivity extends BaseActivity implements View.OnClickListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;

    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    public static final int REQUEST_CODE_SHOP = 1;
    public static final int REQUEST_CODE_EMPLOYEE = 2;

    private int intentMode;

    private String storeId;

    private String managerId = "";
    private String shopId = "";

    private LinearLayout llStoreEditWindow;
    private Button btStoreSave;

    private EditTextITem editTextITemName;
    private SelelctableTextViewItem selelctableTextViewItemManager;
    private SwitchItem switchITem;
    private SelelctableTextViewItem selelctableTextViewItemShop;
    private EditTextITem editTextITemOrderBy;
    private EditTextITem editTextITemNote;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            storeId = getIntent().getStringExtra("STORE_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llStoreEditWindow = (LinearLayout) findViewById(R.id.ll_store_edit_window);
        btStoreSave = (Button) findViewById(R.id.bt_store_save);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入仓库名称");
        selelctableTextViewItemManager = new SelelctableTextViewItem(mContext, null, "负责人", "请选择负责人");
        switchITem = new SwitchItem(mContext, null, "启动", true);
        selelctableTextViewItemShop = new SelelctableTextViewItem(mContext, null, "门店", "请选择门店");
        editTextITemOrderBy = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_edit);
        inte();
        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑仓库");
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增仓库");
        } else {
            finish();
        }

        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui).setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        selelctableTextViewItemManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, EmployeeSelectActivity.class);
                startActivityForResult(intent, REQUEST_CODE_EMPLOYEE);
            }
        });

        selelctableTextViewItemShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, ShopSelectActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SHOP);
            }
        });

        editTextITemName.isNotNull(true).isTop(true);
        llStoreEditWindow.addView(editTextITemName);
        selelctableTextViewItemManager.isNotNull(true);
        llStoreEditWindow.addView(selelctableTextViewItemManager);
        switchITem.isBottom(true);
        llStoreEditWindow.addView(switchITem);

        selelctableTextViewItemShop.isTop(true);
        llStoreEditWindow.addView(selelctableTextViewItemShop);
        editTextITemOrderBy.isNotNull(true);
        llStoreEditWindow.addView(editTextITemOrderBy);
        editTextITemNote.isBottom(true);
        llStoreEditWindow.addView(editTextITemNote);

        btStoreSave.setOnClickListener(this);

        if (intentMode == EDIT_MODE) {
            GetStoreDetail();
        }

    }


    private void GetStoreDetail() {
        final Map<String, String> map = new HashMap<String, String>();
        map.put("warehouse_id", storeId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mStoreDetailAddress, map);
        Request.Builder requestBuilder = new Request.Builder()
                .url(URLStr);
        requestBuilder = OkHttpUtil.RequestAssembly(requestBuilder);
        OkHttpClientManager.getAsyn(requestBuilder.build(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Request request, Exception e) {

            }

            @Override
            public void onResponse(String jsonStr) {
                Gson gson = new Gson();
                StoreDetailEvent storeDetailEvent = gson.fromJson(jsonStr, StoreDetailEvent.class);
                editTextITemName.SetItemValue(storeDetailEvent.getData().getName());
                selelctableTextViewItemManager.SetItemValue(storeDetailEvent.getData().getCharge_user_name());
                selelctableTextViewItemShop.SetItemValue(storeDetailEvent.getData().getShop_name());
                editTextITemOrderBy.SetItemValue(storeDetailEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(storeDetailEvent.getData().getNote());
                switchITem.SetCheck(storeDetailEvent.getData().getState());
                shopId = storeDetailEvent.getData().getShop_id();
                managerId = storeDetailEvent.getData().getCharge_user_id();
            }
        });
    }


    private void EditStore() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == EDIT_MODE) {
            jsonObject.addProperty("warehouse_id", storeId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        jsonObject.addProperty("display_order", editTextITemOrderBy.GetItemValue());
        jsonObject.addProperty("charge_user_id", managerId);
        jsonObject.addProperty("shop_id", shopId);
        jsonObject.addProperty("state", switchITem.IsCheck());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditStoreAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if (commonResultEvent.getCode() == 0) {
                    if (intentMode == ADD_MODE) {
                        Toast.makeText(mContext, "添加成功!", Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (intentMode == EDIT_MODE) {
                        Toast.makeText(mContext, "修改成功!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(mContext, "操作失败！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_SHOP:
                if (resultCode == ShopSelectActivity.RESULT_CODE_SUCCESS) {
                    shopId = data.getStringExtra("SHOP_ID");
                    selelctableTextViewItemShop.SetItemValue(data.getStringExtra("SHOP_NAME"));
                }
                break;
            case REQUEST_CODE_EMPLOYEE:
                if (resultCode == EmployeeSelectActivity.RESULT_CODE_SUCCESS) {
                    managerId = data.getStringExtra("EMPLOYEE_ID");
                    selelctableTextViewItemManager.SetItemValue(data.getStringExtra("EMPLOYEE_NAME"));
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_store_save:
                EditStore();
                break;

            default:
                break;
        }
    }


}
