package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CustomsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class CustomAdapter extends ArrayAdapter<CustomsEvent.Custom> {


    private Context mContext;
    private int resourceId;

    public CustomAdapter(Context context, int resource, List<CustomsEvent.Custom> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomsEvent.Custom custom = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvCustomHomeName.setText(custom.getName());
        holder.tvCustomHomeType.setText("客户类型 : " + "尊贵的钻石vip");
        holder.tvCustomHomeDebt.setText("￥" + custom.getAmount_owed());
        return convertView;
    }


    class ViewHolder {
        private TextView tvCustomHomeName;
        private TextView tvCustomHomeType;
        private TextView tvCustomHomeDebt;

        public ViewHolder(View view) {
            tvCustomHomeName = (TextView) view.findViewById(R.id.tv_custom_home_name);
            tvCustomHomeType = (TextView) view.findViewById(R.id.tv_custom_home_type);
            tvCustomHomeDebt = (TextView) view.findViewById(R.id.tv_custom_home_debt);
            view.setTag(this);
        }
    }
}
