package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.StockChecksEvent;
import com.zhimadi.saas.event.StockChecksEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class StockCheckAdapter extends ArrayAdapter<StockChecksEvent.StockCheck> {

    private Context mContext;
    private int resourceId;

    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals(Constant.STATE_DEFAULT + "")) {
            holder.tvStockCheckHomestate.setText("已盘点");
            holder.tvStockCheckHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_NOTE + "")) {
            holder.tvStockCheckHomestate.setText("草稿");
            holder.tvStockCheckHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_CANCEL + "")) {
            holder.tvStockCheckHomestate.setText("撤销");
            holder.tvStockCheckHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public StockCheckAdapter(Context context, int resource, List<StockChecksEvent.StockCheck> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StockChecksEvent.StockCheck stockCheck = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        showStateText(holder, stockCheck.getState());
        holder.tvStockCheckHomeOrderNumber.setText(stockCheck.getOrder_no());
        holder.tvStockCheckHomeStoreEmployee.setText(stockCheck.getWarehouse_name() + "-" + stockCheck.getCreate_user_name());
        holder.tvStockCheckHomeCreateDate.setText(stockCheck.getCreate_time());
        holder.tvStockCheckHomeStoreEmployee.setText(stockCheck.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {

        private TextView tvStockCheckHomeOrderNumber;
        private TextView tvStockCheckHomeStoreEmployee;
        private TextView tvStockCheckHomeCreateDate;
        private TextView tvStockCheckHomestate;

        public ViewHolder(View view) {

            tvStockCheckHomeOrderNumber = (TextView) view.findViewById(R.id.tv_stock_check_home_order_number);
            tvStockCheckHomeStoreEmployee = (TextView) view.findViewById(R.id.tv_stock_check_home_store_employee);
            tvStockCheckHomeCreateDate = (TextView) view.findViewById(R.id.tv_stock_check_home_create_date);
            tvStockCheckHomestate = (TextView) view.findViewById(R.id.tv_stock_check_home_state);
            view.setTag(this);
        }
    }


}
