package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.StockLossesEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class StockLossAdapter extends ArrayAdapter<StockLossesEvent.StockLoss> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals("0")) {
            holder.tvStockLossHomestate.setText("已报损");
            holder.tvStockLossHomestate.setBackgroundResource(R.drawable.shape_bt_buy_2);
        } else if (state.equals("1")) {
            holder.tvStockLossHomestate.setText("待审核");
            holder.tvStockLossHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("2")) {
            holder.tvStockLossHomestate.setText("已撤销");
            holder.tvStockLossHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("3")) {
            holder.tvStockLossHomestate.setText("草稿");
            holder.tvStockLossHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public StockLossAdapter(Context context, int resource, List<StockLossesEvent.StockLoss> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StockLossesEvent.StockLoss stockLoss = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvStockLossHomeSupplier.setText(stockLoss.getCreate_user_name());
        showStateText(holder, stockLoss.getState());
        holder.tvStockLossHomeOderNumber.setText(stockLoss.getOrder_no());
        holder.tvStockLossHomeToPay.setText("￥" + stockLoss.getTotal_amount());
        holder.tvStockLossHomeCreateDate.setText(stockLoss.getCreate_time());
        holder.tvStockLossHomeStore.setText(stockLoss.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {
        TextView tvStockLossHomeSupplier;
        TextView tvStockLossHomestate;
        TextView tvStockLossHomeOderNumber;
        TextView tvStockLossHomeToPay;
        TextView tvStockLossHomeCreateDate;
        TextView tvStockLossHomeStore;

        public ViewHolder(View view) {
            tvStockLossHomeSupplier = (TextView) view.findViewById(R.id.tv_stock_loss_home_supplier);
            tvStockLossHomestate = (TextView) view.findViewById(R.id.tv_stock_loss_home_state);
            tvStockLossHomeOderNumber = (TextView) view.findViewById(R.id.tv_stock_loss_home_oder_number);
            tvStockLossHomeToPay = (TextView) view.findViewById(R.id.tv_stock_loss_home_to_pay);
            tvStockLossHomeCreateDate = (TextView) view.findViewById(R.id.tv_stock_loss_home_create_date);
            tvStockLossHomeStore = (TextView) view.findViewById(R.id.tv_stock_loss_home_store);
            view.setTag(this);
        }
    }


}
