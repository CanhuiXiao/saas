package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.fragment.SellAgentAgentFragment;
import com.zhimadi.saas.fragment.BaseFragment;
import com.zhimadi.saas.fragment.FragmentController;
import com.zhimadi.saas.fragment.SellAgentOwnerFragment;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

public class SellAgentHomeActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener{

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private FragmentController fragmentController;
    private RadioGroup rgSellAgent;
    private SellAgentOwnerFragment ownerFragment;
    private SellAgentAgentFragment agentFragment;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        fragmentController = new FragmentController(mContext, R.id.fl_sell_agent_home);
        ownerFragment = new SellAgentOwnerFragment();
        ownerFragment.setTag(BaseFragment.FirstTAG);
        agentFragment = new SellAgentAgentFragment();
        agentFragment.setTag(BaseFragment.SecondTAG);
        rgSellAgent = (RadioGroup) findViewById(R.id.rg_sell_agent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_agent_home);
        inte();
        titleBarCommonBuilder.setTitle("我的代卖")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setRightImageRes(R.drawable.tian_jia02)
                .setRightImageResSecond(R.drawable.shou_suo);
        titleBarCommonBuilder.setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarCommonBuilder.setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, SellAgentAgentEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                startActivity(intent);
            }
        });

        rgSellAgent.setOnCheckedChangeListener(this);
        fragmentController.showFragment(ownerFragment, getFragmentManager());
    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.rb_sell_agent_owner:
                fragmentController.showFragment(ownerFragment, getFragmentManager());
                break;
            case R.id.rb_sell_agent_agent:
                fragmentController.showFragment(agentFragment, getFragmentManager());
                break;
            default:
                break;
        }
    }
}
