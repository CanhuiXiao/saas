package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Resours;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Role extends LinearLayout {

    private View returnView;
    private Context mContext;
    private LinearLayout llRoleWindow;
    private Map<String, String> map;
    private List<RoleItem> roleItems;

    public Role(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        roleItems = new ArrayList<RoleItem>();
        returnView = LayoutInflater.from(mContext).inflate(R.layout.widget_role, this);
        llRoleWindow = (LinearLayout) returnView.findViewById(R.id.ll_role_window);
        map = new HashMap<String, String>();
        for (int i = 0; i < Resours.roleTitle.length; i++) {
            final int d = i;
            RoleItem roleItem = new RoleItem(mContext , null, Resours.roleTitle[d], Resours.roleNote[d]);
            roleItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        map.put(d + "", Resours.roleId[d]);
                    }else {
                        map.remove(d+"");
                    }
                }
            });
            llRoleWindow.addView(roleItem);
            roleItems.add(roleItem);
        }
    }

    public String getRoleStr(){
        StringBuilder builder = new StringBuilder();
        Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = it.next();
            builder.append(entry.getValue()+",");
        }
        String temp = builder.toString();
        temp = temp.substring(0, temp.length()-1);
        return temp;
    }


    public void CheckItem(int itemId){
        switch (itemId){
            case 0:
                break;
            case 1:
                break;
            case 2:
                roleItems.get(0).setChecked(true);
                break;
            case 3:
                roleItems.get(1).setChecked(true);
                break;
            case 4:
                roleItems.get(2).setChecked(true);
                break;
            case 5:
                roleItems.get(3).setChecked(true);
                break;
            case 6:
                roleItems.get(4).setChecked(true);
                break;
            default:
                break;
        }
    }


}
