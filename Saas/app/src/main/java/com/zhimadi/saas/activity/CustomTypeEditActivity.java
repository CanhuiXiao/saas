package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CustomTypeDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class CustomTypeEditActivity extends BaseActivity implements View.OnClickListener{

    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    private int intentMode;

    private String customTypeId;

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private LinearLayout llCustomTypeEditWindow;

    private EditTextITem editTextITemName;
    private EditTextITem editTextITemDescribe;
    private RadioGroup rgCustomTypeEditDiscount;

    private Button btCustomSave;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            customTypeId = getIntent().getStringExtra("CUSTOM_TYPE_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llCustomTypeEditWindow = (LinearLayout) findViewById(R.id.ll_custom_type_edit_window);
        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入分类名称");
        editTextITemDescribe = new EditTextITem(mContext, null, "描述", "请输入描述信息");
        rgCustomTypeEditDiscount = (RadioGroup) findViewById(R.id.rg_custom_type_edit_discount);
        btCustomSave = (Button) findViewById(R.id.bt_custom_type_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_type_edit);
        inte();
        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑客户类型");
            GetCustomType();
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增客户类型");
        } else {
            finish();
        }

        titleBarCommonBuilder
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });




        rgCustomTypeEditDiscount.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                switch (id){

                }
            }
        });
        editTextITemName.isTop(true);
        llCustomTypeEditWindow.addView(editTextITemName);
        editTextITemDescribe.isBottom(true);
        llCustomTypeEditWindow.addView(editTextITemDescribe);
        btCustomSave.setOnClickListener(this);


    }

    private void GetCustomType(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("type_id", customTypeId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mCustomTypeDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CustomTypeDetailEvent customTypeDetailEvent = gson.fromJson(response, CustomTypeDetailEvent.class);
                editTextITemName.SetItemValue(customTypeDetailEvent.getData().getName());
                editTextITemDescribe.SetItemValue(customTypeDetailEvent.getData().getNote());
                if(customTypeDetailEvent.getData().getHave_price().equals("1")){
                    rgCustomTypeEditDiscount.check(R.id.rb_custom_type_edit_have_price);
                }else if(customTypeDetailEvent.getData().getHave_discount().equals("1")){
                    rgCustomTypeEditDiscount.check(R.id.rb_custom_type_edit_have_discount);
                }else {
                    rgCustomTypeEditDiscount.check(R.id.rb_custom_type_edit_close);
                }
            }

            @Override
            public void onFail() {

            }
        });

    }


    private void CustomTypeEdit(){
        JsonObject jsonObject = new JsonObject();
        if(intentMode == EDIT_MODE){
            jsonObject.addProperty("type_id", customTypeId);
        }
        jsonObject.addProperty("note", editTextITemDescribe.GetItemValue());
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        //勾选id
        int checkId = rgCustomTypeEditDiscount.getCheckedRadioButtonId();
        String havePrice = "";
        String haveDisCount = "";
        switch (checkId){
            case R.id.rb_custom_type_edit_close:
                havePrice = "0";
                haveDisCount = "0";
                break;
            case R.id.rb_custom_type_edit_have_price:
                havePrice = "1";
                haveDisCount = "0";
                break;
            case R.id.rb_custom_type_edit_have_discount:
                havePrice = "0";
                haveDisCount = "1";
                break;
            default:
                break;
        }
        jsonObject.addProperty("have_price", havePrice);
        jsonObject.addProperty("have_discount", haveDisCount);
        jsonObject.addProperty("display_order", "100");


        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditCustomTypeAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                if(intentMode == ADD_MODE){
                    Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                    finish();
                }else if(intentMode == EDIT_MODE){
                    Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_custom_type_save:
                    CustomTypeEdit();
                break;
            default:
                break;
        }
    }
}
