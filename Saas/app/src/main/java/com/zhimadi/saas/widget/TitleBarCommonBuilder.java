package com.zhimadi.saas.widget;

import com.zhimadi.saas.R;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class TitleBarCommonBuilder {

	private View titleView;
	private TextView titleName;
	private TextView tvTitleLeft;
	private TextView tvTitleRight;
	private TextView tvTitleRightSecond;


	public TitleBarCommonBuilder(Activity context) {
		titleView =  context.findViewById(R.id.widget_titlebar_common);
		titleName = (TextView) titleView.findViewById(R.id.tv_title_name);
		tvTitleLeft = (TextView) titleView.findViewById(R.id.tv_title_left);
		tvTitleRight = (TextView) titleView.findViewById(R.id.tv_title_right);
		tvTitleRightSecond = (TextView) titleView.findViewById(R.id.tv_title_right_second);
	}


	public TitleBarCommonBuilder(View context) {
		titleView =  context.findViewById(R.id.widget_titlebar_common);
		titleName = (TextView) titleView.findViewById(R.id.tv_title_name);
		tvTitleLeft = (TextView) titleView.findViewById(R.id.tv_title_left);
		tvTitleRight = (TextView) titleView.findViewById(R.id.tv_title_right);
		tvTitleRightSecond = (TextView) titleView.findViewById(R.id.tv_title_right_second);
	}

	public View GetTitleView(){
		return titleView;
	}

	public TitleBarCommonBuilder setTitle(String titleName) {
		this.titleName.setText(titleName);
		return this;
	}

	public void setLeftOnClikListener(OnClickListener listener) {
		tvTitleLeft.setOnClickListener(listener);
	}

	public void setRightOnClikListener(OnClickListener listener) {
		tvTitleRight.setOnClickListener(listener);
	}
	
	public TitleBarCommonBuilder setLeftImageRes(int ivResourse) {
		tvTitleLeft.setCompoundDrawablesWithIntrinsicBounds(ivResourse, 0, 0, 0);
		return this;
	}



	public TitleBarCommonBuilder setRightImageRes(int ivResourse) {
		tvTitleRight.setCompoundDrawablesWithIntrinsicBounds(0, 0, ivResourse, 0);
		return this;
		
	}

	public TitleBarCommonBuilder setRightImageResSecond(int ivResourse) {
		tvTitleRightSecond.setCompoundDrawablesWithIntrinsicBounds(0, 0, ivResourse, 0);
		return this;

	}

	public TitleBarCommonBuilder setRightText(String tvText) {
		tvTitleRight.setText(tvText);
		return this;
		
	}
	
	public TitleBarCommonBuilder setLeftText(String tvText) {
		tvTitleLeft.setText(tvText);
		return this;
	}

	public TitleBarCommonBuilder setLeftTextColor(int color) {
		tvTitleLeft.setTextColor(color);
		return this;
	}
}
