package com.zhimadi.saas.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AppsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.AppModel;
import com.zhimadi.saas.widget.AppModelItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

/**
 * Created by carry on 2016/7/8.
 */

public class AppFragment extends BaseFragment {

    private View returnView;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private LinearLayout llAppHomeWindow;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(returnView);
        llAppHomeWindow = (LinearLayout) returnView.findViewById(R.id.ll_app_home_window);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_app, null);
        inte();
        titleBarCommonBuilder.setTitle("应用");


        AsyncUtil.asyncGet(activity, OkHttpUtil.mAppAllAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                AppsEvent appsEvent = gson.fromJson(response, AppsEvent.class);
                for (int i = 0; i < appsEvent.getData().size(); i++) {
                    llAppHomeWindow.addView(new AppModel(activity, null, appsEvent.getData().get(i)));
                }
            }

            @Override
            public void onFail() {

            }
        });

        return returnView;
    }
}