package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class TextViewItem extends LinearLayout {

    private View returnView;
    private Context mContext;
    private TextView tvItemValue;
    private TextView tvItemName;
    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;

    private void inite(){
        returnView = LayoutInflater.from(mContext).inflate(R.layout.widget_textview, this);
        tvItemValue = (TextView) returnView.findViewById(R.id.tv_item_value);
        tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
    }


    public TextViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        inite();
    }

    public TextViewItem(Context context, AttributeSet attrs, String name) {
        super(context, attrs);
        mContext = context;
        inite();
        tvItemName.setText(name);
    }

    public TextViewItem(Context context, AttributeSet attrs, String name, String value) {
        super(context, attrs);
        mContext = context;
        inite();
        tvItemName.setText(name);
        tvItemValue.setText(value);
    }

    public TextViewItem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public TextViewItem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public TextViewItem SetItemValue(String value) {
        tvItemValue.setText(value);
        return this;
    }

    public String GetItemValue() {
        return tvItemValue.getText().toString();
    }

    public TextViewItem SetItemName(String name) {
        tvItemName.setText(name);
        return this;
    }


}
