package com.zhimadi.saas.util;

import android.widget.Toast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by carry on 2016/7/12.
 */
public class EncryptUtil {


    public static String PasswordEncryptUtil(String password) {
        String temp = SHA1(password).toLowerCase();
        temp = SHA1( temp + "12345").toLowerCase();
        return temp;
    }


    private static String SHA1(String decript) {

        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.update(decript.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    private static String MakeSalt(){
        int i = (int) Math.random()*100000;
        return String.valueOf(i);
    }





}
