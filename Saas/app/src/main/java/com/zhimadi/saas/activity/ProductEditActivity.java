package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.ProductDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.EditTextWithUnitItem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SpinnerITem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.SwitchWithTipItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductEditActivity extends BaseActivity implements View.OnClickListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;

    //请求码
    public static final int requestCodeForCategory = 1;
    public static final int requestCodeForFormat = 2;
    public static final int requestCodeForDefineCategory = 3;
    public static final int requestCodeForStore = 4;


    //请求模式
    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    private int intentMode;

    private String productId;

    private Button btProductSave;

    private LinearLayout llProductEditwindow;

    private SelelctableTextViewItem selelctableTextViewItemName;
    private SelelctableTextViewItem selelctableTextViewItemFormat;
    private SwitchWithTipItem switchWithTipItemFix;
    private EditTextWithUnitItem editTextWithUnitItemFixWeight;

    private EditTextITem editTextITemSku;
    private SelelctableTextViewItem selelctableTextViewItemDefineCategory;

    private SpinnerITem spinnerITemPriceBuy;
    private SpinnerITem spinnerITemPriceSell;

    private SelelctableTextViewItem selelctableTextViewItemStore;
    private EditTextWithUnitItem editTextWithUnitItemInventory;
    private SwitchItem switchITem2InventoryHint;
    private EditTextWithUnitItem editTextWithUnitItemInventoryMax;
    private EditTextWithUnitItem editTextWithUnitItemInventoryMin;

    private SwitchWithTipItem switchWithTipItemIsSell;


    //被隐藏的post信息
    private String catagoryName;
    private String typeName;
    private String name;


    private String catagoryId;
    private String typeId;
    private String formatOptionId;
    private String definedCategoryId;
    private String storeId;


    private void inte() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        intentMode = getIntent().getIntExtra("INTENT_MODE", Constant.DEFAULT_MODE);
        if (intentMode == EDIT_MODE) {
            productId = getIntent().getStringExtra("PRODUCT_ID");
        }

        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llProductEditwindow = (LinearLayout) findViewById(R.id.ll_product_edit_window);

        //1
        selelctableTextViewItemName = new SelelctableTextViewItem(mContext, null, "商品名称", "请选择商品名称");
        //2
        selelctableTextViewItemFormat = new SelelctableTextViewItem(mContext, null, "规格", "请选择规格");
        //3
        switchWithTipItemFix = new SwitchWithTipItem(mContext, null, "是否定装", "否");
        //4
        editTextWithUnitItemFixWeight = new EditTextWithUnitItem(mContext, null, "重量", "请输入商品重量", "公斤");

        //1
        editTextITemSku = new EditTextITem(mContext, null, "商品货号", "请输入商品货号");
        //2
        selelctableTextViewItemDefineCategory = new SelelctableTextViewItem(mContext, null, "分类选择", "请选择产品类型");

        //1
        List<String> units = new ArrayList<String>();
        units.add("元/件");
        units.add("元/公斤");
        spinnerITemPriceBuy = new SpinnerITem(mContext, null, "采购价", "请输入采购价", units, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //2
        units = new ArrayList<String>();
        units.add("元/件");
        units.add("元/公斤");
        spinnerITemPriceSell = new SpinnerITem(mContext, null, "销售价", "请输入销售价", units, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //1   只放一个仓库？
        selelctableTextViewItemStore = new SelelctableTextViewItem(mContext, null, "仓库", "请选择关联仓库");
        //2
        editTextWithUnitItemInventory = new EditTextWithUnitItem(mContext, null, "库存量", "请输入库存量", "件");
        //3
        switchITem2InventoryHint = new SwitchItem(mContext, null, "库存警示", false);
        //4
        editTextWithUnitItemInventoryMax = new EditTextWithUnitItem(mContext, null, "上限", "请输入库存量上限", "件");
        //5
        editTextWithUnitItemInventoryMin = new EditTextWithUnitItem(mContext, null, "下限", "请输入库存量下限", "件");

        //1
        switchWithTipItemIsSell = new SwitchWithTipItem(mContext, null, "状态", "下架");


        btProductSave = (Button) findViewById(R.id.bt_product_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);
        inte();
        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑商品");
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增商品");
        } else {
            finish();
        }
        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });


        selelctableTextViewItemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, CategorySelectActivity.class);
                startActivityForResult(intent, requestCodeForCategory);
            }
        });


        selelctableTextViewItemFormat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, FormatSelectActivity.class);
                intent.putExtra("CATEGORY_ID", catagoryId);
                startActivityForResult(intent, requestCodeForFormat);
            }
        });


        selelctableTextViewItemFormat.setClickable(false);

        switchWithTipItemFix.setOnCheckListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    switchWithTipItemFix.SetValue("是");
                    editTextWithUnitItemFixWeight.setEditEnable(true);
                } else {
                    switchWithTipItemFix.SetValue("否");
                    editTextWithUnitItemFixWeight.setEditEnable(false);
                }
            }
        });

        selelctableTextViewItemDefineCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, DefinedCategorySelectActivity.class);
                startActivityForResult(intent, requestCodeForDefineCategory);
            }
        });

        selelctableTextViewItemStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, StoreSelectActivity.class);
                startActivityForResult(intent, requestCodeForStore);
            }
        });

        switchWithTipItemIsSell.setOnCheckListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    switchWithTipItemIsSell.SetValue("上架");
                } else {
                    switchWithTipItemIsSell.SetValue("下架");
                }
            }
        });


        selelctableTextViewItemName.isTop(true);
        llProductEditwindow.addView(selelctableTextViewItemName);
        llProductEditwindow.addView(selelctableTextViewItemFormat);
        llProductEditwindow.addView(switchWithTipItemFix);
        editTextWithUnitItemFixWeight.isBottom(true);
        llProductEditwindow.addView(editTextWithUnitItemFixWeight);

        editTextITemSku.isTop(true);
        llProductEditwindow.addView(editTextITemSku);
        selelctableTextViewItemDefineCategory.isBottom(true);
        llProductEditwindow.addView(selelctableTextViewItemDefineCategory);


        spinnerITemPriceBuy.isTop(true);
        llProductEditwindow.addView(spinnerITemPriceBuy);
        spinnerITemPriceSell.isBottom(true);
        llProductEditwindow.addView(spinnerITemPriceSell);


        selelctableTextViewItemStore.isTop(true);
        llProductEditwindow.addView(selelctableTextViewItemStore);
        llProductEditwindow.addView(editTextWithUnitItemInventory);
        llProductEditwindow.addView(switchITem2InventoryHint);
        llProductEditwindow.addView(editTextWithUnitItemInventoryMax);
        editTextWithUnitItemInventoryMin.isBottom(true);
        llProductEditwindow.addView(editTextWithUnitItemInventoryMin);

        switchWithTipItemIsSell.isTop(true)
                .isBottom(true);
        llProductEditwindow.addView(switchWithTipItemIsSell);


        btProductSave.setOnClickListener(this);


        if (intentMode == EDIT_MODE) {
            getProductDetail();
        }
    }


    private void getProductDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("product_id", productId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mProductDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ProductDetailEvent productDetailEvent = gson.fromJson(response, ProductDetailEvent.class);
                catagoryId = productDetailEvent.getData().getCategory_id();
                typeId = productDetailEvent.getData().getType_id();
                name = productDetailEvent.getData().getName();
                selelctableTextViewItemName.SetItemValue(productDetailEvent.getData().getType_name());
                formatOptionId = productDetailEvent.getData().getProps();
                selelctableTextViewItemFormat.SetItemValue(productDetailEvent.getData().getProps_name());

                if (!(catagoryId == null || catagoryId.equals(""))) {
                    selelctableTextViewItemFormat.setClickable(true);
                }
                //判断是不是定装,0是非定装
                if (productDetailEvent.getData().getIs_fixed().equals("0")) {
                    editTextWithUnitItemFixWeight.setEditEnable(false);
                } else if (productDetailEvent.getData().getIs_fixed().equals("1")) {
                    editTextWithUnitItemFixWeight.setEditEnable(true);
                }
                switchWithTipItemFix.SetCheck(productDetailEvent.getData().getIs_fixed());
                editTextWithUnitItemFixWeight.SetItemValue(productDetailEvent.getData().getFixed_weight());

                editTextITemSku.SetItemValue(productDetailEvent.getData().getSku());

                definedCategoryId = productDetailEvent.getData().getCat_id();
                selelctableTextViewItemDefineCategory.SetItemValue(productDetailEvent.getData().getCat_name());

                spinnerITemPriceBuy.SetItemValue(productDetailEvent.getData().getPurchase_price());
                spinnerITemPriceSell.SetItemValue(productDetailEvent.getData().getSelling_price());

                storeId = productDetailEvent.getData().getWarehouse_id();
//                selelctableTextViewItem2Store.SetItemValue(productDetailEvent.getData().get)
                selelctableTextViewItemDefineCategory.SetItemValue(productDetailEvent.getData().getCat_name());
                editTextWithUnitItemInventory.SetItemValue(productDetailEvent.getData().getQuantity());

                switchITem2InventoryHint.SetCheck(productDetailEvent.getData().getIs_stock_warning());

                editTextWithUnitItemInventoryMax.SetItemValue(productDetailEvent.getData().getWarn_number_top());

                editTextWithUnitItemInventoryMin.SetItemValue(productDetailEvent.getData().getWarn_number_low());

                switchWithTipItemIsSell.SetCheck(productDetailEvent.getData().getIs_sell());
            }

            @Override
            public void onFail() {

            }
        });


    }

    private void productEdit() {
//        "company_id"："1",
//                "category_id": "2",
//                "type_id": 1,
//                "cat_id": 1,
//                "warehouse_id": 1,
//                "name": "奇异果 新西兰奇异果",
//                "description": "新西兰奇异果",
//                "selling_price": 126,
//                "purchase_price": 110,
//                "sku": "a12340",
//                "is_fixed": "0"
//        "fixed_weight": "5.0"
//        "manufacturer_part_number": "xxx",.................................
//                "img_url": "",....................................
//                "mini_num": 10,....................................
//                "quantity": 999,
//                "is_stock_warning": 0,
//                "warn_number_low": 10,
//                "warn_number_top": 200,
//                "status": 0,..................................................
//                "is_sell": 0,
//                "display_order": "100",
//                "props": "1:2;2:4",
//                "pic_urls": "/thumbs/0049250.jpg;/thumbs/0049260.jpg"......................

        if (typeId == null || catagoryId == null) {
            Toast.makeText(mContext, "你还没选择商品名称", Toast.LENGTH_SHORT).show();
        } else if (editTextITemSku.GetItemValue().equals("")) {
            Toast.makeText(mContext, "你还没填写商品货号", Toast.LENGTH_SHORT).show();
        } else {
            JsonObject jsonObject = new JsonObject();
            //判断编辑还是添加
            if (!(productId == null || productId.equals(""))) {
                jsonObject.addProperty("product_id", productId);
            }
            jsonObject.addProperty("company_id", UserInfo.getmCompanyId());
            jsonObject.addProperty("category_id", catagoryId);
            jsonObject.addProperty("type_id", typeId);
            jsonObject.addProperty("props", formatOptionId);

            if (definedCategoryId != null) {
                jsonObject.addProperty("cat_id", definedCategoryId);
            }
            if (storeId != null) {
                jsonObject.addProperty("warehouse_id", storeId);
            }
            //必填
            jsonObject.addProperty("name", name);
            jsonObject.addProperty("purchase_price", spinnerITemPriceBuy.GetItemValue());
            jsonObject.addProperty("selling_price", spinnerITemPriceSell.GetItemValue());
            jsonObject.addProperty("sku", editTextITemSku.GetItemValue());
            jsonObject.addProperty("is_fixed", switchWithTipItemFix.IsCheck());
            jsonObject.addProperty("fixed_weight", editTextWithUnitItemFixWeight.GetItemValue());
            jsonObject.addProperty("quantity", editTextWithUnitItemInventory.GetItemValue());
            jsonObject.addProperty("is_stock_warning", switchITem2InventoryHint.IsCheck());
            jsonObject.addProperty("warn_number_low", editTextWithUnitItemInventoryMin.GetItemValue());
            jsonObject.addProperty("warn_number_top", editTextWithUnitItemInventoryMax.GetItemValue());
            jsonObject.addProperty("is_sell", switchWithTipItemIsSell.IsCheck());
            jsonObject.addProperty("pic_urls", "http://www.baidu.com/img/baidu_jgylogo3.gif");


            AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditProductAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
                @Override
                public void onSuccess(String response) {
                    finish();
                }

                @Override
                public void onFail() {

                }
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case requestCodeForCategory:
                if (resultCode == CategorySelectActivity.resultCodeSuccess) {
                    catagoryId = data.getStringExtra("CATEGORY_ID");
                    catagoryName = data.getStringExtra("CATEGORY_NAME");
                    typeId = data.getStringExtra("TYPE_ID");
                    typeName = data.getStringExtra("TYPE_NAME");
                    name = catagoryName + " " + typeName;
                    selelctableTextViewItemName.SetItemValue(data.getStringExtra("TYPE_NAME"));
                    if (!(catagoryId == null || catagoryId.equals(""))) {
                        selelctableTextViewItemFormat.setClickable(true);
                    }
                    selelctableTextViewItemFormat.SetItemValue("");
                    formatOptionId = "";
                }
                break;

            case requestCodeForFormat:
                if (resultCode == FormatSelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemFormat.SetItemValue(data.getStringExtra("FORMAT_OPTION_VALUE"));
                    formatOptionId = data.getStringExtra("FORMAT_OPTION_ID");
                }
                break;
            case requestCodeForDefineCategory:
                if (resultCode == DefinedCategorySelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemDefineCategory.SetItemValue(data.getStringExtra("DEFINED_NAME"));
                    definedCategoryId = data.getStringExtra("DEFINED_ID");
                }
                break;
            case requestCodeForStore:
                if (resultCode == StoreSelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemStore.SetItemValue(data.getStringExtra("STORE_NAME"));
                    storeId = data.getStringExtra("STORE_ID");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_product_save:
                productEdit();
                break;

            default:
                break;
        }
    }


}
