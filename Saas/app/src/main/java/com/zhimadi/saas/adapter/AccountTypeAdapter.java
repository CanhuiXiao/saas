//package com.zhimadi.saas.adapter;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.event.AccountTypesEvent;
//import com.zhimadi.saas.event.AccountsEvent;
//
//import java.util.List;
//
///**
// * Created by carry on 2016/7/13.
// */
//public class AccountTypeAdapter extends ArrayAdapter<AccountTypesEvent.AccountType>{
//
//
//    private Context mContext;
//    private View returnView;
//    private int resourceId;
//
//
//    public AccountTypeAdapter(Context context, int resource, List<AccountTypesEvent.AccountType> objects) {
//        super(context, resource, objects);
//        mContext = context;
//        resourceId = resource;
//
//    }
//
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        AccountTypesEvent.AccountType accountType = getItem(position);
//        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
//        return returnView;
//    }
//}
