package com.zhimadi.saas.util;

import android.content.Context;
import android.telephony.TelephonyManager;

/**
 * Created by carry on 2016/7/12.
 */
public class PhoneDataUtil {

    public static String GetIMEI(Context context){
        TelephonyManager tm = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }
}
