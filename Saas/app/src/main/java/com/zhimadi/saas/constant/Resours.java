package com.zhimadi.saas.constant;

import android.app.Activity;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.AgentHomeActivity;
import com.zhimadi.saas.activity.BuyHomeActivity;
import com.zhimadi.saas.activity.CustomHomeActivity;
import com.zhimadi.saas.activity.CustomTypeHomeActivity;
import com.zhimadi.saas.activity.DefinedCategoryHomeActivity;
import com.zhimadi.saas.activity.OwnerHomeActivity;
import com.zhimadi.saas.activity.PaymentTypeHomeActivity;
import com.zhimadi.saas.activity.ProductHomeActivity;
import com.zhimadi.saas.activity.SellAgentHomeActivity;
import com.zhimadi.saas.activity.SellHomeActivity;
import com.zhimadi.saas.activity.StoreHomeActivity;
import com.zhimadi.saas.activity.SupplierHomeActivity;
import com.zhimadi.saas.activity.UserManagerActivity;

/**
 * Created by carry on 2016/7/8.
 */
public class Resours {

    public static String[] userHomeTitle = {"芝麻集市", "门店与员工", "我的服务", "购买续费", "设置"};

    public static int[] userHomeIcon = {R.drawable.zhi_ma_ji_shi, R.drawable.men_dian_yu_yuan_gong, R.drawable.wo_de_fu_wu, R.drawable.gou_mai_xu_fei, R.drawable.she_zhi};

    public static Class<? extends Activity> target[] = new Class[]{null, UserManagerActivity.class, null, null, null};


    public static String[] roleTitle = {"采购员", "销售员", "仓库管理员", "运营", "财务"};
    public static String[] roleNote = {"负责采购事宜，包括新建商品、采购单、采购退货库存盘点等", "负责销售业务", "负责仓库管理、库存盘点、库存调拨等", "负责代卖事宜、商品管理、客户管理等", "负责财务事宜"};
    public static String[] roleId = {"2", "3", "4", "5", "6"};

}
