package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AgentsEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class AgentAdapter extends ArrayAdapter<AgentsEvent.Agent> {

    private Context mContext;
    private int resourceId;

    public AgentAdapter(Context context, int resource, List<AgentsEvent.Agent> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AgentsEvent.Agent agent = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvAgentHomeName.setText(agent.getName());
        return convertView;
    }

    class ViewHolder {
        TextView tvAgentHomeName;
        public ViewHolder(View view) {
            tvAgentHomeName = (TextView) view.findViewById(R.id.tv_agent_home_name);
            view.setTag(this);
        }
    }




}
