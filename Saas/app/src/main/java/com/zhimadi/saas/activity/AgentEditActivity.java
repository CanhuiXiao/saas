package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AgentDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class AgentEditActivity extends BaseActivity implements View.OnClickListener {

    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    public static final int REQUEST_CODE_FOR_AREA = 1;
    /**
     * 打开模式
     */
    private int intentMode;

    private String agentId;
    private String areaCode = "";

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private LinearLayout llAgentEditWindow;

    private EditTextITem editTextITemName;
    private SwitchItem switchITem;

    private EditTextITem editTextITemPhone;
    private EditTextITem editTextITemTel;
    private EditTextITem editTextITemEMail;
    private EditTextITem editTextITemFax;
    private EditTextITem editTextITemWebAddress;

    private SelelctableTextViewItem selelctableTextViewItemArea;
    private EditTextITem editTextITemAddress2;

    private EditTextITem editTextITemBank;
    private EditTextITem editTextITemBankCardNumber;
    private EditTextITem editTextITemBankCreateName;

    private EditTextITem editTextITemOrderSize;
    private EditTextITem editTextITemNote;

    private Button btAgentSave;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            agentId = getIntent().getStringExtra("OWNER_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);

        llAgentEditWindow = (LinearLayout) findViewById(R.id.ll_agent_edit_window);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入代卖代卖商名称");
        switchITem = new SwitchItem(mContext, null, "启动", true);

        editTextITemPhone = new EditTextITem(mContext, null, "手机", "请输入代卖商手机");
        editTextITemTel = new EditTextITem(mContext, null, "电话", "请输入电话");
        editTextITemEMail = new EditTextITem(mContext, null, "邮箱", "请输入代卖商邮箱");
        editTextITemFax = new EditTextITem(mContext, null, "传真", "请输入传真号码");
        editTextITemWebAddress = new EditTextITem(mContext, null, "网址", "请输入公司网址");

        selelctableTextViewItemArea = new SelelctableTextViewItem(mContext, null, "所在地区", "请选择所在地区");
        editTextITemAddress2 = new EditTextITem(mContext, null, "详细地址", "请输入详细地址");

        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        editTextITemBank = new EditTextITem(mContext, null, "开户银行", "请输入开户银行");
        editTextITemBankCardNumber = new EditTextITem(mContext, null, "银行账户", "请输入银行账户");
        editTextITemBankCreateName = new EditTextITem(mContext, null, "银行账号", "请输入银行账号");


        btAgentSave = (Button) findViewById(R.id.bt_agent_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_edit);
        inte();

        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑代卖商");
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增代卖商");
        } else {
            finish();
        }


        selelctableTextViewItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, AreaSelectActivity.class);
                startActivityForResult(intent, REQUEST_CODE_FOR_AREA);
            }
        });



        editTextITemName.isTop(true).isNotNull(true);
        llAgentEditWindow.addView(editTextITemName);
        switchITem.isBottom(true);
        llAgentEditWindow.addView(switchITem);

        editTextITemPhone.isTop(true).isBottom(true);
        llAgentEditWindow.addView(editTextITemPhone);
        llAgentEditWindow.addView(editTextITemTel);
        llAgentEditWindow.addView(editTextITemEMail);
        llAgentEditWindow.addView(editTextITemFax);
        editTextITemWebAddress.isBottom(true);
        llAgentEditWindow.addView(editTextITemWebAddress);

        selelctableTextViewItemArea.isTop(true);
        llAgentEditWindow.addView(selelctableTextViewItemArea);
        editTextITemAddress2.isBottom(true);
        llAgentEditWindow.addView(editTextITemAddress2);

        editTextITemBank.isTop(true);
        llAgentEditWindow.addView(editTextITemBank);
        llAgentEditWindow.addView(editTextITemBankCardNumber);
        editTextITemBankCreateName.isBottom(true);
        llAgentEditWindow.addView(editTextITemBankCreateName);

        editTextITemOrderSize.isTop(true).isNotNull(true);
        llAgentEditWindow.addView(editTextITemOrderSize);
        editTextITemNote.isBottom(true);
        llAgentEditWindow.addView(editTextITemNote);


        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        btAgentSave.setOnClickListener(this);
        if (intentMode == EDIT_MODE) {
            GetAgentDetail();
        }else {
            editTextITemOrderSize.SetItemValue("100");
        }

    }


    private void GetAgentDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("agent_id", agentId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mAgentDetailAddress, map);

        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                AgentDetailEvent agentDetailEvent = gson.fromJson(response, AgentDetailEvent.class);
                editTextITemName.SetItemValue(agentDetailEvent.getData().getName());
                switchITem.SetCheck(agentDetailEvent.getData().getState());

                editTextITemPhone.SetItemValue(agentDetailEvent.getData().getPhone());
                editTextITemTel.SetItemValue(agentDetailEvent.getData().getTel());
                editTextITemEMail.SetItemValue(agentDetailEvent.getData().getEmail());
                editTextITemFax.SetItemValue(agentDetailEvent.getData().getFax());
                editTextITemWebAddress.SetItemValue(agentDetailEvent.getData().getWebsite());

                selelctableTextViewItemArea.SetItemValue(agentDetailEvent.getData().getArea_name());
                editTextITemAddress2.SetItemValue(agentDetailEvent.getData().getAddress());

                editTextITemBank.SetItemValue(agentDetailEvent.getData().getBank_name());
                editTextITemBankCreateName.SetItemValue(agentDetailEvent.getData().getBank_username());
                editTextITemBankCardNumber.SetItemValue(agentDetailEvent.getData().getBank_account());

                editTextITemOrderSize.SetItemValue(agentDetailEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(agentDetailEvent.getData().getNote());

                areaCode = agentDetailEvent.getData().getArea_id();
            }

            @Override
            public void onFail() {

            }
        });

    }


    private void AgentEdit() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == EDIT_MODE) {
            jsonObject.addProperty("agent_id", agentId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        jsonObject.addProperty("state", switchITem.IsCheck());

        jsonObject.addProperty("tel", editTextITemTel.GetItemValue());
        jsonObject.addProperty("phone", editTextITemPhone.GetItemValue());
        jsonObject.addProperty("fax", editTextITemFax.GetItemValue());
        jsonObject.addProperty("email", editTextITemEMail.GetItemValue());
        jsonObject.addProperty("website", editTextITemWebAddress.GetItemValue());

        if (!areaCode.equals("")) {
            jsonObject.addProperty("area_id", areaCode);
            jsonObject.addProperty("area_name", selelctableTextViewItemArea.GetItemValue());
        }
        jsonObject.addProperty("address", editTextITemAddress2.GetItemValue());

        jsonObject.addProperty("bank_name", editTextITemBank.GetItemValue());
        jsonObject.addProperty("bank_account", editTextITemBankCardNumber.GetItemValue());
        jsonObject.addProperty("bank_username", editTextITemBankCreateName.GetItemValue());


        jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditAgentAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                if (intentMode == ADD_MODE) {
                    Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                    finish();
                } else if (intentMode == EDIT_MODE) {
                    Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_FOR_AREA:
                if (resultCode == AreaSelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemArea.SetItemValue(data.getStringExtra("AREA_NAME"));
                    areaCode = data.getStringExtra("AREA_CODE");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_agent_save:
                AgentEdit();
                break;
            default:
                break;
        }
    }
}
