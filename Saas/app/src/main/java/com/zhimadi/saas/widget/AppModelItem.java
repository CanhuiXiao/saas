package com.zhimadi.saas.widget;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.AppList;
import com.zhimadi.saas.event.AppsEvent;

public class AppModelItem extends LinearLayout {

    private View returnView;
    private Context mContext;
    private TextView tvHomeAppTitle;
    private TextView tvAppHomeDescription;
    private ImageView ivAppHome;
    //枚举
    private AppList.App appDifine;

    private void init() {
        tvHomeAppTitle = (TextView) returnView.findViewById(R.id.tv_app_home_title);
        tvAppHomeDescription = (TextView) returnView.findViewById(R.id.tv_app_home_description);
        ivAppHome = (ImageView) returnView.findViewById(R.id.iv_app_home);
    }

    public AppModelItem(Context context, AttributeSet attrs, AppsEvent.Model.App app) {
        super(context, attrs);
        mContext = context;
        returnView = LayoutInflater.from(mContext).inflate(R.layout.widget_app_model_item, this);
        init();
        tvHomeAppTitle.setText(app.getName());
        tvAppHomeDescription.setText(app.getDescription());
        try {
            appDifine = AppList.App.getApp(Integer.valueOf(app.getApp_id()));
        } catch (Exception e) {
            appDifine = null;
        }
        if(appDifine != null){
            ivAppHome.setImageResource(appDifine.getDrawableResource());
            if(appDifine.getActivity() != null){
                returnView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, appDifine.getActivity());
                        mContext.startActivity(intent);
                    }
                });
            }
        }
    }
}
