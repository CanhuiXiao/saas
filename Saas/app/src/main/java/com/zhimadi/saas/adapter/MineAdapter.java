package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.MineList;
import com.zhimadi.saas.constant.Resours;
import com.zhimadi.saas.event.SimpleEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */
public class MineAdapter extends ArrayAdapter<MineList.Mine> {

    private Context mContext;
    private int resourceId;

    public MineAdapter(Context context, int resource, List<MineList.Mine> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MineList.Mine mine = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.ivMineHomeIcon.setImageResource(mine.getDrawableResource());
        holder.tvMineHomeTitle.setText(mine.getTitle());
        return convertView;
    }

    class ViewHolder {
        private ImageView ivMineHomeIcon;
        private TextView tvMineHomeTitle;

        public ViewHolder(View view) {
            ivMineHomeIcon = (ImageView) view.findViewById(R.id.iv_mine_home_icon);
            tvMineHomeTitle = (TextView) view.findViewById(R.id.tv_mine_home_title);
            view.setTag(this);
        }
    }
}
