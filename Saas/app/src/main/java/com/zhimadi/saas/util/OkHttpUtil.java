package com.zhimadi.saas.util;

import com.zhimadi.saas.constant.UserInfo;

import java.util.Map;

import okhttp3.Request;

/**
 * Created by carry on 2016/7/12.
 */
public class OkHttpUtil {

    private static final String mApiAddress = "http://api.zhimadi.cn";


    /**---------------------------登陆------------------------------*/
    /**
     * 登陆
     */
    public static final String mLandAddress = mApiAddress + "/auth/login";
    /**
     * 获取验证码
     */
    public static final String mCodeAddress = mApiAddress + "/auth/regCode";
    /**
     * 检验验证码
     */
    public static final String mCheckAddress = mApiAddress + "/auth/checkCode";
    /**
     * 注册
     */
    public static final String mRegisterAddress = mApiAddress + "/auth/signup";

    /**
     * ---------------------------APP------------------------------
     */
    public static final String mAppAllAddress = mApiAddress + "/app/index";


    /**---------------------------门店------------------------------*/
    /**
     * 门店列表
     */
    public static final String mShopAddress = mApiAddress + "/shop/index";
    /**
     * 门店删除
     */
    public static final String mDeleteShopAddress = mApiAddress + "/shop/delete";
    /**
     * 门店编辑
     */
    public static final String mEditShopAddress = mApiAddress + "/shop/save";
    /**
     * 门店详情
     */
    public static final String mShopDetailAddress = mApiAddress + "/shop/detail";

    /**---------------------------雇员------------------------------*/
    /**
     * 雇员列表
     */
    public static final String mUserAddress = mApiAddress + "/user/index";
    /**
     * 雇员删除
     */
    public static final String mDeleteUserAddress = mApiAddress + "/user/delete";
    /**
     * 雇员编辑
     */
    public static final String mEditUserAddress = mApiAddress + "/user/save";
    /**
     * 雇员详情
     */
    public static final String mUserDetailAddress = mApiAddress + "/user/detail";

    /**---------------------------仓库------------------------------*/
    /**
     * 仓库列表
     */
    public static final String mStoreAddress = mApiAddress + "/warehouse/index";
    /**
     * 仓库删除
     */
    public static final String mDeleteStoreAddress = mApiAddress + "/warehouse/delete";
    /**
     * 仓库编辑
     */
    public static final String mEditStoreAddress = mApiAddress + "/warehouse/save";
    /**
     * 仓库详情
     */
    public static final String mStoreDetailAddress = mApiAddress + "/warehouse/detail";

    /**---------------------------供应商------------------------------*/
    /**
     * 供应商列表
     */
    public static final String mSupplierAddress = mApiAddress + "/supplier/index";
    /**
     * 供应商删除
     */
    public static final String mDeleteSupplierAddress = mApiAddress + "/supplier/delete";
    /**
     * 供应商编辑
     */
    public static final String mEditSupplierAddress = mApiAddress + "/supplier/save";
    /**
     * 供应商详情
     */
    public static final String mSupplierDetailAddress = mApiAddress + "/supplier/detail";

    /**---------------------------结算账户------------------------------*/
    /**
     * 账户列表
     */
    public static final String mAccountAddress = mApiAddress + "/BankAccount/index";
    /**
     * 账户删除
     */
    public static final String mDeleteAccountAddress = mApiAddress + "/BankAccount/delete";
    /**
     * 账户编辑
     */
    public static final String mEditAccountAddress = mApiAddress + "/BankAccount/save";
    /**
     * 账户详情
     */
    public static final String mAccountDetailAddress = mApiAddress + "/BankAccount/detail";

    /**---------------------------账户结算方式------------------------------*/
    /**
     * 账户结算方式列表
     */
    public static final String mAccountTypeAddress = mApiAddress + "/bankAccountType/index";

    /**---------------------------结算账目------------------------------*/
    /**
     * 账目列表
     */
    public static final String mPaymentTypeAddress = mApiAddress + "/paymentType/index";
    /**
     * 账目删除
     */
    public static final String mDeletePaymentTypeAddress = mApiAddress + "/paymentType/delete";
    /**
     * 账目编辑
     */
    public static final String mEditPaymentTypeAddress = mApiAddress + "/paymentType/save";
    /**
     * 账目详情
     */
    public static final String mPaymentTypeDetailAddress = mApiAddress + "/paymentType/detail";


    /**-----------------------------客户--------------------------------*/
    /**
     * 客户列表
     */
    public static final String mCustomAddress = mApiAddress + "/custom/index";
    /**
     * 客户删除
     */
    public static final String mDeleteCustomAddress = mApiAddress + "/custom/delete";
    /**
     * 客户编辑
     */
    public static final String mEditCustomAddress = mApiAddress + "/custom/save";
    /**
     * 客户详情
     */
    public static final String mCustomDetailAddress = mApiAddress + "/custom/detail";


    /**---------------------------客户类型------------------------------*/
    /**
     * 客户类型列表
     */
    public static final String mCustomTypeAddress = mApiAddress + "/customType/index";
    /**
     * 客户类型删除
     */
    public static final String mDeleteCustomTypeAddress = mApiAddress + "/customType/delete";
    /**
     * 客户类型编辑
     */
    public static final String mEditCustomTypeAddress = mApiAddress + "/customType/save";
    /**
     * 客户类型详情
     */
    public static final String mCustomTypeDetailAddress = mApiAddress + "/customType/detail";


    /**---------------------------系统商品二级类目------------------------------*/
    /**
     * 商品类型列表
     */
    public static final String mCategoryAddress = mApiAddress + "/category/index";
    /**
     * 商品类型删除
     */
    public static final String mDeleteCategoryAddress = mApiAddress + "/category/delete";
    /**
     * 商品类型编辑
     */
    public static final String mEditCategoryAddress = mApiAddress + "/category/save";
    /**
     * 商品类型详情
     */
    public static final String mCategoryDetailAddress = mApiAddress + "/category/detail";


    /**---------------------------系统商品三级类目------------------------------*/
    /**
     * 商品种类列表
     */
    public static final String mProductTypeAddress = mApiAddress + "/productType/index";
    /**
     * 商品种类删除
     */
    public static final String mDeleteProductTypeAddress = mApiAddress + "/productType/delete";
    /**
     * 商品种类编辑
     */
    public static final String mEditProductTypeAddress = mApiAddress + "/productType/save";
    /**
     * 商品种类详情
     */
    public static final String mProductTypeDetailAddress = mApiAddress + "/productType/detail";


    /**---------------------------系统分类商品三级类目列表------------------------------*/
    /**
     * 商品种类列表
     */
    public static final String mCategorySpecificationAttributeMappingAddress = mApiAddress + "/categorySpecificationAttributeMapping/index";
    /**
     * 商品种类删除
     */
    public static final String mDeleteCategorySpecificationAttributeMappingAddress = mApiAddress + "/categorySpecificationAttributeMapping/delete";
    /**
     * 商品种类编辑
     */
    public static final String mEditCategorySpecificationAttributeMappingAddress = mApiAddress + "/categorySpecificationAttributeMapping/save";
    /**
     * 商品种类详情
     */
    public static final String mCategorySpecificationAttributeMappingDetailAddress = mApiAddress + "/categorySpecificationAttributeMapping/detail";


    /**---------------------------商户自定义商品类型------------------------------*/
    /**
     * 自定义商品类型列表
     */
    public static final String mSellerCatsAddress = mApiAddress + "/sellerCats/index";
    /**
     * 自定义商品类型删除
     */
    public static final String mDeleteSellerCatsAddress = mApiAddress + "/sellerCats/delete";
    /**
     * 自定义商品类型编辑
     */
    public static final String mEditSellerCatsAddress = mApiAddress + "/sellerCats/save";
    /**
     * 自定义商品类型详情
     */
    public static final String mSellerCatsDetailAddress = mApiAddress + "/sellerCats/detail";


    /**-----------------------------------商品-------------------------------------*/
    /**
     * 商品列表
     */
    public static final String mProductAddress = mApiAddress + "/product/index";
    /**
     * 商品删除
     */
    public static final String mDeleteProductAddress = mApiAddress + "/product/delete";
    /**
     * 商品编辑
     */
    public static final String mEditProductAddress = mApiAddress + "/product/save";
    /**
     * 商品详情
     */
    public static final String mProductDetailAddress = mApiAddress + "/product/detail";


    /**
     * -----------------------------------商品属性-------------------------------------
     */
    /*产品属性列表*/
    public static final String mAttributeAddress = mApiAddress + "/specificationAttribute/index";
    /*产品属性删除*/
    public static final String mDeleteAttributeAddress = mApiAddress + "/specificationAttribute/delete";
    /*产品属性编辑*/
    public static final String mEditAttributeAddress = mApiAddress + "/specificationAttribute/save";
    /*产品属性详情*/
    public static final String mAttributeDetailAddress = mApiAddress + "/specificationAttribute/detail";

    /**
     * -----------------------------------商品属性选项-------------------------------------
     */
    /*商品属性选项列表*/
    public static final String mAttributeOptionAddress = mApiAddress + "/specificationAttributeOption/index";
    /*商品属性选项删除*/
    public static final String mDeleteAttributeOptionAddress = mApiAddress + "/specificationAttributeOptionOption/delete";
    /*商品属性选项编辑*/
    public static final String mEditAttributeOptionOptionAddress = mApiAddress + "/specificationAttributeOption/save";
    /*商品属性选项详情*/
    public static final String mAttributeOptionDetailAddress = mApiAddress + "/specificationAttributeOption/detail";


    /**-----------------------------------采购单-------------------------------------*/
    /**
     * 采购单列表
     */
    public static final String mBuyAddress = mApiAddress + "/buy/index";
    /**
     * 采购单删除
     */
    public static final String mDeleteBuyAddress = mApiAddress + "/buyOption/delete";
    /**
     * 采购单编辑
     */
    public static final String mEditBuyAddress = mApiAddress + "/buy/save";
    /**
     * 采购单详情
     */
    public static final String mBuyDetailAddress = mApiAddress + "/buy/detail";


    /**-----------------------------------销售单-------------------------------------*/
    /**
     * 销售单列表
     */
    public static final String mSellAddress = mApiAddress + "/sell/index";
    /**
     * 销售单删除
     */
    public static final String mDeleteSellAddress = mApiAddress + "/sellOption/delete";
    /**
     * 销售单编辑
     */
    public static final String mEditSellAddress = mApiAddress + "/sell/save";
    /**
     * 销售单详情
     */
    public static final String mSellDetailAddress = mApiAddress + "/sell/detail";

    /**-----------------------------货主--------------------------------*/
    /**
     * 货主列表
     */
    public static final String mOwnerAddress = mApiAddress + "/owner/index";
    /**
     * 货主删除
     */
    public static final String mDeleteOwnerAddress = mApiAddress + "/owner/delete";
    /**
     * 货主编辑
     */
    public static final String mEditOwnerAddress = mApiAddress + "/owner/save";
    /**
     * 货主详情
     */
    public static final String mOwnerDetailAddress = mApiAddress + "/owner/detail";

    /**-----------------------------代卖商--------------------------------*/
    /**
     * 代卖商列表
     */
    public static final String mAgentAddress = mApiAddress + "/agent/index";
    /**
     * 代卖商删除
     */
    public static final String mDeleteAgentAddress = mApiAddress + "/agent/delete";
    /**
     * 代卖商编辑
     */
    public static final String mEditAgentAddress = mApiAddress + "/agent/save";
    /**
     * 代卖商详情
     */
    public static final String mAgentDetailAddress = mApiAddress + "/agent/detail";

    /**-----------------------------报损--------------------------------*/
    /**
     * 报损列表
     */
    public static final String mStockLossAddress = mApiAddress + "/stockLoss/index";
    /**
     * 报损删除
     */
    public static final String mDeleteStockLossAddress = mApiAddress + "/stockLoss/delete";
    /**
     * 报损编辑
     */
    public static final String mEditStockLossAddress = mApiAddress + "/stockLoss/save";
    /**
     * 报损详情
     */
    public static final String mStockLossDetailAddress = mApiAddress + "/stockLoss/detail";


    /**-----------------------------我的代卖货主销售--------------------------------*/
    /**
     * 我的代卖货主销售列表
     */
    public static final String mAgentSellAddress = mApiAddress + "/agentSell/index";
    /**
     * 我的代卖货主销售删除
     */
    public static final String mDeleteAgentSellAddress = mApiAddress + "/agentSell/delete";
    /**
     * 我的代卖货主销售编辑
     */
    public static final String mEditAgentSellAddress = mApiAddress + "/agentSell/save";
    /**
     * 我的代卖货主销售详情
     */
    public static final String mAgentSellDetailAddress = mApiAddress + "/agentSell/detail";


    /**-----------------------------我的代卖商销售--------------------------------*/
    /**
     * 我的代卖商销售列表
     */
    public static final String mAgentSellAgentAddress = mApiAddress + "/agentSellAgent/index";
    /**
     * 我的代卖商销售删除
     */
    public static final String mDeleteAgentSellAgentAddress = mApiAddress + "/agentSellAgent/delete";
    /**
     * 我的代卖商销售编辑
     */
    public static final String mEditAgentSellAgentAddress = mApiAddress + "/agentSellAgent/save";
    /**
     * 我的代卖商销售详情
     */
    public static final String mAgentSellAgentDetailAddress = mApiAddress + "/agentSellAgent/detail";

    /*-----------------------------采购退货--------------------------------*/
    /**
     * 采购退货列表
     */
    public static final String mBuyReturnAddress = mApiAddress + "/buyReturn/index";
    /**
     * 采购退货删除
     */
    public static final String mDeleteBuyReturnAddress = mApiAddress + "/buyReturn/delete";
    /**
     * 采购退货编辑
     */
    public static final String mEditBuyReturnAddress = mApiAddress + "/buyReturn/save";
    /**
     * 采购退货详情
     */
    public static final String mBuyReturnDetailAddress = mApiAddress + "/buyReturn/detail";

    /*-----------------------------库存--------------------------------*/
    /**
     * 库存列表
     */
    public static final String mStockAddress = mApiAddress + "/stock/index";
    /**
     * 采购退货删除
     */
    public static final String mDeleteStockAddress = mApiAddress + "/stock/delete";
    /**
     * 采购退货编辑
     */
    public static final String mEditStockAddress = mApiAddress + "/stock/save";
    /**
     * 采购退货详情
     */
    public static final String mStockDetailAddress = mApiAddress + "/stock/detail";

/*-----------------------------存流水--------------------------------*/
    /**
     * 存流水列表
     */
    public static final String mStockAllotAddress = mApiAddress + "/stockAllot/index";
    /**
     * 采购退货删除
     */
    public static final String mDeleteStockAllotAddress = mApiAddress + "/stockAllot/delete";
    /**
     * 采购退货编辑
     */
    public static final String mEditStockAllotAddress = mApiAddress + "/stockAllot/save";
    /**
     * 采购退货详情
     */
    public static final String mStockAllotDetailAddress = mApiAddress + "/stockAllot/detail";


    /*-----------------------------盘点--------------------------------*/
    /**
     * 库存列表
     */
    public static final String mStockCheckAddress = mApiAddress + "/stockCheck/index";
    /**
     * 库存流水删除
     */
    public static final String mDeleteStockCheckAddress = mApiAddress + "/stockCheck/delete";
    /**
     * 库存流水编辑
     */
    public static final String mEditStockCheckAddress = mApiAddress + "/stockCheck/save";
    /**
     * 库存流水详情
     */
    public static final String mStockCheckDetailAddress = mApiAddress + "/stockCheck/detail";


    /*-----------------------------消息--------------------------------*/
    /**
     * 消息未读数量
     */
    public static final String mMessageNotReadAddress = mApiAddress + "/message/count";

    /**
     * url关键词搜索组装
     */
    public static String UrlKeyAssembly(String urlStr, Map<String, String> params) {
        StringBuilder stringBuilder = new StringBuilder(urlStr);
        boolean isFirst = true;
        for (String key : params.keySet()) {
            if (isFirst) {
                stringBuilder.append("?" + key + "=" + params.get(key));
                isFirst = false;
            } else {
                stringBuilder.append("&" + key + "=" + params.get(key));
            }
        }
        urlStr = stringBuilder.toString();
        return urlStr;
    }


    public static Request.Builder RequestAssembly(String urlStr) {
        Request.Builder builder = new Request.Builder()
                .url(urlStr)
                .addHeader("Content-Type", "application/json")
                .addHeader("Auth", UserInfo.getmAuth())
                .addHeader("App-id", "2")
                .addHeader("Version", "1.0")
                .addHeader("Accept", "application/json")
                .addHeader("Client-id", UserInfo.getmIMEI())
                .addHeader("utf-8", "Accept-Charset");
        return builder;
    }


    public static Request.Builder RequestAssembly(Request.Builder builder) {
        builder.addHeader("Auth", UserInfo.getmAuth())
                .addHeader("App-id", "2")
                .addHeader("Version", "1.0")
                .addHeader("content-type", "application/json")
                .addHeader("Accept", "application/json")
                .addHeader("Client-Id", UserInfo.getmIMEI());
        return builder;

    }

    public static Request.Builder RequestAssembly2(Request.Builder builder) {
        builder.addHeader("content-type", "application/json");
        return builder;
    }


}
