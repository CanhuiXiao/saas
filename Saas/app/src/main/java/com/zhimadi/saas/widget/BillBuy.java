package com.zhimadi.saas.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class BillBuy extends LinearLayout {

    private View returnView;
    private Context mcContext;

    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;

    private TextView tvBillNumberTitle;
    private TextView tvBillNumberValue;
    private TextView tvBillPayTitle;
    private TextView tvBillPayValue;
    private TextView tvBillWeightTitle;
    private TextView tvBillWeightValue;


    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_bill, this);
        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);

        tvBillNumberTitle = (TextView) returnView.findViewById(R.id.tv_bill_number_title);
        tvBillNumberValue = (TextView) returnView.findViewById(R.id.tv_bill_number_value);
        tvBillPayTitle = (TextView) returnView.findViewById(R.id.tv_bill_pay_title);
        tvBillPayValue = (TextView) returnView.findViewById(R.id.tv_bill_pay_value);
        tvBillWeightTitle = (TextView) returnView.findViewById(R.id.tv_bill_weight_title);
        tvBillWeightValue = (TextView) returnView.findViewById(R.id.tv_bill_weight_value);

    }

    public BillBuy(Context context, AttributeSet attrs, boolean editEnable) {
        super(context, attrs);
        mcContext = context;
        inte();
        if (editEnable) {
            //蓝底白字，值是主色调
            tvBillNumberTitle.setBackgroundResource(R.color.colorAquamarine);
            tvBillNumberTitle.setTextColor(Color.parseColor("#ffffff"));
            tvBillPayTitle.setBackgroundResource(R.color.colorAquamarine);
            tvBillPayTitle.setTextColor(Color.parseColor("#ffffff"));
            tvBillWeightTitle.setBackgroundResource(R.color.colorAquamarine);
            tvBillWeightTitle.setTextColor(Color.parseColor("#ffffff"));
            tvBillNumberValue.setTextColor(Color.parseColor("#363636"));
            tvBillPayValue.setTextColor(Color.parseColor("#363636"));
            tvBillWeightValue.setTextColor(Color.parseColor("#363636"));
        } else {
            //白底黑字，值为蓝色
            tvBillNumberTitle.setBackgroundResource(R.color.colorWhite);
            tvBillNumberTitle.setTextColor(Color.parseColor("#363636"));
            tvBillPayTitle.setBackgroundResource(R.color.colorWhite);
            tvBillPayTitle.setTextColor(Color.parseColor("#363636"));
            tvBillWeightTitle.setBackgroundResource(R.color.colorWhite);
            tvBillWeightTitle.setTextColor(Color.parseColor("#363636"));
            tvBillNumberValue.setTextColor(Color.parseColor("#26ceb4"));
            tvBillPayValue.setTextColor(Color.parseColor("#26ceb4"));
            tvBillWeightValue.setTextColor(Color.parseColor("#26ceb4"));
        }
    }

    private float number;
    private float weight;
    private float pay;

    public void setNumber(float number) {
        this.number = number;
        tvBillNumberValue.setText(number + "件");
    }

    public void setPay(float pay) {
        this.pay = pay;
        tvBillPayValue.setText("￥" + pay);
    }

    public void setWeight(float weight) {
        this.weight = weight;
        tvBillWeightValue.setText(weight + "公斤");
    }

    public void setNumber(String number) {
        this.number = Float.valueOf(number);
        tvBillNumberValue.setText(number + "件");
    }

    public void setPay(String pay) {
        this.pay = Float.valueOf(pay);
        tvBillPayValue.setText("￥" + pay);
    }

    public void setWeight(String weight) {
        this.weight = Float.valueOf(weight);
        tvBillWeightValue.setText(weight + "公斤");
    }


    public float getNumber() {
        return number;
    }

    public float getPay() {
        return pay;
    }

    public float getWeight() {
        return weight;
    }


    public BillBuy isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public BillBuy isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }

}
