package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.BuyReturnsEvent;
import com.zhimadi.saas.event.BuysEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class BuyReturnAdapter extends ArrayAdapter<BuyReturnsEvent.BuyReturn> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals(Constant.STATE_WAIT_CHECK+"")) {
            holder.tvBuyReturnHomeState.setText("待审核");
            holder.tvBuyReturnHomeState.setBackgroundResource(R.drawable.shape_bt_buy_2);
        } else if (state.equals(Constant.STATE_DEFAULT+"")) {
            holder.tvBuyReturnHomeState.setText("已退货");
            holder.tvBuyReturnHomeState.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_NOTE+"")) {
            holder.tvBuyReturnHomeState.setText("草稿");
            holder.tvBuyReturnHomeState.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_CANCEL+"")) {
            holder.tvBuyReturnHomeState.setText("撤销");
            holder.tvBuyReturnHomeState.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public BuyReturnAdapter(Context context, int resource, List<BuyReturnsEvent.BuyReturn> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BuyReturnsEvent.BuyReturn buyReturn = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvBuyReturnHomeNumber.setText(buyReturn.getTotal_package());
        showStateText(holder, buyReturn.getState());
        holder.tvBuyReturnHomeWeight.setText(buyReturn.getTotal_weight());
        holder.tvBuyReturnHomeMoney.setText("￥" + buyReturn.getTotal_amount());
        holder.tvBuyReturnHomeOrderNumber.setText(buyReturn.getOrder_no());
        holder.tvBuyReturnHomeCreateDate.setText(buyReturn.getCreate_time());
        return convertView;
    }

    class ViewHolder {
        TextView tvBuyReturnHomeNumber;
        TextView tvBuyReturnHomeWeight;
        TextView tvBuyReturnHomeMoney;
        TextView tvBuyReturnHomeOrderNumber;
        TextView tvBuyReturnHomeCreateDate;
        TextView tvBuyReturnHomeState;

        public ViewHolder(View view) {
            tvBuyReturnHomeNumber = (TextView) view.findViewById(R.id.tv_buy_return_home_number);
            tvBuyReturnHomeWeight = (TextView) view.findViewById(R.id.tv_buy_return_home_weight);
            tvBuyReturnHomeMoney = (TextView) view.findViewById(R.id.tv_buy_return_home_money);
            tvBuyReturnHomeOrderNumber = (TextView) view.findViewById(R.id.tv_buy_return_home_order_number);
            tvBuyReturnHomeCreateDate = (TextView) view.findViewById(R.id.tv_buy_return_home_create_date);
            tvBuyReturnHomeState = (TextView) view.findViewById(R.id.tv_buy_return_home_state);
            view.setTag(this);
        }
    }


}
