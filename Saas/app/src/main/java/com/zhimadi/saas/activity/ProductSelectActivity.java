package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.ProductSelectAdapter;
import com.zhimadi.saas.event.ProductData;
import com.zhimadi.saas.event.ProductsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductSelectActivity extends BaseActivity implements View.OnClickListener {

    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_Fail = 0;


    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    //    private EditText etProductSelect;
    private TextView tvProductSelectComfirm;
    //服务器返回的数据
    private List<ProductsEvent.Product> products;
    private ListView lvProductSelect;
    private ProductSelectAdapter productSelectAdapter;
    //获取的数据
    private List<String> productIds;
    //返回的数据
    private List<ProductData> productDatas;

    private void inte() {
        productIds = (List<String>) getIntent().getSerializableExtra("PRODUCT_IDS");
        if (productIds == null) {
            productIds = new ArrayList<String>();
        } else {
            Toast.makeText(mContext, "传进" + productIds.size() + "个id", Toast.LENGTH_SHORT).show();
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
//        etProductSelect = (EditText) findViewById(R.id.et_product_select);
        tvProductSelectComfirm = (TextView) findViewById(R.id.tv_product_select_comfirm);
        products = new ArrayList<ProductsEvent.Product>();
        lvProductSelect = (ListView) findViewById(R.id.lv_product_select);
        productSelectAdapter = new ProductSelectAdapter(mContext, R.layout.item_lv_product_select, products, this);
        productDatas = new ArrayList<ProductData>();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_select);
        inte();
        titleBarCommonBuilder.setTitle("商品管理")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, ProductEditActivity.class);
                        intent.putExtra("INTENT_MODE", ProductEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvProductSelect.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, ProductEditActivity.class);
                intent.putExtra("INTENT_MODE", ProductEditActivity.EDIT_MODE);
                intent.putExtra("PRODUCT_ID", products.get(i).getProduct_id());
                activity.startActivity(intent);
            }
        });

        //加上死锁，没错就是配合动画
        lvProductSelect.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            GetProduct();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        tvProductSelectComfirm.setOnClickListener(this);
        lvProductSelect.setAdapter(productSelectAdapter);
        GetProduct();
    }

    private void GetProduct() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mProductAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ProductsEvent productsEvent = gson.fromJson(response, ProductsEvent.class);
                if (mStart == 0) {
                    products.clear();
                }
                mStart = mStart + productsEvent.getData().getList().size();
                //读取传进来的商品,首次为空
                if (productIds != null) {
                    products.addAll(dataHandle(productsEvent.getData().getList(), productIds));
                } else {
                    products.addAll(productsEvent.getData().getList());
                }
                //打勾
                productSelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    //为服务器传来的数据添加私人属性<select>
    public List<ProductsEvent.Product> dataHandle(List<ProductsEvent.Product> products, List<String> productIds) {
        for (int i = 0; i < products.size(); i++) {
            for (int k = 0; k < productIds.size(); k++) {
                if (products.get(i).getProduct_id().equals(productIds.get(k))) {
                    products.get(i).setSelect(true);
                }
            }
        }
        return products;
    }


    //注意重复添加
    public void select(String productId, String isFit, String format, String name, boolean isCheck, float fitWeight) {
        if (isCheck) {
                ProductData productDataTemp = new ProductData(productId, isFit, format, name, fitWeight);
                productDatas.add(productDataTemp);
                if (productIds != null) {
                    productIds.add(productId);
                }

        } else {
                ProductData temp = null;
                String stringTemp = null;
                for (int i = 0; i < productDatas.size(); i++) {
                    if (productDatas.get(i).getProductId().equals(productId)) {
                        temp = productDatas.get(i);
                    }
                }
                for (int i = 0; i < productIds.size(); i++) {
                    if (productIds.get(i).equals(productId)) {
                        stringTemp = productIds.get(i);
                    }
                }
                productDatas.remove(temp);
                productIds.remove(stringTemp);
        }
    }


    public void postResult() {
        Intent intent = new Intent();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable("PRODUCT_ARRAY", (Serializable) productDatas); // 传递一个user对象列表
        mBundle.putSerializable("PRODUCTIDS_ARRY", (Serializable) productIds);
        intent.putExtras(mBundle);
        setResult(RESULT_SUCCESS, intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_product_select_comfirm:
                postResult();
                break;
        }
    }


}
