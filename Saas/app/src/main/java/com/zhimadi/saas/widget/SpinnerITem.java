package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.zhimadi.saas.R;

import java.util.List;

public class SpinnerITem extends LinearLayout {

    private View returnView;
    private Context mContext;
    private TextView tvItemName;
    private EditText etItemValue;
    private Spinner spItemUnit;

    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;
//	private int unit;


    public SpinnerITem(Context context, AttributeSet attrs, String name, String hint, List<String> units, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        super(context, attrs);
//		unit = 1;
        mContext = context;
        returnView = LayoutInflater.from(mContext).inflate(R.layout.widget_spinner, this);
        tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
        etItemValue = (EditText) returnView.findViewById(R.id.et_item_value);
        spItemUnit = (Spinner) returnView.findViewById(R.id.sp_item_unit);

        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, R.layout.item_spinner, units);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spItemUnit.setAdapter(adapter);
        spItemUnit.setOnItemSelectedListener(onItemSelectedListener);

        tvItemName.setText(name);
        etItemValue.setHint(hint);
    }

    public void SetItemValue(String value) {
        etItemValue.setText(value);
    }

    public String GetItemValue() {
        return etItemValue.getText().toString();
    }

    public SpinnerITem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public SpinnerITem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }


}
