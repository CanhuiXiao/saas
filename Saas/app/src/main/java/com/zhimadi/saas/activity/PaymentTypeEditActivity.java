package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.PaymentTypeEditEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.HashMap;
import java.util.Map;

public class PaymentTypeEditActivity extends BaseActivity implements View.OnClickListener {


    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    private int intentMode;

    private String paymentTypeId;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private LinearLayout llPaymentTypeEditWindow;

    private EditTextITem editTextITemName;
    private EditTextITem editTextITemOrderSize;
    private EditTextITem editTextITemNote;
    private SwitchItem switchITem;

    private Button btPaymentTypeSave;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            paymentTypeId = getIntent().getStringExtra("PAYMENT_TYPE_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llPaymentTypeEditWindow = (LinearLayout) findViewById(R.id.ll_payment_type_edit_window);
        btPaymentTypeSave = (Button) findViewById(R.id.bt_account_payment_type_edit_save);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入类型名称");
        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        switchITem = new SwitchItem(mContext, null, "启动", true);
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_type_edit);
        inte();
        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑账目类型");

        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增账目类型");
        } else {
            finish();
        }


        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui).setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });


        btPaymentTypeSave.setOnClickListener(this);

        editTextITemName.isTop(true).isNotNull(true);
        llPaymentTypeEditWindow.addView(editTextITemName);
        llPaymentTypeEditWindow.addView(editTextITemOrderSize);
        switchITem.isBottom(true);
        llPaymentTypeEditWindow.addView(switchITem);
        editTextITemNote.isBottom(true).isTop(true);
        llPaymentTypeEditWindow.addView(editTextITemNote);

        if (intentMode == EDIT_MODE) {
            GetPaymentTypeDetail();
        }

    }


    private void GetPaymentTypeDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("payment_type_id", paymentTypeId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mPaymentTypeDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                PaymentTypeEditEvent paymentTypeEditEvent = gson.fromJson(response, PaymentTypeEditEvent.class);
                editTextITemName.SetItemValue(paymentTypeEditEvent.getData().getName());
                editTextITemOrderSize.SetItemValue(paymentTypeEditEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(paymentTypeEditEvent.getData().getNote());
                switchITem.SetCheck(paymentTypeEditEvent.getData().getState());

        }

        @Override
        public void onFail () {

        }
    }

    );
}

    private void PaymentTypeDetailEdit() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == EDIT_MODE) {
            jsonObject.addProperty("payment_type_id", paymentTypeId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        if (editTextITemOrderSize.GetItemValue().equals("")) {
            jsonObject.addProperty("display_order", "100");
        } else {
            jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
        }

        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
        jsonObject.addProperty("state", switchITem.IsCheck());
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditPaymentTypeAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                if (intentMode == ADD_MODE) {
                    Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                    finish();
                } else if (intentMode == EDIT_MODE) {
                    Toast.makeText(mContext, "编辑成功！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });

    }


    @Override
    public void onClick(View view) {
        PaymentTypeDetailEdit();
    }
}
