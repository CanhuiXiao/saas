package com.zhimadi.saas.event;

/**
 * Created by carry on 2016/7/13.
 */

public class PaymentTypeEditEvent {


    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data{
        private String payment_type_id;
        private String name;
        private String display_order;
        private String is_system;
        private String state;
        private String note;

        public String getPayment_type_id() {
            return payment_type_id;
        }

        public void setPayment_type_id(String payment_type_id) {
            this.payment_type_id = payment_type_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }

        public String getIs_system() {
            return is_system;
        }

        public void setIs_system(String is_system) {
            this.is_system = is_system;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }
}
