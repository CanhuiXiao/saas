package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StoreAdapter;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.StoresEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StoreHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private SwipeMenuListView lvStoreHome;
    private List<StoresEvent.Store> stores;
    private StoreAdapter storeAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        stores = new ArrayList<StoresEvent.Store>();
        lvStoreHome = (SwipeMenuListView) findViewById(R.id.lv_store_home);
        storeAdapter = new StoreAdapter(mContext, R.layout.item_lv_store_home, stores);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_home);
        inte();
        titleBarCommonBuilder.setTitle("仓库管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, StoreEditActivity.class);
                        intent.putExtra("INTENT_MODE", StoreEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };
        lvStoreHome.setMenuCreator(creator);
        lvStoreHome.setAdapter(storeAdapter);
        lvStoreHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index){
                    case 0:
                        StoreDelete(stores.get(position).getWarehouse_id());
                        break;
                    default:
                        break;
                }
                return true;
            }

        });
        lvStoreHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(activity, StoreEditActivity.class);
                intent.putExtra("INTENT_MODE", StoreEditActivity.EDIT_MODE);
                intent.putExtra("STORE_ID", stores.get(position).getWarehouse_id());
                activity.startActivity(intent);
            }
        });

        lvStoreHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetStore();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });


        lvStoreHome.setAdapter(storeAdapter);
        GetStore();


    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetStore();
    }



    private void GetStore() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mStoreAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                StoresEvent storesEvent = gson.fromJson(response, StoresEvent.class);
                if (mStart == 0) {
                    stores.clear();
                }
                mStart = mStart + storesEvent.getData().getList().size();
                stores.addAll(storesEvent.getData().getList());
                storeAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    private void StoreDelete(String stroeId){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("warehouse_id", stroeId);

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteStoreAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if(commonResultEvent.getCode() == 0){
                    mStart = 0;
                    GetStore();
                }else {
                    Toast.makeText(mContext, commonResultEvent.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });

    }







}
