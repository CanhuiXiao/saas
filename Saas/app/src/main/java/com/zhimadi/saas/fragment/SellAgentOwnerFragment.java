package com.zhimadi.saas.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.OwnerEditActivity;
import com.zhimadi.saas.adapter.SellAgentOwnerAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.SellAgentOwnersEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/8.
 */
public class SellAgentOwnerFragment extends BaseFragment implements RadioGroupWithLayout.OnCheckedChangeListener {

    private View returnView;

    private int mStart = 0;
    private int mLimit = 10;

    private RadioGroupWithLayout rgSellAgentOwnerHome;
    //滑块集合
    private List<View> sliders;

    private List<SellAgentOwnersEvent.SellAgentOwner> sellAgentOwners;
    private ListView lvSellAgentOwnerHome;
    private SellAgentOwnerAdapter sellAgentOwnerAdapter;

    private void inte() {
        rgSellAgentOwnerHome = (RadioGroupWithLayout) returnView.findViewById(R.id.rg_sell_agent_owner_home);
        sliders = new ArrayList<View>();
        View viewOwnerHomeAll = returnView.findViewById(R.id.view_sell_agent_owner_home_all);
        sliders.add(viewOwnerHomeAll);
        View viewOwnerHomeWaitCheck = returnView.findViewById(R.id.view_sell_agent_owner_home_wait_check);
        sliders.add(viewOwnerHomeWaitCheck);
        View viewOwnerHomeWaitComfirm = returnView.findViewById(R.id.view_sell_agent_owner_home_wait_comfirm);
        sliders.add(viewOwnerHomeWaitComfirm);
        View viewOwnerHomeFinish = returnView.findViewById(R.id.view_sell_agent_owner_home_finish);
        sliders.add(viewOwnerHomeFinish);
        View viewOwnerHomeNote = returnView.findViewById(R.id.view_sell_agent_owner_home_note);
        sliders.add(viewOwnerHomeNote);
        View viewOwnerHomeCancel = returnView.findViewById(R.id.view_sell_agent_owner_home_cancel);
        sliders.add(viewOwnerHomeCancel);
        lvSellAgentOwnerHome = (ListView) returnView.findViewById(R.id.lv_sell_agent_owner_home);
        sellAgentOwners = new ArrayList<SellAgentOwnersEvent.SellAgentOwner>();
        sellAgentOwnerAdapter = new SellAgentOwnerAdapter(activity, R.layout.item_lv_sell_agent_owner_home, sellAgentOwners);
    }


    //显示滑块的方法
    private void ShowSlider(int RadioButtonId) {
        for (int i = 0; i < sliders.size(); i++) {
            if (RadioButtonId == sliders.get(i).getId()) {
                sliders.get(i).setVisibility(View.VISIBLE);
            } else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_owner, null);
        inte();
        lvSellAgentOwnerHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(activity, OwnerEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("OWNER_ID", sellAgentOwners.get(position).getOwner_id());
                intent.putExtra("STATE", sellAgentOwners.get(position).getState());
                startActivity(intent);
            }
        });

        lvSellAgentOwnerHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetSellAgentOwner();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvSellAgentOwnerHome.setAdapter(sellAgentOwnerAdapter);
        GetSellAgentOwner();
        rgSellAgentOwnerHome.setOnCheckedChangeListener(this);
        return returnView;
    }



    private void GetSellAgentOwner() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        if (state != Constant.STATE_ALL) {
            map.put("state", state + "");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mAgentSellAgentAddress, map);
        AsyncUtil.asyncGet(activity, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SellAgentOwnersEvent sellAgentOwnersEvent = gson.fromJson(response, SellAgentOwnersEvent.class);
                if (mStart == 0) {
                    sellAgentOwners.clear();
                }
                mStart = mStart + sellAgentOwnersEvent.getData().getList().size();
                sellAgentOwners.addAll(sellAgentOwnersEvent.getData().getList());
                sellAgentOwnerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private int state = Constant.STATE_ALL;


    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_sell_agent_owner_home_all:
                ShowSlider(R.id.view_sell_agent_owner_home_all);
                mStart = 0;
                state = Constant.STATE_ALL;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            case R.id.rb_sell_agent_owner_home_wait_check:
                ShowSlider(R.id.view_sell_agent_owner_home_wait_check);
                mStart = 0;
                state = Constant.STATE_WAIT_CHECK;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            case R.id.rb_sell_agent_owner_home_wait_comfirm:
                ShowSlider(R.id.view_sell_agent_owner_home_wait_comfirm);
                mStart = 0;
                state = Constant.STATE_WAIT_COMFIRM;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            case R.id.rb_sell_agent_owner_home_finish:
                ShowSlider(R.id.view_sell_agent_owner_home_finish);
                mStart = 0;
                state = Constant.STATE_FINISH;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            case R.id.rb_sell_agent_owner_home_note:
                ShowSlider(R.id.view_sell_agent_owner_home_note);
                mStart = 0;
                state = Constant.STATE_NOTE;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            case R.id.rb_sell_agent_owner_home_cancel:
                ShowSlider(R.id.view_sell_agent_owner_home_cancel);
                mStart = 0;
                state = Constant.STATE_CANCEL;
                lvSellAgentOwnerHome.setSelection(0);
                GetSellAgentOwner();
                break;
            default:
                break;
        }
    }
}