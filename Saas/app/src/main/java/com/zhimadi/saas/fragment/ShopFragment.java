package com.zhimadi.saas.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.ShopEditActivity;
import com.zhimadi.saas.adapter.ShopAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.ShopsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.LogHead;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */

public class ShopFragment extends BaseFragment {

    private View returnView;

    private SwipeMenuCreator creator;
    private List<ShopsEvent.Shop> shops;
    private SwipeMenuListView lvShopHome;
    private ShopAdapter shopAdapter;

    private int mStart = 0;
    private int mLimit = 10;

    private void inte(){
        shops = new ArrayList<ShopsEvent.Shop>();
        lvShopHome = (SwipeMenuListView) returnView.findViewById(R.id.lv_shop_home);
        shopAdapter = new ShopAdapter(activity, R.layout.item_lv_shop_home, shops);
        lvShopHome.setAdapter(shopAdapter);
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem openItem = new SwipeMenuItem(activity);
                openItem.setBackground(new ColorDrawable(Color.rgb(0x26, 0xCF, 0xB3)));
                openItem.setWidth(90);
                openItem.setIcon(R.drawable.bian_ji02);
                menu.addMenuItem(openItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9, 0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_shop, null);
        inte();
        lvShopHome.setMenuCreator(creator);
        lvShopHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index){
                    case 0:
                        Intent intent = new Intent();
                        intent.setClass(activity, ShopEditActivity.class);
                        intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                        activity.startActivity(intent);
                        break;
                    case 1:
                        deleteShop(shops.get(position).getShop_id());
                        break;
                    default:
                        break;
                }
                return true;
            }

        });

        lvShopHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetShop();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        
        lvShopHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(activity, ShopEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("SHOP_ID", shops.get(position).getShop_id());
                activity.startActivity(intent);
            }
        });
        GetShop();
        return  returnView;
    }



    public void refresh(){
        mStart = 0;
        GetShop();
    }


    public void GetShop(){
        AsyncUtil.asyncGet(activity, OkHttpUtil.mShopAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ShopsEvent shopsEvent = gson.fromJson(response, ShopsEvent.class);
                if(mStart == 0){
                    shops.clear();
                }
                mStart = mStart + shopsEvent.getData().getList().size();
                shops.addAll(shopsEvent.getData().getList());
                shopAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {
            }
        });
    }


    private void deleteShop(String shopId){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("shop_id", shopId);
        AsyncUtil.asyncJsonPost(activity, OkHttpUtil.mDeleteShopAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                mStart = 0;
                GetShop();
            }

            @Override
            public void onFail() {

            }
        });
    }



}