package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/19.
 */
public class SuppliersEvent {

    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public  Supplier getA(){
        Supplier supplier = new Supplier();
        return supplier;
    }

    public class Data {
        private String count;
        private List<Supplier> list;

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Supplier> getList() {
            return list;
        }

        public void setList(List<Supplier> list) {
            this.list = list;
        }


    }


    public class Supplier {
        private String supplier_id;
        private String amount_topay;
        private String name;
        private String charge_man;
        private String state;

        public String getAmount_topay() {
            return amount_topay;
        }

        public void setAmount_topay(String amount_topay) {
            this.amount_topay = amount_topay;
        }

        public String getCharge_man() {
            return charge_man;
        }

        public void setCharge_man(String charge_man) {
            this.charge_man = charge_man;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getSupplier_id() {
            return supplier_id;
        }

        public void setSupplier_id(String supplier_id) {
            this.supplier_id = supplier_id;
        }
    }
}
