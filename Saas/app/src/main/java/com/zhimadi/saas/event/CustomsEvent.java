package com.zhimadi.saas.event;

import android.app.PendingIntent;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class CustomsEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Custom> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Custom> getList() {
            return list;
        }

        public void setList(List<Custom> list) {
            this.list = list;
        }
    }

    public class Custom
    {
        private String custom_id;
        private String name;
        private String state;
        private String type_id;
        private String amount_owed;
        private String type_name;

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }
        //        custom_id":"28","type_id":"22","name":"老郭","state":"0","type_name":"66","amount_owed":"0"

        public String getCustom_id() {
            return custom_id;
        }

        public void setCustom_id(String custom_id) {
            this.custom_id = custom_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getAmount_owed() {
            return amount_owed;
        }

        public void setAmount_owed(String amount_owed) {
            this.amount_owed = amount_owed;
        }
    }

}
