package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class LoadFooter extends LinearLayout {

	private View returnView;
	private TextView tvLoading;

	public LoadFooter(Context context, AttributeSet attrs) {
		super(context, attrs);
		returnView = LayoutInflater.from(context).inflate(R.layout.widget_footer_load, this);
		tvLoading = (TextView) findViewById(R.id.tv_loading);
	}



}
