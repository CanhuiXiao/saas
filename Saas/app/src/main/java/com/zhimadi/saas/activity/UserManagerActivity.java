package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.fragment.BaseFragment;
import com.zhimadi.saas.fragment.EmployeeFragment;
import com.zhimadi.saas.fragment.FragmentController;
import com.zhimadi.saas.fragment.ShopFragment;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

public class UserManagerActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private FragmentController fragmentController;
    private EmployeeFragment employeeFragment;
    private ShopFragment shopFragment;

    private RadioGroupWithLayout rgShopHome;

    //标记
    private int stateShow = Constant.STATE_EMPLOYEE;

    //两个破方块
    private View viewShopHomeEmployee;
    private View viewShopHomeShop;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        fragmentController = new FragmentController(mContext, R.id.fl_shop_home);
        employeeFragment = new EmployeeFragment();
        employeeFragment.setTag(BaseFragment.FirstTAG);
        shopFragment = new ShopFragment();
        shopFragment.setTag(BaseFragment.SecondTAG);
        rgShopHome = (RadioGroupWithLayout) findViewById(R.id.rg_shop_home);
        viewShopHomeEmployee = findViewById(R.id.view_shop_home_employee);
        viewShopHomeShop = findViewById(R.id.view_shop_home_shop);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_manager);
        inte();
        titleBarCommonBuilder.setTitle("员工与门店")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });


        //这里要判断
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        if (stateShow == Constant.STATE_EMPLOYEE) {
                            intent.setClass(mContext, EmployeeEditActivity.class);
                        } else if (stateShow == Constant.STATE_SHOP) {
                            intent.setClass(mContext, ShopEditActivity.class);

                        }
                        intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        rgShopHome.setOnCheckedChangeListener(this);
        fragmentController.showFragment(employeeFragment, getFragmentManager());
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        if(stateShow == Constant.STATE_EMPLOYEE){
            employeeFragment.refresh();
        }else if(stateShow == Constant.STATE_SHOP){
            shopFragment.refresh();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_shop_home_employee:
                stateShow = Constant.STATE_EMPLOYEE;
                fragmentController.showFragment(employeeFragment, getFragmentManager());
                viewShopHomeEmployee.setVisibility(View.VISIBLE);
                viewShopHomeShop.setVisibility(View.INVISIBLE);
                break;
            case R.id.rb_shop_home_shop:
                stateShow = Constant.STATE_SHOP;
                fragmentController.showFragment(shopFragment, getFragmentManager());
                viewShopHomeEmployee.setVisibility(View.INVISIBLE);
                viewShopHomeShop.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}
