package com.zhimadi.saas.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.SellAgentAgentAdapter;
import com.zhimadi.saas.event.SellAgentAgentsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/8.
 */
public class SellAgentAgentFragment extends BaseFragment implements RadioGroupWithLayout.OnCheckedChangeListener {

    private View returnView;

    public static final int AGENT_ALL = -1;
    public static final int AGENT_WAIT_COMFIRM = 4;
    public static final int AGENT_FINISH = 5;

    private int mStart = 0;
    private int mLimit = 10;

    private RadioGroupWithLayout rgSellAgentAgentHome;
    //滑块集合
    private List<View> sliders;

    private List<SellAgentAgentsEvent.SellAgentAgent> sellAgentAgents;
    private ListView lvSellAgentAgentHome;
    private SellAgentAgentAdapter sellAgentAgentAdapter;

    private void inte() {
        rgSellAgentAgentHome = (RadioGroupWithLayout) returnView.findViewById(R.id.rg_sell_agent_agent_home);

        sliders = new ArrayList<View>();

        View viewSellAgentAgentHomeAll = returnView.findViewById(R.id.view_sell_agent_agent_home_all);
        sliders.add(viewSellAgentAgentHomeAll);
        View viewSellAgentAgentHomeWaitComfirm = returnView.findViewById(R.id.view_sell_agent_agent_home_wait_comfirm);
        sliders.add(viewSellAgentAgentHomeWaitComfirm);
        View viewSellAgentAgentHomeFinish = returnView.findViewById(R.id.view_sell_agent_agent_home_finish);
        sliders.add(viewSellAgentAgentHomeFinish);




        lvSellAgentAgentHome = (ListView) returnView.findViewById(R.id.lv_sell_agent_agent_home);
        sellAgentAgents = new ArrayList<SellAgentAgentsEvent.SellAgentAgent>();
        sellAgentAgentAdapter = new SellAgentAgentAdapter(activity, R.layout.item_lv_sell_agent_agent_home, sellAgentAgents);
    }


    //显示滑块的方法
    private void ShowSlider(int RadioButtonId) {
        for (int i = 0; i < sliders.size(); i++) {
            if (RadioButtonId == sliders.get(i).getId()) {
                sliders.get(i).setVisibility(View.VISIBLE);
            } else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_agent, null);
        inte();
        lvSellAgentAgentHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent();
//                intent.setClass(activity, AgentEditActivity.class);
//                intent.putExtra("INTENT_MODE", AgentEditActivity.EDIT_MODE);
//                intent.putExtra("AGENT_ID", sellAgentAgents.get(position).getAgent_id());
//                intent.putExtra("STATE", sellAgentAgents.get(position).getState());
//                startActivity(intent);
            }
        });

        lvSellAgentAgentHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetSellAgentAgent(state);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvSellAgentAgentHome.setAdapter(sellAgentAgentAdapter);
        GetSellAgentAgent(state);
        rgSellAgentAgentHome.setOnCheckedChangeListener(this);
        return returnView;
    }

    private void GetSellAgentAgent(int state) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        if (state != AGENT_ALL) {
            map.put("state", state + "");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mAgentSellAddress, map);
        AsyncUtil.asyncGet(activity, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SellAgentAgentsEvent sellAgentAgentsEvent = gson.fromJson(response, SellAgentAgentsEvent.class);
                if (mStart == 0) {
                    sellAgentAgents.clear();
                }
                mStart = mStart + sellAgentAgentsEvent.getData().getList().size();
                sellAgentAgents.addAll(sellAgentAgentsEvent.getData().getList());
                sellAgentAgentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private int state = AGENT_ALL;


    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_sell_agent_agent_home_all:
                ShowSlider(R.id.view_sell_agent_agent_home_all);
                mStart = 0;
                state = AGENT_ALL;
                lvSellAgentAgentHome.setSelection(0);
                GetSellAgentAgent(state);
                break;
            case R.id.rb_sell_agent_agent_home_wait_comfirm:
                ShowSlider(R.id.view_sell_agent_agent_home_wait_comfirm);
                mStart = 0;
                state = AGENT_WAIT_COMFIRM;
                lvSellAgentAgentHome.setSelection(0);
                GetSellAgentAgent(state);
                break;
            case R.id.rb_sell_agent_agent_home_finish:
                ShowSlider(R.id.view_sell_agent_agent_home_finish);
                mStart = 0;
                state = AGENT_FINISH;
                lvSellAgentAgentHome.setSelection(0);
                GetSellAgentAgent(state);
                break;
            default:
                break;
        }
    }
}