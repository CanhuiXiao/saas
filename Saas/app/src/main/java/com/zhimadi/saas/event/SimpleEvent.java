package com.zhimadi.saas.event;

import android.app.Activity;

/**
 * Created by carry on 2016/7/12.
 */
public class SimpleEvent {


    private int iconPicture;

    private String title;

    private Class<Activity> target;

    public int getIconPicture() {
        return iconPicture;
    }

    public void setIconPicture(int iconPicture) {
        this.iconPicture = iconPicture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Class<Activity> getTarget() {
        return target;
    }

    public void setTarget(Class<Activity> target) {
        this.target = target;
    }
}
