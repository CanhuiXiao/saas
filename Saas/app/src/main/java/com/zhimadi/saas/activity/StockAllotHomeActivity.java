package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StockAllotAdapter;
import com.zhimadi.saas.event.StockAllotsEvent;
import com.zhimadi.saas.event.StocksAgentEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */

public class StockAllotHomeActivity extends BaseActivity {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<StockAllotsEvent.StockAllot> stockAllots;
    private ListView lvStockWaterHome;
    private StockAllotAdapter stockAllotAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        stockAllots = new ArrayList<StockAllotsEvent.StockAllot>();
        lvStockWaterHome = (ListView) findViewById(R.id.lv_stock_water_home);
        stockAllotAdapter = new StockAllotAdapter(mContext, R.layout.item_lv_stock_allot_home, stockAllots);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_allot);
        inte();
        titleBarCommonBuilder.setTitle("库存流水").setLeftImageRes(R.drawable.fan_hui).setLeftText("返回").setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lvStockWaterHome.setAdapter(stockAllotAdapter);
        getStockWater();
    }

    private void getStockWater(){
        AsyncUtil.asyncGet(mContext, OkHttpUtil.mStockAllotAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                StockAllotsEvent stockAllotsEvent = gson.fromJson(response, StockAllotsEvent.class);
                stockAllots.addAll(stockAllotsEvent.getData().getList());
                stockAllotAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }




}