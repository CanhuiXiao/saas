package com.zhimadi.saas.constant;

/**
 * Created by carry on 2016/9/2.
 */
public class Constant {

    //成功
    public static final int RESULT_CODE_SUCCESS = 1;
    //失败
    public static final int RESULT_CODE_Fail = 0;


    //定装
    public static final int STATE_FIT = 1;
    //散装
    public static final int STATE_NOT_FIT = 0;

    //代卖
    public static final int STATE_ALL = -1;//全部
    public static final int STATE_DEFAULT = 0;//默认
    public static final int STATE_WAIT_CHECK = 1;//待审核
    public static final int STATE_CANCEL = 2;//撤销
    public static final int STATE_NOTE = 3;//草稿
    public static final int STATE_WAIT_COMFIRM = 4;//待确认
    public static final int STATE_FINISH = 5;//已完成
    public static final int STATE_REJECT = 6;//拒绝接收代卖单

    //角色
    public static final int ROLE_OWNER = 1; //创始人,管理员
    public static final int ROLE_BUYER = 2;//采购
    public static final int ROLE_SALER = 3;//销售
    public static final int ROLE_WAREHOUSE_KEEPER = 4; //仓库管理
    public static final int ROLE_MANAGER = 5; //运营人员
    public static final int ROLE_FINANCIAL  = 6; //财务

    //自营商品
    public static final int SELL = 0;
    //代卖商品
    public static final int AGENT = 1;

    //代卖货主
    public static final int SELL_AGENT_OWNER = 1;
    //代卖商
    public static final int SELL_AGENT_AGENT = 2;

    //编辑还是添加
    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;
    public static final int DEFAULT_MODE = 0;


    //活动请求码

    //选择客户
    public static final int REQUEST_CODE_FOR_CUSTOM = 1;
    //选择供应商
    public static final int REQUEST_CODE_FOR_SUPPLIER = 2;
    //选择仓库
    public static final int REQUEST_CODE_FOR_STORE = 3;
    //选择门店
    public static final int REQUEST_CODE_FOR_SHOP = 4;
    //选择商品
    public static final int REQUEST_CODE_FOR_PRODUCT = 5;
    //填写客户发货联系方式
    public static final int REQUEST_CODE_FOR_CUSTOM_DETAIL = 6;
    //地区
    public static final int REQUEST_CODE_FOR_AREA = 7;


    //消息类型
    //芝麻地
    public static final int MESSAGE_ZHI_MA_DI = 1;
    //代办事项
    public static final int MESSAGE_SOMETHING_TO_DO = 2;
    //系统消息
    public static final int MESSAGE_SYSTEM = 3;
    //公告
    public static final int MESSAGE_ANNOUNCE = 4;
    //通知
    public static final int MESSAGE_NOTICE = 5;

    //员工
    public static final int STATE_EMPLOYEE = 1;
    //门店
    public static final int STATE_SHOP = 2;

}
