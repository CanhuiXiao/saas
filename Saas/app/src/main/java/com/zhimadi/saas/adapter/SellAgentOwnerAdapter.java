package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.SellAgentOwnersEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class SellAgentOwnerAdapter extends ArrayAdapter<SellAgentOwnersEvent.SellAgentOwner> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals("1")) {
            holder.tvBuyHomestate.setText("待审核");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("5")) {
            holder.tvBuyHomestate.setText("已代卖");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("3")) {
            holder.tvBuyHomestate.setText("草稿");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals("2")) {
            holder.tvBuyHomestate.setText("撤销");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if(state.equals("4")){
            holder.tvBuyHomestate.setText("待确认");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if(state.equals("0")){
            holder.tvBuyHomestate.setText("代卖中");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public SellAgentOwnerAdapter(Context context, int resource, List<SellAgentOwnersEvent.SellAgentOwner> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SellAgentOwnersEvent.SellAgentOwner sellAgentOwner = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        showStateText(holder, sellAgentOwner.getState());
        holder.tvBuyHomeOwner.setText(sellAgentOwner.getAgent_name());
        holder.tvBuyHomeOderNumber.setText(sellAgentOwner.getOrder_no());
        holder.tvBuyHomeToPay.setText("￥" + sellAgentOwner.getTotal_amount());
        holder.tvBuyHomeCreateDate.setText(sellAgentOwner.getCreate_time());
        holder.tvBuyHomeStore.setText(sellAgentOwner.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {
        TextView tvBuyHomeOwner;
        TextView tvBuyHomestate;
        TextView tvBuyHomeOderNumber;
        TextView tvBuyHomeToPay;
        TextView tvBuyHomeCreateDate;
        TextView tvBuyHomeStore;

        public ViewHolder(View view) {
            tvBuyHomeOwner = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_owner);
            tvBuyHomestate = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_state);
            tvBuyHomeOderNumber = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_oder_number);
            tvBuyHomeToPay = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_to_pay);
            tvBuyHomeCreateDate = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_create_date);
            tvBuyHomeStore = (TextView) view.findViewById(R.id.tv_sell_agent_owner_home_store);
            view.setTag(this);
        }
    }


}
