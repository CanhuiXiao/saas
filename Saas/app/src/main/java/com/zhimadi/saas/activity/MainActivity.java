package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioGroup;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.fragment.AppFragment;
import com.zhimadi.saas.fragment.BaseFragment;
import com.zhimadi.saas.fragment.FragmentController;
import com.zhimadi.saas.fragment.HomeFragment;
import com.zhimadi.saas.fragment.MessageFragment;
import com.zhimadi.saas.fragment.UserFragment;

public class MainActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener{

    //请求码
    public static final int requestCodeForLogin = 1;


    private FragmentController fragmentController;
    private RadioGroup rgHome;
    private HomeFragment homeFragment;
    private AppFragment appFragment;
    private UserFragment userFragment;
    private MessageFragment messageFragment;

    private void inte() {
        fragmentController = new FragmentController(mContext, R.id.fl_home_window);
        rgHome = (RadioGroup) findViewById(R.id.rg_home);
        homeFragment = new HomeFragment();
        homeFragment.setTag(BaseFragment.FirstTAG);
        appFragment = new AppFragment();
        appFragment.setTag(BaseFragment.SecondTAG);
        userFragment = new UserFragment();
        userFragment.setTag(BaseFragment.ThirdTAG);
        messageFragment = new MessageFragment();
        messageFragment.setTag(BaseFragment.FouthTAG);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inte();
        rgHome.setOnCheckedChangeListener(this);
        if(UserInfo.getmAuth() == null || UserInfo.getmAuth().equals("")) {//判断为未登录
            Intent intent = new Intent();
            intent.setClass(mContext, LoginHomeActivity.class);
            startActivityForResult(intent, requestCodeForLogin);
        }else {
            fragmentController.showFragment(homeFragment, getFragmentManager());
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case requestCodeForLogin:
                if(resultCode != Constant.RESULT_CODE_SUCCESS){
                    finish();
                }else {
                    fragmentController.showFragment(homeFragment, getFragmentManager());
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.rb_home_home:
                fragmentController.showFragment(homeFragment, getFragmentManager());
                break;
            case R.id.rb_home_app:
                fragmentController.showFragment(appFragment, getFragmentManager());
                break;
            case R.id.rb_home_mine:
                fragmentController.showFragment(userFragment, getFragmentManager());
                break;
            case R.id.rb_home_message:
                fragmentController.showFragment(messageFragment, getFragmentManager());
                break;
            default:
                break;
        }
    }
}
