package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class StocksSellEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Stock> list;
        private String total_stock;
        private String total_weight;
        private String total_money;
        private String low_stock;

        public String getTotal_stock() {
            return total_stock;
        }

        public void setTotal_stock(String total_stock) {
            this.total_stock = total_stock;
        }

        public String getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(String total_weight) {
            this.total_weight = total_weight;
        }

        public String getTotal_money() {
            return total_money;
        }

        public void setTotal_money(String total_money) {
            this.total_money = total_money;
        }

        public String getLow_stock() {
            return low_stock;
        }

        public void setLow_stock(String low_stock) {
            this.low_stock = low_stock;
        }

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Stock> getList() {
            return list;
        }

        public void setList(List<Stock> list) {
            this.list = list;
        }
    }

    public class Stock {
        private String product_id;
//        private String cat_id;
        private String name;
        private String is_fixed;
        private String fixed_weight;
        private String sku;
        private String price_unit;
        private String created_time;
        private Float price;
        private String weight;
        private String quantity;
        private String props_name;
        private String pic_urls;

        public Float getPrice() {
            return price;
        }

        public void setPrice(Float price) {
            this.price = price;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

//        public String getCat_id() {
//            return cat_id;
//        }

//        public void setCat_id(String cat_id) {
//            this.cat_id = cat_id;
//        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIs_fixed() {
            return is_fixed;
        }

        public void setIs_fixed(String is_fixed) {
            this.is_fixed = is_fixed;
        }

        public String getFixed_weight() {
            return fixed_weight;
        }

        public void setFixed_weight(String fixed_weight) {
            this.fixed_weight = fixed_weight;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getPrice_unit() {
            return price_unit;
        }

        public void setPrice_unit(String price_unit) {
            this.price_unit = price_unit;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getProps_name() {
            return props_name;
        }

        public void setProps_name(String props_name) {
            this.props_name = props_name;
        }

        public String getPic_urls() {
            return pic_urls;
        }

        public void setPic_urls(String pic_urls) {
            this.pic_urls = pic_urls;
        }
    }

}
