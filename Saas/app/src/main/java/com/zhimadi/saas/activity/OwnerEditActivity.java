package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.OwnerDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class OwnerEditActivity extends BaseActivity implements View.OnClickListener {
    

    /**
     * 打开模式
     */
    private int intentMode;

    private String ownerId;
    private String areaCode = "";

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private LinearLayout llOwnerEditWindow;

    private EditTextITem editTextITemName;
    private SwitchItem switchITem;

    private EditTextITem editTextITemPhone;
    private EditTextITem editTextITemTel;
    private EditTextITem editTextITemEMail;
    private EditTextITem editTextITemFax;
    private EditTextITem editTextITemWebAddress;

    private SelelctableTextViewItem selelctableTextViewItemArea;
    private EditTextITem editTextITemAddress2;

    private EditTextITem editTextITemBank;
    private EditTextITem editTextITemBankCardNumber;
    private EditTextITem editTextITemBankCreateName;

    private EditTextITem editTextITemOrderSize;
    private EditTextITem editTextITemNote;

    private Button btOwnerSave;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == Constant.EDIT_MODE) {
            ownerId = getIntent().getStringExtra("OWNER_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);

        llOwnerEditWindow = (LinearLayout) findViewById(R.id.ll_owner_edit_window);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入代卖货主名称");
        switchITem = new SwitchItem(mContext, null, "启动", true);

        editTextITemPhone = new EditTextITem(mContext, null, "手机", "请输入货主手机");
        editTextITemTel = new EditTextITem(mContext, null, "电话", "请输入电话");
        editTextITemEMail = new EditTextITem(mContext, null, "邮箱", "请输入货主邮箱");
        editTextITemFax = new EditTextITem(mContext, null, "传真", "请输入传真号码");
        editTextITemWebAddress = new EditTextITem(mContext, null, "网址", "请输入公司网址");

        selelctableTextViewItemArea = new SelelctableTextViewItem(mContext, null, "所在地区", "请选择所在地区");
        editTextITemAddress2 = new EditTextITem(mContext, null, "详细地址", "请输入详细地址");

        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        editTextITemBank = new EditTextITem(mContext, null, "开户银行", "请输入开户银行");
        editTextITemBankCardNumber = new EditTextITem(mContext, null, "银行账户", "请输入银行账户");
        editTextITemBankCreateName = new EditTextITem(mContext, null, "银行账号", "请输入银行账号");


        btOwnerSave = (Button) findViewById(R.id.bt_owner_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_edit);
        inte();

        if (intentMode == Constant.EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑代卖货主");
        } else if (intentMode == Constant.ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增代卖货主");
        } else {
            finish();
        }


        selelctableTextViewItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, AreaSelectActivity.class);
                startActivityForResult(intent, Constant.REQUEST_CODE_FOR_AREA);
            }
        });



        editTextITemName.isTop(true).isNotNull(true);
        llOwnerEditWindow.addView(editTextITemName);
        switchITem.isBottom(true);
        llOwnerEditWindow.addView(switchITem);

        editTextITemPhone.isTop(true).isBottom(true);
        llOwnerEditWindow.addView(editTextITemPhone);
        llOwnerEditWindow.addView(editTextITemTel);
        llOwnerEditWindow.addView(editTextITemEMail);
        llOwnerEditWindow.addView(editTextITemFax);
        editTextITemWebAddress.isBottom(true);
        llOwnerEditWindow.addView(editTextITemWebAddress);

        selelctableTextViewItemArea.isTop(true);
        llOwnerEditWindow.addView(selelctableTextViewItemArea);
        editTextITemAddress2.isBottom(true);
        llOwnerEditWindow.addView(editTextITemAddress2);

        editTextITemBank.isTop(true);
        llOwnerEditWindow.addView(editTextITemBank);
        llOwnerEditWindow.addView(editTextITemBankCardNumber);
        editTextITemBankCreateName.isBottom(true);
        llOwnerEditWindow.addView(editTextITemBankCreateName);

        editTextITemOrderSize.isTop(true).isNotNull(true);
        llOwnerEditWindow.addView(editTextITemOrderSize);
        editTextITemNote.isBottom(true);
        llOwnerEditWindow.addView(editTextITemNote);


        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        btOwnerSave.setOnClickListener(this);
        if (intentMode == Constant.EDIT_MODE) {
            GetOwnerDetail();
        }else {
            editTextITemOrderSize.SetItemValue("100");
        }

    }


    private void GetOwnerDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("owner_id", ownerId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mOwnerDetailAddress, map);

        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                OwnerDetailEvent ownerDetailEvent = gson.fromJson(response, OwnerDetailEvent.class);
                editTextITemName.SetItemValue(ownerDetailEvent.getData().getName());
                switchITem.SetCheck(ownerDetailEvent.getData().getState());

                editTextITemPhone.SetItemValue(ownerDetailEvent.getData().getPhone());
                editTextITemTel.SetItemValue(ownerDetailEvent.getData().getTel());
                editTextITemEMail.SetItemValue(ownerDetailEvent.getData().getEmail());
                editTextITemFax.SetItemValue(ownerDetailEvent.getData().getFax());
                editTextITemWebAddress.SetItemValue(ownerDetailEvent.getData().getWebsite());

                selelctableTextViewItemArea.SetItemValue(ownerDetailEvent.getData().getArea_name());
                editTextITemAddress2.SetItemValue(ownerDetailEvent.getData().getAddress());

                editTextITemOrderSize.SetItemValue(ownerDetailEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(ownerDetailEvent.getData().getNote());

                areaCode = ownerDetailEvent.getData().getArea_id();
            }

            @Override
            public void onFail() {

            }
        });

    }


    private void OwnerEdit() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == Constant.EDIT_MODE) {
            jsonObject.addProperty("owner_id", ownerId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        jsonObject.addProperty("state", switchITem.IsCheck());

        jsonObject.addProperty("tel", editTextITemTel.GetItemValue());
        jsonObject.addProperty("phone", editTextITemPhone.GetItemValue());
        jsonObject.addProperty("fax", editTextITemFax.GetItemValue());
        jsonObject.addProperty("email", editTextITemEMail.GetItemValue());
        jsonObject.addProperty("website", editTextITemWebAddress.GetItemValue());

        if (!areaCode.equals("")) {
            jsonObject.addProperty("area_id", areaCode);
            jsonObject.addProperty("area_name", selelctableTextViewItemArea.GetItemValue());
        }
        jsonObject.addProperty("address", editTextITemAddress2.GetItemValue());

        jsonObject.addProperty("bank_name", editTextITemBank.GetItemValue());
        jsonObject.addProperty("bank_account", editTextITemBankCardNumber.GetItemValue());
        jsonObject.addProperty("bank_username", editTextITemBankCreateName.GetItemValue());


        jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditOwnerAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                if (intentMode == Constant.ADD_MODE) {
                    Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                    finish();
                } else if (intentMode == Constant.EDIT_MODE) {
                    Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_FOR_AREA:
                if (resultCode == AreaSelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemArea.SetItemValue(data.getStringExtra("AREA_NAME"));
                    areaCode = data.getStringExtra("AREA_CODE");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_owner_save:
                OwnerEdit();
                break;
            default:
                break;
        }
    }
}
