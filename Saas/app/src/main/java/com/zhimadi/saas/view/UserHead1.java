package com.zhimadi.saas.view;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.UserDataActivity;
import com.zhimadi.saas.constant.UserInfo;

/**
 * Created by carry on 2016/7/8.
 */
public class UserHead1 extends LinearLayout{

    private View returnView;
    private Context mContext;

    private TextView tvUserHomeCompanyName;
    private TextView tvUserHomeName;

    public UserHead1(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        returnView = LayoutInflater.from(mContext).inflate(R.layout.view_user_head, this);
        tvUserHomeCompanyName = (TextView) findViewById(R.id.tv_user_home_company_name);
        tvUserHomeName = (TextView) findViewById(R.id.tv_user_home_name);
        tvUserHomeCompanyName.setText(UserInfo.getmCompanyName());
        tvUserHomeName.setText(UserInfo.getmUserName());
        returnView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, UserDataActivity.class);
                mContext.startActivity(intent);
            }
        });
    }










}
