package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.EmployeeSelectActivity;
import com.zhimadi.saas.event.EmployeesEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class EmployeeSelectAdapter extends ArrayAdapter<EmployeesEvent.Employee>{

    private Context mContext;
    private EmployeeSelectActivity mActivity;
    private int resourceId;

    private TextView tvEmployeeSelectName;
    private TextView tvEmployeeSelectPhone;
    private TextView tvEmployeeSelectShop;
    private TextView tvEmployeeSelectLandTime;
    private TextView tvEmployeeSelectCreateTime;
    private CheckBox cbEmployeeSelect;

    public EmployeeSelectAdapter(Context context, int resource, List<EmployeesEvent.Employee> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        mActivity = (EmployeeSelectActivity) activity;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final EmployeesEvent.Employee employee  = getItem(position);
        convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvEmployeeSelectName = (TextView) convertView.findViewById(R.id.tv_employee_select_name);
        tvEmployeeSelectPhone = (TextView) convertView.findViewById(R.id.tv_employee_select_phone);
        tvEmployeeSelectShop = (TextView) convertView.findViewById(R.id.tv_employee_select_shop);
        tvEmployeeSelectLandTime = (TextView) convertView.findViewById(R.id.tv_employee_select_land_time);
        tvEmployeeSelectCreateTime = (TextView) convertView.findViewById(R.id.tv_employee_select_create_time);
        cbEmployeeSelect = (CheckBox) convertView.findViewById(R.id.cb_employee_select);
        cbEmployeeSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    Intent intent = new Intent();
                    intent.putExtra("EMPLOYEE_NAME", employee.getName());
                    intent.putExtra("EMPLOYEE_ID", employee.getUser_id());
                    mActivity.setResult(mActivity.RESULT_CODE_SUCCESS, intent);
                    mActivity.finish();
                }
            }
        });
        tvEmployeeSelectName.setText(employee.getName());
        tvEmployeeSelectPhone.setText(employee.getAccount());
        tvEmployeeSelectShop.setText("所属门店 : " + employee.getShop_name());
        tvEmployeeSelectLandTime.setText("最后登录时间 : " + employee.getLast_login_time());
        tvEmployeeSelectCreateTime.setText("创建时间 : " + employee.getAtime());
        return convertView;
    }
}
