package com.zhimadi.saas.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.util.DisplayUtil;

public class ButtonFooter extends LinearLayout {

	private View returnView;
	private TextView tvButtonFooter;

	public ButtonFooter(Context context, AttributeSet attrs) {
		super(context, attrs);
		returnView = LayoutInflater.from(context).inflate(R.layout.view_button_footer, this);
		tvButtonFooter = (TextView) returnView.findViewById(R.id.tv_button_footer);
	}

	public ButtonFooter setText(String text){
		tvButtonFooter.setText(text);
		return this;
	}

	public ButtonFooter setTopMargin(Context context, float topMargin){
		LinearLayout.LayoutParams layoutParams = (LayoutParams) tvButtonFooter.getLayoutParams();
		layoutParams.topMargin = DisplayUtil.dip2px(context, topMargin);
		tvButtonFooter.setLayoutParams(layoutParams);
		return this;
	}

	public void setOnClickListener(OnClickListener onClickListener){
		tvButtonFooter.setOnClickListener(onClickListener);
	}

}
