package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.AttributeOptionsEvent;
import com.zhimadi.saas.event.AttributesEvent;
import com.zhimadi.saas.event.CategoryFormatsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.FlowLayout;
import com.zhimadi.saas.widget.Format;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormatSelectActivity extends BaseActivity implements View.OnClickListener {

    //缓存规格控件，用于注入规格值
    private List<Format> formats;

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_FAIL = 0;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private Button btFormatSave;
    private LinearLayout llFormatEditWindow;

    private String categoryId;

    private void inte() {
        categoryId = getIntent().getStringExtra("CATEGORY_ID");
        formats = new ArrayList<Format>();
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        btFormatSave = (Button) findViewById(R.id.bt_format_save);
        llFormatEditWindow = (LinearLayout) findViewById(R.id.ll_format_edit_window);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_format_select);
        inte();

        titleBarCommonBuilder.setTitle("选择规格")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        btFormatSave.setOnClickListener(this);
        GetAttributeUserble();
    }

    //传入分类规格列表
    private void GetAttribute(final List<CategoryFormatsEvent.CategoryFormat> categoryFormats) {
        AsyncUtil.asyncGet(mContext, OkHttpUtil.mAttributeAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                //获取系统所有规格
                Gson gson = new Gson();
                AttributesEvent attributesEvent = gson.fromJson(response, AttributesEvent.class);
                List<AttributesEvent.Attribute> attributes = attributesEvent.getData().getList();
                //遍历所有规格看看能不能在分类规格内找到
                for (int i = 0; i < categoryFormats.size(); i++) {
                    for (int k = 0; k < attributes.size(); k++) {
                        if (categoryFormats.get(i).getAttribute_id().equals(attributes.get(k).getAttribute_id())) {
                            Format format = new Format(mContext, null);
                            format.SetMarginVertical(DisplayUtil.dip2px(mContext, 9.6f));
                            format.SetMarginHorizontal(DisplayUtil.dip2px(mContext, 13.44f));
                            format.setFormatName(attributes.get(k).getName());
                            GetAttributeOption(format, attributes.get(k).getAttribute_id());
                            formats.add(format);
                            llFormatEditWindow.addView(format);
                        }
                    }


//                for (int i = 0; i < attributes.size(); i++) {
//
//
//                    for (int k = 0; k < categoryFormats.size(); k++) {
//                        if (attributes.get(i).getAttribute_id().equals(categoryFormats.get(k).getAttribute_id())) {
//
//                            Format format = new Format(mContext, null);
//                            format.SetMarginVertical(DisplayUtil.dip2px(mContext, 9.6f));
//                            format.SetMarginHorizontal(DisplayUtil.dip2px(mContext, 13.44f));
//                            format.setFormatName(attributes.get(i).getName());
//                            GetAttributeOption(format, attributes.get(i).getAttribute_id());
//                            formats.add(format);
//                            llFormatEditWindow.addView(format);
//                        }
                }
            }

            @Override
            public void onFail() {

            }

        });
    }


    private void GetAttributeOption(final Format format, String attributeId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("attribute_id", attributeId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mAttributeOptionAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                AttributeOptionsEvent attributeOptionsEvent = gson.fromJson(response, AttributeOptionsEvent.class);
                final List<AttributeOptionsEvent.AttributeOption> attributeOptions = attributeOptionsEvent.getData().getList();
                for (int i = 0; i < attributeOptions.size(); i++) {
                    final int d = i;
                    RadioButton radioButton = new RadioButton(mContext);
                    radioButton.setChecked(false);
                    radioButton.setButtonDrawable(R.color.colorMainBody);
                    radioButton.setText(attributeOptions.get(d).getName());
                    radioButton.setTextSize(11.52f);
                    FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(-2, DisplayUtil.dip2px(mContext, 26.4f));
                    radioButton.setLayoutParams(params);
                    radioButton.setTextColor(Color.rgb(0x36, 0x36, 0x36));
                    radioButton.setBackgroundResource(R.drawable.sel_check_format);
                    format.addOption(radioButton);
                    radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                format.Select((RadioButton) compoundButton);
                                format.setAttributeOptionId(attributeOptions.get(d).getAttribute_id() + ":" + attributeOptions.get(d).getOption_id() + ";");
                                format.setAttributeOptionValue(attributeOptions.get(d).getName() + ";");
                            }
                        }
                    });
                    format.addFarmatItem(radioButton);
                }
            }

            @Override
            public void onFail() {

            }
        });
    }

    //获取可用规格
    private void GetAttributeUserble() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("category_id", categoryId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mCategorySpecificationAttributeMappingAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CategoryFormatsEvent categoryFormatsEvent = gson.fromJson(response, CategoryFormatsEvent.class);
                List<CategoryFormatsEvent.CategoryFormat> categoryFormats = categoryFormatsEvent.getData().getList();
                GetAttribute(categoryFormats);
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_format_save:
                boolean isFinish = true;
                //检查是否填完了
                if (formats.size() > 0) {
                    for (int i = 0; i < formats.size(); i++) {
                        if (formats.get(i).getAttributeOptionId() == null || formats.get(i).getAttributeOptionId().equals("")) {
                            isFinish = false;
                        }
                    }
                } else {
                    isFinish = false;
                }

                if (isFinish) {
                    Intent intent = new Intent();
                    StringBuilder builderFormatId = new StringBuilder();
                    StringBuilder builderFormatValue = new StringBuilder();
                    for (int i = 0; i < formats.size(); i++) {
                        if (formats.get(i).getAttributeOptionId() != null && !formats.get(i).getAttributeOptionId().equals("")) {
                            builderFormatId.append(formats.get(i).getAttributeOptionId());
                            builderFormatValue.append(formats.get(i).getAttributeOptionValue());
                        }
                    }
                    intent.putExtra("FORMAT_OPTION_ID", builderFormatId.toString().substring(0, builderFormatId.toString().length() - 1));
                    intent.putExtra("FORMAT_OPTION_VALUE", builderFormatValue.toString().substring(0, builderFormatId.toString().length() - 1));
                    setResult(RESULT_CODE_SUCCESS, intent);
                    finish();
                } else {
                    Toast.makeText(mContext, "请选择完整后再确认!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }


}
