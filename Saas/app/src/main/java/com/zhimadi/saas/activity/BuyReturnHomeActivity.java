package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.BuyReturnAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.BuyReturnsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.RadioGroupWithLayout;
import com.zhimadi.saas.widget.TitleBarCommon;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/8.
 */
public class BuyReturnHomeActivity extends BaseActivity implements RadioGroupWithLayout.OnCheckedChangeListener {

    private int mStart = 0;
    private int mLimit = 10;
    private int mState = Constant.STATE_ALL;

    TitleBarCommonBuilder titleBarCommonBuilder;
    private RadioGroupWithLayout rgBuyReturnHome;
    //滑块集合
    private List<View> sliders;

    private List<BuyReturnsEvent.BuyReturn> buyReturns;
    private ListView lvBuyReturnHome;
    private BuyReturnAdapter buyReturnAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        rgBuyReturnHome = (RadioGroupWithLayout) findViewById(R.id.rg_buy_return_home);

        sliders = new ArrayList<View>();
        View viewBuyReturnHomeAll = findViewById(R.id.view_buy_return_home_all);
        sliders.add(viewBuyReturnHomeAll);
        View viewBuyReturnHomeWaitCheck = findViewById(R.id.view_buy_return_home_wait_check);
        sliders.add(viewBuyReturnHomeWaitCheck);
        View viewBuyReturnHomeFinish = findViewById(R.id.view_buy_return_home_finish);
        sliders.add(viewBuyReturnHomeFinish);
        View viewBuyReturnHomeNote = findViewById(R.id.view_buy_return_home_note);
        sliders.add(viewBuyReturnHomeNote);
        View viewBuyReturnHomeCancel = findViewById(R.id.view_buy_return_home_cancel);
        sliders.add(viewBuyReturnHomeCancel);

        lvBuyReturnHome = (ListView) findViewById(R.id.lv_buy_return_home);
        buyReturns = new ArrayList<BuyReturnsEvent.BuyReturn>();
        buyReturnAdapter = new BuyReturnAdapter(activity, R.layout.item_lv_buy_return_home, buyReturns);
    }


    //显示滑块的方法
    private void ShowSlider(int RadioButtonId) {
        for (int i = 0; i < sliders.size(); i++) {
            if (RadioButtonId == sliders.get(i).getId()) {
                sliders.get(i).setVisibility(View.VISIBLE);
            } else {
                sliders.get(i).setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_return_home);
        inte();
        titleBarCommonBuilder.setTitle("退货历史").setLeftText("返回").setLeftImageRes(R.drawable.fan_hui).setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lvBuyReturnHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
//                intent.setClass(activity, EditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("STATE", buyReturns.get(position).getState());
                startActivity(intent);
            }
        });

        lvBuyReturnHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        //加载更多功能的代码
                        GetBuyReturn();
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvBuyReturnHome.setAdapter(buyReturnAdapter);
        mState = Constant.STATE_ALL;
        GetBuyReturn();
        rgBuyReturnHome.setOnCheckedChangeListener(this);
    }


    private void GetBuyReturn() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        if (mState != Constant.STATE_ALL) {
            map.put("state", mStart + "");
        }
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mBuyReturnAddress, map);
        AsyncUtil.asyncGet(activity, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                BuyReturnsEvent buyReturnsEvent = gson.fromJson(response, BuyReturnsEvent.class);
                if (mStart == 0) {
                    buyReturns.clear();
                }
                mStart = mStart + buyReturnsEvent.getData().getList().size();
                buyReturns.addAll(buyReturnsEvent.getData().getList());
                buyReturnAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    public void onCheckedChanged(RadioGroupWithLayout group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_buy_return_home_all:
                ShowSlider(R.id.view_buy_return_home_all);
                mStart = 0;
                mState = Constant.STATE_ALL;
                lvBuyReturnHome.setSelection(0);
                GetBuyReturn();
                break;
            case R.id.rb_buy_return_home_wait_check:
                ShowSlider(R.id.view_buy_return_home_wait_check);
                mStart = 0;
                mState = Constant.STATE_WAIT_CHECK;
                lvBuyReturnHome.setSelection(0);
                GetBuyReturn();
                break;
            case R.id.rb_buy_return_home_finish:
                ShowSlider(R.id.view_buy_return_home_finish);
                mStart = 0;
                mState = Constant.STATE_DEFAULT;
                lvBuyReturnHome.setSelection(0);
                GetBuyReturn();
                break;
            case R.id.rb_buy_return_home_note:
                ShowSlider(R.id.view_buy_return_home_note);
                mStart = 0;
                mState = Constant.STATE_NOTE;
                lvBuyReturnHome.setSelection(0);
                GetBuyReturn();
                break;
            case R.id.rb_buy_return_home_cancel:
                ShowSlider(R.id.view_buy_return_home_cancel);
                mStart = 0;
                mState = Constant.STATE_CANCEL;
                lvBuyReturnHome.setSelection(0);
                GetBuyReturn();
                break;
            default:
                break;
        }
    }
}