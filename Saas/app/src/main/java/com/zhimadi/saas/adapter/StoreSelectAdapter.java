package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.StoreSelectActivity;
import com.zhimadi.saas.event.StoresEvent;

import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/13.
 */
public class StoreSelectAdapter extends ArrayAdapter<StoresEvent.Store>{

    private StoreSelectActivity mActivity;
    private Context mContext;
    private View returnView;
    private int resourceId;

    private TextView tvStoreSelectName;
    private TextView tvStoreSelectShop;
    private TextView tvStoreSelectManeger;
    private CheckBox cbStoreSelect;


    public StoreSelectAdapter(Context context, int resource, List<StoresEvent.Store> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
        mActivity = (StoreSelectActivity)activity;
    }


    private void inte(){
        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvStoreSelectName = (TextView) returnView.findViewById(R.id.tv_store_select_name);
        tvStoreSelectShop = (TextView) returnView.findViewById(R.id.tv_store_select_shop);
        tvStoreSelectManeger = (TextView) returnView.findViewById(R.id.tv_store_select_maneger);
        cbStoreSelect = (CheckBox) returnView.findViewById(R.id.cb_store_select);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final StoresEvent.Store store  = getItem(position);
        inte();
        tvStoreSelectName.setText(store.getName());
        tvStoreSelectShop.setText("所属门店 : " + store.getShop_name());
        tvStoreSelectManeger.setText("负责人 : " + store.getCharge_user_name());
        cbStoreSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mActivity.select(store.getWarehouse_id(), store.getName());
                }
            }
        });
        return returnView;
    }
}
