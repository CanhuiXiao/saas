package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.ShopDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.HashMap;
import java.util.Map;

public class ShopEditActivity extends BaseActivity implements View.OnClickListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private LinearLayout llShopEditWindow;
    private Button btShopSave;

    private EditTextITem edittextItemItemName;
    private EditTextITem edittextItemItemOrderBy;
    private EditTextITem edittextItemItemAddress;
    private EditTextITem edittextItemItemPhone;

    private int intentMode;
    private String shopId;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", Constant.DEFAULT_MODE);
        if (intentMode == Constant.EDIT_MODE) {
            shopId = getIntent().getStringExtra("SHOP_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llShopEditWindow = (LinearLayout) findViewById(R.id.ll_shop_edit_window);
        edittextItemItemName = new EditTextITem(mContext, null, "名称", "请输入门店名称");
        edittextItemItemOrderBy = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        edittextItemItemAddress = new EditTextITem(mContext, null, "地址", "请输入门店地址");
        edittextItemItemPhone = new EditTextITem(mContext, null, "电话", "请输入门店电话");
        btShopSave = (Button) findViewById(R.id.bt_shop_save);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_edit);
        inte();
        if (intentMode == Constant.EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑门店");
        } else {
            titleBarCommonBuilder.setTitle("新增门店");
        }

        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui).setLeftText("返回").setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        edittextItemItemName.isNotNull(true);
        llShopEditWindow.addView(edittextItemItemName);
        edittextItemItemOrderBy.isNotNull(true);
        llShopEditWindow.addView(edittextItemItemOrderBy);
        llShopEditWindow.addView(edittextItemItemAddress);
        edittextItemItemPhone.isBottom(true);
        llShopEditWindow.addView(edittextItemItemPhone);
        btShopSave.setOnClickListener(this);
        if (intentMode == Constant.EDIT_MODE) {
            GetShopDetail();
        } else {
            edittextItemItemOrderBy.SetItemValue("100");
        }
    }


    private void GetShopDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("shop_id", shopId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mShopDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                ShopDetailEvent shopDetailEvent = gson.fromJson(response, ShopDetailEvent.class);
                edittextItemItemName.SetItemValue(shopDetailEvent.getData().getName());
                edittextItemItemOrderBy.SetItemValue(shopDetailEvent.getData().getDisplay_order());
                edittextItemItemAddress.SetItemValue(shopDetailEvent.getData().getAddress());
                edittextItemItemPhone.SetItemValue(shopDetailEvent.getData().getTel());
            }

            @Override
            public void onFail() {
            }
        });
    }


    private void EditShop() {
        if (!(edittextItemItemName.GetItemValue().equals("") && !edittextItemItemOrderBy.GetItemValue().equals(""))) {
            JsonObject jsonObject = new JsonObject();
            if (intentMode == Constant.EDIT_MODE) {
                jsonObject.addProperty("shop_id", shopId);
            }
            jsonObject.addProperty("name", edittextItemItemName.GetItemValue());
            jsonObject.addProperty("display_order", edittextItemItemOrderBy.GetItemValue());
            jsonObject.addProperty("address", edittextItemItemAddress.GetItemValue());
            jsonObject.addProperty("tel", edittextItemItemPhone.GetItemValue());

            AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditShopAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
                @Override
                public void onSuccess(String response) {
                    if (intentMode == Constant.EDIT_MODE) {
                        Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }

                @Override
                public void onFail() {

                }
            });
        } else {
            Toast.makeText(mContext, "资料填写不完整", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_shop_save:
                EditShop();
                break;
            default:
                break;
        }
    }
}
