package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class CustomDetailEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String custom_id;
        private String type_id;
        private String name;
        private String bill_cycle_val;
        private String bill_cycle_unit;
        private String risk_amount;
        private String phone;
        private String tel;
        private String fax;
        private String email;
        private String website;
        private String area_id;
        private String address;
        private String bank_name;
        private String bank_account;
        private String bank_username;
        private String display_order;
        private String note;
        private String state;
        private String type_name;
        private String area_name;

        public String getType_name() {
            return type_name;
        }

        public void setType_name(String type_name) {
            this.type_name = type_name;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getCustom_id() {
            return custom_id;
        }

        public void setCustom_id(String custom_id) {
            this.custom_id = custom_id;
        }

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBill_cycle_val() {
            return bill_cycle_val;
        }

        public void setBill_cycle_val(String bill_cycle_val) {
            this.bill_cycle_val = bill_cycle_val;
        }

        public String getBill_cycle_unit() {
            return bill_cycle_unit;
        }

        public void setBill_cycle_unit(String bill_cycle_unit) {
            this.bill_cycle_unit = bill_cycle_unit;
        }

        public String getRisk_amount() {
            return risk_amount;
        }

        public void setRisk_amount(String risk_amount) {
            this.risk_amount = risk_amount;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getFax() {
            return fax;
        }

        public void setFax(String fax) {
            this.fax = fax;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getArea_id() {
            return area_id;
        }

        public void setArea_id(String area_id) {
            this.area_id = area_id;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getBank_account() {
            return bank_account;
        }

        public void setBank_account(String bank_account) {
            this.bank_account = bank_account;
        }

        public String getBank_username() {
            return bank_username;
        }

        public void setBank_username(String bank_username) {
            this.bank_username = bank_username;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
