package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.MessageList;
import com.zhimadi.saas.event.AreaEvent;
import com.zhimadi.saas.event.MessagesEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class MessageAdapter extends ArrayAdapter<MessageList.Message> {

    private Context mContext;
    private int resourceId;

    public MessageAdapter(Context context, int resource, List<MessageList.Message> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageList.Message message = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvMessageHomeNotRead.setText(message.getCount());
        holder.ivMessageHomeIcon.setImageResource(message.getDrawableResource());
        holder.tvMessageHomeTitle.setText(message.getTitle());
        holder.tvMessageHomeDescribe.setText(message.getDescribe());
        return convertView;
    }

    class ViewHolder {
        private TextView tvMessageHomeNotRead;
        private ImageView ivMessageHomeIcon;
        private TextView tvMessageHomeTitle;
        private TextView tvMessageHomeDescribe;

        private ViewHolder(View view) {
            tvMessageHomeNotRead = (TextView) view.findViewById(R.id.tv_message_home_not_read);
            ivMessageHomeIcon = (ImageView) view.findViewById(R.id.iv_message_home_icon);
            tvMessageHomeTitle = (TextView) view.findViewById(R.id.tv_message_home_title);
            tvMessageHomeDescribe = (TextView) view.findViewById(R.id.tv_message_home_describe);
            view.setTag(this);
        }
    }


}
