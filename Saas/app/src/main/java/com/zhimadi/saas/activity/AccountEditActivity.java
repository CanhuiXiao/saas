//package com.zhimadi.saas.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.LinearLayout;
//import android.widget.Toast;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.event.AccountDetailEvent;
//import com.zhimadi.saas.event.AccountEditEvent;
//import com.zhimadi.saas.util.AsyncUtil;
//import com.zhimadi.saas.util.OkHttpClientManager;
//import com.zhimadi.saas.util.OkHttpUtil;
//import com.zhimadi.saas.widget.EditTextITem;
//import com.zhimadi.saas.widget.SelectITem;
//import com.zhimadi.saas.widget.TitleBarCommonBuilder;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import okhttp3.Request;
//
//public class AccountEditActivity extends BaseActivity implements View.OnClickListener {
//
//
//    public static final int ADD_MODE = 1;
//    public static final int EDIT_MODE = 2;
//
//    private int intentMode;
//
//    private String accountId;
//    private TitleBarCommonBuilder titleBarCommonBuilder;
//    private LinearLayout llAccountEditWindow;
//    private Button btAccountSave;
//
//    private EditTextITem editTextITemName;
//    private EditTextITem editTextITemAccount;
//    private EditTextITem editTextITemCreateName;
//    private EditTextITem editTextITemOrderSize;
//    private EditTextITem editTextITemNote;
//
//    private SelectITem selectITemShop;
//    private SelectITem selectITemAccountType;
//
//    private void inte() {
//        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
//        if (intentMode == EDIT_MODE) {
//            accountId = getIntent().getStringExtra("ACCOUNT_ID");
//        }
//        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
//        llAccountEditWindow = (LinearLayout) findViewById(R.id.ll_account_edit_window);
//        btAccountSave = (Button) findViewById(R.id.bt_account_save);
//        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入账户名称");
//        editTextITemAccount = new EditTextITem(mContext, null, "帐号", "请输入帐号");
//        editTextITemCreateName = new EditTextITem(mContext, null, "开户人", "请输入开户人");
//        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
//        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");
//        selectITemShop = new SelectITem(mContext, null, "门店", "公共");
//        selectITemAccountType = new SelectITem(mContext, null, "账户类型", "银行账户");
//
//
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_account_edit);
//        inte();
//        if (intentMode == EDIT_MODE) {
//            titleBarCommonBuilder.setTitle("编辑账户");
//
//        } else if (intentMode == ADD_MODE) {
//            titleBarCommonBuilder.setTitle("新增账户");
//        } else {
//            finish();
//        }
//        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui).setLeftText("返回")
//                .setLeftOnClikListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                });
//
//        selectITemShop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, ShopSelectActivity.class);
//                activity.startActivity(intent);
//            }
//        });
//
//        selectITemAccountType.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, AccountTypeSelectActivity.class);
//                activity.startActivity(intent);
//            }
//        });
//
//        btAccountSave.setOnClickListener(this);
//
//        editTextITemName.isTop(true);
//        llAccountEditWindow.addView(editTextITemName);
//        llAccountEditWindow.addView(editTextITemAccount);
//        editTextITemCreateName.isBottom(true);
//        llAccountEditWindow.addView(editTextITemCreateName);
//
//
//        selectITemShop.isTop(true);
//        llAccountEditWindow.addView(selectITemShop);
//        selectITemAccountType.isBottom(true);
//        llAccountEditWindow.addView(selectITemAccountType);
//
//        editTextITemOrderSize.isTop(true);
//        llAccountEditWindow.addView(editTextITemOrderSize);
//        editTextITemNote.isBottom(true);
//        llAccountEditWindow.addView(editTextITemNote);
//
//
//        if (intentMode == EDIT_MODE) {
//            GetAccountDetail(accountId);
//        }
//
//    }
//
//
//    private void GetAccountDetail(String accountId) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("account_id", accountId);
//        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mAccountDetailAddress, map);
//
//        AsyncUtil.asyncGet(URLStr, new AsyncUtil.SuccessListener() {
//            @Override
//            public void onSuccess(String response) {
//                Gson gson = new Gson();
//                AccountDetailEvent accountDetailEvent = gson.fromJson(response, AccountDetailEvent.class);
//                editTextITemName.SetItemValue(accountDetailEvent.getData().getName());
//                editTextITemAccount.SetItemValue(accountDetailEvent.getData().getAccount());
//                editTextITemCreateName.SetItemValue(accountDetailEvent.getData().getAccount_holder());
//                editTextITemOrderSize.SetItemValue(accountDetailEvent.getData().getDisplay_order());
//                editTextITemNote.SetItemValue(accountDetailEvent.getData().getNote());
//                selectITemShop.SetItemValue(accountDetailEvent.getData().getShop_name());
//                selectITemAccountType.SetItemValue(accountDetailEvent.getData().getType_name());
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
//
//    }
//
//
//    private void AccountEdit(String accountId) {
//        JsonObject jsonObject = new JsonObject();
//        if (intentMode == EDIT_MODE) {
//            jsonObject.addProperty("account_id", Integer.valueOf(accountId));
//        }
//        jsonObject.addProperty("name", editTextITemName.GetItemValue());
//        jsonObject.addProperty("account", editTextITemAccount.GetItemValue());
//        jsonObject.addProperty("account_holder", editTextITemCreateName.GetItemValue());
////        jsonObject.addProperty("type_id", "");
//        jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
////        jsonObject.addProperty("shop_id", "");
//        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
//
//        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mAddAccountAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
//            @Override
//            public void onSuccess(String response) {
//                Gson gson = new Gson();
//                AccountEditEvent accountEditEvent = gson.fromJson(response, AccountEditEvent.class);
//                if (accountEditEvent.getCode() == 0) {
//                    Toast.makeText(mContext, "添加成功", Toast.LENGTH_SHORT).show();
//                    finish();
//                } else {
//                    Toast.makeText(mContext, accountEditEvent.getMsg(), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
//
//
//    }
//
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.bt_account_save:
//                AccountEdit(accountId);
//                break;
//            default:
//                break;
//        }
//    }
//}
