package com.zhimadi.saas.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.SupplierAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.SuppliersEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.LoadFooter;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplierHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<SuppliersEvent.Supplier> suppliers;
    private SwipeMenuListView lvSupplierHome;
    private SupplierAdapter supplierAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        suppliers = new ArrayList<SuppliersEvent.Supplier>();
        lvSupplierHome = (SwipeMenuListView) findViewById(R.id.lv_supplier_home);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_home);
        inte();
        titleBarCommonBuilder.setTitle("供应商管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, SupplierEditActivity.class);
                        intent.putExtra("INTENT_MODE", StoreEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //付款
                SwipeMenuItem payItem = new SwipeMenuItem(mContext);
                payItem.setBackground(new ColorDrawable(Color.rgb(0x48, 0xCF, 0xAD)));
                payItem.setWidth(90);
                payItem.setTitle("付款");
                payItem.setTitleSize(12);
                payItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(payItem);

                //打电话
                SwipeMenuItem callItem = new SwipeMenuItem(mContext);
                callItem.setBackground(new ColorDrawable(Color.rgb(0x6C, 0xE0, 0xC3)));
                callItem.setWidth(90);
                callItem.setIcon(R.drawable.shou_ji);
                menu.addMenuItem(callItem);

                //发短信
                SwipeMenuItem smsItem = new SwipeMenuItem(mContext);
                smsItem.setBackground(new ColorDrawable(Color.rgb(0x8D, 0xEA, 0xD3)));
                smsItem.setWidth(90);
                smsItem.setIcon(R.drawable.duan_xin);
                menu.addMenuItem(smsItem);

                //删除
                SwipeMenuItem deleteItem = new SwipeMenuItem(mContext);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9, 0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };


        lvSupplierHome.setMenuCreator(creator);
        lvSupplierHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, SupplierEditActivity.class);
                intent.putExtra("SUPPLIER_ID", suppliers.get(i).getSupplier_id());
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                SupplierHomeActivity.this.startActivity(intent);
            }
        });

        lvSupplierHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (index == 3) {
                    SuppllierDelete(suppliers.get(position).getSupplier_id());
                }
                return true;
            }
        });

        //加上死锁，没错就是配合动画
        lvSupplierHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetSuppllier();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        supplierAdapter = new SupplierAdapter(mContext, R.layout.item_lv_supplier_home, suppliers);
        lvSupplierHome.setAdapter(supplierAdapter);
        GetSuppllier();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetSuppllier();
    }


    private void GetSuppllier() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mSupplierAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SuppliersEvent suppliersEvent = gson.fromJson(response, SuppliersEvent.class);
                if (mStart == 0) {
                    suppliers.clear();
                }
                mStart = mStart + suppliersEvent.getData().getList().size();
                suppliers.addAll(suppliersEvent.getData().getList());
                supplierAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void SuppllierDelete(String supplierId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("supplier_id", supplierId);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteSupplierAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if(commonResultEvent.getCode() == 0){
                    mStart = 0;
                    GetSuppllier();
                }

            }
            @Override
            public void onFail() {
            }
        });
    }
}
