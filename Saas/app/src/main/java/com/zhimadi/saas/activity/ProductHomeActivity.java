package com.zhimadi.saas.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.ProductAdapter;
import com.zhimadi.saas.event.ProductsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;


    private TitleBarCommonBuilder titleBarCommonBuilder;

//    private EditText etProductHome;

    private List<ProductsEvent.Product> products;
    private ListView lvProductHome;
    private ProductAdapter productAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        products = new ArrayList<ProductsEvent.Product>();
//        etProductHome = (EditText) findViewById(R.id.et_product_home);
        lvProductHome = (ListView) findViewById(R.id.lv_product_home);
        productAdapter = new ProductAdapter(mContext, R.layout.item_lv_product_home, products);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_home);
        inte();
        titleBarCommonBuilder.setTitle("商品管理")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui02)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, ProductEditActivity.class);
                        intent.putExtra("INTENT_MODE", ProductEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        lvProductHome.setAdapter(productAdapter);
        lvProductHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, ProductEditActivity.class);
                intent.putExtra("INTENT_MODE", ProductEditActivity.EDIT_MODE);
                intent.putExtra("PRODUCT_ID", products.get(i).getProduct_id());
                activity.startActivity(intent);
            }
        });


        lvProductHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            GetProduct();
                        }
                        break;
                    default:
                        break;
                }
            }


            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        GetProduct();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetProduct();
    }

    private void GetProduct() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mProductAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();

                ProductsEvent productsEvent = gson.fromJson(response, ProductsEvent.class);
                if (mStart == 0) {
                    products.clear();
                }
                mStart = mStart + productsEvent.getData().getList().size();
                products.addAll(productsEvent.getData().getList());
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {
            }
        });


    }


}
