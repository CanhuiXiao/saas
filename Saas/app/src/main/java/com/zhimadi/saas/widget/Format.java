package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.zhimadi.saas.R;
import java.util.ArrayList;
import java.util.List;

public class Format extends LinearLayout {
//    "product_id":"",
//            "company_id"："1",
//            "category_id": "2",
//            "type_id": 1,
//            "cat_id": 1,
//            "warehouse_id": 1,
//            "name": "奇异果 新西兰奇异果",
//            "description": "新西兰奇异果",
//            "selling_price": 126,
//            "purchase_price": 110,
//            "sku": "a12340",
//            "is_fixed": "0"
//            "fixed_weight": "5.0"
//            "manufacturer_part_number": "xxx",
//            "img_url": "",
//            "mini_num": 10,
//            "quantity": 999,
//            "is_stock_warning": 0,
//            "warn_number_low": 10,
//            "warn_number_top": 200,
//            "status": 0,
//            "is_sell": 0,
//            "display_order": "100",
//            "props": "1:2;2:4",
//            "pic_urls": "/thumbs/0049250.jpg;/thumbs/0049260.jpg"

    private View returnView;
    private TextView tvFormatName;
    private FlowLayout flFormat;

    private List<RadioButton> rbOptions;

    private String attributeOptionId;
    private String attributeOptionValue;

    public Format(Context context, AttributeSet attrs) {
        super(context, attrs);
        returnView = LayoutInflater.from(context).inflate(R.layout.widget_format, this);
        tvFormatName = (TextView) findViewById(R.id.tv_format_name);
        flFormat = (FlowLayout) returnView.findViewById(R.id.fl_format);
        rbOptions = new ArrayList<RadioButton>();
    }

    public String getAttributeOptionId() {
        return attributeOptionId;
    }

    public void setAttributeOptionId(String attributeOptionId) {
        this.attributeOptionId = attributeOptionId;
    }

    public String getAttributeOptionValue() {
        return attributeOptionValue;
    }

    public void setAttributeOptionValue(String attributeOptionValue) {
        this.attributeOptionValue = attributeOptionValue;
    }

    public void SetMarginHorizontal(int marginHorizontal) {
        flFormat.SetMarginHorizontal(marginHorizontal);
    }


    public void SetMarginVertical(int marginVertical) {
        flFormat.SetMarginVertical(marginVertical);
    }

    public void setFormatName(String formatName) {
        tvFormatName.setText(formatName);
    }

    public void addFarmatItem(View view) {
        flFormat.addView(view);
    }

    public void addOption(RadioButton radioButton){
        rbOptions.add(radioButton);
    }

    public void Select(RadioButton radioButton){
        for (int i = 0; i < rbOptions.size(); i++) {
            if(rbOptions.get(i) != radioButton){
                rbOptions.get(i).setChecked(false);
            }
        }
        radioButton.setChecked(true);
    }

}
