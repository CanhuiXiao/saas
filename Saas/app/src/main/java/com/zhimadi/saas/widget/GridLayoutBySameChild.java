package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
public class GridLayoutBySameChild extends ViewGroup {

	private Context mContext;


	private int width = 0;
	private int height = 0;

	private int columnCount = 2;

	private int marginHorizontal = 0;

	private int marginVertical = 0;

	private int rawCount = 0;

	private int childWidth = 0;

	private int childHeight = 0;

	public GridLayoutBySameChild(Context context) {
		super(context);
		mContext = context;
	}

	public GridLayoutBySameChild(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
	}
	
    @Override  
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(mContext, attrs); 
        
    } 

	public void setColumnCount(int columnCount) {
		this.columnCount = columnCount;

	}

	public void setMarginVertical(int marginVertical) {
		this.marginVertical = marginVertical;

	}

	public void setMarginHorizontal(int marginHorizontal) {
		this.marginHorizontal = marginHorizontal;

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		int modeWidth = MeasureSpec.getMode(widthMeasureSpec);
		int modeHeight = MeasureSpec.getMode(heightMeasureSpec);
		int sizeWidth = MeasureSpec.getSize(widthMeasureSpec);
		int sizeHeight = MeasureSpec.getSize(heightMeasureSpec);
		int cCount = getChildCount();

		if(cCount > 0){
			if (cCount % columnCount == 0) {
				rawCount = cCount / columnCount;
			} else {
				rawCount = (cCount / columnCount) + 1;
			}



			//测量所有孩子(必须)
			for (int i = 0; i < cCount; i++) {
				if (!(getChildAt(i) == null)) {
					View child = getChildAt(i);
					measureChild(child, widthMeasureSpec, heightMeasureSpec);
					childWidth = child.getMeasuredWidth();
					childHeight = child.getMeasuredHeight();
				}
			}
			//获取样本




			height = rawCount * childHeight + (rawCount - 1) * marginVertical;
			width = childWidth * columnCount + marginHorizontal * (columnCount - 1);
			setMeasuredDimension((modeHeight == MeasureSpec.EXACTLY) ? sizeWidth : width,(modeHeight == MeasureSpec.EXACTLY) ? sizeHeight : height);
		}else {
			setMeasuredDimension((modeHeight == MeasureSpec.EXACTLY) ? sizeWidth : width,(modeHeight == MeasureSpec.EXACTLY) ? sizeHeight : height);
		}
	}

	
	
	
	
	@Override
	public void addView(View child, int index, LayoutParams params) {
		super.addView(child, index, params);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		 int cCount = getChildCount();
		
		 int left = 0;
		
		 int top = 0;
		
		 for (int i = 0; i < cCount; i++) {
		
		 View child = getChildAt(i);
		
		 if (child.getVisibility() == View.GONE) {
		 continue;
		 }
			 //判断换行
		 if ((i + 1) % columnCount == 1 && i != 0) {
		 left = 0;
		 top += childHeight + marginVertical;
		 }
		
		 int lc = left;
		 int tc = top;
		 int rc = lc + childWidth;
		 int bc = tc + childHeight;
		 child.layout(lc, tc, rc, bc);
		 left += childWidth + marginHorizontal;
		 }
	}
}
