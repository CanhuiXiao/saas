//package com.zhimadi.saas.event;
//
//import java.util.List;
//
///**
// * Created by carry on 2016/7/14.
// */
//public class AccountDetailEvent {
//
//
//    private int code;
//    private String msg;
//    private Data data;
//
//
//    public int getCode() {
//        return code;
//    }
//
//    public void setCode(int code) {
//        this.code = code;
//    }
//
//    public Data getData() {
//        return data;
//    }
//
//    public void setData(Data data) {
//        this.data = data;
//    }
//
//    public String getMsg() {
//        return msg;
//    }
//
//    public void setMsg(String msg) {
//        this.msg = msg;
//    }
//
//    public class Data {
//        private String account_id;
//        private String name;
//        private String account;
//        private String account_holder;
//        private String type_id;
//        private String shop_id;
//        private String display_order;
//        private String note;
//        private String shop_name;
//        private String type_name;
//
//        public String getAccount_id() {
//            return account_id;
//        }
//
//        public void setAccount_id(String account_id) {
//            this.account_id = account_id;
//        }
//
//        public String getName() {
//            return name;
//        }
//
//        public void setName(String name) {
//            this.name = name;
//        }
//
//        public String getAccount() {
//            return account;
//        }
//
//        public void setAccount(String account) {
//            this.account = account;
//        }
//
//        public String getAccount_holder() {
//            return account_holder;
//        }
//
//        public void setAccount_holder(String account_holder) {
//            this.account_holder = account_holder;
//        }
//
//        public String getType_id() {
//            return type_id;
//        }
//
//        public void setType_id(String type_id) {
//            this.type_id = type_id;
//        }
//
//        public String getShop_id() {
//            return shop_id;
//        }
//
//        public void setShop_id(String shop_id) {
//            this.shop_id = shop_id;
//        }
//
//        public String getDisplay_order() {
//            return display_order;
//        }
//
//        public void setDisplay_order(String display_order) {
//            this.display_order = display_order;
//        }
//
//        public String getNote() {
//            return note;
//        }
//
//        public void setNote(String note) {
//            this.note = note;
//        }
//
//        public String getShop_name() {
//            return shop_name;
//        }
//
//        public void setShop_name(String shop_name) {
//            this.shop_name = shop_name;
//        }
//
//        public String getType_name() {
//            return type_name;
//        }
//
//        public void setType_name(String type_name) {
//            this.type_name = type_name;
//        }
//    }
//
//}
