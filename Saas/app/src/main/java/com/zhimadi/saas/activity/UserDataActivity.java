package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.view.ButtonFooter;
import com.zhimadi.saas.view.UserHead2;

public class UserDataActivity extends BaseActivity {

    private LinearLayout llUserData;
    private UserHead2 userHead2;
    private ButtonFooter buttonFooter;

    private void inte() {
        llUserData = (LinearLayout) findViewById(R.id.ll_user_data);
        userHead2 = new UserHead2(mContext, null);
        buttonFooter = new ButtonFooter(mContext, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        inte();

        buttonFooter.setTopMargin(mContext, 38.4f).setText("安全退出").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        ;
        llUserData.addView(userHead2);
        llUserData.addView(buttonFooter);
    }

    private void logout() {
        UserInfo.clear();
    }


}
