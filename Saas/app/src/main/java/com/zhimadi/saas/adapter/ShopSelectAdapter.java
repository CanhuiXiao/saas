package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.BaseActivity;
import com.zhimadi.saas.activity.ShopSelectActivity;
import com.zhimadi.saas.event.ShopsEvent;

import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/13.
 */
public class ShopSelectAdapter extends ArrayAdapter<ShopsEvent.Shop> {

    private ShopSelectActivity mActivity;
    private Context mContext;
    private int resourceId;


    public ShopSelectAdapter(Context context, int resource, List<ShopsEvent.Shop> objects, BaseActivity baseActivity) {
        super(context, resource, objects);
        mContext = context;
        mActivity = (ShopSelectActivity) baseActivity;
        resourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ShopsEvent.Shop shop = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvShopSelect.setText(shop.getName());
        holder.cbShopSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isCheck) {
                if(isCheck){
                    mActivity.select(shop.getShop_id(), shop.getName());
                }
            }
        });

        return convertView;
    }


    class ViewHolder {
        private TextView tvShopSelect;
        private CheckBox cbShopSelect;

        public ViewHolder(View view) {
            tvShopSelect = (TextView) view.findViewById(R.id.tv_shop_select);
            cbShopSelect = (CheckBox) view.findViewById(R.id.cb_shop_select);
            view.setTag(this);
        }
    }

}
