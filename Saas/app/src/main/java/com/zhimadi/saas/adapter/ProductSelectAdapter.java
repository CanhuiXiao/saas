package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.ProductSelectActivity;
import com.zhimadi.saas.event.ProductsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */

//没挂图片
public class ProductSelectAdapter extends ArrayAdapter<ProductsEvent.Product> {

    private Context mContext;
    private int resourceId;
    private String unit;
    private ProductSelectActivity productSelectActivity;

    private List<String> mProductIds;

    private ImageView ivProductHomePic;

    private TextView tvProductHomeName;
    private TextView tvProductHomeFormat;

    private TextView tvProductHomePrice;

    private TextView tvProductHomeSku;
    private TextView tvProductHomeWeight;
    private TextView tvProductHomeQuantity;

    private CheckBox cbProductSelect;

    public ProductSelectAdapter(Context context, int resource, List<ProductsEvent.Product> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        productSelectActivity = (ProductSelectActivity) activity;
        resourceId = resource;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ProductsEvent.Product product = getItem(position);
        convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
        //根据isSelect属性判断打勾，所以在构成数据的时候就要对其进行处理
        ivProductHomePic = (ImageView) convertView.findViewById(R.id.iv_product_home_pic);
        tvProductHomeName = (TextView) convertView.findViewById(R.id.tv_product_home_name);
        tvProductHomeFormat = (TextView) convertView.findViewById(R.id.tv_product_home_format);
        tvProductHomePrice = (TextView) convertView.findViewById(R.id.tv_product_home_price);
        tvProductHomeSku = (TextView) convertView.findViewById(R.id.tv_product_home_sku);
        tvProductHomeQuantity = (TextView) convertView.findViewById(R.id.tv_product_home_quantity);
        tvProductHomeWeight = (TextView) convertView.findViewById(R.id.tv_product_home_weight);
        cbProductSelect = (CheckBox) convertView.findViewById(R.id.cb_product_select);
        cbProductSelect.setChecked(product.isSelect());

        //1定装，0非定装
        if (product.getIs_fixed().equals("0")) {
            tvProductHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.shan, 0, 0, 0);
            unit = "/公斤";
        } else if (product.getIs_fixed().equals("1")) {
            tvProductHomeName.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ding, 0, 0, 0);
            unit = "/件";
        }

        tvProductHomeName.setText(product.getType_name());
        tvProductHomeFormat.setText(product.getProps_name());

        tvProductHomePrice.setText("￥" + product.getSelling_price() + unit);

        tvProductHomeSku.setText("货号: " + product.getSku());
        tvProductHomeQuantity.setText("数量: " + product.getQuantity() + " 件");
        tvProductHomeWeight.setText("重量: " + product.getWeight() + " 公斤");
        cbProductSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                productSelectActivity.select(product.getProduct_id(), product.getIs_fixed(), product.getProps_name(), product.getType_name(), isChecked, Float.valueOf(product.getFixed_weight()));
                if (isChecked) {
                    getItem(position).setSelect(true);
                } else {
                    getItem(position).setSelect(false);
                }
            }
        });
        return convertView;
    }

}
