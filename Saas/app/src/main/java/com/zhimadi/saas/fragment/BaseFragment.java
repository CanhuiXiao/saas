package com.zhimadi.saas.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by carry on 2016/7/8.
 */

public abstract class BaseFragment extends Fragment{

    protected Activity activity;


    // 常量封装
    public static final String NON = "0";
    public static final String FirstTAG = "1";
    public static final String SecondTAG = "2";
    public static final String ThirdTAG = "3";
    public static final String FouthTAG = "4";

    public String tag = NON;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
    }

    protected  void Intent2Activity(Class<? extends Activity> tarActivity) {
        Intent intent = new Intent(getActivity(), tarActivity);
        startActivity(intent);
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
