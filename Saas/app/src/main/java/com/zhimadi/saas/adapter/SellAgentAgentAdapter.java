package com.zhimadi.saas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.event.SellAgentAgentsEvent;
import com.zhimadi.saas.event.SellAgentAgentsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class SellAgentAgentAdapter extends ArrayAdapter<SellAgentAgentsEvent.SellAgentAgent> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals("5")) {
            holder.tvBuyHomestate.setText("已代卖");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }  else if(state.equals("4")){
            holder.tvBuyHomestate.setText("待确认");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if(state.equals("0")){
            holder.tvBuyHomestate.setText("代卖中");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if(state.equals("1")){
            holder.tvBuyHomestate.setText("审核中");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public SellAgentAgentAdapter(Context context, int resource, List<SellAgentAgentsEvent.SellAgentAgent> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SellAgentAgentsEvent.SellAgentAgent sellAgentAgent = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        showStateText(holder, sellAgentAgent.getState());
        holder.tvBuyHomeAgent.setText(sellAgentAgent.getAgent_name());
        holder.tvBuyHomeOderNumber.setText(sellAgentAgent.getOrder_no());
        holder.tvBuyHomeToPay.setText("￥" + sellAgentAgent.getTotal_amount());
        holder.tvBuyHomeCreateDate.setText(sellAgentAgent.getCreate_time());
        holder.tvBuyHomeStore.setText(sellAgentAgent.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {
        TextView tvBuyHomeAgent;
        TextView tvBuyHomestate;
        TextView tvBuyHomeOderNumber;
        TextView tvBuyHomeToPay;
        TextView tvBuyHomeCreateDate;
        TextView tvBuyHomeStore;

        public ViewHolder(View view) {
            tvBuyHomeAgent = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_agent);
            tvBuyHomestate = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_state);
            tvBuyHomeOderNumber = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_oder_number);
            tvBuyHomeToPay = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_to_pay);
            tvBuyHomeCreateDate = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_create_date);
            tvBuyHomeStore = (TextView) view.findViewById(R.id.tv_sell_agent_agent_home_store);
            view.setTag(this);
        }
    }


}
