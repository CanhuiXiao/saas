package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.OwnerAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.OwnersEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OwnerHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private SwipeMenuListView lvOwnerHome;
    private List<OwnersEvent.Owner> owners;
    private OwnerAdapter ownerAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        owners = new ArrayList<OwnersEvent.Owner>();
        lvOwnerHome = (SwipeMenuListView) findViewById(R.id.lv_owner_home);
        ownerAdapter = new OwnerAdapter(mContext, R.layout.item_lv_owner_home, owners);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_home);
        inte();
        titleBarCommonBuilder.setTitle("代卖货主管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, OwnerEditActivity.class);
                        intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //付款
                SwipeMenuItem payItem = new SwipeMenuItem(
                        activity);
                payItem.setBackground(new ColorDrawable(Color.rgb(0x48,
                        0xCF, 0xAD)));
                payItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                payItem.setTitle("付款");
                payItem.setTitleSize(12);
                payItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(payItem);

                //打电话
                SwipeMenuItem callItem = new SwipeMenuItem(
                        activity);
                callItem.setBackground(new ColorDrawable(Color.rgb(0x6C, 0xE0,
                        0xC3)));
                callItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                callItem.setIcon(R.drawable.shou_ji);
                menu.addMenuItem(callItem);

                //发短信
                SwipeMenuItem smsItem = new SwipeMenuItem(
                        activity);
                smsItem.setBackground(new ColorDrawable(Color.rgb(0x8D, 0xEA,
                        0xD3)));
                smsItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                smsItem.setIcon(R.drawable.duan_xin);
                menu.addMenuItem(smsItem);

                //删除
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };

        lvOwnerHome.setMenuCreator(creator);
        lvOwnerHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (index == 3) {
                    OwnerDelete(owners.get(position).getOwner_id());
                }

                return false;
            }
        });

        lvOwnerHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, OwnerEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.EDIT_MODE);
                intent.putExtra("OWNER_ID", owners.get(i).getOwner_id());
                startActivity(intent);
            }
        });

        lvOwnerHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetOwner();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvOwnerHome.setMenuCreator(creator);
        lvOwnerHome.setAdapter(ownerAdapter);
        GetOwner();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetOwner();
    }

    private void GetOwner() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mOwnerAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                OwnersEvent ownersEvent = gson.fromJson(response, OwnersEvent.class);
                if (mStart == 0) {
                    owners.clear();
                }
                mStart = mStart + ownersEvent.getData().getList().size();
                owners.addAll(ownersEvent.getData().getList());
                ownerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });


    }


    private void OwnerDelete(String ownerId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("owner_id", ownerId);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteOwnerAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                mStart = 0;
                GetOwner();
            }

            @Override
            public void onFail() {

            }
        });

    }


}
