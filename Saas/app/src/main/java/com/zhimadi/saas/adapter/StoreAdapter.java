package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.StoresEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class StoreAdapter extends ArrayAdapter<StoresEvent.Store>{
    
    private Context mContext;
    private int resourceId;

    public StoreAdapter(Context context, int resource, List<StoresEvent.Store> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StoresEvent.Store store  = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvStoreHomeName.setText(store.getName());
        holder.tvStoreHomeShop.setText("所属门店 : " + store.getShop_name());
        holder.tvStoreHomeManeger.setText("负责人 : " + store.getCharge_user_name());
        return convertView;
    }



    class ViewHolder {
        private TextView tvStoreHomeName;
        private TextView tvStoreHomeShop;
        private TextView tvStoreHomeManeger;
        
        public ViewHolder(View view) {
            tvStoreHomeName = (TextView) view.findViewById(R.id.tv_store_home_name);
            tvStoreHomeShop = (TextView) view.findViewById(R.id.tv_store_home_shop);
            tvStoreHomeManeger = (TextView) view.findViewById(R.id.tv_store_home_maneger);
            view.setTag(this);
        }
    }
}
