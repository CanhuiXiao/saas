//package com.zhimadi.saas.activity;
//
//import android.os.Bundle;
//import android.view.View;
//import android.widget.ListView;
//import android.widget.Toast;
//
//import com.google.gson.Gson;
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.adapter.AccountTypeAdapter;
//import com.zhimadi.saas.event.AccountTypesEvent;
//import com.zhimadi.saas.util.OkHttpClientManager;
//import com.zhimadi.saas.util.OkHttpUtil;
//import com.zhimadi.saas.widget.TitleBarCommonBuilder;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import okhttp3.Request;
//
//public class AccountTypeSelectActivity extends BaseActivity {
//
//    private TitleBarCommonBuilder titleBarCommonBuilder;
//    private ListView lvAccountTypeHome;
//    private List<AccountTypesEvent.AccountType> accountTypes;
//    private AccountTypeAdapter accountTypeAdapter;
//
//    private void inte() {
//        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
//        accountTypes = new ArrayList<AccountTypesEvent.AccountType>();
//        lvAccountTypeHome = (ListView) findViewById(R.id.lv_account_type_home);
//        accountTypeAdapter = new AccountTypeAdapter(mContext, R.layout.item_lv_account_home, accountTypes);
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_account_type_select);
//        inte();
//        titleBarCommonBuilder.setTitle("选择账户类型")
//                .setLeftImageRes(R.drawable.fan_hui)
//                .setLeftText("返回")
//                .setLeftOnClikListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                });
//        GetType();
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        GetType();
//    }
//
//    private void GetType(){
//        Map<String, String> map = new HashMap<String, String>();
//        Request.Builder builder = new Request.Builder()
//                .url(OkHttpUtil.mAccountTypeAddress);
//        builder = OkHttpUtil.RequestAssembly(builder);
//        OkHttpClientManager.getAsyn(builder.build(), new OkHttpClientManager.ResultCallback<String>() {
//            @Override
//            public void onError(Request request, Exception e) {
//
//            }
//
//            @Override
//            public void onResponse(String jsonStr) {
//                Toast.makeText(mContext, jsonStr, Toast.LENGTH_SHORT).show();
//                Gson gson = new Gson();
//                AccountTypesEvent accountTypesEvent
//                 = gson.fromJson(jsonStr, AccountTypesEvent.class);
//                accountTypes.clear();
//                accountTypes.addAll(accountTypesEvent.getData().getList());
//                accountTypeAdapter = new AccountTypeAdapter(mContext, R.layout.item_lv_account_home, accountTypes);
//                lvAccountTypeHome.setAdapter(accountTypeAdapter);
//            }
//        });
//    }
//
//
//
//
//}
