package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.BuysEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class BuyAdapter extends ArrayAdapter<BuysEvent.Buy> {

    private Context mContext;
    private int resourceId;


    //选择状态标志
    private void showStateText(ViewHolder holder, String state) {
        if (state.equals(Constant.STATE_WAIT_CHECK+"")) {
            holder.tvBuyHomestate.setText("待审核");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_2);
        } else if (state.equals(Constant.STATE_DEFAULT+"")) {
            holder.tvBuyHomestate.setText("已采购");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_NOTE+"")) {
            holder.tvBuyHomestate.setText("草稿"+"");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        } else if (state.equals(Constant.STATE_CANCEL)) {
            holder.tvBuyHomestate.setText("撤销"+"");
            holder.tvBuyHomestate.setBackgroundResource(R.drawable.shape_bt_buy_1);
        }
    }

    public BuyAdapter(Context context, int resource, List<BuysEvent.Buy> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BuysEvent.Buy buy = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvBuyHomeSupplier.setText(buy.getSupplier_name());
        showStateText(holder, buy.getState());
        holder.tvBuyHomeOderNumber.setText(buy.getOrder_no());
        holder.tvBuyHomeToPay.setText("￥" + buy.getTotal_amount());
        holder.tvBuyHomeCreateDate.setText(buy.getCreate_time());
        holder.tvBuyHomeStore.setText(buy.getWarehouse_name());
        return convertView;
    }

    class ViewHolder {
        TextView tvBuyHomeSupplier;
        TextView tvBuyHomestate;
        TextView tvBuyHomeOderNumber;
        TextView tvBuyHomeToPay;
        TextView tvBuyHomeCreateDate;
        TextView tvBuyHomeStore;

        public ViewHolder(View view) {
            tvBuyHomeSupplier = (TextView) view.findViewById(R.id.tv_buy_home_supplier);
            tvBuyHomestate = (TextView) view.findViewById(R.id.tv_buy_home_state);
            tvBuyHomeOderNumber = (TextView) view.findViewById(R.id.tv_buy_home_oder_number);
            tvBuyHomeToPay = (TextView) view.findViewById(R.id.tv_buy_home_to_pay);
            tvBuyHomeCreateDate = (TextView) view.findViewById(R.id.tv_buy_home_create_date);
            tvBuyHomeStore = (TextView) view.findViewById(R.id.tv_buy_home_store);
            view.setTag(this);
        }
    }


}
