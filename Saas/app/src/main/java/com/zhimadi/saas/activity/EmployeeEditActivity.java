package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.EmployeeDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.Role;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class EmployeeEditActivity extends BaseActivity implements View.OnClickListener {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private int intentMode;

    private LinearLayout llEmployeeEditWindow;

    private EditTextITem editTextITemPhone;
    private EditTextITem editTextITemName;
    private SwitchItem switchITem;
    private SelelctableTextViewItem selelctableTextViewItemShop;
    private EditTextITem editTextITemNote;
    private Role role;
    private Button btShopSave;

    private String employeeId;
    private String shopId;


    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", Constant.DEFAULT_MODE);
        if (intentMode == Constant.EDIT_MODE) {
            employeeId = getIntent().getStringExtra("EMPLOYEE_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);

        llEmployeeEditWindow = (LinearLayout) findViewById(R.id.ll_employee_edit_window);

        editTextITemPhone = new EditTextITem(mContext, null, "账号", "请输入员工手机号码");
        editTextITemName = new EditTextITem(mContext, null, "姓名", "请输入员工姓名");
        switchITem = new SwitchItem(mContext, null, "账号是否启动", true);

        selelctableTextViewItemShop = new SelelctableTextViewItem(mContext, null, "门店", "请选择门店");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        role = new Role(mContext, null);
        btShopSave = (Button) findViewById(R.id.bt_employee_edit_save);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_edit);
        inte();
        if (intentMode == Constant.EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑员工");
            btShopSave.setText("确定");
        } else {
            titleBarCommonBuilder.setTitle("新增员工");
            btShopSave.setText("绑定");
        }

        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        selelctableTextViewItemShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, ShopSelectActivity.class);
                startActivityForResult(intent, Constant.REQUEST_CODE_FOR_SHOP);
            }
        });


        editTextITemPhone.isTop(true);
        editTextITemPhone.isNotNull(true);
        llEmployeeEditWindow.addView(editTextITemPhone);
        llEmployeeEditWindow.addView(editTextITemName);
        switchITem.isBottom(true);
        llEmployeeEditWindow.addView(switchITem);

        selelctableTextViewItemShop.isTop(true);
        llEmployeeEditWindow.addView(selelctableTextViewItemShop);
        editTextITemNote.isBottom(true);
        llEmployeeEditWindow.addView(editTextITemNote);

        llEmployeeEditWindow.addView(role);
        btShopSave.setOnClickListener(this);

        if (intentMode == Constant.EDIT_MODE) {
            getEmployeeDetail();
        }
    }

    private void getEmployeeDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("user_id", employeeId);
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mUserDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                EmployeeDetailEvent employeeDetailEvent = gson.fromJson(response, EmployeeDetailEvent.class);
                editTextITemPhone.SetItemValue(employeeDetailEvent.getData().getAccount());
                editTextITemName.SetItemValue(employeeDetailEvent.getData().getName());
                switchITem.SetCheck(employeeDetailEvent.getData().getState());
                shopId = employeeDetailEvent.getData().getShop_id();
                selelctableTextViewItemShop.SetItemValue(employeeDetailEvent.getData().getShop_name());
                editTextITemNote.SetItemValue(employeeDetailEvent.getData().getNote());
                showRole(employeeDetailEvent.getData().getRole_id().split(","));
            }

            @Override
            public void onFail() {

            }
        });
    }

    private void EmployeeEdit() {
        JsonObject jsonObject = new JsonObject();
        if (employeeId != null && !employeeId.equals("")) {
            jsonObject.addProperty("user_id", Integer.valueOf(employeeId));
        }
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
//        jsonObject.addProperty("account", editTextITemPhone.GetItemValue());
        jsonObject.addProperty("state", switchITem.IsCheck());
        if(shopId != null){
            jsonObject.addProperty("shop_id", shopId);
        }
        String roleStr = role.getRoleStr();
        if(roleStr != null && !roleStr.equals("")){
            jsonObject.addProperty("role_id", roleStr );
        }
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditUserAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                finish();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void showRole(String[] itemIds) {
        for (int i = 0; i < itemIds.length; i++) {
            role.CheckItem(Integer.valueOf(itemIds[i]));
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_FOR_SHOP:
                if(resultCode == ShopSelectActivity.RESULT_CODE_SUCCESS){
                    shopId = data.getStringExtra("SHOP_ID");
                    selelctableTextViewItemShop.SetItemValue(data.getStringExtra("SHOP_NAME"));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_employee_edit_save:
                EmployeeEdit();
                break;
            default:
                break;
        }
    }
}
