package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;

import java.util.List;


public class PicUpdate extends LinearLayout {

    private View returnView;
    private Context mcContext;

    private LinearLayout llCatWindow;
    private TextView tvCatAdd;
    private TextView tvCatEdit;

    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_cat, this);
        llCatWindow = (LinearLayout) findViewById(R.id.ll_cat_window);
        tvCatAdd = (TextView) returnView.findViewById(R.id.tv_cat_add);
        tvCatEdit = (TextView) returnView.findViewById(R.id.tv_cat_edit);

    }

    public PicUpdate(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcContext = context;
        inte();
    }

    public void setOnClickListenerAdd(OnClickListener onClickListener) {
        tvCatAdd.setOnClickListener(onClickListener);
    }

    public void setOnClickListenerEdit(OnClickListener onClickListener) {
        tvCatEdit.setOnClickListener(onClickListener);
    }

    public void addView(View view) {
        llCatWindow.addView(view);
    }

    public void removeView(View view) {
        llCatWindow.removeView(view);
    }

    public void removeAllViews(List<String> productIds) {
        for (int i = 0; i < productIds.size(); i++) {
            for (int k = 0; k < llCatWindow.getChildCount(); k++) {
                if(((CatItem)llCatWindow.getChildAt(k)).getProductId().equals(productIds.get(i))){
                    llCatWindow.removeViewAt(k);
                }
            }
        }
    }

}
