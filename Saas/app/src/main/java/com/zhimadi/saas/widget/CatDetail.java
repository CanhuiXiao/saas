package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zhimadi.saas.R;

public class CatDetail extends LinearLayout {

	private View returnView;
	private Context mcContext;

	private LinearLayout llCatWindow;

	private void inte(){
		returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_cat_detail, this);
		llCatWindow = (LinearLayout) findViewById(R.id.ll_cat_window);
	}

	public CatDetail(Context context, AttributeSet attrs) {
		super(context, attrs);
		mcContext = context;
		inte();
	}


	public void addView(View view){
		llCatWindow.addView(view);
	}

	public void removeView(View view){
		llCatWindow.removeView(view);
	}
}
