package com.zhimadi.saas.event;

/**
 * Created by carry on 2016/7/21.
 */
public class CustomTypeDetailEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String type_id;
        private String name;
        private String display_order;
        private String have_discount;
        private String have_price;
        private String note;

        public String getType_id() {
            return type_id;
        }

        public void setType_id(String type_id) {
            this.type_id = type_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }

        public String getHave_discount() {
            return have_discount;
        }

        public void setHave_discount(String have_discount) {
            this.have_discount = have_discount;
        }

        public String getHave_price() {
            return have_price;
        }

        public void setHave_price(String have_price) {
            this.have_price = have_price;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }

}
