package com.zhimadi.saas.event;

import java.util.List;

/**
 * Created by carry on 2016/7/14.
 */
public class OwnersEvent {


    private int code;
    private String msg;
    private Data data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Owner> list;


        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Owner> getList() {
            return list;
        }

        public void setList(List<Owner> list) {
            this.list = list;
        }
    }

    public class Owner {

        private String owner_id;
        private String name;
        private String charge_man;
        private String state;

        public String getOwner_id() {
            return owner_id;
        }

        public void setOwner_id(String owner_id) {
            this.owner_id = owner_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCharge_man() {
            return charge_man;
        }

        public void setCharge_man(String charge_man) {
            this.charge_man = charge_man;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }

}
