package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.DefinedCategorySelectAdapter;
import com.zhimadi.saas.event.DefinedCategoriesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefinedCategorySelectActivity extends BaseActivity {

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_Fail = 0;

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private ListView lvDefinedCategoryHome;
    private List<DefinedCategoriesEvent.DefinedCategory> definedCategories;
    private DefinedCategorySelectAdapter definedCategorySelectAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        definedCategories = new ArrayList<DefinedCategoriesEvent.DefinedCategory>();
        lvDefinedCategoryHome = (ListView) findViewById(R.id.lv_defined_category_select);
        definedCategorySelectAdapter = new DefinedCategorySelectAdapter(mContext, R.layout.item_lv_defined_category_select, definedCategories, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defined_category_select);
        inte();
        titleBarCommonBuilder.setTitle("店铺商品分类")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightText("添加分类")
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, DefinedCategoryEditActivity.class);
                        intent.putExtra("INTENT_MODE", DefinedCategoryEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });


        lvDefinedCategoryHome.setAdapter(definedCategorySelectAdapter);

        GetDefinedCategory();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        GetDefinedCategory();
    }


    private int start = 0;
    private int limit = 10;

    private void GetDefinedCategory() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start",start+"");
        map.put("limit",limit+"");
        AsyncUtil.UrlKeyAssembly(OkHttpUtil.mSellerCatsAddress, map);
        AsyncUtil.asyncGet(mContext, OkHttpUtil.mSellerCatsAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                DefinedCategoriesEvent definedCategoriesEvent = gson.fromJson(response, DefinedCategoriesEvent.class);
                definedCategories.clear();
                definedCategories.addAll(definedCategoriesEvent.getData().getList());
                definedCategorySelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }

    //注入门店id和门店名字
    public void select(String definedCategoryId, String definedCategoryName) {
        Intent intent = new Intent();
        intent.putExtra("DEFINED_NAME", definedCategoryName);
        intent.putExtra("DEFINED_ID", definedCategoryId);
        setResult(RESULT_CODE_SUCCESS, intent);
        Toast.makeText(mContext, definedCategoryId+":"+definedCategoryName, Toast.LENGTH_SHORT).show();
        finish();
    }



}
