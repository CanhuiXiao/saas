package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.constant.UserInfo;
import com.zhimadi.saas.event.UserEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.EncryptUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.util.PhoneDataUtil;

public class LoginHomeActivity extends BaseActivity implements View.OnClickListener {


    private EditText etLoginHomeAccount;
    private EditText etLoginHomePassword;
    private TextView tvLoginHomeRegister;
    private TextView tvLoginHomeForget;
    private Button btLoginHomeLand;

    private void inte() {
        etLoginHomeAccount = (EditText) findViewById(R.id.et_login_home_account);
        etLoginHomePassword = (EditText) findViewById(R.id.et_login_home_password);
        tvLoginHomeRegister = (TextView) findViewById(R.id.tv_login_home_register);
        tvLoginHomeForget = (TextView) findViewById(R.id.tv_login_home_forget);
        btLoginHomeLand = (Button) findViewById(R.id.bt_login_home_land);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_home);
        inte();
        etLoginHomeAccount.setText("13672478266");
        etLoginHomePassword.setText("111111");
//        13672478266
        tvLoginHomeRegister.setOnClickListener(this);
        tvLoginHomeForget.setOnClickListener(this);
        btLoginHomeLand.setOnClickListener(this);
    }

    private void Land() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("account", etLoginHomeAccount.getText().toString());
        jsonObject.addProperty("password", EncryptUtil.PasswordEncryptUtil(etLoginHomePassword.getText().toString()));
        jsonObject.addProperty("salt", "12345");
        AsyncUtil.Land(mContext, OkHttpUtil.mLandAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                String iw = response;
                UserEvent userEvent = gson.fromJson(response, UserEvent.class);
                UserInfo.setmIMEI(PhoneDataUtil.GetIMEI(mContext));
                UserInfo.setmAuth(userEvent.getData().getAuth());
                UserInfo.setmRoleId(userEvent.getData().getRole_id());
                UserInfo.setmCompanyId(userEvent.getData().getCompany_id());
                UserInfo.setmCompanyName(userEvent.getData().getCompany_name());
                UserInfo.setmUserName(userEvent.getData().getName());
                UserInfo.setmPhone(userEvent.getData().getPhone());
                setResult(Constant.RESULT_CODE_SUCCESS);
                finish();
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_login_home_land:
                Land();
                break;
            case R.id.tv_login_home_register:
                Intent intent = new Intent();
                intent.setClass(mContext, LoginRegisterActivity.class);
                startActivity(intent);
                break;
            default:
        }
    }

}
