package com.zhimadi.saas.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.zhimadi.saas.R;

public class TitleBarCommon extends LinearLayout {

	
	
	private View returnView;

	
	public TitleBarCommon(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		returnView = LayoutInflater.from(context).inflate(R.layout.component_titlebar_common, this);
		
	}

}
