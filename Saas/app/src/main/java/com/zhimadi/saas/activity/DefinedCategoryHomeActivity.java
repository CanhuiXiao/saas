package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.DefinedCategoryAdapter;
import com.zhimadi.saas.event.CommonResultEvent;
import com.zhimadi.saas.event.DefinedCategoriesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefinedCategoryHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private SwipeMenuListView lvDefinedCategoryHome;
    private List<DefinedCategoriesEvent.DefinedCategory> definedCategories;
    private DefinedCategoryAdapter definedCategoryAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);

        definedCategories = new ArrayList<DefinedCategoriesEvent.DefinedCategory>();
        lvDefinedCategoryHome = (SwipeMenuListView) findViewById(R.id.lv_defined_category_home);
        definedCategoryAdapter = new DefinedCategoryAdapter(mContext, R.layout.item_lv_defined_categogry_home, definedCategories);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defined_category_home);
        inte();
        titleBarCommonBuilder.setTitle("店铺商品分类管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightText("添加分类")
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, DefinedCategoryEditActivity.class);
                        intent.putExtra("INTENT_MODE", DefinedCategoryEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //删除
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(90);
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };

        lvDefinedCategoryHome.setMenuCreator(creator);
        lvDefinedCategoryHome.setAdapter(definedCategoryAdapter);
        lvDefinedCategoryHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if(index == 0){
                    DefinedCategoryDelete(definedCategories.get(position).getCat_id());
                }
                return true;
            }
        });

        lvDefinedCategoryHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, DefinedCategoryEditActivity.class);
                intent.putExtra("INTENT_MODE", DefinedCategoryEditActivity.EDIT_MODE);
                intent.putExtra("DEFINED_CATEGORY_ID", definedCategories.get(i).getCat_id());
                startActivity(intent);
            }
        });
        lvDefinedCategoryHome.setMenuCreator(creator);
        lvDefinedCategoryHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetDefinedCategory();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        GetDefinedCategory();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetDefinedCategory();
    }

    private void GetDefinedCategory(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart+"");
        map.put("limit", mLimit+"");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mSellerCatsAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                DefinedCategoriesEvent definedCategoriesEvent = gson.fromJson(response, DefinedCategoriesEvent.class);
                if(mStart == 0){
                    definedCategories.clear();
                }
                mStart = mStart + definedCategoriesEvent.getData().getList().size();
                definedCategories.addAll(definedCategoriesEvent.getData().getList());
                definedCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void DefinedCategoryDelete(String definedCategoryId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("cat_id", definedCategoryId);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteSellerCatsAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
                if(commonResultEvent.getCode() == 0){
                    mStart = 0;
                    GetDefinedCategory();
                }
            }
            @Override
            public void onFail() {

            }
        });

    }

}
