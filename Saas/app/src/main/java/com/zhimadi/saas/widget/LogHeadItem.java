package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class LogHeadItem extends LinearLayout {

    private View returnView;
    private Context mcContext;
    private TextView tvLogHeadTitle;
    private TextView tvLogHeadValue;

    private void inte() {
        returnView = LayoutInflater.from(mcContext).inflate(R.layout.widget_log_head_item, this);
        tvLogHeadTitle= (TextView) findViewById(R.id.tv_log_head_title);
        tvLogHeadValue= (TextView) findViewById(R.id.tv_log_head_value);
    }

    public LogHeadItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        mcContext = context;
        inte();
    }

    public LogHeadItem(Context context, AttributeSet attrs, String title) {
        super(context, attrs);
        mcContext = context;
        inte();
        tvLogHeadTitle.setText(title);
    }

    public LogHeadItem(Context context, AttributeSet attrs, String title, String value) {
        super(context, attrs);
        mcContext = context;
        inte();
        tvLogHeadTitle.setText(title);
        tvLogHeadValue.setText(value);
    }

    public void setTitle(String title){
        tvLogHeadTitle.setText(title);
    }

    public void setValue(String value){
        tvLogHeadValue.setText(value);
    }

    public void setTitleDrawerble(){

    }
}
