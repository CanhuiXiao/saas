package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.CustomDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.EditTextWithUnitItem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class CustomEditActivity extends BaseActivity implements View.OnClickListener {

    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    public static final int REQUEST_CODE_FOR_AREA = 1;
    public static final int REQUEST_CODE_FOR_TYPE = 2;
    /**
     * 打开模式
     */
    private int intentMode;

    private String customId;
    private String customTypeId = "";
    private String areaCode = "";

    private TitleBarCommonBuilder titleBarCommonBuilder;

    private LinearLayout llCustomEditWindow;

    private EditTextITem editTextITemName;

    private EditTextITem editTextITemRisk;

    private EditTextITem editTextITemPhone;
    private EditTextITem editTextITemTel;
    private EditTextITem editTextITemEMail;
    private EditTextITem editTextITemFax;
    private EditTextITem editTextITemWebAddress;

    private EditTextITem editTextITemAddress2;
    private EditTextITem editTextITemOrderSize;
    private EditTextITem editTextITemNote;

    private SwitchItem switchITem;

    private EditTextWithUnitItem editTextWithUnitItemCycle;
    private SelelctableTextViewItem selelctableTextViewItemType;
    private SelelctableTextViewItem selelctableTextViewItemArea;

    private View viewLoadMore;
    private TextView tvLoadMore;
    private Button btCustomSave;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            customId = getIntent().getStringExtra("CUSTOM_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);

        llCustomEditWindow = (LinearLayout) findViewById(R.id.ll_custom_edit_window);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入客户名称");
        editTextITemRisk = new EditTextITem(mContext, null, "风险金额", "请输入客户风险金额");
        editTextITemPhone = new EditTextITem(mContext, null, "手机", "请输入客户手机");
        editTextITemTel = new EditTextITem(mContext, null, "电话", "请输入客户电话");
        editTextITemEMail = new EditTextITem(mContext, null, "邮箱", "请输入客户邮箱");
        editTextITemFax = new EditTextITem(mContext, null, "传真", "请输入传真号码");
        editTextITemWebAddress = new EditTextITem(mContext, null, "网址", "请输入公司网址");
        editTextITemAddress2 = new EditTextITem(mContext, null, "详细地址", "请输入详细地址");

        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        switchITem = new SwitchItem(mContext, null, "启动", true);
        editTextWithUnitItemCycle = new EditTextWithUnitItem(mContext, null, "结算周期", "请输入客户结算周期", "天");
        selelctableTextViewItemType = new SelelctableTextViewItem(mContext, null, "分类", "请选择客户分类");
        selelctableTextViewItemArea = new SelelctableTextViewItem(mContext, null, "所在地区", "请选择所在地区");

        viewLoadMore = LayoutInflater.from(mContext).inflate(R.layout.view_load_more, null);
        tvLoadMore = (TextView) viewLoadMore.findViewById(R.id.tv_load_more);

        btCustomSave = (Button) findViewById(R.id.bt_custom_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_edit);
        inte();

        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑客户");
        } else {
            titleBarCommonBuilder.setTitle("新增客户");
        }

        titleBarCommonBuilder.setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        selelctableTextViewItemType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        selelctableTextViewItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, AreaSelectActivity.class);
                startActivityForResult(intent, REQUEST_CODE_FOR_AREA);
            }
        });

        editTextITemName.isTop(true).isNotNull(true);
        llCustomEditWindow.addView(editTextITemName);
        llCustomEditWindow.addView(selelctableTextViewItemType);
        switchITem.isBottom(true);
        llCustomEditWindow.addView(switchITem);

        editTextWithUnitItemCycle.isTop(true).setVisibility(View.GONE);
        llCustomEditWindow.addView(editTextWithUnitItemCycle);
        editTextITemRisk.isBottom(true).setVisibility(View.GONE);
        llCustomEditWindow.addView(editTextITemRisk);

        editTextITemPhone.isTop(true).isBottom(true);
        llCustomEditWindow.addView(editTextITemPhone);

        tvLoadMore.setOnClickListener(this);
        llCustomEditWindow.addView(viewLoadMore);

        btCustomSave.setOnClickListener(this);
        if (intentMode == EDIT_MODE) {
            GetCustomDetail();
        }else {
            editTextITemOrderSize.SetItemValue("100");
        }


    }


    private void LoadMore() {
        llCustomEditWindow.removeView(viewLoadMore);
        editTextWithUnitItemCycle.setVisibility(View.VISIBLE);
        editTextITemRisk.setVisibility(View.VISIBLE);
        editTextITemPhone.isBottom(false);

        llCustomEditWindow.addView(editTextITemTel);
        llCustomEditWindow.addView(editTextITemEMail);
        llCustomEditWindow.addView(editTextITemFax);
        editTextITemWebAddress.isBottom(true);
        llCustomEditWindow.addView(editTextITemWebAddress);

        selelctableTextViewItemArea.isTop(true);
        llCustomEditWindow.addView(selelctableTextViewItemArea);

        editTextITemAddress2.isBottom(true);
        llCustomEditWindow.addView(editTextITemAddress2);

        editTextITemOrderSize.isTop(true).isNotNull(true);
        llCustomEditWindow.addView(editTextITemOrderSize);
        editTextITemNote.isBottom(true);
        llCustomEditWindow.addView(editTextITemNote);
    }


    private void GetCustomDetail() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("custom_id", customId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mCustomDetailAddress, map);

        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                CustomDetailEvent customDetailEvent = gson.fromJson(response, CustomDetailEvent.class);
                editTextITemName.SetItemValue(customDetailEvent.getData().getName());
                selelctableTextViewItemType.SetItemValue(customDetailEvent.getData().getType_name());
                switchITem.SetCheck(customDetailEvent.getData().getState());

                editTextWithUnitItemCycle.SetItemValue(customDetailEvent.getData().getBill_cycle_val());
                editTextITemRisk.SetItemValue(customDetailEvent.getData().getRisk_amount());

                editTextITemPhone.SetItemValue(customDetailEvent.getData().getPhone());
                editTextITemTel.SetItemValue(customDetailEvent.getData().getTel());
                editTextITemEMail.SetItemValue(customDetailEvent.getData().getEmail());
                editTextITemFax.SetItemValue(customDetailEvent.getData().getFax());
                editTextITemWebAddress.SetItemValue(customDetailEvent.getData().getWebsite());

                selelctableTextViewItemArea.SetItemValue(customDetailEvent.getData().getArea_name());
                editTextITemAddress2.SetItemValue(customDetailEvent.getData().getAddress());

                editTextITemOrderSize.SetItemValue(customDetailEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(customDetailEvent.getData().getNote());

                customTypeId = customDetailEvent.getData().getType_id();
                areaCode = customDetailEvent.getData().getArea_id();
            }

            @Override
            public void onFail() {

            }
        });

    }


    private void CustomEdit() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == EDIT_MODE) {
            jsonObject.addProperty("custom_id", customId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        if (!customTypeId.equals("")) {
            jsonObject.addProperty("type_id", customTypeId);
            jsonObject.addProperty("type_name", selelctableTextViewItemType.GetItemValue());
        }
        jsonObject.addProperty("state", switchITem.IsCheck());

        if (!editTextWithUnitItemCycle.GetItemValue().equals("")) {
            jsonObject.addProperty("bill_cycle_val", editTextWithUnitItemCycle.GetItemValue());
            jsonObject.addProperty("bill_cycle_unit", "1");
        }

        if (!editTextITemRisk.GetItemValue().equals("")) {
            jsonObject.addProperty("risk_amount", editTextITemRisk.GetItemValue());
        }

        jsonObject.addProperty("tel", editTextITemTel.GetItemValue());
        jsonObject.addProperty("phone", editTextITemPhone.GetItemValue());
        jsonObject.addProperty("fax", editTextITemFax.GetItemValue());
        jsonObject.addProperty("email", editTextITemEMail.GetItemValue());
        jsonObject.addProperty("website", editTextITemWebAddress.GetItemValue());
        if (!areaCode.equals("")) {
            jsonObject.addProperty("area_id", areaCode);
            jsonObject.addProperty("area_name", selelctableTextViewItemArea.GetItemValue());
        }
        jsonObject.addProperty("address", editTextITemAddress2.GetItemValue());
        jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());



        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditCustomAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                if (intentMode == ADD_MODE) {
                    Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                    finish();
                } else if (intentMode == EDIT_MODE) {
                    Toast.makeText(mContext, "修改成功！", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFail() {

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_FOR_TYPE:
                break;
            case REQUEST_CODE_FOR_AREA:
                if (resultCode == AreaSelectActivity.RESULT_CODE_SUCCESS) {
                    selelctableTextViewItemArea.SetItemValue(data.getStringExtra("AREA_NAME"));
                    areaCode = data.getStringExtra("AREA_CODE");
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_load_more:
                LoadMore();
                break;
            case R.id.bt_custom_save:
                CustomEdit();
                break;
            default:
                break;
        }
    }
}
