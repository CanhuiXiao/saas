package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.StoreSelectActivity;
import com.zhimadi.saas.activity.SupplierSelectActivity;
import com.zhimadi.saas.event.StoresEvent;
import com.zhimadi.saas.event.SuppliersEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class SupplierSelectAdapter extends ArrayAdapter<SuppliersEvent.Supplier>{

    private SupplierSelectActivity mActivity;
    private Context mContext;
    private View returnView;
    private int resourceId;

    private TextView tvSupplierSelectName;
    private TextView tvSupplierSelectDept;
    private TextView tvSupplierSelectManager;
    private CheckBox cbSupplierSelect;


    public SupplierSelectAdapter(Context context, int resource, List<SuppliersEvent.Supplier> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
        mActivity = (SupplierSelectActivity)activity;
    }


    private void inte(){
        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
        tvSupplierSelectName = (TextView) returnView.findViewById(R.id.tv_supplier_select_name);
        tvSupplierSelectManager = (TextView) returnView.findViewById(R.id.tv_supplier_select_manager);
        tvSupplierSelectDept = (TextView) returnView.findViewById(R.id.tv_supplier_select_dept);
        cbSupplierSelect = (CheckBox) returnView.findViewById(R.id.cb_supplier_select);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final SuppliersEvent.Supplier supplier  = getItem(position);
        inte();
        tvSupplierSelectName.setText(supplier.getName());
        tvSupplierSelectDept.setText("供应商欠款 : " + supplier.getAmount_topay());
        tvSupplierSelectManager.setText("负责人 : " + supplier.getCharge_man());
        cbSupplierSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mActivity.select(supplier.getSupplier_id(), supplier.getName(), supplier.getAmount_topay());
                }
            }
        });
        return returnView;
    }
}
