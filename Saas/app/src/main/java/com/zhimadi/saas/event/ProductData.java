package com.zhimadi.saas.event;

import java.io.Serializable;

/**
 * Created by carry on 2016/8/10.
 */
public class ProductData implements Serializable {
    private String productId;
    private String isFit;
    private String format;
    private String name;
    private float fitWeight;

    public ProductData(String productId, String isFit, String format, String name, float fitWeight) {
        this.productId = productId;
        this.isFit = isFit;
        this.format = format;
        this.name = name;
        this.fitWeight = fitWeight;
    }


    public float getFitWeight() {
        return fitWeight;
    }

    public void setFitWeight(float fitWeight) {
        this.fitWeight = fitWeight;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getIsFit() {
        return isFit;
    }

    public void setIsFit(String isFit) {
        this.isFit = isFit;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
