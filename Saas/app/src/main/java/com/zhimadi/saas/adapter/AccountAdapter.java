//package com.zhimadi.saas.adapter;
//
//import android.app.Activity;
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.event.AccountsEvent;
//import com.zhimadi.saas.event.ShopsEvent;
//
//import java.util.List;
//
///**
// * Created by carry on 2016/7/13.
// */
//public class AccountAdapter extends ArrayAdapter<AccountsEvent.Account>{
//
//    private Activity mActivity;
//    private Context mContext;
//    private View returnView;
//    private int resourceId;
//
//    private TextView tvAccountHomeName;
//    private TextView tvAccountHomeShop;
//    private TextView tvAccounthomehold;
//
//    public AccountAdapter(Context context, int resource, List<AccountsEvent.Account> objects) {
//        super(context, resource, objects);
//        mContext = context;
//        resourceId = resource;
//
//
//
//
//    }
//
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        AccountsEvent.Account account = getItem(position);
//        returnView = LayoutInflater.from(mContext).inflate(resourceId, null);
//        tvAccountHomeName = (TextView) returnView.findViewById(R.id.tv_account_home_name);
//        tvAccountHomeShop = (TextView) returnView.findViewById(R.id.tv_account_home_shop);
//        tvAccounthomehold = (TextView) returnView.findViewById(R.id.tv_account_home_hold);
//        tvAccountHomeName.setText(account.getName());
//        tvAccountHomeShop.setText(account.getShop_name());
//        tvAccounthomehold.setText(account.getBalance());
//        return returnView;
//    }
//}
