package com.zhimadi.saas.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.MediaController;

import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.MessageAdapter;
import com.zhimadi.saas.constant.MessageList;
import com.zhimadi.saas.event.MessagesEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */

public class MessageFragment extends BaseFragment {

    private View returnView;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<MessageList.Message> messages;
    private ListView lvMessageHome;
    private MessageAdapter messageAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(returnView);
        messages = new ArrayList<MessageList.Message>();
        lvMessageHome = (ListView) returnView.findViewById(R.id.lv_message_home);
        messageAdapter = new MessageAdapter(activity, R.layout.item_lv_message_home, messages);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_message, null);
        inte();
        titleBarCommonBuilder.setTitle("消息");
        messages.add(MessageList.Message.getMessage(1));
        messages.add(MessageList.Message.getMessage(2));
        messages.add(MessageList.Message.getMessage(3));
        messages.add(MessageList.Message.getMessage(4));
        messages.add(MessageList.Message.getMessage(5));
        lvMessageHome.setAdapter(messageAdapter);
        getMessage();
        return returnView;
    }


    private void getMessage() {
        AsyncUtil.asyncGet(activity, OkHttpUtil.mMessageNotReadAddress, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                MessagesEvent messagesEvent = gson.fromJson(response, MessagesEvent.class);
                for (int i = 0; i < messages.size(); i++) {
                    for (int k = 0; k < messagesEvent.getData().size(); k++) {
                        try {
                            if (messages.get(i).getType().equals(messagesEvent.getData().get(k).getType())) {
                                messages.get(i).setCount(messagesEvent.getData().get(k).getCount());
                            }
                        } catch (Exception e) {

                        }
                    }
                }
                messageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


}