package com.zhimadi.saas.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.zhimadi.saas.R;

public class SelelctableTextViewItem extends LinearLayout {

    private View returnView;
    private TextView tvItemValue;
    private TextView tvItemName;
    private TextView tvItemNotNull;
    private View viewDivider;
    private View viewDividerFirst;
    private View viewDividerLast;
    private ImageView ivItemGo;

    public SelelctableTextViewItem(Context context, AttributeSet attrs, String itemName, String hint) {
        super(context, attrs);
        returnView = LayoutInflater.from(context).inflate(R.layout.widget_selectable_textview, this);
        tvItemValue = (TextView) returnView.findViewById(R.id.tv_item_value);
        tvItemName = (TextView) returnView.findViewById(R.id.tv_item_name);
        tvItemNotNull = (TextView) returnView.findViewById(R.id.tv_item_not_null);
        viewDivider = returnView.findViewById(R.id.view_divider);
        viewDividerFirst = returnView.findViewById(R.id.view_divider_first);
        viewDividerLast = returnView.findViewById(R.id.view_divider_last);
        ivItemGo = (ImageView) returnView.findViewById(R.id.iv_item_go);

        tvItemName.setText(itemName);
        tvItemValue.setHint(hint);
    }

    public SelelctableTextViewItem isTop(boolean isTop) {
        if (isTop) {
            viewDividerFirst.setVisibility(View.VISIBLE);
        } else {
            viewDividerFirst.setVisibility(View.GONE);
        }
        return this;
    }

    public SelelctableTextViewItem isBottom(boolean isBottom) {
        if (isBottom) {
            viewDividerLast.setVisibility(View.VISIBLE);
            viewDivider.setVisibility(View.GONE);
        } else {
            viewDividerLast.setVisibility(View.GONE);
            viewDivider.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public SelelctableTextViewItem isNotNull(boolean isNotNull){
        if(isNotNull){
            tvItemNotNull.setVisibility(View.VISIBLE);
        }else {
            tvItemNotNull.setVisibility(View.GONE);
        }
        return this;
    }

    public SelelctableTextViewItem SetItemValue(String value) {
        tvItemValue.setText(value);
        return this;
    }

    public String GetItemValue() {
        return tvItemValue.getText().toString();
    }

    public SelelctableTextViewItem SetItemHint(String hint) {
        tvItemValue.setHint(hint);
        return this;
    }

    public SelelctableTextViewItem SetItemName(String name) {
        tvItemName.setText(name);
        return this;
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        if (enabled) {
            ivItemGo.setVisibility(View.VISIBLE);
        } else {
            ivItemGo.setVisibility(View.INVISIBLE);
        }
    }
}
