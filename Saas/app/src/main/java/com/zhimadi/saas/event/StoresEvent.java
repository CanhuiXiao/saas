package com.zhimadi.saas.event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/14.
 */
public class StoresEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String count;
        private List<Store> list;

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public List<Store> getList() {
            return list;
        }

        public void setList(List<Store> list) {
            this.list = list;
        }
    }

    public class Store implements Serializable {
        private String warehouse_id;
        private String shop_id;
        private String name;
        private String display_order;
        private String charge_user_id;
        private String state;
        private String shop_name;
        private String charge_user_name;

        public String getCharge_user_id() {
            return charge_user_id;
        }

        public void setCharge_user_id(String charge_user_id) {
            this.charge_user_id = charge_user_id;
        }

        public String getCharge_user_name() {
            return charge_user_name;
        }

        public void setCharge_user_name(String charge_user_name) {
            this.charge_user_name = charge_user_name;
        }

        public String getDisplay_order() {
            return display_order;
        }

        public void setDisplay_order(String display_order) {
            this.display_order = display_order;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getShop_id() {
            return shop_id;
        }

        public void setShop_id(String shop_id) {
            this.shop_id = shop_id;
        }

        public String getShop_name() {
            return shop_name;
        }

        public void setShop_name(String shop_name) {
            this.shop_name = shop_name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getWarehouse_id() {
            return warehouse_id;
        }

        public void setWarehouse_id(String warehouse_id) {
            this.warehouse_id = warehouse_id;
        }
    }


}
