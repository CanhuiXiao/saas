package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.tv.TvContentRating;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.PaymentTypesEvent;
import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class PaymentTypeAdapter extends ArrayAdapter<PaymentTypesEvent.PaymentType>{

    private Context mContext;
    private int resourceId;

    public PaymentTypeAdapter(Context context, int resource, List<PaymentTypesEvent.PaymentType> objects) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PaymentTypesEvent.PaymentType paymentType = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvPaymentTypeHomeName.setText(paymentType.getName());
        holder.tvPaymentTypeHomeOrderSize.setText("排序 : " + paymentType.getDisplay_order());
        holder.tvPaymentTypeHomeNote.setText(paymentType.getNote());
        return convertView;

    }

    class ViewHolder {
        private TextView tvPaymentTypeHomeName;
        private TextView tvPaymentTypeHomeOrderSize;
        private TextView tvPaymentTypeHomeNote;

        public ViewHolder(View view) {
            tvPaymentTypeHomeName = (TextView) view.findViewById(R.id.tv_payment_type_home_name);
            tvPaymentTypeHomeOrderSize = (TextView) view.findViewById(R.id.tv_payment_type_home_order_size);
            tvPaymentTypeHomeNote = (TextView) view.findViewById(R.id.tv_payment_type_home_note);
            view.setTag(this);
        }
    }
}
