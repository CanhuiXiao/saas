package com.zhimadi.saas.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.StockAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.StocksAgentEvent;
import com.zhimadi.saas.event.StocksSellEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.LogHead;
import com.zhimadi.saas.widget.LogHeadItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockHomeActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<StocksSellEvent.Stock> stocks;
    private ListView lvStockHome;
    private StockAdapter stockAdapter;
    private LogHead logHead;
    private LogHeadItem logHeadItemNumber;
    private LogHeadItem logHeadItemWeight;
    private LogHeadItem logHeadItemMoney;
    private LogHeadItem logHeadItemLowStock;
    private RadioButton rbStockSell;
    private RadioButton rbStockAgent;
    private View viewStockSell;
    private View viewStockAgent;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        stocks = new ArrayList<StocksSellEvent.Stock>();
        lvStockHome = (ListView) findViewById(R.id.lv_stock_home);
        stockAdapter = new StockAdapter(mContext, R.layout.item_lv_stock_home, stocks);
        logHead = (LogHead) findViewById(R.id.lg_stock_home);
        logHeadItemNumber = new LogHeadItem(mContext, null, "总数量(件)");
        logHeadItemWeight = new LogHeadItem(mContext, null, "总重量(公斤)");
        logHeadItemMoney = new LogHeadItem(mContext, null, "库存金额(元)");
        logHeadItemLowStock = new LogHeadItem(mContext, null, "预警数目");
        rbStockSell = (RadioButton) findViewById(R.id.rb_stock_sell);
        rbStockAgent = (RadioButton) findViewById(R.id.rb_stock_agent);
        viewStockSell = findViewById(R.id.view_stock_sell);
        viewStockAgent = findViewById(R.id.view_stock_agent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_home);
        inte();
        titleBarCommonBuilder.setTitle("库存查询")
                .setLeftText("返回")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        logHead.addItem(logHeadItemNumber);
        logHead.addItem(logHeadItemWeight);
        logHead.addItem(logHeadItemMoney);
        logHead.addItem(logHeadItemLowStock);

        rbStockAgent.setOnCheckedChangeListener(this);
        rbStockSell.setOnCheckedChangeListener(this);

        lvStockHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, StockEditActivity.class);
//                intent.putExtra("INTENT_MODE", StockEditActivity.EDIT_MODE);
//                intent.putExtra("PRODUCT_ID", stocks.get(i).getStock_id());
//                activity.startActivity(intent);
            }
        });


        lvStockHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() == view.getCount() - 1) {
                            //加载更多功能的代码
                            getStock();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        lvStockHome.setAdapter(stockAdapter);
        getStock();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        getStock();
    }


    private int isSell = Constant.AGENT;

    private void getStock() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        map.put("is_sell", isSell + "");
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mStockAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                if(isSell == Constant.SELL){
                    StocksSellEvent stocksEvent = gson.fromJson(response, StocksSellEvent.class);
                    logHeadItemNumber.setValue(stocksEvent.getData().getTotal_stock());
                    logHeadItemWeight.setValue(stocksEvent.getData().getTotal_weight());
                    logHeadItemMoney.setValue(stocksEvent.getData().getTotal_money());
                    logHeadItemLowStock.setValue(stocksEvent.getData().getLow_stock());
                    if (mStart == 0) {
                        stocks.clear();
                    }
                    mStart = mStart + stocksEvent.getData().getList().size();
                    stocks.addAll(stocksEvent.getData().getList());
                    stockAdapter.notifyDataSetChanged();
                }else if(isSell == Constant.AGENT) {
                    StocksAgentEvent stocksAgentEvent = gson.fromJson(response, StocksAgentEvent.class);
                    logHeadItemNumber.setValue(stocksAgentEvent.getData().getTotal_stock());
                    logHeadItemWeight.setValue(stocksAgentEvent.getData().getTotal_weight());
                    logHeadItemMoney.setValue(stocksAgentEvent.getData().getTotal_money());
                    logHeadItemLowStock.setValue(stocksAgentEvent.getData().getLow_stock());
                    if (mStart == 0) {
                        stocks.clear();
                    }
                    mStart = mStart + stocksAgentEvent.getData().getList().size();
                    stockAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFail() {
            }
        });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked){
            switch (buttonView.getId()){
                case R.id.rb_stock_sell:
                    rbStockAgent.setChecked(false);
                    viewStockSell.setVisibility(View.VISIBLE);
                    viewStockAgent.setVisibility(View.INVISIBLE);
                    rbStockAgent.setBackgroundResource(R.color.colorNull);
                    rbStockSell.setBackgroundResource(R.color.colorWhite);
                    mStart = 0;
                    isSell = Constant.SELL;
                    getStock();
                    break;
                case R.id.rb_stock_agent:
                    rbStockSell.setChecked(false);
                    viewStockAgent.setVisibility(View.VISIBLE);
                    viewStockSell.setVisibility(View.INVISIBLE);
                    rbStockAgent.setBackgroundResource(R.color.colorWhite);
                    rbStockSell.setBackgroundResource(R.color.colorNull);
                    mStart = 0;
                    isSell = Constant.AGENT;
                    getStock();
                    break;
            }
        }
    }
}
