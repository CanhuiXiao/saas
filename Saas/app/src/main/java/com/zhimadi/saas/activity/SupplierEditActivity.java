package com.zhimadi.saas.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.event.SupplierDetailEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.EditTextITem;
import com.zhimadi.saas.widget.EditTextWithUnitItem;
import com.zhimadi.saas.widget.SelelctableTextViewItem;
import com.zhimadi.saas.widget.SwitchItem;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.HashMap;
import java.util.Map;

public class SupplierEditActivity extends BaseActivity implements View.OnClickListener {

    /**
     * 打开模式
     */
    public static final int ADD_MODE = 1;
    public static final int EDIT_MODE = 2;

    //请求码
    public static final int requestCodeForEmployee = 1;
    public static final int requestCodeForArea = 2;


    private int intentMode;

    private String supplierId;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private LinearLayout llSupplierEditWindow;

    private EditTextITem editTextITemName;
    private SelelctableTextViewItem selelctableTextViewItemManager;
    private SwitchItem switchItem;

    private EditTextWithUnitItem editTextWithUnitItemCycle;

    private EditTextITem editTextITemPhone;
    private EditTextITem editTextITemTel;
    private EditTextITem editTextITemMail;
    private EditTextITem editTextITemFax;
    private EditTextITem editTextITemWebAddress;

    private SelelctableTextViewItem selelctableTextViewItemArea;
    private EditTextITem editTextITemAddress;

    private EditTextITem editTextITemBank;
    private EditTextITem editTextITemBankCardNumber;
    private EditTextITem editTextITemBankCreateName;
    private EditTextITem editTextITemOrderSize;
    private EditTextITem editTextITemNote;


    private View viewLoadMore;
    private TextView tvLoadMore;
    private Button btSupplierSave;

    private int areaCode = 0;

    private void inte() {
        intentMode = getIntent().getIntExtra("INTENT_MODE", 0);
        if (intentMode == EDIT_MODE) {
            supplierId = getIntent().getStringExtra("SUPPLIER_ID");
        }
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        llSupplierEditWindow = (LinearLayout) findViewById(R.id.ll_supplier_edit_window);

        editTextITemName = new EditTextITem(mContext, null, "名称", "请输入供应商名称");
        selelctableTextViewItemManager = new SelelctableTextViewItem(mContext, null, "负责人", "请选择负责人");
        switchItem = new SwitchItem(mContext, null, "启动", true);

        editTextWithUnitItemCycle = new EditTextWithUnitItem(mContext, null, "结算周期", "请输入客户结算周期", "天");

        editTextITemPhone = new EditTextITem(mContext, null, "手机", "请输入客户手机");
        editTextITemTel = new EditTextITem(mContext, null, "电话", "请输入客户电话");
        editTextITemMail = new EditTextITem(mContext, null, "邮箱", "请输入客户邮箱");
        editTextITemFax = new EditTextITem(mContext, null, "传真", "请输入传真号码");
        editTextITemWebAddress = new EditTextITem(mContext, null, "网址", "请输入公司网址");

        selelctableTextViewItemArea = new SelelctableTextViewItem(mContext, null, "所在地区", "请选择所在地区");
        editTextITemAddress = new EditTextITem(mContext, null, "详细地址", "请输入详细地址");

        editTextITemBank = new EditTextITem(mContext, null, "开户银行", "请输入开户银行");
        editTextITemBankCreateName = new EditTextITem(mContext, null, "银行账户", "请输入银行账户");
        editTextITemBankCardNumber = new EditTextITem(mContext, null, "银行账号", "请输入银行帐号");

        editTextITemOrderSize = new EditTextITem(mContext, null, "排序", "由0开始，数字越小越靠前");
        editTextITemNote = new EditTextITem(mContext, null, "备注", "请输入备注信息");

        //查看更多
        viewLoadMore = LayoutInflater.from(mContext).inflate(R.layout.view_load_more, null);
        //这里添加点击事件
        tvLoadMore = (TextView) viewLoadMore.findViewById(R.id.tv_load_more);

        btSupplierSave = (Button) findViewById(R.id.bt_supplier_save);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_edit);
        inte();

        selelctableTextViewItemManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, EmployeeSelectActivity.class);
                startActivityForResult(intent, requestCodeForEmployee);
            }
        });
        selelctableTextViewItemArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, AreaSelectActivity.class);
                startActivityForResult(intent, requestCodeForArea);
            }
        });

        editTextITemName.isTop(true);
        editTextITemName.isNotNull(true);
        llSupplierEditWindow.addView(editTextITemName);
        llSupplierEditWindow.addView(selelctableTextViewItemManager);
        switchItem.isBottom(true);
        llSupplierEditWindow.addView(switchItem);
        editTextITemOrderSize.SetItemValue("100");

        if (intentMode == EDIT_MODE) {
            titleBarCommonBuilder.setTitle("编辑供应商");
        } else if (intentMode == ADD_MODE) {
            titleBarCommonBuilder.setTitle("新增供应商");
        } else {
            finish();
        }

        titleBarCommonBuilder
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
        tvLoadMore.setOnClickListener(this);
        llSupplierEditWindow.addView(viewLoadMore);
        btSupplierSave.setOnClickListener(this);
        if (intentMode == EDIT_MODE) {
            GetSupplierDetail(supplierId);
        }

    }


    private void LoadMore() {
        llSupplierEditWindow.removeView(viewLoadMore);
        editTextWithUnitItemCycle.isTop(true).isBottom(true);
        llSupplierEditWindow.addView(editTextWithUnitItemCycle);
        editTextITemPhone.isTop(true);
        llSupplierEditWindow.addView(editTextITemPhone);
        llSupplierEditWindow.addView(editTextITemTel);
        llSupplierEditWindow.addView(editTextITemMail);
        llSupplierEditWindow.addView(editTextITemFax);
        editTextITemWebAddress.isBottom(true);
        llSupplierEditWindow.addView(editTextITemWebAddress);

        selelctableTextViewItemArea.isTop(true);
        llSupplierEditWindow.addView(selelctableTextViewItemArea);
        editTextITemAddress.isBottom(true);
        llSupplierEditWindow.addView(editTextITemAddress);

        editTextITemBank.isTop(true);
        llSupplierEditWindow.addView(editTextITemBank);
        llSupplierEditWindow.addView(editTextITemBankCreateName);
        editTextITemBankCardNumber.isBottom(true);
        llSupplierEditWindow.addView(editTextITemBankCardNumber);

        editTextITemOrderSize.isTop(true).isNotNull(true);
        llSupplierEditWindow.addView(editTextITemOrderSize);
        editTextITemNote.isBottom(true);
        llSupplierEditWindow.addView(editTextITemNote);
    }

    private void GetSupplierDetail(String supplierId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("supplier_id", supplierId);
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mSupplierDetailAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                SupplierDetailEvent supplierDetailEvent = gson.fromJson(response, SupplierDetailEvent.class);
                editTextITemName.SetItemValue(supplierDetailEvent.getData().getName());
                selelctableTextViewItemManager.SetItemValue(supplierDetailEvent.getData().getCharge_man());
                switchItem.SetCheck(supplierDetailEvent.getData().getState());

                editTextWithUnitItemCycle.SetItemValue(supplierDetailEvent.getData().getBill_cycle_val());

                editTextITemPhone.SetItemValue(supplierDetailEvent.getData().getPhone());
                editTextITemTel.SetItemValue(supplierDetailEvent.getData().getTel());
                editTextITemMail.SetItemValue(supplierDetailEvent.getData().getEmail());
                editTextITemFax.SetItemValue(supplierDetailEvent.getData().getFax());
                editTextITemWebAddress.SetItemValue(supplierDetailEvent.getData().getWebsite());

                areaCode = Integer.valueOf(supplierDetailEvent.getData().getArea_id());
                editTextITemAddress.SetItemValue(supplierDetailEvent.getData().getAddress());

                editTextITemBank.SetItemValue(supplierDetailEvent.getData().getBank_name());
                editTextITemBankCardNumber.SetItemValue(supplierDetailEvent.getData().getBank_account());
                editTextITemBankCreateName.SetItemValue(supplierDetailEvent.getData().getBank_username());

                editTextITemOrderSize.SetItemValue(supplierDetailEvent.getData().getDisplay_order());
                editTextITemNote.SetItemValue(supplierDetailEvent.getData().getNote());
            }

            @Override
            public void onFail() {

            }
        });
    }


    private void SupplierEdit() {
        JsonObject jsonObject = new JsonObject();
        if (intentMode == EDIT_MODE) {
            jsonObject.addProperty("supplier_id", supplierId);
        }
        jsonObject.addProperty("name", editTextITemName.GetItemValue());
        jsonObject.addProperty("charge_man", selelctableTextViewItemManager.GetItemValue());
        jsonObject.addProperty("state", switchItem.IsCheck());

        jsonObject.addProperty("bill_cycle_val", editTextWithUnitItemCycle.GetItemValue());
        //结算周期单位：1：天;2:周;3:月;4:年
        jsonObject.addProperty("bill_cycle_unit", "1");

        jsonObject.addProperty("phone", editTextITemPhone.GetItemValue());
        jsonObject.addProperty("tel", editTextITemTel.GetItemValue());
        jsonObject.addProperty("email", editTextITemMail.GetItemValue());
        jsonObject.addProperty("fax", editTextITemFax.GetItemValue());
        jsonObject.addProperty("website", editTextITemWebAddress.GetItemValue());

        if(areaCode != 0){
            jsonObject.addProperty("area_id", areaCode);
        }
        jsonObject.addProperty("address", editTextITemAddress.GetItemValue());

        jsonObject.addProperty("bank_name", editTextITemBank.GetItemValue());
        jsonObject.addProperty("bank_account", editTextITemBankCardNumber.GetItemValue());
        jsonObject.addProperty("bank_username", editTextITemBankCreateName.GetItemValue());

        jsonObject.addProperty("display_order", editTextITemOrderSize.GetItemValue());
        jsonObject.addProperty("note", editTextITemNote.GetItemValue());

        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mEditSupplierAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                    if(intentMode == ADD_MODE){
                        Toast.makeText(mContext, "添加成功！", Toast.LENGTH_SHORT).show();
                        finish();
                    } else if (intentMode == EDIT_MODE) {
                        Toast.makeText(mContext, "编辑成功！", Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFail() {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case requestCodeForEmployee:
                if(resultCode == EmployeeSelectActivity.RESULT_CODE_SUCCESS){
                    selelctableTextViewItemManager.SetItemValue(data.getStringExtra("EMPLOYEE_NAME"));
                }
                break;
            case requestCodeForArea:
                if(resultCode == AreaSelectActivity.RESULT_CODE_SUCCESS){
                    areaCode = data.getIntExtra("AREA_CODE", 0);
                    selelctableTextViewItemArea.SetItemValue(data.getStringExtra("AREA_NAME"));
                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_load_more:
                LoadMore();
                break;
            case R.id.bt_supplier_save:
                SupplierEdit();
                break;
            default:
                break;
        }
    }
}
