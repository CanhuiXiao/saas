package com.zhimadi.saas.activity;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.EmployeeAdapter;
import com.zhimadi.saas.adapter.EmployeeSelectAdapter;
import com.zhimadi.saas.constant.Constant;
import com.zhimadi.saas.event.EmployeesEvent;
import com.zhimadi.saas.fragment.BaseFragment;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by carry on 2016/7/8.
 */

public class EmployeeSelectActivity extends BaseActivity {

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<EmployeesEvent.Employee> employees;
    private ListView lvEmployeeSelect;
    private EmployeeSelectAdapter employeeSelectAdapter;

    private int mStart = 0;
    private int mLimit = 10;

    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_Fail = 0;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        employees = new ArrayList<EmployeesEvent.Employee>();
        lvEmployeeSelect = (ListView) findViewById(R.id.lv_employee_select);
        employeeSelectAdapter = new EmployeeSelectAdapter(mContext, R.layout.item_lv_employee_select, employees, this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_select);
        inte();
        titleBarCommonBuilder.setTitle("选择负责人").setLeftImageRes(R.drawable.fan_hui02).setLeftOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02).setRightOnClikListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext, EmployeeEditActivity.class);
                intent.putExtra("INTENT_MODE", Constant.ADD_MODE);
                startActivity(intent);
            }
        });
        lvEmployeeSelect.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetEmployees();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        lvEmployeeSelect.setAdapter(employeeSelectAdapter);
        GetEmployees();
    }


    private void GetEmployees() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = AsyncUtil.UrlKeyAssembly(OkHttpUtil.mUserAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                EmployeesEvent employeesEvent = gson.fromJson(response, EmployeesEvent.class);
                if (mStart == 0) {
                    employees.clear();
                }
                mStart = mStart + employeesEvent.getData().getList().size();
                employees.addAll(employeesEvent.getData().getList());
                employeeSelectAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });
    }


}