package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.DefinedCategorySelectActivity;
import com.zhimadi.saas.activity.SupplierSelectActivity;
import com.zhimadi.saas.event.DefinedCategoriesEvent;
import com.zhimadi.saas.event.SuppliersEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class DefinedCategorySelectAdapter extends ArrayAdapter<DefinedCategoriesEvent.DefinedCategory> {

    private DefinedCategorySelectActivity mActivity;
    private Context mContext;
    private int resourceId;

    public DefinedCategorySelectAdapter(Context context, int resource, List<DefinedCategoriesEvent.DefinedCategory> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
        mActivity = (DefinedCategorySelectActivity) activity;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final DefinedCategoriesEvent.DefinedCategory definedCategory = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(resourceId, null);
            new ViewHolder(convertView);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.tvDefinedCategorySelect.setText(definedCategory.getName());
        holder.cbDefinedCategorySelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mActivity.select(definedCategory.getCat_id(), definedCategory.getName());
                }
            }
        });
        return convertView;
    }


    class ViewHolder {

        private TextView tvDefinedCategorySelect;
        private CheckBox cbDefinedCategorySelect;

        public ViewHolder(View view) {
            tvDefinedCategorySelect = (TextView) view.findViewById(R.id.tv_defined_category_select);
            cbDefinedCategorySelect = (CheckBox) view.findViewById(R.id.cb_defined_category_select);
            view.setTag(this);
        }
    }


}
