package com.zhimadi.saas.event;

import com.google.gson.annotations.SerializedName;
import com.zhimadi.saas.R;

import java.util.List;

/**
 * Created by carry on 2016/7/21.
 */
public class SellDetailEvent {


    private int code;
    private String msg;
    private Data data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public class Data {
        private String sell_id;
        private String create_user_id;
        private String create_time;
        private String custom_id;
        private String warehouse_id;
        private String total_amount;
        private String received_amount;
        private String note;
        private String state;
        private String order_no;
        private String total_package;
        private String total_weight;
        private String total_net_weight;
        private String warehouse_name;
        private String custom_name;
        private String total_owed;
        private String create_tim ;
        private String create_user_name;
        private String[] actions;
        private List<Product> products;
        private String owed_amount;

        public String getOwed_amount() {
            return owed_amount;
        }

        public void setOwed_amount(String owed_amount) {
            this.owed_amount = owed_amount;
        }

        public String getSell_id() {
            return sell_id;
        }

        public void setSell_id(String sell_id) {
            this.sell_id = sell_id;
        }

        public String getCreate_user_id() {
            return create_user_id;
        }

        public void setCreate_user_id(String create_user_id) {
            this.create_user_id = create_user_id;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getCustom_id() {
            return custom_id;
        }

        public void setCustom_id(String custom_id) {
            this.custom_id = custom_id;
        }

        public String getWarehouse_id() {
            return warehouse_id;
        }

        public void setWarehouse_id(String warehouse_id) {
            this.warehouse_id = warehouse_id;
        }

        public String getTotal_amount() {
            return total_amount;
        }

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public String getReceived_amount() {
            return received_amount;
        }

        public void setReceived_amount(String received_amount) {
            this.received_amount = received_amount;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getTotal_package() {
            return total_package;
        }

        public void setTotal_package(String total_package) {
            this.total_package = total_package;
        }

        public String getTotal_weight() {
            return total_weight;
        }

        public void setTotal_weight(String total_weight) {
            this.total_weight = total_weight;
        }

        public String getTotal_net_weight() {
            return total_net_weight;
        }

        public void setTotal_net_weight(String total_net_weight) {
            this.total_net_weight = total_net_weight;
        }

        public String getWarehouse_name() {
            return warehouse_name;
        }

        public void setWarehouse_name(String warehouse_name) {
            this.warehouse_name = warehouse_name;
        }

        public String getCustom_name() {
            return custom_name;
        }

        public void setCustom_name(String custom_name) {
            this.custom_name = custom_name;
        }

        public String getTotal_owed() {
            return total_owed;
        }

        public void setTotal_owed(String total_owed) {
            this.total_owed = total_owed;
        }

        public String getCreate_tim() {
            return create_tim;
        }

        public void setCreate_tim(String create_tim) {
            this.create_tim = create_tim;
        }

        public String getCreate_user_name() {
            return create_user_name;
        }

        public void setCreate_user_name(String create_user_name) {
            this.create_user_name = create_user_name;
        }

        public String[] getActions() {
            return actions;
        }

        public void setActions(String[] actions) {
            this.actions = actions;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }
    }

    public class Product {

        private String id;
        private String sell_id;
        private String product_id;
        private String weight;
        @SerializedName("package")
        private String package_;
        private String price;
        private String price_unit;
        private String quantity;
        private String is_fixed;
        private String fixed_weight;
        private String name;
        private String props_name;
        private String order_no;
        private String agent_sell_id;
        private String tare;
        private String owner_commission;
        private String custom_commission;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getPackage_() {
            return package_;
        }

        public void setPackage_(String package_) {
            this.package_ = package_;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getPrice_unit() {
            return price_unit;
        }

        public void setPrice_unit(String price_unit) {
            this.price_unit = price_unit;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getIs_fixed() {
            return is_fixed;
        }

        public void setIs_fixed(String is_fixed) {
            this.is_fixed = is_fixed;
        }

        public String getFixed_weight() {
            return fixed_weight;
        }

        public void setFixed_weight(String fixed_weight) {
            this.fixed_weight = fixed_weight;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProps_name() {
            return props_name;
        }

        public void setProps_name(String props_name) {
            this.props_name = props_name;
        }

        public String getSell_id() {
            return sell_id;
        }

        public void setSell_id(String sell_id) {
            this.sell_id = sell_id;
        }

        public String getOrder_no() {
            return order_no;
        }

        public void setOrder_no(String order_no) {
            this.order_no = order_no;
        }

        public String getAgent_sell_id() {
            return agent_sell_id;
        }

        public void setAgent_sell_id(String agent_sell_id) {
            this.agent_sell_id = agent_sell_id;
        }

        public String getTare() {
            return tare;
        }

        public void setTare(String tare) {
            this.tare = tare;
        }

        public String getOwner_commission() {
            return owner_commission;
        }

        public void setOwner_commission(String owner_commission) {
            this.owner_commission = owner_commission;
        }

        public String getCustom_commission() {
            return custom_commission;
        }

        public void setCustom_commission(String custom_commission) {
            this.custom_commission = custom_commission;
        }
    }


}
