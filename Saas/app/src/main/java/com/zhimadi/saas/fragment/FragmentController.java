package com.zhimadi.saas.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.widget.Toast;

public class FragmentController {

	private String fragmentTag = BaseFragment.NON;

	private FragmentManager manager;

	private int mLayoutId;

	private Context mContext;

	public FragmentController(Context context, int layoutId) {
		mLayoutId = layoutId;
		mContext = context;
	}


	public void showFragment(BaseFragment fragment, FragmentManager manager) {

		this.manager = manager;
		if (!fragment.tag.equals(BaseFragment.NON)) {

			if (!fragment.isAdded()) {
				manager.beginTransaction().add(mLayoutId, fragment, fragment.tag).commit();
				if (!fragmentTag.equals(BaseFragment.NON)) {
					manager.beginTransaction().hide(manager.findFragmentByTag(fragmentTag)).commit();
				}
				fragmentTag = fragment.tag;
			} else {
				manager.beginTransaction().hide(manager.findFragmentByTag(fragmentTag)).commit();
				manager.beginTransaction().show(fragment).commit();
				fragmentTag = fragment.tag;
			}

		}
		
	}

}
