package com.zhimadi.saas.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.zhimadi.saas.R;
import com.zhimadi.saas.adapter.AgentAdapter;
import com.zhimadi.saas.event.AgentsEvent;
import com.zhimadi.saas.util.AsyncUtil;
import com.zhimadi.saas.util.DisplayUtil;
import com.zhimadi.saas.util.OkHttpUtil;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AgentHomeActivity extends BaseActivity {

    private int mStart = 0;
    private int mLimit = 10;

    private TitleBarCommonBuilder titleBarCommonBuilder;
    private SwipeMenuListView lvAgentHome;
    private List<AgentsEvent.Agent> agents;
    private AgentAdapter agentAdapter;

    private void inte() {
        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
        agents = new ArrayList<AgentsEvent.Agent>();
        lvAgentHome = (SwipeMenuListView) findViewById(R.id.lv_agent_home);
        agentAdapter = new AgentAdapter(mContext, R.layout.item_lv_agent_home, agents);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_home);
        inte();
        titleBarCommonBuilder.setTitle("代卖商管理")
                .setLeftImageRes(R.drawable.fan_hui)
                .setLeftText("返回")
                .setLeftOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });

        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
                .setRightOnClikListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setClass(mContext, AgentEditActivity.class);
                        intent.putExtra("INTENT_MODE", AgentEditActivity.ADD_MODE);
                        activity.startActivity(intent);
                    }
                });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                //付款
                SwipeMenuItem payItem = new SwipeMenuItem(
                        activity);
                payItem.setBackground(new ColorDrawable(Color.rgb(0x48,
                        0xCF, 0xAD)));
                payItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                payItem.setTitle("付款");
                payItem.setTitleSize(12);
                payItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(payItem);

                //打电话
                SwipeMenuItem callItem = new SwipeMenuItem(
                        activity);
                callItem.setBackground(new ColorDrawable(Color.rgb(0x6C, 0xE0,
                        0xC3)));
                callItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                callItem.setIcon(R.drawable.shou_ji);
                menu.addMenuItem(callItem);

                //发短信
                SwipeMenuItem smsItem = new SwipeMenuItem(
                        activity);
                smsItem.setBackground(new ColorDrawable(Color.rgb(0x8D, 0xEA,
                        0xD3)));
                smsItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                smsItem.setIcon(R.drawable.duan_xin);
                menu.addMenuItem(smsItem);

                //删除
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        activity);
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
                        0xB9, 0xB9)));
                deleteItem.setWidth(DisplayUtil.dip2px(mContext, 67.2f));
                deleteItem.setTitle("删除");
                deleteItem.setTitleSize(12);
                deleteItem.setTitleColor(Color.WHITE);
                menu.addMenuItem(deleteItem);
            }
        };

        lvAgentHome.setMenuCreator(creator);
        lvAgentHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                if (index == 3) {
                    AgentDelete(agents.get(position).getAgent_id());
                }

                return false;
            }
        });

        lvAgentHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent();
                intent.setClass(mContext, AgentEditActivity.class);
                intent.putExtra("INTENT_MODE", AgentEditActivity.EDIT_MODE);
                intent.putExtra("OWNER_ID", agents.get(i).getAgent_id());
                startActivity(intent);
            }
        });

        lvAgentHome.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                        // 手指触屏拉动准备滚动，只触发一次        顺序: 1
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_FLING:
                        // 持续滚动开始，只触发一次                顺序: 2
                        break;
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 整个滚动事件结束，只触发一次            顺序: 4
                        if (view.getLastVisiblePosition() >= view.getCount() - 2) {
                            //加载更多功能的代码
                            GetAgent();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });

        lvAgentHome.setMenuCreator(creator);
        lvAgentHome.setAdapter(agentAdapter);
        GetAgent();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mStart = 0;
        GetAgent();
    }

    private void GetAgent() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("start", mStart + "");
        map.put("limit", mLimit + "");
        String URLStr = OkHttpUtil.UrlKeyAssembly(OkHttpUtil.mAgentAddress, map);
        AsyncUtil.asyncGet(mContext, URLStr, new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                Gson gson = new Gson();
                AgentsEvent agentsEvent = gson.fromJson(response, AgentsEvent.class);
                if (mStart == 0) {
                    agents.clear();
                }
                mStart = mStart + agentsEvent.getData().getList().size();
                agents.addAll(agentsEvent.getData().getList());
                agentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail() {

            }
        });


    }


    private void AgentDelete(String agentId) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("agent_id", agentId);
        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteAgentAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
            @Override
            public void onSuccess(String response) {
                mStart = 0;
                GetAgent();
            }

            @Override
            public void onFail() {

            }
        });

    }


}
