//package com.zhimadi.saas.activity;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.Toast;
//
//import com.baoyz.swipemenulistview.SwipeMenu;
//import com.baoyz.swipemenulistview.SwipeMenuCreator;
//import com.baoyz.swipemenulistview.SwipeMenuItem;
//import com.baoyz.swipemenulistview.SwipeMenuListView;
//import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.zhimadi.saas.R;
//import com.zhimadi.saas.adapter.AccountAdapter;
//import com.zhimadi.saas.event.AccountsEvent;
//import com.zhimadi.saas.event.CommonResultEvent;
//import com.zhimadi.saas.util.AsyncUtil;
//import com.zhimadi.saas.util.OkHttpClientManager;
//import com.zhimadi.saas.util.OkHttpUtil;
//import com.zhimadi.saas.widget.TitleBarCommonBuilder;
//
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import okhttp3.Request;
//
//public class AccountHomeActivity extends BaseActivity {
//
//
//    private TitleBarCommonBuilder titleBarCommonBuilder;
//    private SwipeMenuListView lvAccountHome;
//    private List<AccountsEvent.Account> accounts;
//    private AccountAdapter accountAdapter;
//
//    private void inte() {
//        titleBarCommonBuilder = new TitleBarCommonBuilder(activity);
//        accounts = new ArrayList<AccountsEvent.Account>();
//        lvAccountHome = (SwipeMenuListView) findViewById(R.id.lv_account_home);
//        accountAdapter = new AccountAdapter(mContext, R.layout.item_lv_account_home, accounts);
//    }
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_account_home);
//        inte();
//        titleBarCommonBuilder.setTitle("结算用户管理")
//                .setLeftImageRes(R.drawable.fan_hui)
//                .setLeftText("返回")
//                .setLeftOnClikListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                });
//
//        titleBarCommonBuilder.setRightImageRes(R.drawable.tian_jia02)
//                .setRightOnClikListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent();
//                        intent.setClass(mContext, AccountEditActivity.class);
//                        intent.putExtra("INTENT_MODE", AccountEditActivity.ADD_MODE);
//                        activity.startActivity(intent);
//                    }
//                });
//
//        lvAccountHome.setAdapter(accountAdapter);
//
//
//        SwipeMenuCreator creator = new SwipeMenuCreator() {
//
//            @Override
//            public void create(SwipeMenu menu) {
//                SwipeMenuItem deleteItem = new SwipeMenuItem(
//                        activity);
//                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xB9,
//                        0xB9, 0xB9)));
//                deleteItem.setWidth(90);
//                deleteItem.setTitle("删除");
//                deleteItem.setTitleSize(12);
//                deleteItem.setTitleColor(Color.WHITE);
//                menu.addMenuItem(deleteItem);
//
//
//            }
//
//        };
//        lvAccountHome.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
//                if (index == 0) {
//                    AccountDelete(accounts.get(position).getAccount_id());
//                }
//                return true;
//            }
//
//        });
//        lvAccountHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                Intent intent = new Intent();
//                intent.setClass(mContext, AccountEditActivity.class);
//                intent.putExtra("INTENT_MODE", AccountEditActivity.EDIT_MODE);
//                intent.putExtra("ACCOUNT_ID", accounts.get(i).getAccount_id());
//                activity.startActivity(intent);
//            }
//        });
//        lvAccountHome.setMenuCreator(creator);
//        GetAccount();
//    }
//
//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        GetAccount();
//    }
//
//    private void GetAccount() {
//        AsyncUtil.asyncGet(OkHttpUtil.mAccountAddress, new AsyncUtil.SuccessListener() {
//            @Override
//            public void onSuccess(String response) {
//                Gson gson = new Gson();
//                AccountsEvent accountsEvent = gson.fromJson(response, AccountsEvent.class);
//                accounts.clear();
//                accounts.addAll(accountsEvent.getData().getList());
//                accountAdapter = new AccountAdapter(mContext, R.layout.item_lv_account_home, accounts);
//                lvAccountHome.setAdapter(accountAdapter);
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
//    }
//
//
//    private void AccountDelete(String accountId) {
//
//        JsonObject jsonObject = new JsonObject();
//        jsonObject.addProperty("account_id", accountId);
//
//        AsyncUtil.asyncJsonPost(mContext, OkHttpUtil.mDeleteAccountAddress, jsonObject.toString(), new AsyncUtil.SuccessListener() {
//            @Override
//            public void onSuccess(String response) {
//                Gson gson = new Gson();
//                CommonResultEvent commonResultEvent = gson.fromJson(response, CommonResultEvent.class);
//                if (commonResultEvent.getCode() == 0) {
//                    GetAccount();
//                } else {
//                    Toast.makeText(mContext, commonResultEvent.getMsg(), Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
//
//
//
//    }
//
//
//}
