package com.zhimadi.saas.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;

import com.zhimadi.saas.activity.CustomSelectActivity;
import com.zhimadi.saas.activity.StoreSelectActivity;
import com.zhimadi.saas.event.CustomsEvent;

import java.util.List;

/**
 * Created by carry on 2016/7/13.
 */
public class CustomSelectAdapter extends ArrayAdapter<CustomsEvent.Custom>{

    private CustomSelectActivity mActivity;
    private Context mContext;
    private View returnView;
    private int resourceId;

    public CustomSelectAdapter(Context context, int resource, List<CustomsEvent.Custom> objects, Activity activity) {
        super(context, resource, objects);
        mContext = context;
        resourceId = resource;
        mActivity = (CustomSelectActivity)activity;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final CustomsEvent.Custom custom  = getItem(position);
        convertView = LayoutInflater.from(mContext).inflate(resourceId, null);

//        cbStoreSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if(isChecked){
//                    mActivity.select(custom.getWarehouse_id(), custom.getName());
//                }
//            }
//        });
        return convertView;
    }
}
