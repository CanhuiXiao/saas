package com.zhimadi.saas.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.zhimadi.saas.R;
import com.zhimadi.saas.activity.UserManagerActivity;
import com.zhimadi.saas.adapter.MineAdapter;
import com.zhimadi.saas.constant.MineList;
import com.zhimadi.saas.view.UserHead1;
import com.zhimadi.saas.widget.TitleBarCommonBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by carry on 2016/7/8.
 */
public class UserFragment extends BaseFragment {

    private View returnView;
    private TitleBarCommonBuilder titleBarCommonBuilder;
    private List<MineList.Mine> mines;
    private ListView lvMineHome;
    private MineAdapter userHomeAdapter;



    private void inte(){
        titleBarCommonBuilder = new TitleBarCommonBuilder(returnView);
        lvMineHome = (ListView) returnView.findViewById(R.id.lv_mine_home);
        mines = new ArrayList<MineList.Mine>();
        userHomeAdapter = new MineAdapter(activity, R.layout.item_lv_mine_home, mines);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        returnView = inflater.inflate(R.layout.fragment_mine, null);
        inte();
        titleBarCommonBuilder.setTitle("我的");
        mines.add(MineList.Mine.getMine(1));
        mines.add(MineList.Mine.getMine(2));
        mines.add(MineList.Mine.getMine(3));
        mines.add(MineList.Mine.getMine(4));
        mines.add(MineList.Mine.getMine(5));
        UserHead1 userHead = new UserHead1(activity, null);
        lvMineHome.addHeaderView(userHead);
        lvMineHome.setAdapter(userHomeAdapter);
        lvMineHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(mines.get(position-1).getActivity() != null){
                    Intent intent = new Intent();
                    intent.setClass(activity, UserManagerActivity.class);
                    activity.startActivity(intent);
                }
            }
        });
        return  returnView;
    }
}